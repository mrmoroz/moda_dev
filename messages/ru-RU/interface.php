<?php
return [
    'Password' => 'Пароль',
    'Parent Category' => 'Родительская категория',
    'Title' => 'Название',
    'Description' => 'Описание',
    'Image' => 'Изображение',
    'Price' => 'Цена',
    'Discount Price' => 'Цена со скидкой',
    'Discount' => 'Распродажа',
    'Recommend' => 'Рекоммендем',
    'Recommendations' => 'Рекоммендуемые товары (артикулы через запятую)',
    'Category ID' => 'Категория',
    'Pos' => 'Позиция',
    'Active' => 'Актив.',
    'User Information' => 'Информация о пользователе',
    'User profile' => 'Профиль пользователя',
    'Update user profile information success'=>'Профиль пользователя успешно сохранен!',
    'Update user information success'=>'Информация о пользователе успешно сохранена!'
];