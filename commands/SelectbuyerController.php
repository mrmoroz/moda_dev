<?php
namespace app\commands;

use yii\console\Controller;
use yii\helpers\ArrayHelper;

class SelectbuyerController extends Controller
{
    public function actionIndex()
    {
        $ids = [];
        $str = '';

        $sql_pl = "SELECT id FROM products WHERE category_id IN ('1','323','324','325','49') 
                    AND is_active = true 
                    AND is_archive = FALSE 
                    AND is_wholesale_active = true
                    ORDER BY tmp_rating DESC 
                    LIMIT 18 ";
        $plData = \Yii::$app->db->createCommand($sql_pl)->queryAll();

        $sql_bl = "SELECT id FROM products WHERE category_id IN ('36') 
                    AND is_active = true 
                    AND is_archive = FALSE 
                    AND is_wholesale_active = true
                    ORDER BY tmp_rating DESC 
                    LIMIT 6 ";
        $blData = \Yii::$app->db->createCommand($sql_bl)->queryAll();

        $sql_kos = "SELECT id FROM products WHERE category_id IN ('58','89','114') 
                    AND is_active = true 
                    AND is_archive = FALSE 
                    AND is_wholesale_active = true
                    ORDER BY tmp_rating DESC 
                    LIMIT 4 ";
        $kosData = \Yii::$app->db->createCommand($sql_kos)->queryAll();

        $sql_br = "SELECT id FROM products WHERE category_id IN ('450','449','451','448','446','51','445') 
                    AND is_active = true 
                    AND is_archive = FALSE 
                    AND is_wholesale_active = true
                    ORDER BY tmp_rating DESC 
                    LIMIT 4 ";
        $brData = \Yii::$app->db->createCommand($sql_br)->queryAll();

        $sql_ub = "SELECT id FROM products WHERE category_id IN ('52') 
                    AND is_active = true 
                    AND is_archive = FALSE 
                    AND is_wholesale_active = true
                    ORDER BY tmp_rating DESC 
                    LIMIT 4 ";
        $ubData = \Yii::$app->db->createCommand($sql_ub)->queryAll();

        $sql_men = "SELECT id FROM products 
                    WHERE category_id IN (SELECT id FROM product_categories WHERE product_categories.type='2' AND is_active=true) 
                    AND is_active = true 
                    AND is_archive = FALSE 
                    AND is_wholesale_active = true
                    ORDER BY tmp_rating DESC 
                    LIMIT 4";
        $menData = \Yii::$app->db->createCommand($sql_men)->queryAll();

        $sql_manual = "SELECT products.id FROM products 
                       WHERE products.id IN (
                            SELECT prod_id FROM prods_cats_params_values WHERE param_id = '9' AND value='184'
                       ) 
                       AND products.is_active = true 
                       AND is_wholesale_active = true
                       AND products.is_archive=false";

        $manualData = \Yii::$app->db->createCommand($sql_manual)->queryAll();

        $_tmp_arr = array_merge($plData, $blData, $kosData, $brData, $ubData, $menData, $manualData);

        if(count($_tmp_arr)>0){
            $ids = ArrayHelper::getColumn($_tmp_arr, 'id');
            $str = implode(',', $ids);
            file_put_contents(__DIR__.'/ids.txt', $str);
        }
    }
}