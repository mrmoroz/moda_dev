<?php

namespace app\commands;

use app\models\Product;
use app\models\ProductSearch;
use app\models\ProductSelection;
use Yii;
use app\models\DeferredMultipleOperation;
use yii\filters\VerbFilter;
use yii\console\Controller;

/**
 * DeferredMultipleOperationController implements the CRUD actions for DeferredMultipleOperation model.
 */
class DeferredOperationController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex($message = 'hello world')
    {
        echo $message . "\n";
        var_dump(__DIR__);
        exit;
    }

    /**
     * Применение массовой операции по крону
     */
    public function actionApplyDeferredOperation()
    {
        date_default_timezone_set('Asia/Novosibirsk');
        $currentDate = date('Y-m-d H:i:s', strtotime('1 hour'));

        $multipleOperations = DeferredMultipleOperation::find()->where(['<=', 'date', $currentDate])
            ->andWhere(['is_completed' => false])
            ->with('parameters')->orderBy('date DESC')->all();

        if (!empty($multipleOperations)) {
            foreach ($multipleOperations as $multipleOperation) {
                /* @var $multipleOperation DeferredMultipleOperation */
                $multipleOperationParameters[] = $multipleOperation->mode_parameter_value;
                $productSearch = new ProductSearch();
                $productSearch = $multipleOperation->parameters->setProductSearch($productSearch);
                $productSearch->checkedSizes = $multipleOperation->getCheckedSizesAsArray();
                $dataProvider = $productSearch->search();


                Yii::$app->categoryService->applyMultipleOperation($dataProvider, $multipleOperation->operation_mode, $multipleOperationParameters);

                $multipleOperation->is_completed = true;
                $multipleOperation->save();
            }
        }
    }

    /**
     * Перезапись темпового рейтинга
     */
    public function actionChangeRating()
    {
        Yii::$app->db->createCommand("UPDATE products SET rating = tmp_rating WHERE is_active = TRUE AND is_archive = FALSE")->execute();
        ProductSelection::deleteAll();
    }

    /**
     * Пересчет рейтинга в фоне
     * @param $categories
     */
    public function actionBackgroundCalculateRating($categories)
    {
        $categories = explode(',', $categories);

        $products = Product::getProductsForRating($categories);
        Product::changeRating($products);

        ProductSelection::deleteAll();
    }
}


