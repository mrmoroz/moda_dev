<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 26.06.17
 * Time: 10:41
 */

namespace app\commands;
use app\models\User;
use app\models\UserProfile;
use app\modules\admin\models\MlCfg;
use app\modules\admin\models\OrdersStatusesHistory;
use app\modules\admin\models\OrdstatList;
use app\modules\admin\models\Sales;
use app\modules\admin\models\SmsTemplates;
use app\modules\cart\models\Orders;
use yii\console\Controller;
use app\modules\admin\models\SmsHistory;
use app\models\Regions;
use Yii;
use app\components\SMSMessage;
use SoapClient;
use app\models\PostOperationHistory;


class RussianpostcheckerController extends Controller
{
    public $wsdlurl = 'https://tracking.russianpost.ru/rtm34?wsdl';
    public $soapClient = "";

    public function  init()
    {
        parent::init();
        ini_set("soap.wsdl_cache_enabled", "0");
        $this->soapClient = new SoapClient($this->wsdlurl, array('trace' => 1, 'soap_version' => SOAP_1_2));
    }

    public function actionIndex()
    {
        $sql = "SELECT t.id, t.id_post, t.order_id, t.type FROM sales t
                    JOIN
                    (
                        SELECT order_id, MAX(id) AS id
                        FROM sales
                        GROUP BY 1
                    ) q ON (q.id = t.id) AND (q.order_id = t.order_id)
                    JOIN orders O ON t.order_id = O.id
                WHERE
                    finish_tracking='0'
                AND
                    t.id_post IS NOT NULL
                AND
                    t.id_post != ''
                AND
                    O.cdate > '2015-12-01 00:00:00'";
        $sales = Yii::$app->db->createCommand($sql)->queryAll();
        if(count($sales)>0){
            foreach ($sales as $sale){
                $this->saveSaleInRussianPostHistory($sale, $this->soapClient);
            }
        }
    }

    private function saveSaleInRussianPostHistory($sale, $soapClient)
    {
        if (!$soapClient) {
            return;
        }
        $salePostId = substr(trim($sale['id_post']),0, 14);
        file_put_contents(__DIR__.'/autostatus.txt', date('Y-m-d H:i:s')." PostID:".$salePostId."\n", FILE_APPEND);

        if(!preg_match('/[0-9aZ]/i',$salePostId)){
            file_put_contents(__DIR__.'/autostatus.txt', date('Y-m-d H:i:s')."-incorrect PostID:".$salePostId."\n", FILE_APPEND);
            $sql2 = "UPDATE sales SET finish_tracking='2' WHERE id='{$sale['id']}'";
            Yii::$app->db->createCommand($sql2)->execute();
            return;
        }
        if(strlen($salePostId) < 12)
        {
            file_put_contents(__DIR__.'/autostatus.txt', date('Y-m-d H:i:s')."-incorrect PostID:".$salePostId."\n", FILE_APPEND);
            $sql3 = "UPDATE sales SET finish_tracking='3' WHERE id='{$sale['id']}'";
            Yii::$app->db->createCommand($sql3)->execute();
            return;
        }

        $requestParams = array('OperationHistoryRequest' => array('Barcode' => $salePostId, 'MessageType' => '0', 'Language' => 'RUS'),'AuthorizationHeader' => array('login' => 'QqzWqIxTzPqUGD', 'password' => 'uCUlmGIbdL5C'));
        $result = $soapClient->getOperationHistory(new \SoapParam($requestParams, 'OperationHistoryRequest'));

        if (isset($result->OperationHistoryData)) {
            $existHistoryDates = PostOperationHistory::findOne($sale['id']);
            $exData = array();
            if($existHistoryDates){
                foreach ($existHistoryDates as $existHistoryData) {
                    $exData[strtotime($existHistoryData->operation_date)] = true;
                }
            }

            if(is_array($result->OperationHistoryData->historyRecord)){
                foreach ($result->OperationHistoryData->historyRecord as $historyData) {
                    if (!isset($exData[strtotime($historyData->OperationParameters->OperDate)])) {
                        $operAttrId = isset($historyData->OperationParameters->OperAttr) ? $historyData->OperationParameters->OperAttr->Id : null;
                        $operAttrName = isset($historyData->OperationParameters->OperAttr) ? $historyData->OperationParameters->OperAttr->Name : null;
                        $model = new PostOperationHistory();
                        $model->sale_id = $sale['id'];
                        $model->operation_date = $historyData->OperationParameters->OperDate;
                        $model->operation_type_id = $historyData->OperationParameters->OperType->Id;
                        $model->operation_type_name = $historyData->OperationParameters->OperType->Name;
                        $model->operation_attr_id = $operAttrId;
                        $model->operation_attr_name = $operAttrName;
                        $model->operation_address_index = $historyData->AddressParameters->OperationAddress->Index;
                        $model->operation_address_description = $historyData->AddressParameters->OperationAddress->Description;
                        $model->save(false);

                        file_put_contents(__DIR__.'/autostatus.txt', 'Status: '.$historyData->OperationParameters->OperType->Id.' - '.$operAttrId."\n", FILE_APPEND);

                        if ($historyData->OperationParameters->OperType->Id == 2 && $operAttrId !=2 && $operAttrId !=4) {
                            if($sale['type'] == 0) {
                                $this->setOrderStatus($sale['order_id'], 22, $sale);
                            }

                        } else if ($historyData->OperationParameters->OperType->Id == 8 && $operAttrId == 2) {
                            if($sale['type'] == 0) {
                                $this->setOrderStatus($sale['order_id'], 50, $sale);
                            }

                        }else if ($historyData->OperationParameters->OperType->Id == 3 && $operAttrId == 1) {
                            if($sale['type'] == 0) {
                                $this->setOrderStatus($sale['order_id'], 46, $sale);
                            }
                        }
                        $sModel = Sales::findOne($sale['id']);
                        if ($this->isFinishRussianPostOperation($historyData->OperationParameters->OperType->Id, $operAttrId)) {
                            $sModel->finish_tracking = '1';
                            $sModel->save(false);
                        }
                        $rawPostData = serialize($result->OperationHistoryData->historyRecord);
                        $sModel->raw_post_tracking_result = $rawPostData;
                        $sModel->save(false);
                    }
                }
            }
        }
    }

    private function isFinishRussianPostOperation($typeId, $attrId)
    {
        if ($typeId == 2 || $typeId == 15 || $typeId == 16 || $typeId == 17 || $typeId == 18) {
            return true;
        } elseif ($typeId == 5 && ($attrId == 1 || $attrId == 2)) {
            return true;
        } elseif ($typeId == 3 && $attrId == 1) {
            return true;
        }
        return false;
    }

    private function setOrderStatus($orderID, $statusID, $sale)
    {
        $message = date('Y-m-d H:i:s')."--try set status {$statusID} for order {$orderID}\n";
        file_put_contents(__DIR__.'/autostatus.txt', $message, FILE_APPEND);
        $orderInfo = Orders::findOne($orderID);
        if($orderInfo && $statusID != $orderInfo->status) {
            if($statusID === 50){
                $cdate = date("Y-m-d H:i:s");
                $addUpdate = ",deliv_date = '{$cdate}'";
            }else{
                $addUpdate = "";
            }
            if($statusID == 22 && !empty($orderInfo->next_orders)){
                Yii::$app->mailer->compose()
                    ->setFrom('info@beauti-full.ru')
                    ->setTo('beauti-full@mail.ru')
                    ->setSubject('Заказ #'.$orderInfo->id.' получен. Следующие заказы готовы к обработке')
                    ->setHtmlBody("Заказ #{$orderInfo->id} получен. <br/>Можно обрабатывать заказ(ы) {$orderInfo->next_orders}")
                    ->send();
            }
            $statusHistoryModel = new OrdersStatusesHistory();
            $statusHistoryModel->order_id = $orderID;
            $statusHistoryModel->old_status = $orderInfo->status;
            $statusHistoryModel->new_status = $statusID;
            $statusHistoryModel->created_at = date("Y-m-d H:i:s");
            $statusHistoryModel->save(false);

            $sql = "UPDATE orders SET status='{$statusID}' {$addUpdate} WHERE id='{$orderID}'";
            Yii::$app->db->createCommand($sql)->execute();

            $smsTemplate = SmsTemplates::findOne(['action'=>$statusID,'visible'=>'1']);

            $postOrderStatusInfo = OrdstatList::findOne($statusID);

            if($postOrderStatusInfo){
                $orderUser = UserProfile::findOne(['user_id'=>$orderInfo->user_id]);
                $managerUser = UserProfile::findOne(['user_id'=>$orderInfo->manager]);
                if($orderUser && $managerUser){
                    $noteAddText = str_replace(
                        array('[name]', '[surname]', '[name3]', '[n_order]', '[name_cons]', '[fname_cons]'),
                        array(
                            $orderUser->first_name, $orderUser->last_name, $orderUser->second_name, $orderInfo->id,
                            $managerUser->last_name, $managerUser->first_name
                        ),
                        $postOrderStatusInfo->auto_comment
                    );
                    $orderInfo->comment_k = $noteAddText;
                    $orderInfo->save(false);
                }
            }

            if ($smsTemplate) {
                $orderInfo = Orders::findOne($orderID);
                $profile = UserProfile::findOne(['user_id'=>$orderInfo->user_id]);
                $managerUser = UserProfile::findOne(['user_id'=>$orderInfo->manager]);
                $user = User::findOne($orderInfo->user_id);
                $smsMessage = new SMSMessage();
                $smsMessage->sendSMS($statusID, $profile, $orderInfo,$sale);

                if ($statusID === 50) {
                    $oModel = MlCfg::findOne(['mnemo'=>'deliv_order']);

                    if($oModel && $user && $managerUser){
                        $text = str_replace(
                            array('{name}', '{surname}', '{n_order}', '{name_cons}', '{fname_cons}','{post_id}'),
                            array(
                                $profile->first_name, $profile->last_name, $orderInfo->id,
                                $managerUser->last_name, $managerUser->first_name, $sale['id_post']
                            ),
                            $oModel->ebody_html
                        );
                        Yii::$app->mailer->compose()
                            ->setFrom([Yii::$app->params['supportEmail'] => 'Отправлено с сайта beauti-full.ru'])
                            ->setTo($user->login)
                            ->setSubject($oModel->esubj)
                            ->setHtmlBody($text)
                            ->send();
                    }
                }
            }
        }
    }
}