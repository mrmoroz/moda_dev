<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 18.04.17
 * Time: 11:20
 */

namespace app\commands;

use yii\console\Controller;
use app\modules\admin\models\SmsHistory;
use app\models\Regions;
use Yii;
use app\components\SMSMessage;

class SmscronController extends Controller
{
    public function actionIndex()
    {
        date_default_timezone_set("UTC");
        $sql = "DELETE FROM sms_history WHERE sms_history.number='' OR text='' OR region='0' OR region IS NULL";
        Yii::$app->db->createCommand($sql)->execute();
        file_put_contents(__DIR__.'/smslog.txt', date('Y-m-d H:i:s')."-Delete incorrect messages......Done\n", FILE_APPEND);

        $sms = SmsHistory::find()->limit(50)->all();
        if($sms){
            $regInfos = array();
            foreach ($sms as $message) {
                if (isset($regInfos[$message->region])) {
                    $regInfo = $regInfos[$message->region];
                } else {
                    $regInfo = $regInfos[$message->region] = Regions::findOne($message->region);
                }
                $currentTime = time();
                $regionTime = $currentTime + ($regInfo->timezone * 60 * 60);
                $regionHour = date('H', $regionTime);
                if ($regionHour >= 9 && $regionHour < 21) {
                    $smsMessage = new SMSMessage();
                    $smsResult = $smsMessage->send($message->number, $message->text, $message->user_id);
                    if($smsResult === -1){
                        file_put_contents(__DIR__.'/smslog.txt', date('Y-m-d H:i:s')." Incorrect message to ".$message->number." with text:'".$message->text." Deleting....\n", FILE_APPEND);
                    }else{
                        file_put_contents(__DIR__.'/smslog.txt', date('Y-m-d H:i:s')." Send message to ".$message->number." with text:'".$message->text."'\n", FILE_APPEND);
                    }
                    $curSms = SmsHistory::findOne($message->id);
                    $curSms->delete();
                }
            }
        }
        file_put_contents(__DIR__.'/smslog.txt', date('Y-m-d H:i:s')."-Success\n", FILE_APPEND);

    }
}