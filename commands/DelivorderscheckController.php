<?php
namespace app\commands;

use app\modules\admin\models\OrdersStatusesHistory;
use yii\console\Controller;


class DelivorderscheckController extends Controller
{
    public function actionIndex(){
        $currentDate = date('Y-m-d', strtotime('-14 days'));
        file_put_contents(__DIR__.'/delivLog.txt', "Проверка даты: {$currentDate} \n\n", FILE_APPEND);
        $start = $currentDate." 00:00:00";
        $end = $currentDate. " 23:59:59";
        $currentSQLDate = date("Y-m-d H:i:s");
        $sql = "SELECT id FROM orders WHERE deliv_date >= '{$start}' AND deliv_date <= '{$end}' AND status = '50'";
        $orders = \Yii::$app->db->createCommand($sql)->queryAll();
        if(count($orders)>0){
            foreach ($orders as $order) {
                $sql2 = "UPDATE orders SET status='60' WHERE id='{$order['id']}'";
                \Yii::$app->db->createCommand($sql2)->execute();
                $model = new OrdersStatusesHistory();
                $model->order_id = $order['id'];
                $model->old_status = '50';
                $model->new_status = '60';
                $model->created_at = $currentSQLDate;
                $model->save(false);
                file_put_contents(__DIR__.'/delivLog.txt', "-Обновлен заказ {$order['id']} \n\n", FILE_APPEND);
            }
        }
    }
}