<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ProdsCatsParam;
use yii\db\Expression;

/**
 * ProdsCatsParamSearch represents the model behind the search form about `app\models\ProdsCatsParam`.
 */
class ProdsCatsParamSearch extends ProdsCatsParam
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type', 'position', 'sys_language', 'source'], 'integer'],
            [['name', 'desc'], 'safe'],
            [['visible'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProdsCatsParam::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'source' => $this->source,
            'position' => $this->position,
            'visible' => $this->visible,
            'sys_language' => $this->sys_language,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'desc', $this->desc]);

        $query->orderBy([new Expression('position IS NULL ASC, position ASC')]);

        return $dataProvider;
    }
}
