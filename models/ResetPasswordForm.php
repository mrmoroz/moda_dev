<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 28.02.2017
 * Time: 11:27
 */

namespace app\models;
use Yii;
use yii\base\Model;
use yii\base\InvalidParamException;
use app\models\User;

class ResetPasswordForm extends Model
{
    public $password;
    private $_user;

    public function rules()
    {
        return [
            ['password', 'required'],
            ['password', 'string','min' => 6, 'max' => 10, 'message' => 'Значение «Пароль» должно содержать от 6 до 10 символов.'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'password'=>'Пароль'
        ];
    }

    public function __construct($key, array $config=[])
    {
        if(empty($key) || !is_string($key)){
            throw new InvalidParamException('Ключ не может быть пустым');
        }
        $this->_user = User::findBySecretKey($key);
        if(!$this->_user){
            throw new InvalidParamException('Неверный ключ');
        }
        parent::__construct($config);
    }

    public function resetPassword(){
        $user = $this->_user;
        $user->setPassword($this->password); //хеш введеного нового пароля
        $user->removeSecretKey();
        return $user->save(false);
    }
}