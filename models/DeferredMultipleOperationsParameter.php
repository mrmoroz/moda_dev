<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "deferred_multiple_operations_parameters".
 *
 * @property integer $id
 * @property integer $operation_id
 * @property integer $manufacturer
 * @property integer $tag
 * @property integer $category_tag
 * @property integer $category_id
 * @property string $size_mode
 * @property string $additional_size_mode
 * @property integer $type_id
 * @property boolean $search_in_current_category
 * @property string $article
 * @property boolean $is_archive
 */
class DeferredMultipleOperationsParameter extends \yii\db\ActiveRecord
{
    public static $sizeModes = [
        'big-sizes' => 'Большие размеры',
        'standart-sizes' => 'Стандартные размеры',
        'all-sizes' => 'Все размеры'
    ];

    public static $additionalSizeModes = [
        'having-us-sizes' => 'В наличии у нас',
        'visible-mo' => 'Товары на МодаОптом'
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'deferred_multiple_operations_parameters';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['operation_id', 'manufacturer', 'tag', 'category_tag', 'category_id', 'type_id'], 'integer'],
            [['size_mode', 'additional_size_mode', 'article'], 'string'],
            [['search_in_current_category', 'is_archive'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'operation_id' => 'Operation ID',
            'manufacturer' => 'Manufacturer',
            'tag' => 'Tag',
            'category_tag' => 'Category Tag',
            'category_id' => 'Category ID',
            'size_mode' => 'Size Mode',
            'additional_size_mode' => 'Additional Size Mode',
            'type_id' => 'Type ID',
            'search_in_current_category' => 'Search In Current Category',
            'article' => 'Article',
        ];
    }

    public function getManufacturer()
    {
        return $this->hasOne(Manufacturer::className(), ['id' => 'manufacturer']);
    }

    public function getManufacturerName()
    {
        $manufacturer = $this->getManufacturer()->one();
        if ($manufacturer) {
            return $manufacturer->name;
        }

        return "-";
    }

    public function getTag()
    {
        return $this->hasOne(Tag::className(), ['id' => 'tag']);
    }

    public function getTagName()
    {
        $tag = $this->getTag()->one();
        if ($tag) {
            return $tag->name;
        }

        return "-";
    }

    public function getCategoryTag()
    {
        return $this->hasOne(ProductCategoriesTag::className(), ['id' => 'category_tag']);
    }

    public function getCategoryTagName()
    {
        $categoryTag = $this->getCategoryTag()->one();
        if ($categoryTag) {
            return $categoryTag->name;
        }

        return "-";
    }

    public function getCategory()
    {
        return $this->hasOne(ProductCategory::className(), ['id' => 'category_id']);
    }

    public function getCategoryName()
    {
        $category = $this->getCategory()->one();
        if ($category) {
            return $category->title;
        }

        return "-";
    }

    public function getAdditionalSizeModesNames()
    {
        $additionalSizeModes = explode(',', $this->additional_size_mode);
        $result = '';

        if (!empty($additionalSizeModes)) {
            foreach ($additionalSizeModes as $additionalSizeMode) {
                $result .= self::$additionalSizeModes[$additionalSizeMode] . ', ';
            }

            return rtrim($result, ', ');
        } elseif ($this->additional_size_mode) {
            return self::$additionalSizeModes[$this->additional_size_mode];
        }

        return "-";
    }

    public function setProductSearch(ProductSearch $productSearch)
    {
        $productSearch->article = $this->article;
        $productSearch->manufacturer = $this->manufacturer;
        $productSearch->tag = $this->tag;
        $productSearch->categoryTag = $this->category_tag;
        if ($this->category_id) {
            $productSearch->category_id = $this->category_id;
        }
        $productSearch->is_archive = $this->is_archive;

        $productSearch->sizeMode = $this->size_mode;
        $productSearch->categoryType = $this->type_id;
        $productSearch->searchCurrentCategory = $this->search_in_current_category;
        $productSearch->additionalSizeModes = explode(',', $this->additional_size_mode);

        return $productSearch;
    }
}
