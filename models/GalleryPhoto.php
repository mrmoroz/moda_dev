<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gallery_photo".
 *
 * @property integer $id
 * @property integer $gallery_id
 * @property string $image
 * @property integer $position
 * @property integer $active
 * @property string $description
 * @property string $source
 * @property string $link
 * @property string $author
 */
class GalleryPhoto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gallery_photo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image', 'description'], 'string'],
            [['position', 'active', 'gallery_id'], 'integer'],
            [['source', 'link', 'author'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Image',
            'position' => 'Position',
            'active' => 'Active',
            'description' => 'Description',
            'source' => 'Source',
            'link' => 'Link',
            'author' => 'Author',
        ];
    }
}
