<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 09.12.2020
 * Time: 9:41
 */

namespace app\models\repositories;


use app\models\Parametrs;

class HomeReadRepository
{
    public function getParamValue($param_id)
    {
        return Parametrs::findOne($param_id);
    }
}