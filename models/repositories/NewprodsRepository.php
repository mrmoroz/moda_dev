<?php
namespace app\models\repositories;

use app\models\Product;
use yii\db\Expression;

class NewprodsRepository
{
    public function getNewprods()
    {
        $prods = [];
        $products = Product::find()
            ->with(['category'])
            ->where(['is_wholesale_active' => true,
                     'is_active' => true,
                     'is_new' => true,
                     'is_archive' => false])
            ->orderBy(new Expression('random()'))
            ->limit(40)
            ->all();
        if($products){
            foreach ($products as $product){
                $prods[$product->id]['img'] = $product->getPrev($product->getFirstImage());
                $prods[$product->id]['cat_id'] = $product->category_id;
                $prods[$product->id]['article'] = $product->article;
                $prods[$product->id]['title'] = $product->title;
                $prods[$product->id]['price'] = $product->wholesale_price;
                $prods[$product->id]['dprice'] = $product->wholesale_discount_price;
                $prods[$product->id]['disc'] = $product->wholesale_discount;
                $prods[$product->id]['url'] = $this->getUrl($product->url, $product->category->type, $product->visible_b_mo);
            }
        }
        //vd($prods);
        return $prods;
    }

    private function getUrl($url, $type, $bigsize)
    {
        $sect = '/novinki';
        $subsect = '/zhenskaja/';
        switch ($type){
            case 1:
                if($bigsize){
                    $subsect = '/zhenskaja-bolshie-razmery/';
                }else{
                    $subsect = '/zhenskaja/';
                }
                break;
            case 2:
                $subsect = '/muzhskaja/';
                break;
            case 3:
                $subsect = '/detskaja/';
                break;
            case 4:
                $subsect = '/dom/';
                break;

        }

        return $sect.$subsect.$url;
    }
}