<?php
namespace app\models\repositories;


use app\models\Product;

class SensationRepository
{
    public function getSensation($ids)
    {
        $prods = [];
        if(!empty($ids)) {
            $idsRand = '';
            $_idsSen = explode(',',$ids);
            $ids_rand = array_rand($_idsSen, 20);
            foreach ($ids_rand as $id){
                $idsRand .= "'".$_idsSen[$id]."',";
            }
            $idsRand = rtrim($idsRand, ',');

            $sensations = Product::findBySql("
                    SELECT products.id, products.article, products.is_new, products.category_id, products.url,products.title,products.wholesale_discount, products.wholesale_discount_price, products.wholesale_price, MAX(product_changelog.operation_date) as operation_date FROM products 
                        LEFT JOIN product_changelog ON product_changelog.product_id = products.id
                        WHERE products.id IN (
                            " . $idsRand . "
                        ) AND products.is_active = true AND products.is_archive=false AND is_wholesale_active = true
                        GROUP BY products.id
                        ORDER BY operation_date DESC")->all();
            if($sensations){
                foreach ($sensations as $product){
                    $prods[$product->id]['img'] = $product->getPrev($product->getFirstImage());
                    $prods[$product->id]['cat_id'] = $product->category_id;
                    $prods[$product->id]['is_new'] = $product->is_new;
                    $prods[$product->id]['article'] = $product->article;
                    $prods[$product->id]['title'] = $product->title;
                    $prods[$product->id]['price'] = $product->wholesale_price;
                    $prods[$product->id]['dprice'] = $product->wholesale_discount_price;
                    $prods[$product->id]['disc'] = $product->wholesale_discount;
                    $prods[$product->id]['url'] = $product->getUrl();
                }
            }
        }

        return $prods;
    }


}