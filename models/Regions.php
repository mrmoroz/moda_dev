<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\models\Country;

/**
 * This is the model class for table "regions".
 *
 * @property integer $id
 * @property integer $country_id
 * @property string $name
 * @property string $name_geo
 * @property integer $timezone
 * @property string $text_timezone
 * @property string $note
 * @property integer $pos
 * @property integer $visible
 * @property integer $sys_language
 */
class Regions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'regions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'timezone', 'pos', 'visible', 'sys_language'], 'integer'],
            [['name', 'name_geo', 'text_timezone', 'note'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'country_id' => 'Страна',
            'name' => 'Регион',
            'name_geo' => 'Гео. регион',
            'timezone' => 'Временная зона',
            'text_timezone' => 'Text Timezone',
            'note' => 'Заметка',
            'pos' => 'Позиция',
            'visible' => 'Видимость',
            'sys_language' => 'Sys Language',
            'countryName' => 'Страна',
        ];
    }

    public function regionsListForForm(){
        return ArrayHelper::map(Regions::find()->orderBy('name')->all(),'id','name');
    }

    /* Связь с моделью Страны*/
    public function getCountry(){
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /* Геттер для названия страны */
    public function getCountryName() {
        return $this->country->name;
    }


}
