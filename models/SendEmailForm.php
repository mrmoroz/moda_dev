<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 28.02.2017
 * Time: 10:47
 */

namespace app\models;
use Yii;
use yii\base\Model;
use app\models\User;

class SendEmailForm extends Model
{
    public $email;

    public function rules()
    {
        return [
            ['email','filter','filter'=>'trim'],
            ['email','required'],
            ['email','email'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email'=>'Емайл'
        ];
    }

    public function sendEmail(){
        $user = User::findOne(['login'=>$this->email]);
        if($user){
            $user->generateSecretKey(); //генерируем секретный ключ
            if($user->save(false)){
                //отправляем письмо пользователю
                return Yii::$app->mailer->compose('resetPassword', ['user'=>$user])
                    ->setFrom([Yii::$app->params['supportEmail']=>'Отправлено с сайта'])
                    ->setTo($this->email)
                    ->setSubject('Сброс пароля')
                    ->send();
            }
        }
        return false;
    }

}