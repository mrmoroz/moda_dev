<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "size_chart".
 *
 * @property integer $id
 * @property string $name
 * @property boolean $active
 */
class SizeChart extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'size_chart';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['size_ids'], 'each', 'rule' => ['integer']],
            [['name'], 'required'],
            [['active'], 'boolean'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => \voskobovich\behaviors\ManyToManyBehavior::className(),
                'relations' => [
                    'size_ids' => 'sizes',
                ],
            ],
        ];
    }

    public function getSizes()
    {
        return $this->hasMany(Size::className(), ['id' => 'size_id'])
            ->viaTable('{{%size_chart_size}}', ['size_chart_id' => 'id'])->orderBy('name');
    }

    public static function listAll($keyField = 'id', $valueField = 'name', $asArray = true)
    {
        $query = static::find();
        if ($asArray) {
            $query->select([$keyField, $valueField])->asArray();
        }

        return ArrayHelper::map($query->all(), $keyField, $valueField);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'active' => 'Активна',
            'size_ids' => 'Размеры'
        ];
    }
}
