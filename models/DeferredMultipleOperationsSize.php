<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "deferred_multiple_operations_sizes".
 *
 * @property integer $id
 * @property integer $operation_id
 * @property integer $size_id
 */
class DeferredMultipleOperationsSize extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'deferred_multiple_operations_sizes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['operation_id', 'size_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'operation_id' => 'Operation ID',
            'size_id' => 'Size ID',
        ];
    }
}
