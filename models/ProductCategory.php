<?php

namespace app\models;

use app\helpers\RtHelper;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "product_categories".
 *
 * @property integer $id
 * @property integer $type
 * @property string $title
 * @property string $menu_title
 * @property string $url
 * @property integer $parent_id
 * @property integer $size_chart_id
 * @property integer $position
 * @property boolean $is_active
 * @property boolean $is_active_wholesale
 * @property boolean $access
 * @property integer $big_new_products_count
 * @property integer $standard_new_products_count
 * @property string $h1_title
 * @property string $h1_title_b
 *
 * @property ProductCategory $parent
 * @property ProductCategory[] $productCategories
 * @property Product[] $products
 */
class ProductCategory extends \yii\db\ActiveRecord
{
    public $withStdSizes;
    public $withBigSizes;
    public $productsCount;

    const TYPE_ARCHIVE = 0;
    const TYPE_WOMAN = 1;
    const TYPE_MAN = 2;
    const TYPE_CHILDREN = 3;
    const TYPE_HOME = 4;

    const SIZE_BIG = 11;
    const SIZE_STD = 12;
    const SIZE_ALL = 13;

    public static $typesLabes = [
        self::TYPE_ARCHIVE => 'Архив',
        self::TYPE_WOMAN => 'Женщинам',
        self::TYPE_MAN => 'Мужчинам',
        self::TYPE_CHILDREN => 'Детям',
        self::TYPE_HOME => 'Дом',
    ];

    public static $categoryTypeUrls = [
        self::TYPE_WOMAN => 'zhenskaja',
        self::TYPE_MAN => 'muzhskaja',
        self::TYPE_CHILDREN => 'detskaja',
        self::TYPE_HOME => 'dom',
    ];

    public static $categorySizeUrls = [
        self::SIZE_BIG => 'bolshie-razmery',
        self::SIZE_STD => 'bazovie-razmery',
        self::SIZE_ALL => 'vse-razmery',
    ];

    public $standard_size_ids, $big_size_ids, $category_tags_ids;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['standard_size_ids', 'big_size_ids', 'category_tags_ids'], 'each', 'rule' => ['integer']],
            [['title', 'type'], 'required'],
            [['h1_title', 'h1_title_b'], 'string'],
            [['url'], 'unique'],
            [['type', 'parent_id', 'position', 'size_chart_id','visible_nsk', 'standard_new_products_count',
                'big_new_products_count'], 'integer'],
            [['is_active', 'access', 'is_active_wholesale'], 'boolean'],
            [['title', 'menu_title'], 'string', 'max' => 255],
            [['parent_id'], 'compare', 'compareAttribute' => 'id', 'operator' => '!='],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductCategory::className(), 'targetAttribute' => ['parent_id' => 'id']],
            [['size_chart_id'], 'compare', 'compareAttribute' => 'id', 'operator' => '!='],
            [['size_chart_id'], 'exist', 'skipOnError' => true, 'targetClass' => SizeChart::className(), 'targetAttribute' => ['size_chart_id' => 'id']],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => \voskobovich\behaviors\ManyToManyBehavior::className(),
                'relations' => [
                    'standard_size_ids' => 'standardSizes',
                    'big_size_ids' => 'bigSizes',
                    'category_tags_ids' => 'categoryTags'
                ],
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (empty($this->url)) {
            $this->url = strtolower(RtHelper::urlToTranslit($this->title));
        }
        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Раздел',
            'title' => 'Название',
            'menu_title' => 'Название для меню',
            'parent_id' => 'Родительская категория',
            'position' => 'Позиция',
            'is_active' => 'Отображать на сайте',
            'size_chart_id' => 'Размерная сетка',
            'visible_nsk' => 'Видимость в Новосибирске',
            'standard_new_products_count' => 'Количество новинок стандартных размеров',
            'big_new_products_count' => 'Количество новинок больших размеров',
            'is_active_wholesale' => 'Отображать на МодаОптом',
            'h1_title' => 'Заголовок страницы',
            'h1_title_b' => 'Заголовок страницы для больших размеров',
        ];
    }

    /**
     * @param $categoryUrl
     * @param bool $active
     * @return self
     */
    public static function findByUrl($categoryUrl, $active = true)
    {
        if (empty($categoryUrl)) {
            return null;
        }
        return self::find()
            ->where('url=:url', ['url' => $categoryUrl])
            ->andWhere('is_active=:active', ['active' => $active])
            ->one();
    }

    /**
     * Возвращает список меток категорий
     * @return mixed
     */
    public function getCategoryTags($forCheckBoxList = false)
    {
        $productCategoriesTags = $this->hasMany(ProductCategoriesTag::className(), ['id' => 'product_category_tag_id'])
            ->viaTable('{{%product_category_product_category_tags}}', ['product_category_id' => 'id'])
            ->where(['is_active' => true])
            ->orderBy('name');

        if ($forCheckBoxList) {
            return ArrayHelper::getColumn($productCategoriesTags->all(), 'id');
        } else {
            return $productCategoriesTags;
        }
    }

    public function getStandardSizes()
    {
        if ($this->size_chart_id != null) {
            $categoryWithSizeChart = $this;
        } elseif (isset($this->parent) && $this->parent->size_chart_id != null) {
            $categoryWithSizeChart = $this->parent;
        } else {
            return null;
        }

        return $categoryWithSizeChart->hasMany(Size::className(), ['id' => 'size_id'])
            ->viaTable('{{%category_sizes}}', ['category_id' => 'id'],  function ($query) {
                /* @var $query \yii\db\ActiveQuery */
                $query->andWhere(['type' => 'std']);
            })->orderBy('name');
    }

    public function getBigSizes()
    {
        if ($this->size_chart_id != null) {
            $categoryWithSizeChart = $this;
        } elseif (isset($this->parent) && $this->parent->size_chart_id != null) {
            $categoryWithSizeChart = $this->parent;
        } else {
            return null;
        }

        return $categoryWithSizeChart->hasMany(Size::className(), ['id' => 'size_id'])
            ->viaTable('{{%category_sizes}}', ['category_id' => 'id'],  function ($query) {
                /* @var $query \yii\db\ActiveQuery */
                $query->andWhere(['type' => 'big']);
            })->orderBy('name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(ProductCategory::className(), ['id' => 'parent_id'])->inverseOf('productCategories');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSizeChart()
    {
        $sizeChart = $this->hasOne(SizeChart::className(), ['id' => 'size_chart_id'])->one();
        if ($sizeChart != null) {
            return $sizeChart;
        } elseif ($this->parent_id != null) {
            return SizeChart::findOne($this->parent->size_chart_id);
        } else {
            return null;
        }
    }

    public function getSizeChartId()
    {
        $sizeChart = $this->getSizeChart();

        if ($sizeChart) {
            return $sizeChart->id;
        }

        return null;
    }

    public function getChildrenCount()
    {
        return $this->hasMany(ProductCategory::className(), ['parent_id' => 'id'])->count();
    }

    public static function getRootCategoriesCount($type = null)
    {
        $query = ProductCategory::find()->where(['parent_id' => null]);
        if ($type) {
            $query->andWhere(['type' => $type]);
        }

        return $query->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCategories($isActive = false)
    {
        $query = $this->hasMany(ProductCategory::className(), ['parent_id' => 'id'])
            ->inverseOf('parent')->orderBy('position');

        if ($isActive) {
            $query = $query->where(['is_active' => true]);
        }

        return $query;
    }

    public static function getIdsCats($id){
        $sql = "WITH RECURSIVE r AS (
               SELECT id, parent_id
               FROM product_categories
               WHERE id = '{$id}'
            
               UNION
            
               SELECT product_categories.id, product_categories.parent_id
               FROM product_categories
                  JOIN r
                      ON product_categories.parent_id = r.id
            )
            SELECT id FROM r;";

        $ids =  ArrayHelper::getColumn(\Yii::$app->db->createCommand($sql)->queryAll(),'id');
        return $ids;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this
            ->hasMany(Product::className(), ['category_id' => 'id'])
            ->where(['is_active' => true])
            ->andWhere(['is_wholesale_active' => true])
            ->with('gallery', 'category');
    }

    public function getUrl()
    {
        $url = '/' . self::$categoryTypeUrls[$this->type] . '/vse-razmery';
        /*if ($this->parent_id) {
            $url .= '/' . $this->parent->url;
        }*/
        $url .= '/' . $this->url;

        return $url;
    }

    /**
     * URL для больших размеров
     * @return string
     */
    public function getBigUrl()
    {
        $url = '/' . self::$categoryTypeUrls[$this->type] . '/bolshie-razmery';
        /*if ($this->parent_id) {
            $url .= '/' . $this->parent->url;
        }*/
        $url .= '/' . $this->url;

        return $url;
    }

    /**
     * URL для стандартных размеров
     * @return string
     */
    public function getStandardUrl()
    {
        $url = '/' . self::$categoryTypeUrls[$this->type] . '/bazovie-razmery';
        /*if ($this->parent_id) {
            $url .= '/' . $this->parent->url;
        }*/
        $url .= '/' . $this->url;
        
        return $url;
    }

    /**
     * Получить массив id всех дочерних категорий, включая выбранную
     */
    public function getSearchCategories()
    {
        $categories = ArrayHelper::getColumn($this->getProductCategories()->select('id')->asArray()->all(), 'id');
        $categories[] = $this->id;

        return $categories;
    }

    /**
     * @param $categoryId
     * Возвращает массив родственных категорий включая текущую
     * @return array
     */
    public static function getAllRelatedCategories($categoryId)
    {
        $category = self::findOne($categoryId);

        if (!$category->parent_id) {
            $categories = $category->getSearchCategories();
        } else {
            $categories = $category->parent->getSearchCategories();
        }

        return $categories;
    }

    /**
     * Возвращает список производителей привязанных к данной категории
     */
    public function getManufacturers()
    {
        $categories = implode(',', $this->getSearchCategories());

        return ArrayHelper::map(ClothForSearch::findBySql("SELECT M.client_name, M.id
                                   FROM manufacturer M
                                   JOIN prods_cats_params_values PV ON PV.value = M.id::VARCHAR
                                   JOIN products P ON PV.prod_id = P.id
                                   WHERE PV.param_id = 5
                                   AND P.category_id IN ({$categories})
                                   GROUP BY M.id
                                   ORDER BY M.client_name")->asArray()->all(), 'id', 'client_name');
    }

    /**
     * Возвращает типы тканей, привязанные к данной категории
     */
    public function getTextureTypes()
    {
        $categories = implode(',', $this->getSearchCategories());

        return ArrayHelper::map(ClothForSearch::findBySql("SELECT CS.name, CS.id
                                   FROM cloth_for_search CS
                                   JOIN prods_cats_params_values PV ON PV.value = CS.id::VARCHAR
                                   JOIN products P ON PV.prod_id = P.id
                                   WHERE PV.param_id = 26
                                   AND P.category_id IN ({$categories})
                                   GROUP BY CS.id
                                   ORDER BY CS.name")->asArray()->all(), 'id', 'name');
    }

    /**
     * Возвращает цвета для поиска, привязанные к данной категории
     */
    public function getColorForSearch()
    {
        $categories = implode(',', $this->getSearchCategories());

        return ArrayHelper::map(ClothForSearch::findBySql("SELECT SC.name, SC.id
                                                           FROM search_colors SC
                                                           JOIN prods_cats_params_values PV ON PV.value = SC.id::VARCHAR
                                                           JOIN products P ON PV.prod_id = P.id
                                                           WHERE PV.param_id = 31
                                                           AND P.category_id IN ({$categories})
                                                           GROUP BY SC.id
                                                           ORDER BY SC.name")->asArray()->all(), 'id', 'name');
    }

    public function getAvailableCategoryTags()
    {
        $categories = implode(',', $this->getSearchCategories());

        return ArrayHelper::map(CategoryTag::findBySql("SELECT PCT.id, PCT.client_name FROM product_categories_tags PCT
                            JOIN product_category_product_category_tags PCPCT ON PCPCT.product_category_tag_id = PCT.id
                            JOIN product_categories PC ON PC.id = PCPCT.product_category_id
                            JOIN products P ON P.category_id = PC.id
                            JOIN prods_cats_params_values PV ON PV.prod_id = P.id
                            WHERE PV.param_id = 17
                            AND PV.value = PCT.id::VARCHAR
                            AND P.is_active = TRUE
                            AND P.is_archive = FALSE
                            AND P.category_id IN ({$categories})
                            GROUP BY PCT.id
                            ORDER BY PCT.position")->asArray()->all(), 'id', 'client_name');
    }

    public function checkStandardSizes()
    {
        $sizesIds = $this->getStandardSizes();
        if (empty($sizesIds)) {
            return false;
        }

        $sizesIds = implode(',', ArrayHelper::getColumn($sizesIds->all(), 'id'));

        if (empty($sizesIds)) {
            return false;
        }

        $categoryIds = implode(',', $this->getSearchCategories());

        $productCount = Product::findBySql("SELECT COUNT(P.id)
                                            FROM products P
                                            JOIN stock_position SP ON P.id = SP.product_id
                                            JOIN product_categories PC ON P.category_id = PC.id
                                            JOIN prods_cats_params_values PV ON P.id = PV.prod_id
                                            JOIN manufacturer M ON M.id::VARCHAR = PV.value
                                            WHERE SP.quantity > 0
                                            AND SP.size_id IN ({$sizesIds})
                                            AND PC.id IN ($categoryIds)
                                            AND PV.param_id = 5
                                            AND M.show_in_wholesale = TRUE
                                            AND PC.is_active_wholesale = TRUE
                                            AND P.is_active = TRUE 
                                            AND P.is_std_size = TRUE 
                                            AND P.is_archive = FALSE
                                            AND P.is_wholesale_active = TRUE")->asArray()->all()[0]['count'];

        if ($productCount > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function checkBigSizes()
    {
        $sizesIds = $this->getBigSizes();
        if (empty($sizesIds)) {
            return false;
        }

        $sizesIds = implode(',', ArrayHelper::getColumn($sizesIds->all(), 'id'));

        if (empty($sizesIds)) {
            return false;
        }

        $categoryIds = implode(',', $this->getSearchCategories());

        $productCount = Product::findBySql("SELECT COUNT(P.id)
                                            FROM products P
                                            JOIN stock_position SP ON P.id = SP.product_id
                                            JOIN product_categories PC ON P.category_id = PC.id
                                            JOIN prods_cats_params_values PV ON P.id = PV.prod_id
                                            JOIN manufacturer M ON M.id::VARCHAR = PV.value
                                            WHERE SP.quantity > 0
                                            AND SP.size_id IN ({$sizesIds})
                                            AND PC.id IN ($categoryIds)
                                            AND PV.param_id = 5
                                            AND M.show_in_wholesale = TRUE
                                            AND PC.is_active_wholesale = TRUE
                                            AND P.is_active = TRUE 
                                            AND P.is_big_size = TRUE 
                                            AND P.is_archive = FALSE
                                            AND P.is_wholesale_active = TRUE")->asArray()->all()[0]['count'];

        if ($productCount > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function checkChildCategories($childId)
    {
        if (ProductCategory::find()->where(['parent_id' => $this->id])->andWhere(['id' => $childId])->count() > 0) {
            return true;
        } else {
            return false;
        }
    }
}
