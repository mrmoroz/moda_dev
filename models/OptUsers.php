<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "opt_users".
 *
 * @property integer $id
 * @property string $email
 * @property string $f_name
 * @property string $l_name
 * @property string $t_name
 * @property string $phone
 * @property integer $region_id
 * @property string $region_name
 * @property string $city_name
 * @property string $tk
 * @property integer $realizations
 * @property string $last_order_date
 * @property string $create_time
 * @property string $cdate
 * @property integer $ignore_mail_list
 * @property string $call_date
 * @property string $comment
 * @property integer $pos
 * @property integer $visible
 * @property integer $sys_language
 */
class OptUsers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'opt_users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'f_name', 'l_name', 't_name', 'phone', 'city_name', 'pos'], 'required'],
            [['region_id', 'realizations', 'ignore_mail_list', 'pos', 'visible', 'sys_language'], 'integer'],
            [['last_order_date', 'create_time', 'call_date'], 'safe'],
            [['comment'], 'string'],
            [['email', 'f_name', 'l_name', 't_name', 'phone', 'region_name', 'city_name', 'tk', 'cdate'], 'string', 'max' => 255],
            [['email'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'f_name' => 'F Name',
            'l_name' => 'L Name',
            't_name' => 'T Name',
            'phone' => 'Phone',
            'region_id' => 'Region ID',
            'region_name' => 'Region Name',
            'city_name' => 'City Name',
            'tk' => 'Tk',
            'realizations' => 'Realizations',
            'last_order_date' => 'Last Order Date',
            'create_time' => 'Create Time',
            'cdate' => 'Cdate',
            'ignore_mail_list' => 'Ignore Mail List',
            'call_date' => 'Call Date',
            'comment' => 'Comment',
            'pos' => 'Pos',
            'visible' => 'Visible',
            'sys_language' => 'Sys Language',
        ];
    }
}
