<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cab_email".
 *
 * @property integer $id
 * @property integer $page_id
 * @property integer $user_id
 * @property integer $new_msg
 * @property string $manager
 * @property string $cdate
 * @property string $message
 * @property integer $from11
 * @property integer $pos
 * @property integer $visible
 * @property integer $sys_language
 */
class CabEmail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cab_email';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_id', 'user_id', 'new_msg', 'from11', 'pos', 'visible', 'sys_language'], 'integer'],
            [['cdate'], 'safe'],
            [['message'], 'string'],
            [['manager'], 'string', 'max' => 255],
            [['message'],'required','on'=>'acc_emial']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => 'Page ID',
            'user_id' => 'User ID',
            'new_msg' => 'New Msg',
            'manager' => 'Manager',
            'cdate' => 'Cdate',
            'message' => 'Сообщение',
            'from11' => 'From11',
            'pos' => 'Pos',
            'visible' => 'Visible',
            'sys_language' => 'Sys Language',
        ];
    }
}
