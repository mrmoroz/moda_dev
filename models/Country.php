<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "country".
 *
 * @property integer $id
 * @property string $name
 * @property integer $pos
 * @property integer $visible
 * @property integer $sys_language
 * @property string $text_timezone
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'country';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pos', 'visible', 'sys_language'], 'integer'],
            [['name', 'text_timezone'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Страна',
            'pos' => 'Позиция',
            'visible' => 'Видимость',
            'sys_language' => 'Системный язык',
            'text_timezone' => 'Временная зона',
        ];
    }

    public function countryListForForm(){
        return ArrayHelper::map(Country::find()->all(),'id','name');
    }
}
