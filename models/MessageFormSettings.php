<?php

namespace app\models;

use app\helpers\RtHelper;
use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "message_form_settings".
 *
 * @property integer $id
 * @property integer $type
 * @property string $name
 * @property string $menu_title
 * @property string $url
 * @property string $HTML_title
 * @property string $HTML_description
 * @property string $HTML_keywords
 * @property integer $quantity_rows
 * @property boolean $pluggable
 * @property boolean $ability_to_respond
 * @property boolean $ability_to_publish
 * @property boolean $moderation
 * @property string $mail
 * @property boolean $active
 */
class MessageFormSettings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message_form_settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'quantity_rows'], 'integer'],
            [['name', 'menu_title', 'url', 'HTML_title', 'HTML_description', 'HTML_keywords', 'mail'], 'string'],
            [['pluggable', 'ability_to_respond', 'ability_to_publish', 'moderation', 'active'], 'boolean'],
            [['name'], 'unique'],
        ];
    }

    public function beforeSave($insert)
    {
        if (empty($this->url)) {
            $this->url = strtolower(RtHelper::urlToTranslit($this->name));
        }
        return parent::beforeSave($insert);
    }

    public function getFields()
    {
        return $this->hasMany(MessageField::className(), ['form_type' => 'type'])
                    ->orderBy(new Expression('position IS NULL ASC, position ASC'));
    }

    public function getButtons()
    {
        return $this->hasMany(MessageButton::className(), ['form_type' => 'type']);
    }

    public function getUrl()
    {
        $url = "/callback/";

        if (!empty($this->url)) {
            $url .= $this->url;
        } else {
            $url .= $this->id;
        }

        return $url;
    }

    public static function findOneByUrl($url)
    {
        return self::find()
            ->where(['url' => $url])
            ->one();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'name' => 'Название',
            'menu_title' => 'Название для меню',
            'url' => 'URL',
            'HTML_title' => 'HTML-заголовок',
            'HTML_description' => 'HTML-описание',
            'HTML_keywords' => 'HTML-keywords',
            'quantity_rows' => 'Количество колонок',
            'pluggable' => 'Подключаемая форма',
            'ability_to_respond' => 'Возможность ответа',
            'ability_to_publish' => 'Возможность публикации',
            'moderation' => 'Пре/пост-модерация',
            'mail' => 'E-mail администратора',
            'active' => 'Актив',
        ];
    }
}
