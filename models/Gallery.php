<?php

namespace app\models;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "gallery".
 *
 * @property integer $id
 * @property string $entity_class
 * @property integer $entity_id
 */
class Gallery extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gallery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entity_class'], 'string'],
            [['entity_id'], 'integer'],
        ];
    }

    public function getPhotos()
    {
        return $this->hasMany(GalleryPhoto::className(), ['gallery_id' => 'id'])
                    ->orderBy('position');
    }

    public function deletePhotos()
    {
        GalleryPhoto::deleteAll(["gallery_id" => $this->id]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'entity_class' => 'Entity Class',
            'entity_id' => 'Entity ID',
        ];
    }
}
