<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "category_sizes".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $size_id
 * @property string $type
 */
class CategorySize extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category_sizes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'size_id'], 'integer'],
            [['type'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'size_id' => 'Size ID',
            'type' => 'Type',
        ];
    }
}
