<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "message_field".
 *
 * @property integer $id
 * @property string $name
 * @property integer $type
 * @property boolean $required
 * @property boolean $for_list_messages
 * @property integer $form_type
 * @property integer $position
 * @property boolean $published
 * @property integer $content_type
 * @property boolean $name_field
 */
class MessageField extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message_field';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['type', 'form_type', 'position', 'content_type'], 'integer'],
            [['name'], 'string'],
            [['required', 'for_list_messages', 'published', 'name_field'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'type' => 'Тип',
            'required' => 'Обязательно',
            'position' => 'Позиция',
            'published' => 'Публикация',
            'content_type' => 'Тип данных',
            'for_list_messages' => 'Для списка сообщений',
        ];
    }
}
