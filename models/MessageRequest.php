<?php

namespace app\models;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "message_request".
 *
 * @property integer $id
 * @property string $entity_type
 * @property integer $entity_id
 * @property integer $form_type
 * @property integer $status
 * @property boolean $published
 * @property string $title
 * @property string $date_send
 */
class MessageRequest extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message_request';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entity_type', 'title'], 'string'],
            [['entity_id', 'status', 'form_type'], 'integer'],
            [['published'], 'boolean'],
            [['date_send'], 'safe'],
        ];
    }

    public function getFields()
    {
        return $this->hasMany(MessageRequestField::className(), ['message_id' => 'id'])->with('field');
    }

    public static function getCountAllUnreadMessages()
    {
        $count = MessageRequest::find()->where(['status' => 0])->count();
        if ($count > 99) {
            $count = '99+';
        }

        return $count;
    }

    public static function getCountUnreadMessages($id)
    {
        $count = MessageRequest::find()->where(['status' => 0, 'form_type' => $id])->count();
        if ($count > 99) {
            $count = '99+';
        }

        return $count;
    }

    /**
     * Возвращает 4 случайных отзыва о магазине
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getRandomReviews()
    {
        return self::find()->where(['entity_type' => 'callback'])
                           ->andWhere(['entity_id' => 3, 'form_type' => 3])
                           ->andWhere(['published' => true])
                           ->with('fields')
                           ->orderBy(new Expression('random()'))
                           ->limit(3)
                           ->all();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'entity_type' => 'Entity Type',
            'entity_id' => 'Entity ID',
            'status' => 'Статус',
            'published' => 'Публикуется',
            'date_send' => 'Дата отправки',
            'title' => 'Имя отправителя'
        ];
    }
}
