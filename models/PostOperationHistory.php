<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "post_operation_history".
 *
 * @property integer $id
 * @property integer $sale_id
 * @property string $operation_date
 * @property integer $operation_type_id
 * @property string $operation_type_name
 * @property integer $operation_attr_id
 * @property string $operation_attr_name
 * @property string $operation_address_index
 * @property string $operation_address_description
 */
class PostOperationHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post_operation_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sale_id', 'operation_type_id', 'operation_attr_id'], 'integer'],
            [['operation_date', 'operation_type_name', 'operation_attr_name', 'operation_address_index', 'operation_address_description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sale_id' => 'Sale ID',
            'operation_date' => 'Operation Date',
            'operation_type_id' => 'Operation Type ID',
            'operation_type_name' => 'Operation Type Name',
            'operation_attr_id' => 'Operation Attr ID',
            'operation_attr_name' => 'Operation Attr Name',
            'operation_address_index' => 'Operation Address Index',
            'operation_address_description' => 'Operation Address Description',
        ];
    }
}
