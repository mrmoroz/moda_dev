<?php

namespace app\models\readModels;

class UserReadModel
{
    public function getUsersToDate($date)
    {
        $sql = "SELECT email, f_name, l_name, t_name, phone, realizations, ignore_mail_list FROM opt_users
                WHERE last_order_date <= '{$date}' AND visible = '1'";
        return  \Yii::$app->db->createCommand($sql)->queryAll();
    }

}