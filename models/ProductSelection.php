<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "product_selection".
 *
 * @property string $entity_class
 * @property integer $entity_id
 * @property integer $product_id
 * @property string $size_type
 * @property integer $position
 */
class ProductSelection extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_selection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entity_class', 'entity_id', 'product_id', 'position'], 'required'],
            [['entity_class', 'size_type'], 'string'],
            [['entity_id', 'product_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'entity_class' => 'Entity Class',
            'entity_id' => 'Entity ID',
            'product_id' => 'Product ID',
        ];
    }

    public static function getProducts($entityClass, $entityId, $offset, $limit, $sizeType) {
        $ids = ArrayHelper::getColumn(self::find()->select('product_id')
                        ->where([
                            'entity_id' => $entityId,
                            'entity_class' => $entityClass,
                            'size_type' => $sizeType,
                            'is_wholesale' => true
                        ])
                        ->orderBy('position ASC')->offset($offset)->limit($limit)->asArray()->all(), 'product_id');

        $orderExpression = 'CASE id ';

        foreach ($ids as $number => $id) {
            $orderExpression .= " WHEN '" . $id . "' THEN " . $number;
        }

        $orderExpression .= ' END';

        $products = Product::find()->where(['IN', 'id', $ids])
            ->orderBy([new \yii\db\Expression($orderExpression)])->all();


        return $products;
    }

    public static function checkProducts($entityClass, $entityId, $sizeType)
    {
        return self::find()->where([
            'entity_class' => $entityClass,
            'entity_id' => $entityId,
            'size_type' => $sizeType,
            'is_wholesale' => true
        ])->count();
    }

    public static function clearQueryCacheByProductId($productId)
    {
        $entityData = self::find()->select('entity_id, entity_class')->where(['product_id' => $productId])->all();

        foreach ($entityData as $entity) {
            ProductSelection::deleteAll(['entity_class' => $entity->entity_class, 'entity_id' => $entity->entity_id]);
        }

        return true;
    }
}
