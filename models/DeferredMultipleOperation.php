<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "deferred_multiple_operations".
 *
 * @property integer $id
 * @property string $operation_mode
 * @property string $date
 * @property integer $creator_id
 * @property string $mode_parameter_value
 * @property boolean $is_completed
 */
class DeferredMultipleOperation extends \yii\db\ActiveRecord
{
    public static $operationNames = [
        'to-archive' => 'Добавление в архив',
        'set-discount' => 'Установка скидки',
        'change-discount' => 'Изменение скидки',
        'set-wholesale-discount' => 'Установка оптовой скидки',
        'change-wholesale-discount' => 'Изменение оптовой скидки',
        'set-tag' => 'Установка метки',
        'set-gift' => 'Добавление подарка',
        'delete-gift' => 'Удаление подарка'
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'deferred_multiple_operations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['creator_id'], 'integer'],
            [['mode_parameter_value'], 'string'],
            [['operation_mode'], 'string', 'max' => 255],
            [['is_completed'], 'boolean']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'operation_mode' => 'Тип операции',
            'date' => 'Дата операции',
            'creator_id' => 'Создатель',
            'mode_parameter_value' => 'Значение параметра',
        ];
    }

    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'creator_id']);
    }

    public function getCreatorName()
    {
        $creator = $this->getCreator()->one();

        if ($creator) {
            return $creator->login;
        }

        return null;
    }

    public function getParameters()
    {
        return $this->hasOne(DeferredMultipleOperationsParameter::className(), ['operation_id' => 'id']);
    }

    public function getCheckedSizes()
    {
        return $this->hasMany(DeferredMultipleOperationsSize::className(), ['operation_id' => 'id']);
    }

    public function getCheckedSizesAsArray()
    {
        $checkedSizes = $this->getCheckedSizes()->all();

        if (!empty($checkedSizes)) {
            return ArrayHelper::getColumn($checkedSizes, 'size_id');
        }

        return null;
    }
}
