<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_selection_item".
 *
 * @property integer $product_selection_id
 * @property integer $product_id
 */
class ProductSelectionItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_selection_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_selection_id', 'product_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_selection_id' => 'Product Selection ID',
            'product_id' => 'Product ID',
        ];
    }
}
