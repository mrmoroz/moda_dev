<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "stocks".
 *
 * @property integer $id
 * @property string $name
 * @property integer $position
 * @property boolean $is_manufacturer
 * @property boolean $active
 */
class Stock extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stocks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['position'], 'integer'],
            [['active', 'is_manufacturer'], 'boolean'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'position' => 'Позиция',
            'is_manufacturer' => 'Склад поставщика',
            'active' => 'Активен',
        ];
    }

    public static function isManufacturerStock($id) {
        $stock = self::findOne($id);

        return $stock->is_manufacturer;
    }

    public static function getName($id)
    {
        return self::findOne($id)->name;
    }
}
