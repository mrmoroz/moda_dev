<?php

namespace app\models;

use Yii;

const CONTAINER = 0;
const PRODUCT = 1;
const CATEGORY = 2;
const INFOPAGE = 3;
const ALL_CATEGORIES = 4;
const TAG = 5;
const MANUFACTURER = 6;
const SEARCH_COLOR = 7;
const CLOTH_TYPE = 8;
const CALLBACK = 9;

/**
 * This is the model class for table "menu".
 *
 * @property integer $id
 * @property integer $menu_id
 * @property integer $parent_id
 * @property integer $menu_item_id
 * @property integer $position
 */
class Menu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['menu_id', 'parent_id', 'menu_item_id', 'position'], 'integer'],
        ];
    }

    public function getChildren()
    {
        return $this->hasMany(Menu::className(), ['parent_id' => 'id'])->orderBy('position');
    }

    public function getMenuItem()
    {
        return $this->hasOne(MenuItem::className(), ['id' => 'menu_item_id']);
    }

    public static function getChildrenNodes($node)
    {
        $newNode['title'] = $node->menuItem->title;
        switch ($node->menuItem->entity_type) {
            case CONTAINER:
                $newNode['icon'] = "fa fa-folder-open-o";
                break;
            case PRODUCT:
                $newNode['icon'] = "fa fa-shopping-cart";
                break;
            case CATEGORY:
                $newNode['icon'] = "fa fa-bars";
                break;
            case INFOPAGE:
                $newNode['icon'] = "fa fa-pinterest";
                break;
            case ALL_CATEGORIES:
                $newNode['icon'] = "fa fa-cubes";
                break;
            case TAG:
                $newNode['icon'] = "fa fa-tag";
                break;
            case MANUFACTURER:
                $newNode['icon'] = "fa fa-truck";
                break;
            case SEARCH_COLOR:
                $newNode['icon'] = "fa fa-paint-brush";
                break;
            case CLOTH_TYPE:
                $newNode['icon'] = "fa fa-square";
                break;
            case CALLBACK:
                $newNode['icon'] = "fa fa-paper-plane-o";
                break;
        }
        if ($node->menuItem->entity_type == CONTAINER) {
            if ($node->menuItem->open_first_child) {
                $newNode['key'] = "active-";
            } else {
                $newNode['key'] = "";
            }
            $newNode['key'] .= "containerNode_" . $node->menu_id . "_" . $node->id;
        } else {
            $newNode['key'] = $node->menu_id . "_" . $node->id;
        }

        $nodes = array();
        foreach($node->children as $childNode) {
            $newChildNode = self::getChildrenNodes($childNode);
            array_push($nodes, $newChildNode);
        }

        if ($node->menuItem->entity_type != ALL_CATEGORIES) {
            $addNode['title'] = "Добавить пункт";
            $addNode['key'] = "addNode_" . $node->menu_id . "_" . $node->id;
            $addNode['folder'] = true;
            $addNode['icon'] = "fa fa-plus";
            array_push($nodes, $addNode);
        }

        $newNode['children'] = $nodes;

        return $newNode;
    }

    public static function getMenu($id = null)
    {
        $allNodes = Menu::find()->where(['menu_id' => $id])
                                ->with('children')
                                ->with('menuItem')
                                ->orderBy('position')
                                ->all();
        $menu = array();
        $rootNodes = array();

        foreach ($allNodes as $node) {
            if ($node->parent_id == null) {
                array_push($rootNodes, $node);
            }
        }

        foreach ($rootNodes as $rootNode) {
            // конфигурация узла в зависимости от типа сущности
            switch ($rootNode->menuItem->entity_type) {
                case CONTAINER:
                    $newNode['icon'] = "fa fa-folder-open-o";
                    break;
                case PRODUCT:
                    $newNode['icon'] = "fa fa-shopping-cart";
                    break;
                case CATEGORY:
                    $newNode['icon'] = "fa fa-bars";
                    break;
                case INFOPAGE:
                    $newNode['icon'] = "fa fa-pinterest";
                    break;
                case TAG:
                    $newNode['icon'] = "fa fa-tag";
                    break;
                case MANUFACTURER:
                    $newNode['icon'] = "fa fa-truck";
                    break;
                case SEARCH_COLOR:
                    $newNode['icon'] = "fa fa-paint-brush";
                    break;
                case CLOTH_TYPE:
                    $newNode['icon'] = "fa fa-square";
                    break;
                case CALLBACK:
                    $newNode['icon'] = "fa fa-paper-plane-o";
                    break;
                case ALL_CATEGORIES:
                    $newNode['icon'] = "fa fa-cubes";
                    break;
            }

            $newNode['title'] = $rootNode->menuItem->title;
            if ($rootNode->menuItem->entity_type == CONTAINER) {
                if ($rootNode->menuItem->open_first_child) {
                    $newNode['key'] = "active-";
                } else {
                    $newNode['key'] = "";
                }
                $newNode['key'] .= "containerNode_" . $rootNode->menu_id . "_" . $rootNode->id;
            } else {
                $newNode['key'] = $rootNode->menu_id . "_" . $rootNode->id;
            }

            $nodes = array();
            foreach($rootNode->children as $childNode) {
                $newChildNode = self::getChildrenNodes($childNode);
                array_push($nodes, $newChildNode);
            }

            if ($rootNode->menuItem->entity_type != ALL_CATEGORIES) {
                $addNode['title'] = "Добавить пункт";
                $addNode['key'] = "addNode_" . $id . "_" . $rootNode->id;
                $addNode['icon'] = "fa fa-plus";
                array_push($nodes, $addNode);
            }

            $newNode['children'] = $nodes;

            $menu[] = $newNode;
            unset($newNode);
        }

        $addNode['title'] = "Добавить пункт";
        $addNode['key'] = "addNode_root_" . $id;
        $addNode['icon'] = "fa fa-plus";
        array_push($menu, $addNode);

        return $menu;
    }

    public function getUrl()
    {
        $item = $this->menuItem;

        switch ($item->entity_type) {
            case PRODUCT:
                $product = Product::findOne($item->entity_id);
                $url = $product->getUrl();
                break;
            case CATEGORY:
                $category = ProductCategory::findOne($item->entity_id);
                $url = $category->getUrl();
                break;
            case INFOPAGE:
                $page = Page::findOne($item->entity_id);
                $url = $page->getUrl();
                break;
            case TAG:
                $tag = Tag::findOne($item->entity_id);
                $url = $tag->getUrl();
                break;
            case MANUFACTURER:
                $manufacturer = Manufacturer::findOne($item->entity_id);
                $url = $manufacturer->getUrl();
                break;
            case SEARCH_COLOR:
                $searchColor = SearchColor::findOne($item->entity_id);
                $url = $searchColor->getUrl();
                break;
            case CLOTH_TYPE:
                $clothType = ClothType::findOne($item->entity_id);
                $url = $clothType->getUrl();
                break;
            case CALLBACK:
                $callbacks = MessageFormSettings::findOne($item->entity_id);
                $url = $callbacks->getUrl();
                break;
            default:
                return null;
        }

        return $url;
    }
    
    public static function removeItem($id)
    {
        $menuItem = Menu::find()->with('children')->with('menuItem')->where(['id' => $id])->one();
        $menuId = $menuItem->menu_id;

        $connection = Yii::$app->db;

        // Обновляем позиции у элементов ниже
        $sql = "UPDATE {{menu}} SET position = position - 1 WHERE position > "
            . $menuItem->position . " AND menu_id = " . $menuId;
        if ($menuItem->parent_id) {
            $sql .= " AND parent_id = " . $menuItem->parent_id;
        } else {
            $sql .= " AND parent_id IS NULL";
        }
        $connection->createCommand($sql)->execute();

        foreach ($menuItem->children as $child) {
            self::removeChildren($child);
        }

        $menuItem->menuItem->delete();
        $menuItem->delete();

        return true;
    }

    public static function removeChildren($menuItem)
    {
        foreach ($menuItem->children as $child) {
            self::removeChildren($child);
        }

        $menuItem->menuItem->delete();
        $menuItem->delete();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'menu_id' => 'Menu ID',
            'parent_id' => 'Parent ID',
            'menu_item_id' => 'Menu Item ID',
            'position' => 'Position',
        ];
    }
}
