<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "product_categories_tags".
 *
 * @property integer $id
 * @property string $name
 * @property string $client_name
 * @property integer $position
 * @property boolean $is_active
 */
class ProductCategoriesTag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_categories_tags';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'client_name'], 'string'],
            [['position'], 'integer'],
            [['is_active'], 'boolean'],
        ];
    }

    public static function listAll($keyField = 'id', $valueField = 'name', $asArray = true)
    {
        $query = static::find()->where(['is_active' => true])->orderBy('name ASC');
        if ($asArray) {
            $query->select([$keyField, $valueField])->asArray();
        }

        return ArrayHelper::map($query->all(), $keyField, $valueField);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название для администратора',
            'client_name' => 'Название для клиента',
            'position' => 'Позиция',
            'is_active' => 'Активна',
        ];
    }
}
