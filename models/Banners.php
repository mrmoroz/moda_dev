<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "banners".
 *
 * @property integer $id
 * @property integer $type
 * @property string $src
 * @property string $link
 * @property boolean $is_retail_visible
 * @property boolean $is_wholesale_visible
 * @property boolean $is_visible_nsk
 * @property boolean $is_visible_other_regions
 * @property string $start_showing
 * @property string $stop_showing
 * @property integer $position
 * @property boolean $is_active
 * @property string $src2
 * @property string $src_size
 * @property string $src2_size
 * @property boolean $show_page
 * @property string $src3
 * @property string $src4
 * @property string $src3_size
 * @property string $src4_size
 * @property boolean $src_d_size
 * @property boolean $src3_d_size
 * @property string $link2
 * @property string $link3
 * @property string $link4
 * @property boolean $women
 * @property boolean $men
 * @property boolean $kids
 * @property boolean $dom
 */
class Banners extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'banners';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'position'], 'integer'],
            [['src', 'link', 'src2', 'link2', 'link3', 'link4'], 'string'],
            [['is_retail_visible', 'is_wholesale_visible', 'is_visible_nsk', 'is_visible_other_regions', 'is_active', 'show_page', 'src_d_size', 'src3_d_size', 'women', 'men', 'kids', 'dom'], 'boolean'],
            [['start_showing', 'stop_showing'], 'safe'],
            [['src_size', 'src2_size', 'src3', 'src4', 'src3_size', 'src4_size'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'src' => 'Src',
            'link' => 'Link',
            'is_retail_visible' => 'Is Retail Visible',
            'is_wholesale_visible' => 'Is Wholesale Visible',
            'is_visible_nsk' => 'Is Visible Nsk',
            'is_visible_other_regions' => 'Is Visible Other Regions',
            'start_showing' => 'Start Showing',
            'stop_showing' => 'Stop Showing',
            'position' => 'Position',
            'is_active' => 'Is Active',
            'src2' => 'Src2',
            'src_size' => 'Src Size',
            'src2_size' => 'Src2 Size',
            'show_page' => 'Show Page',
            'src3' => 'Src3',
            'src4' => 'Src4',
            'src3_size' => 'Src3 Size',
            'src4_size' => 'Src4 Size',
            'src_d_size' => 'Src D Size',
            'src3_d_size' => 'Src3 D Size',
            'link2' => 'Link2',
            'link3' => 'Link3',
            'link4' => 'Link4',
            'women' => 'Women',
            'men' => 'Men',
            'kids' => 'Kids',
            'dom' => 'Dom',
        ];
    }
}
