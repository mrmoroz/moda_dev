<?php

namespace app\models;

use app\components\pagination\CatalogLinkPager;
use app\components\pagination\CatalogPagination;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/**
 * ProductSearch represents the model behind the search form about `app\models\Product`.
 */
class ProductSearch extends Product
{
    public $tag;
    public $manufacturer;
    public $checkedSizes;
    public $searchCurrentCategory = true;
    public $sizeMode;
    public $additionalSizeModes;
    public $categoryType;
    public $searchColor;
    public $categoryTag;
    public $categories;
    public $searchTextureTypes;
    public $minPrice;
    public $maxPrice;
    public $hasDiscount;

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tag' => 'Метка',
            'manufacturer' => 'Производитель',
            'searchCurrentCategory' => 'Искать в текущей категории',
            'hasDiscount' => 'Есть скидка'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'discount', 'price', 'discount_price', 'position'], 'integer'],
            [['article', 'title'], 'safe'],
            [['is_active', 'is_big_size', 'is_std_size', 'is_archive', 'is_wholesale_active'], 'boolean'],
            [['tag', 'manufacturer', 'checkedSizes', 'searchCurrentCategory', 'sizeMode', 'sizeMode',
                'additionalSizeModes', 'categoryType', 'searchColor', 'categoryTag', 'categories',
                'searchTextureTypes', 'minPrice', 'maxPrice', 'hasDiscount'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params = null)
    {
        $query = Product::find()
            ->with('category', 'gallery');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if($_SERVER['SERVER_NAME'] != 'newbeautiful' && $params['ProductSearch']['geo']){
            if(isset($params['ProductSearch']['geo']->city_id) && $params['ProductSearch']['geo']->city_id==2012){
                $query->andFilterWhere(['visible_nsk'=>1]);
            }
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'discount' => $this->discount,
            'price' => $this->price,
            'discount_price' => $this->discount_price,
            'position' => $this->position,
            'is_active' => $this->is_active,
            'is_big_size' => $this->is_big_size,
            'is_std_size' => $this->is_std_size,
            'is_archive' => $this->is_archive,
            'is_new'=>$this->is_new
        ]);



        if ($this->categoryType) {
            $categoryType = $this->categoryType;
            $query->joinWith(['category' => function($query) use ($categoryType) {
                return $query->where(['type' => $categoryType]);
            }]);
        }

        if ($this->searchCurrentCategory || empty($this->article) && !empty($this->categories)) {
            $query->andFilterWhere(['IN', 'category_id', $this->categories]);
        }

        $query->andFilterWhere(['like', 'LOWER(article)', mb_strtolower($this->article)])
            ->orFilterWhere(['like', 'LOWER(title)', mb_strtolower($this->title)]);

        if (!empty($this->tag) || !empty($this->manufacturer) || !empty($this->searchColor) || !empty($this->searchTextureTypes)) {
            $query->leftJoin(ProdsCatsParamValue::tableName(), "prods_cats_params_values.prod_id = products.id")
                  ->andWhere(Product::tableName() . '.id=' . ProdsCatsParamValue::tableName() . '.prod_id');
        }

        if (!empty($this->tag)) {
            $subQuery = ProdsCatsParamValue::find()->select('prod_id')
                ->where(['param_id' => 9])
                ->andWhere(['value' => $this->tag]);
            $query->andWhere(['products.id' => $subQuery]);
        }

        if (!empty($this->manufacturer)) {
            $subQuery = ProdsCatsParamValue::find()->select('prod_id')
                                           ->where(['param_id' => 5])
                                           ->andWhere(['IN', 'value', $this->manufacturer]);
            $query->andWhere(['products.id' => $subQuery]);
        }

        if (!empty($this->searchColor)) {
            $subQuery = ProdsCatsParamValue::find()->select('prod_id')
                ->where(['param_id' => 31])
                ->andWhere(['IN', 'value', $this->searchColor]);
            $query->andWhere(['products.id' => $subQuery]);
        }

        if (!empty($this->categoryTag)) {
            $subQuery = ProdsCatsParamValue::find()->select('prod_id')
                ->where(['param_id' => 17])
                ->andWhere(['IN', 'value', $this->categoryTag]);
            $query->andWhere(['products.id' => $subQuery]);
        }

        if (!empty($this->searchTextureTypes)) {
            $subQuery = ProdsCatsParamValue::find()->select('prod_id')
                ->where(['param_id' => 26])
                ->andWhere(['IN', 'value', $this->searchTextureTypes]);
            $query->andWhere(['products.id' => $subQuery]);
        }

        if (!empty($this->checkedSizes)) {
            $query->leftJoin(StockPosition::tableName(), "stock_position.product_id = products.id")
                  ->andWhere(Product::tableName() . ".id=" . StockPosition::tableName() . ".product_id")
                  ->andWhere(['IN', 'stock_position.size_id', $this->checkedSizes]);
        }

        if (!empty($this->sizeMode)) {
            switch ($this->sizeMode) {
                case "big-sizes" :
                    $query->andFilterWhere(['visible_b_mo' => true]);
                    break;
                case "standard-sizes" :
                    $query->andFilterWhere(['visible_s_mo' => true]);
                    break;
            }
        }

        if (!empty($this->hasDiscount)) {
            $query->andWhere(['>', 'discount_price', 0]);
        }

        if (!empty($this->minPrice)) {
            if (!empty($this->hasDiscount)) {
                $query->andWhere(['>=', 'discount_price', $this->minPrice]);
            } else {
                $query->andWhere(['>=', 'price', $this->minPrice]);
            }
        }

        if (!empty($this->maxPrice)) {
            if (!empty($this->hasDiscount)) {
                $query->andWhere(['<=', 'discount_price', $this->maxPrice]);
            } else {
                $query->andWhere(['<=', 'price', $this->maxPrice]);
            }
        }

        if (!empty($this->additionalSizeModes) && empty($this->checkedSizes)) {
            if (in_array("having-us-sizes", $this->additionalSizeModes)) {
                $query->leftJoin(StockPosition::tableName(), "stock_position.product_id = products.id");
            }
        }

        if (!empty($this->additionalSizeModes)) {
            foreach ($this->additionalSizeModes as $additionalSizeMode) {
                switch ($additionalSizeMode) {
                    case "visible-mo":
                        $query->andFilterWhere(['is_wholesale_active' => true]);
                        break;
                    case "having-us-sizes":
                        $query->andWhere('stock_position.stock_id != 1')
                              ->andWhere(Product::tableName() . ".id=" . StockPosition::tableName() . ".product_id");
                        break;
                }
            }
        }

        $query->andWhere(['is_wholesale_active' => true]);
        $query->orderBy([new Expression('products.position IS NULL ASC, products.position ASC, id DESC')]);
        $query->groupBy("products.id");

        return $dataProvider;
    }

    public function isVisibleFilters()
    {
        if ($this->manufacturer || $this->tag || $this->categoryTag) {
            return true;
        }

        return false;
    }

    /**
     * @param $priceMin
     * @param $priceMax
     * @return bool Проверка пустоты фильтров в каталоге
     * Проверка пустоты фильтров в каталоге
     */
    public function checkCatalogFilters($priceMin = null, $priceMax = null)
    {
        if ($this->checkedSizes || $this->manufacturer || $this->searchColor || $this->categoryTag || $this->searchTextureTypes
            || ($this->minPrice && $this->minPrice != $priceMin) || ($this->maxPrice && $this->maxPrice != $priceMax)) {
            return true;
        }

        return false;
    }

    /**
     * Возвращает параметры для GET запроса
     */
    public function createGetParamsForCatalogFilters()
    {
        $url = '&' . ltrim(Url::to(['',
            'ProductSearch[manufacturer]' => $this->manufacturer,
            'ProductSearch[searchColor]' => $this->searchColor,
            'ProductSearch[categoryTag]' => $this->categoryTag,
            'ProductSearch[searchTextureTypes]' => $this->searchTextureTypes,
            'ProductSearch[minPrice]' => $this->minPrice,
            'ProductSearch[maxPrice]' => $this->maxPrice,
            'ProductSearch[checkedSizes]' => $this->checkedSizes
        ]), '/?');

        return $url;
    }
}
