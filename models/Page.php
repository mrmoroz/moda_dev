<?php

namespace app\models;

use app\helpers\RtHelper;
use Yii;

/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property string $title
 * @property string $menu_title
 * @property string $url
 * @property integer $show_menu
 * @property integer $active
 * @property integer $show_back_connect_up
 * @property integer $show_back_connect_down
 * @property integer $position
 * @property boolean $has_left_menu
 */
class Page extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['show_menu', 'active', 'show_back_connect_up', 'show_back_connect_down', 'position'], 'integer'],
            [['title', 'menu_title', 'url', 'seo_keywords', 'seo_descrption', 'seo_title'], 'string', 'max' => 255],
            [['has_left_menu'], 'boolean'],
        ];
    }

    public function getPageBlocks()
    {
        return $this->hasMany(PageBlock::className(), ['page_id' => 'id']);
    }

    public function getForms()
    {
        return $this->hasMany(MessageForm::className(), ['entity_id' => 'id'])->where(['entity_type' => 'page'])->all();
    }
    
    public function getFormsWithFields()
    {
        return $this->hasMany(MessageForm::className(), ['entity_id' => 'id'])->where(['entity_type' => 'page'])
            ->with('formSettings')->all();
    }

    public function getPluggableForm()
    {
        return MessageFormSettings::find()->where(['type' => 7])->one();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('interface', 'ID'),
            'title' => Yii::t('interface', 'Title'),
            'menu_title' => Yii::t('interface', 'Название для меню'),
            'url' => Yii::t('interface', 'Url'),
            'show_menu' => Yii::t('interface', 'Show Menu'),
            'active' => Yii::t('interface', 'Active Page'),
            'has_left_menu' => Yii::t('interface', 'Шаблон с левым меню'),
            'show_back_connect_up' => Yii::t('interface', 'Форма обратной связи в верхней части страницы'),
            'show_back_connect_down' => Yii::t('interface', 'Форма обратной связи в нижней части страницы'),
            'position' => Yii::t('interface', 'Position Page'),
            'seo_title' => Yii::t('interface', 'Seo Title'),
            'seo_descrption' => Yii::t('interface', 'Seo Descrption'),
            'seo_keywords' => Yii::t('interface', 'Seo Keywords'),
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if (!isset($this->show_menu)) {
                $this->show_menu = 0;
            }
            if (!isset($this->active)) {
                $this->active = 0;
            }
            if (!isset($this->show_back_connect_up)) {
                $this->show_back_connect_up = 0;
            }
            if (!isset($this->show_back_connect_down)) {
                $this->show_back_connect_down = 0;
            }

            if(empty($this->url)){
                $this->url = strtolower(RtHelper::urlToTranslit($this->title));
            }

            return true;
        }
        return false;
    }

    public function getUrl()
    {
        $url = "/page/";

        if (!empty($this->url)) {
            $url .= $this->url;
        } else {
            $url .= $this->id;
        }

        return $url;
    }

    /**
     * @param $url
     * @return self
     */
    public static function findOneByUrl($url)
    {
        return self::find()
            ->where(['url' => $url])
            ->one();
    }
}
