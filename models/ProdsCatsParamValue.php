<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "prods_cats_params_values".
 *
 * @property integer $id
 * @property integer $prod_id
 * @property integer $param_id
 * @property string $value
 */
class ProdsCatsParamValue extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prods_cats_params_values';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['prod_id', 'param_id', 'value'], 'required'],
            [['prod_id', 'param_id'], 'integer'],
            [['value'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'prod_id' => 'Prod ID',
            'param_id' => 'Param ID',
            'value' => 'Value',
        ];
    }

    public static function getParamValue($productId, $paramId)
    {
        $param = self::find()->where(['prod_id' => $productId])->andWhere(['param_id' => $paramId])->one();

        if ($param) {
            return $param->value;
        } else {
            return null;
        }
    }

}
