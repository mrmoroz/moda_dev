<?php

namespace app\models;

use app\components\rbac\AuthAssignment;
use app\components\WebUser;
use app\modules\admin\models\MlCfg;
use app\models\UserProfile;
use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use app\modules\cart\models\Orders;



class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_WAIT = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_BLOCKED = 0;

    public static $statusesArray = [
        self::STATUS_WAIT => 'Waiting approve',
        self::STATUS_ACTIVE => 'Active',
        self::STATUS_BLOCKED => 'Blocked'
    ];
    

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'opt_users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            ['email', 'unique', 'targetClass' => self::className(), 'message' => 'Такой email уже используется'],
            [['f_name','l_name','t_name','phone','region_id','city_name','tk','realizations','ignore_mail_list','visible','create_time','pos'],'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [

        ];
    }

    public static function findById($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * @param $login
     * @return User
     */
    public static function findByEmail($login)
    {
        return static::findOne(['LOWER(email)' => mb_strtolower($login)]);
    }


    /*public function isNotIp(){
        $str = str_replace(array("\r","\n"," "),"",$this->ip_white_list);
        if(empty($str)){
            return false;
        }
        $ip = explode(',',$str);
        if(in_array($_SERVER['REMOTE_ADDR'],$ip)){
            return false;
        }
        return true;
    }*/

    /**
     * @return bool
     */
    public function isBlocked()
    {
        return $this->visible == self::STATUS_BLOCKED;
    }

    public function setBlocked()
    {
        $this->visible = self::STATUS_BLOCKED;
    }


    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }



    /**
     * @param $password
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('findIdentityByAccessToken is not implemented.');
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function getStatusName()
    {
        $statuses = self::$statusesArray;

        return isset($statuses[$this->status]) ? $statuses[$this->status] : '';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoles()
    {
        return $this->hasMany(AuthAssignment::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'id']);
    }

    public function getRolesNamesList()
    {
        $roleNames = [];
        foreach ($this->roles as $role) {
            $roleNames[] = $role->item_name;
        }

        return $roleNames;
    }

    /**
     * @return bool
     */
    public function isSuperAdmin()
    {
        return in_array(WebUser::ROLE_SUPERADMIN, $this->getRolesNamesList());
    }

    /**
     * Проверка что ключ не пуст и его время действия не истекло
     * @param $key
     * @return bool
     */
    public static function isSecretKeyExpire($key){
        if(empty($key)){return false;}
        $expire = Yii::$app->params['secretKeyExpire'];
        $parts = explode('_',$key);
        $timestamp = (int)end($parts);
        return $timestamp+$expire >= time();
    }

    /**
     * Возвращает объект пользователя с данным секретным ключом
     * @param $key
     * @return null|static
     */
    public static function findBySecretKey($key){
        if(!static::isSecretKeyExpire($key)){return null;}
        return self::findOne(['secret_key'=>$key]);
    }

    /**
     * Генерация секретного ключа
     */
    public function generateSecretKey(){
        $this->secret_key = Yii::$app->security->generateRandomString().'_'.time();
    }

    /**
     * Сброс секретного ключа
     */
    public function removeSecretKey(){
        $this->secret_key = null;
    }

    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['user_id' => 'id']);
    }

    public static function unsubscribe($ignore_mail_list, $mail)
    {
        $mail = mb_strtolower($mail);
        $sql = "UPDATE opt_users SET ignore_mail_list='{$ignore_mail_list}' WHERE LOWER(email)='{$mail}'";
        Yii::$app->db->createCommand($sql)->execute();
    }


}
