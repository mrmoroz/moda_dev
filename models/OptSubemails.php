<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "opt_subemails".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $create_dt
 */
class OptSubemails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'opt_subemails';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['create_dt'], 'safe'],
            [['name', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'create_dt' => 'Create Dt',
        ];
    }
}
