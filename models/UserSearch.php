<?php
/**
 * Created by Andrey Shabanov
 * Date: 20.02.2015
 * Time: 15:39
 */

namespace app\models;

use app\components\rbac\AuthAssignment;
use app\components\WebUser;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class UserSearch extends User
{
    public $first_name;
    public $last_name;
    public $second_name;
    public $role;
    public $mobile_phone;
    public $phone_code;
    public $phone;
    public $discount_user;
    public $bust;
    public $waist;
    public $hip;
    public $grow;
    public $address;
    public $user_address;
    public $fio;
    public $dt_ot_reg;
    public $dt_do_reg;
    public $dt_ord_ot,$dt_ord_do;

    public function rules()
    {
        return [
            [['id','dt_ord_ot','dt_ord_do','dt_ot_reg','dt_do_reg','user_address','fio','login', 'last_name', 'first_name', 'second_name', 'mobile_phone', 'role','phone_code','phone','discount_user','bust','waist','hip','grow','address','bdate','ignore_mail_list'], 'safe']
        ];
    }

    public function search($params)
    {
        $query = self::find()
            ->select('users.*')
            ->addSelect('user_address.address AS user_address')
            ->addSelect('user_profile.last_name AS last_name')
            ->addSelect('user_profile.first_name AS first_name')
            ->addSelect('user_profile.second_name AS second_name')
            ->joinWith(['roles', 'profile'])
            ->leftJoin('user_address', 'user_address.user_id = users.id AND user_address.default = true');
        $query->andFilterWhere(['=', AuthAssignment::tableName() . '.item_name', 'User']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ]
        ]);

        /*$dataProvider->sort->attributes['last_name'] = [
            'asc' => [UserProfile::tableName() . '.last_name' => SORT_ASC],
            'desc' => [UserProfile::tableName() . '.last_name' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['first_name'] = [
            'asc' => [UserProfile::tableName() . '.first_name' => SORT_ASC],
            'desc' => [UserProfile::tableName() . '.first_name' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['second_name'] = [
            'asc' => [UserProfile::tableName() . '.second_name' => SORT_ASC],
            'desc' => [UserProfile::tableName() . '.second_name' => SORT_DESC],
        ];*/

        if (!($this->load($params) && $this->validate())) {

            return $dataProvider;
        }

        if(!empty($this->address)){
            $id_users_arr = \Yii::$app->db->createCommand("SELECT DISTINCT user_id FROM user_address WHERE LOWER(user_address.address) LIKE '%".mb_strtolower($this->address)."%'")->queryAll();
            $ids = ArrayHelper::getColumn($id_users_arr,'user_id');
            $query->andFilterWhere(['IN','users.id',$ids]);
        }

        if(!empty($this->dt_ot_reg)){
            $query->andFilterWhere(['>=', 'users.created_at', $this->dt_ot_reg]);
        }
        if(!empty($this->dt_do_reg)){
            $query->andFilterWhere(['<=', 'users.created_at', $this->dt_do_reg]);
        }

        if(!empty($this->dt_ord_ot)){
            $query->andFilterWhere(['>=', 'users.last_order_date', $this->dt_ord_ot]);
        }
        if(!empty($this->dt_ord_do)){
            $query->andFilterWhere(['<=', 'users.last_order_date', $this->dt_ord_do]);
        }
        //user searching
        $query
            ->andFilterWhere(['=', 'users.id', $this->id])
            ->andFilterWhere(['=', 'users.ignore_mail_list', $this->ignore_mail_list])
            ->andFilterWhere(['=', 'users.bdate', $this->bdate])
            ->andFilterWhere(['like', 'LOWER(users.login)', mb_strtolower(trim($this->login))])
            //->andFilterWhere(['like', 'users.created_at', $this->created_at])
            ->andFilterWhere(['=', 'users.discount_user', $this->discount_user])
            ->andFilterWhere(['=', AuthAssignment::tableName() . '.item_name', $this->role])
            ->andFilterWhere(['=', UserProfile::tableName() . '.phone_code', $this->phone_code])
            ->andFilterWhere(['like', 'LOWER('.UserProfile::tableName() . '.last_name)', mb_strtolower(trim($this->last_name))])
            ->andFilterWhere(['like', 'LOWER('.UserProfile::tableName() . '.first_name)', mb_strtolower(trim($this->first_name))])
            ->andFilterWhere(['like', 'LOWER('.UserProfile::tableName() . '.second_name)', mb_strtolower(trim($this->second_name))])
            ->andFilterWhere(['=', UserProfile::tableName() . '.bust', $this->bust])
            ->andFilterWhere(['=', UserProfile::tableName() . '.waist', $this->waist])
            ->andFilterWhere(['=', UserProfile::tableName() . '.hip', $this->hip])
            ->andFilterWhere(['=', UserProfile::tableName() . '.grow', $this->grow])
            ->andFilterWhere(['like', "regexp_replace(".UserProfile::tableName() . ".mobile_phone,'[^0-9]','','g')", preg_replace('/[^0-9]/', '${1}', $this->mobile_phone)])
            ->andFilterWhere(['like', "regexp_replace(".UserProfile::tableName() . ".phone,'[^0-9]','','g')", preg_replace('/[^0-9]/', '${1}', $this->phone)]);

        return $dataProvider;
    }

    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'last_name' => ' Фамилия',
                'first_name' => ' Имя',
                'second_name' => ' Отчество',
                'bdate'=>'Дата рождения',
                'comment'=>'Комментарий',
                'last_order_date'=>'Последний заказ',
                'ignore_mail_list'=>'Отписан от рассылки',
                'user_address'=>'Адрес',
                'status'=>'Видимость'
            ]
        );
    }
}