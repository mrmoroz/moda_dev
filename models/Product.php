<?php

namespace app\models;

use app\helpers\RtHelper;
use app\modules\cart\models\Cart;
use app\modules\cart\models\CartAddingLog;
use app\modules\cart\models\Orders;
use Yii;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "products".
 *
 * @property integer $id
 * @property string $article
 * @property string $title
 * @property string $menu_title
 * @property integer $category_id
 * @property integer $discount
 * @property integer $price
 * @property integer $discount_price
 * @property integer $position
 * @property integer $url
 * @property string $admin_comment
 * @property boolean $is_active
 * @property boolean $is_wholesale_active
 * @property boolean $is_big_size
 * @property boolean $is_std_size
 * @property boolean $is_archive
 * @property string $video
 * @property string $description
 * @property string $additional_description_retail
 * @property string $color_size_note
 * @property string $composition
 * @property string $model_growth
 * @property string $model_size
 * @property string $manufacturer
 * @property string $manufacturer_robots
 * @property string $recommends
 * @property integer $wholesale_discount
 * @property integer $wholesale_price
 * @property integer $wholesale_discount_price
 * @property string $create_info
 * @property integer $creator_id
 * @property string $created_at
 * @property string $length
 * @property string $sync_info
 * @property integer $rating
 * @property integer $tmp_rating
 * @property integer $second_rating
 * @property integer $second_tmp_rating
 * @property integer $additional_rating_parameter
 * @property string $additional_rating_parameter_date_change
 * @property ProductCategory $category
 * @property ProductGallery[] $gallery
 */
class Product extends \yii\db\ActiveRecord
{
    private $recommendProducts = [];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'article', 'price'], 'required'],
            ['discount', 'number', 'max' => 100],
            [['category_id', 'discount', 'price', 'discount_price', 'position',
              'wholesale_discount_price', 'wholesale_price', 'wholesale_discount','visible_nsk', 'rating',
                'tmp_rating', 'second_rating', 'second_tmp_rating', 'additional_rating_parameter', 'creator_id'], 'integer'],
            [['is_active', 'is_big_size', 'is_std_size', 'is_archive', 'is_wholesale_active'], 'boolean'],
            [['video', 'description', 'additional_description_retail', 'color_size_note', 'composition', 'model_growth',
              'model_size', 'manufacturer', 'manufacturer_robots', 'recommends', 'created_at',
              'length', 'admin_comment'], 'string'],
            [['sync_info', 'additional_rating_parameter_date_change'], 'string'],
            [['article', 'title', 'menu_title'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true,
                'targetClass' => ProductCategory::className(),
                'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'article' => 'Артикул',
            'title' => 'Название',
            'menu_title' => 'Название для меню',
            'category_id' => 'Категория',
            'discount' => 'Скидка',
            'price' => 'Цена',
            'discount_price' => 'Цена со скидкой',
            'position' => 'Позиция',
            'is_active' => 'Отображать на сайте',
            'is_wholesale_active' => 'Отображать на МодаОптом',
            'is_std_size' => 'Отображать в базовых размерах',
            'is_big_size' => 'Отображать в больших размерах',
            'is_archive' => 'В архиве',
            'video' => 'Код видео',
            'description' => 'Описание',
            'additional_description_retail' => 'Дополнительное описание для розницы',
            'color_size_note' => 'Примечание по цвету и рисунку',
            'composition' => 'Состав',
            'model_growth' => 'Рост модели',
            'model_size' => 'Размер модели',
            'manufacturer' => 'Производитель',
            'manufacturer_robots' => 'Производитель для роботов',
            'recommends' => 'Рекоммендации',
            'wholesale_discount' => 'Оптовая скидка',
            'wholesale_price' => 'Оптовая цена',
            'wholesale_discount_price' => 'Оптовая цена со скидкой',
            'length' => 'Длина',
            'visible_nsk' => 'Видимость для Новосибирска',
            'additional_rating_parameter' => 'Дополнительный параметр N7'
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        if (empty($this->url)) {
            $this->url = strtolower(RtHelper::urlToTranslit($this->title)) . '-' . $this->id;
            self::save(false);
        }
        parent::afterSave($insert, $changedAttributes);
    }

    public function getCategorySettings($categoryId)
    {
        $query = new Query();

        return $query->addSelect(['par.id', 'par.name', 'par.type', 'par.source'])
            ->from(['product_category_setting cat_set', 'prods_cats_params par', 'product_categories cat'])
            ->where('cat_set.cat_id = cat.id')
            ->andWhere('par.id = cat_set.param_id')
            ->andWhere(['par.visible' => true])
            ->andWhere(['cat.id' => $categoryId])
            ->groupBy('par.id')
            ->orderBy([new Expression('par.position IS NULL ASC, par.position ASC')])
            ->all();
    }

    public function getGift()
    {
        $giftArticle = ProdsCatsParamValue::find()->select('value')->where(['param_id' => '21'])
                                          ->andWhere(['prod_id' => $this->id])->one();

        if (!empty($giftArticle->value)) {
            return Product::find()->where(['article' => $giftArticle->value])->orderBy('id DESC')->one();
        } else {
            return null;
        }
    }

    /**
     * Возвращает товары для блока "Сейчас покупают"
     * @return array
     */
    public static function getBuyNowProducts($needleTag)
    {
        return self::find()->with('category')
            ->leftJoin(ProdsCatsParamValue::tableName(), "prods_cats_params_values.prod_id = products.id")
            ->where([ProdsCatsParamValue::tableName() . '.value' => $needleTag])
            ->andWhere(Product::tableName() . '.id=' . ProdsCatsParamValue::tableName() . '.prod_id')
            ->andWhere([ProdsCatsParamValue::tableName() . ".param_id" => 9])
            ->andWhere(['prods_cats_params_values.param_id' => 9])
            ->andWhere(Product::tableName(). '.id=' . ProdsCatsParamValue::tableName() . '.prod_id')
            ->andWhere(['products.is_archive' => false])
            ->andWhere(['products.is_active' => true])
            ->joinWith(['category' => function($query) {
                return $query->where(['product_categories.is_active' => true]);
            }])
            ->orderBy(new Expression('random()'))
            ->limit(4)
            ->all();
    }

    public static function getProductName($id)
    {
        return Product::findOne($id)->title;
    }

    public static function getProductArticle($id)
    {
        return Product::findOne($id)->article;
    }

    public function getQuickAnswerForm()
    {
        return MessageForm::find()->where('entity_id IS null')->andWhere(['entity_type' => 'quickAnswer'])
            ->with('formSettings')->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ProductCategory::className(), ['id' => 'category_id'])->inverseOf('products');
    }

    public function getClothType()
    {
        return ClothType::find()
               ->select('name')
               ->from([ProdsCatsParamValue::tableName(), Product::tableName(), ClothType::tableName()])
               ->where([ProdsCatsParamValue::tableName() . '.prod_id' => $this->id])
               ->andWhere([ProdsCatsParamValue::tableName() . '.param_id' => 26])
               ->andWhere(ClothType::tableName() . '.id=' . ProdsCatsParamValue::tableName() . '.value::integer')
               ->one();
    }

    public function getCompos(){
        return ProdsCatsParamValue::find()->select('value')->where(['param_id'=>8,'prod_id'=>$this->id])->one();
    }

    public function getLen(){
        return ProdsCatsParamValue::find()->select('value')->where(['param_id'=>19,'prod_id'=>$this->id])->one();
    }

    public function getPsize(){
        return ProdsCatsParamValue::find()->select('value')->where(['param_id'=>24,'prod_id'=>$this->id])->one();
    }

    public function getGrowth(){
        return ProdsCatsParamValue::find()->select('value')->where(['param_id'=>18,'prod_id'=>$this->id])->one();
    }

    public function getTagsIds(){
        return ArrayHelper::getColumn(ProdsCatsParamValue::find()->select('value')->where(['param_id'=>9,'prod_id'=>$this->id])->all(),'value');
    }



    public function getClothTypeName()
    {
        $clothType = $this->getClothType();

        if ($clothType) {
            return $clothType->name;
        }

        return null;
    }

    public function getManufacturer()
    {
        /*return Manufacturer::find()
            ->select('*')
            ->from([ProdsCatsParamValue::tableName(), Product::tableName(), Manufacturer::tableName()])
            ->where([ProdsCatsParamValue::tableName() . '.prod_id' => $this->id])
            ->andWhere([ProdsCatsParamValue::tableName() . '.param_id' => 5])
            ->andWhere(Manufacturer::tableName() . '.id=' . ProdsCatsParamValue::tableName() . '.value::integer')
            ->one();*/
        return Manufacturer::findBySql("SELECT * FROM manufacturer WHERE id IN (
            SELECT value::integer FROM prods_cats_params_values WHERE prod_id = '".$this->id."' AND param_id = '5'
        )")->one();
    }

    public function getManufacturerId()
    {
        return $this->hasOne(ProdsCatsParamValue::className(), ['prod_id' => 'id'])->where(['param_id' => 5]);
    }

    public function getManufacturerName()
    {
        $manufacturer = $this->getManufacturer();

        if ($manufacturer) {
            return $manufacturer->name;
        }

        return null;
    }

    public function getGallery(int $limit = null)
    {
        $query = $this
            ->hasMany(ProductGallery::className(), ['product_id' => 'id'])
            ->inverseOf('product')
            ->orderBy([
                'position' => SORT_ASC,
                'id' => SORT_ASC,
            ]);
        if ($limit) {
            $query->limit($limit);
        }
        return $query;
    }

    public function getRecommendProducts(int $limit = null)
    {
        if($this->recommends && empty($this->recommendProducts)){
            $recommendArticles = explode(',', $this->recommends);
            foreach ($recommendArticles as $recommendArticle){
                $recommendProd = Product::find()->where('article=:p_article AND is_active=true', ['p_article'=>trim($recommendArticle)])
                                        ->andWhere(['!=', 'is_archive', true])
                                        ->orderBy('id DESC')->one();
                if($recommendProd){
                    $this->recommendProducts[] = $recommendProd;
                }
            }
        }
        if($limit){
            return array_slice($this->recommendProducts, 0, $limit);
        } else {
            return $this->recommendProducts;
        }
    }

    public function getCreator()
    {
        return UserProfile::findOneByUserId($this->creator_id);
    }

    public function getStockPositions()
    {
        $stockPositions = StockPosition::find()->where(['product_id' => $this->id])->all();
        $stocks = array();

        foreach ($stockPositions as $stockPosition) {
            $stockId = $stockPosition->stock_id;
            $colorId = $stockPosition->color_id;
            $sizeId = $stockPosition->size_id;
            $quantity = $stockPosition->quantity;

            if (!isset($stocks[$stockId])) {
                $stocks[$stockId] = array();
                $stocks[$stockId]['name'] = Stock::findOne($stockId)->name;
                $stocks[$stockId]['id'] = $stockId;
            }

            if (!isset($stocks[$stockId][$colorId])) {
                $stocks[$stockId][$colorId] = array();
            }

            $stocks[$stockId][$colorId][$sizeId] = $quantity;
        }

        return $stocks;
    }

    /**
     * @return array
     * Возвращает массив цветов-размеров на нашем складе
     */
    public function getOurStockPositions()
    {
        $stockPositions = StockPosition::find()->leftJoin(Stock::tableName(), 'stocks.id = stock_id')
                                       ->where(['product_id' => $this->id, 'stocks.is_manufacturer' => false])->all();
        $stocks = array();

        foreach ($stockPositions as $stockPosition) {
            $stockId = $stockPosition->stock_id;
            $colorId = $stockPosition->color_id;
            $sizeId = $stockPosition->size_id;

            if (!isset($stocks[$stockId])) {
                $stocks[$stockId] = array();
            }

            if (!isset($stocks[$stockId][$colorId])) {
                $stocks[$stockId][$colorId] = array();
            }

            $stocks[$stockId][$colorId][] = $sizeId;
        }

        return $stocks;
    }

    /**
     * Возвращает логи по размерам
     */
    public function getSizeChangelog()
    {
        return $this->hasMany(ProductSizeChangelog::className(), ['product_id' => 'id']);
    }

    /**
     * Возвращает логи по товару
     */
    public function getChangelog()
    {
        return $this->hasMany(ProductChangelog::className(), ['product_id' => 'id']);
    }

    public function getLastUpdateLog()
    {
        return $this->hasOne(ProductChangelog::className(), ['product_id' => 'id'])
                    ->where(['type' => ProductChangelog::EDIT_PRODUCT_INFO])
                    ->orderBy('operation_date DESC')
                    ->limit(1);
    }

    /**
     *
     */
    public function getFirstImage()
    {
        if (!empty($this->gallery)) {
            $image = $this->gallery[0];
            if ($image) {
                return $image->path;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public function getSizeChart()
    {
        if (!empty($this->category->size_chart_id)) {
            return $this->category->size_chart_id;
        } elseif (!empty($this->category->parent->size_chart_id)) {
            return $this->category->parent->size_chart_id;
        }

        return null;
    }

    public function getSizes()
    {
        $sizes = array();
        $sizeChart = SizeChart::findOne($this->getSizeChart());
        if ($sizeChart != null) {
            $productSizes = ArrayHelper::map($sizeChart->getSizes()->all(), 'id', 'name');
            if (!empty($productSizes)) {
                foreach ($productSizes as $id => $size) {
                    $resultSize['id'] = $id;
                    $resultSize['name'] = $size;
                    array_push($sizes, $resultSize);
                    unset($resultSize);
                }
            }
        }

        return $sizes;
    }

    // все размеры в наличии
    public function getAvailableSizes()
    {
        return Size::find()->select("sizes.name")->from(StockPosition::tableName())
                           ->innerJoin(Size::tableName(), "stock_position.size_id = sizes.id")
                           ->where(['stock_position.product_id' => $this->id])->all();
    }

    /**
     * Доступные размеру у поставщика
     * @return array
     */
    public function getManufacturerAvailableSizes()
    {
        return Size::find()->select("sizes.name")->from(StockPosition::tableName())
            ->innerJoin(Size::tableName(), "stock_position.size_id = sizes.id")
            ->where(['stock_position.product_id' => $this->id])
            ->andWhere('stock_position.stock_id = 1')
            ->orderBy('name')
            ->all();
    }


    /**
     * Доступные размеры у нас
     * @return array
     */
    public function getOurAvailableSizes()
    {
        return Size::find()->select("sizes.name")->from(StockPosition::tableName())
            ->innerJoin(Size::tableName(), "stock_position.size_id = sizes.id")
            ->where(['stock_position.product_id' => $this->id])
            ->andWhere('stock_position.stock_id != 1')
            ->orderBy('name')
            ->all();
    }

    public function getColors()
    {
        return ArrayHelper::getColumn(StockPosition::find()->select('color_id')
                                                           ->where(['product_id' => $this->id])
                                                           ->distinct('color_id')
                                                           ->all(), 'color_id');

    }

    public function getUrl()
    {
        if($this->category) {
            return $this->category->getUrl() . '/' . $this->url;
        }
        return $this->url;
    }

    /**
     * Проверка наличия метки "Новинки размеров 40+"
     * @return bool
     */
    public function isNew()
    {
        $value = ProdsCatsParamValue::find()->where(['prod_id' => $this->id])
                                            ->andWhere(['param_id' => 9])
                                            ->andWhere(['value' => 1])
                                            ->one();

        if (!empty($value) || $this->is_new) {
            return true;
        }

        return false;
    }

    /**
     * Проверка наличия метки "Новинки размеров 52+"
     * @return bool
     */
    public function isBigNew()
    {
        $value = ProdsCatsParamValue::find()->where(['prod_id' => $this->id])
                                            ->andWhere(['param_id' => 9])
                                            ->andWhere(['value' => 12])
                                            ->one();;

        if (!empty($value)) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     * Проверка наличия метки "Хит"
     */
    public function isHit()
    {
        $value = ProdsCatsParamValue::find()->where(['prod_id' => $this->id])
            ->andWhere(['param_id' => 9])
            ->andWhere(['value' => 35])
            ->one();;

        if (!empty($value)) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isComing()
    {
        $value = ProdsCatsParamValue::find()->where(['prod_id' => $this->id])
            ->andWhere(['param_id' => 9])
            ->andWhere(['value' => 3])
            ->one();;

        if (!empty($value)) {
            return true;
        }

        return false;
    }

    public function getPrice($price,$kurs){
        if($kurs){
            return ceil($price*$kurs);
        }
        return $price;
    }

    public function getSymbol($country=NULL){
        if($country){
            switch ($country){
                case 'KZ': return "т.";
                default: return "p.";
            }
        }
        return "p.";
    }

    /**
     * Возвращает выборку товаров по альтернативной сортировке
     * @param ActiveQuery $query
     * @param $offset
     * @param $limit
     * @param $className
     * @param $entityId
     * @param bool $searchWithFilters
     * @return array
     */
    public static function getAlternateSortedProducts(ActiveQuery $query, $offset, $limit, $className, $entityId, $searchWithFilters = false)
    {
        $start = microtime(true);

        $sortedProductsCount = 0;

        // Выбор размеров
        if (Yii::$app->request->get('sizeType') == 'bolshie-razmery') {
            $sizeType = 'big-sizes';
        } else {
            $sizeType = 'std-sizes';
        }

        // Добавление новых товаров
        if ($className == 'category' && $offset == 0) {
            $category = ProductCategory::findOne($entityId);
            if ($sizeType == 'big-sizes') {
                $newProductsCount = $category->big_new_products_count;
            } else {
                $newProductsCount = $category->standard_new_products_count;
            }

            if ($newProductsCount > 0 ) {
                $newProductsQuery = clone $query;
                $newProducts = $newProductsQuery->andWhere(['>=', 'additional_rating_parameter', 0])
                                     ->orderBy('id DESC')->limit($newProductsCount)->all();
                $limit -= $newProductsCount;
            }

            foreach ($newProducts as $newProduct) {
                $cachedProducts[] = [
                    'entity_class' => $className,
                    'entity_id' => $entityId,
                    'product_id' => $newProduct->id,
                    'position' => $sortedProductsCount,
                    'size_type' => $sizeType
                ];
                $sortedProductsCount += 1;
            }
        }

        if (ProductSelection::checkProducts($className, $entityId, $sizeType) > 0 && !$searchWithFilters) {
            $resultProducts = ProductSelection::getProducts($className, $entityId, $offset, $limit, $sizeType);
        } else {
            $products = $query->orderBy('rating DESC')->with('manufacturerId')->all();

            $resultProductsCount = 0;

            $resultProducts = array();
            $cachedProducts = array();

            $slotQuantities = Manufacturer::getSlotQuantity();

            foreach ($products as $productNumber => $product) {
                if (!key_exists($productNumber, $products) || !isset($product->manufacturerId)) {
                    continue;
                }

                $sortedProductsCount += 1;
                if ($sortedProductsCount > $offset && $resultProductsCount < $limit) {
                    $resultProducts[] = $product;
                    $resultProductsCount += 1;
                }

                $slotQuantity = $slotQuantities[$product->manufacturerId->value];
                $currentManufacturerId = $product->manufacturerId->value;
                $currentSlotCount = 1;

                if (!$searchWithFilters) {
                    $cachedProducts[] = [
                        'entity_class' => $className,
                        'entity_id' => $entityId,
                        'product_id' => $product->id,
                        'position' => $sortedProductsCount,
                        'size_type' => $sizeType,
                        'is_wholesale' => true
                    ];
                }

                unset($products[$productNumber]);

                foreach ($products as $manufacturerProductCount => $manufacturerProduct) {
                    if (!key_exists($manufacturerProductCount, $products)) {
                        continue;
                    }

                    if ($currentSlotCount >= $slotQuantity) {
                        break;
                    }

                    if (isset($manufacturerProduct->manufacturerId) && $manufacturerProduct->manufacturerId->value == $currentManufacturerId) {
                        $currentSlotCount += 1;
                        $sortedProductsCount += 1;
                        if ($sortedProductsCount > $offset && $resultProductsCount < $limit) {
                            $resultProducts[] = $manufacturerProduct;
                            $resultProductsCount += 1;
                        }

                        if (!$searchWithFilters) {
                            $cachedProducts[] = [
                                'entity_class' => $className,
                                'entity_id' => $entityId,
                                'product_id' => $manufacturerProduct->id,
                                'position' => $sortedProductsCount,
                                'size_type' => $sizeType,
                                'is_wholesale' => true
                            ];
                        }

                        unset($products[$manufacturerProductCount]);
                    }
                }
            }


            // сохраняем выборку в базу
            if (!$searchWithFilters) {
                Yii::$app->db->createCommand()->batchInsert(
                    ProductSelection::tableName(),
                    ['entity_class', 'entity_id', 'product_id', 'position', 'size_type', 'is_wholesale'],
                    $cachedProducts)->execute();
            }
        }

        if (!empty($newProducts)) {
            $resultProducts = ArrayHelper::merge($newProducts, $resultProducts);
        }

        $scriptTime = str_replace('.', ',', (microtime(true) - $start));

        //mail('sibrose10@gmail.com', 'time', $scriptTime);

        return $resultProducts;
    }

    public function getFavouritesCount()
    {
        return UserFavouritesAddingLog::find()->where(['product_id' => $this->id])->count();
    }

    /**
     * @param $productCategories
     * @return array
     * Возвращает список родительских товаров из указанной категории
     */
    public static function getParentIds($productCategories)
    {
        return ArrayHelper::getColumn(Product::findBySql("SELECT parent_product_id FROM products
                                            WHERE category_id {$productCategories}
                                            AND parent_product_id IS NOT NULL")->asArray()->all(), 'parent_product_id');
    }

    /**
     * @param $categoriesAdd
     * Возвращает количество сочетаний размеров/цветов на складе
     *
     * @return array
     */
    public static function getSizesQuantity($categoriesAdd, $productId)
    {
        $productIdAdd = '';

        if ($productId) {
            $productIdAdd = 'AND P.id = ' . $productId;
        }

        return  ArrayHelper::map(self::findBySql("SELECT P.id, COUNT(SP.product_id) AS qty
                                                 FROM products P
                                                 JOIN stock_position SP ON SP.product_id = P.id
                                                 WHERE P.category_id {$categoriesAdd}
                                                 {$productIdAdd}
                                                 GROUP BY P.id")->asArray()->all(), 'id', 'qty');
    }

    /**
     * @param $productCategories
     * @param null $productId
     * @return array
     *
     * Возвращает
     */
    public static function getProductsForRating($productCategories, $productId = null)
    {
        $productIdAdd = '';

        $categoriesAdd = 'IN (';
        if ($productId) {
            $categoriesAdd .= Product::getProductCategoryById($productId);
        } else {
            foreach ($productCategories as $productCategory) {
                $categoriesAdd .= $productCategory . ',';
            }
        }

        $categoriesAdd = rtrim($categoriesAdd, ',') . ') ';

        if ($productId) {
            $productIdAdd = 'AND P.id = ' . $productId;
        }

        $time = strtotime("-10 days", time());
        $additionalParameterDeleteDate = date("Y-m-d", $time);

        $D2 = strtotime(date('Y-m-d'));

        $formulas = Yii::$app->categoryService->getRatingFormulas();
        $A = 7;
        if (!empty($formulas['$A'])) {
            eval($formulas['$A']);
        }
        $borderDate = date('Y-m-d', $D2 - $A * 24 * 60 * 60);

        $resultProducts = [];

        $products = self::findBySql("SELECT P.id, P.article, P.created_at, P.additional_rating_parameter, 
                                     P.additional_rating_parameter_date_change, M.rating AS manufacturer_rating
                                     FROM products P
                                     JOIN prods_cats_params_values PV
                                     ON PV.prod_id = P.id
                                     JOIN manufacturer M
                                     ON M.id::varchar = PV.value
                                     WHERE P.is_active = TRUE
                                     AND P.is_archive = FALSE
                                     AND P.category_id {$categoriesAdd}
                                     {$productIdAdd}")
                        ->asArray()
                        ->all();

        $ordersDates = Orders::getOrdersDatesForRatingByProductCategory($categoriesAdd, $productId);

        $orderQuantityBefore = Orders::getProductOrdersCount($categoriesAdd, $borderDate, true, $productId);
        $orderQuantityAfter = Orders::getProductOrdersCount($categoriesAdd, $borderDate, false, $productId);
        $addingToCartQuantity = CartAddingLog::getProductCartAdding($categoriesAdd, null, true, $productId);
        // Данные по корзине для N13
        $addingCartOldQuantity = CartAddingLog::getProductCartAdding($categoriesAdd, '2017-04-27', true, $productId);
        $afterAprilOrders = Orders::getProductOrdersCount($categoriesAdd, '2017-04-28', false, $productId);
        $addingFavouritesQuantity = UserFavouritesAddingLog::getProductFavouritesAdding($categoriesAdd, $productId);
        $sizesQuantity = self::getSizesQuantity($categoriesAdd, $productId);

        foreach ($products as $product) {
            $resultProduct['article'] = $product['article'];
            $resultProduct['D'] = 0;
            $resultProduct['K1'] = 0;
            $resultProduct['K2'] = 0;
            $resultProduct['K3'] = 0;
            $resultProduct['K31'] = 0;
            $resultProduct['K4'] = 0;
            $resultProduct['N5'] = 0;

            $D = 0;
            $N6 = 0;

            /** @var Product $product */
            if (isset($ordersDates[$product['id']]) && strtotime($ordersDates[$product['id']]) < strtotime($product['created_at'])) {
                $D1 = strtotime($ordersDates[$product['id']]);
            } else {
                $D1 = strtotime($product['created_at']);
            }

            eval($formulas['$D']);

            $resultProduct['D'] = $D;
            if (isset($orderQuantityBefore[$product['id']])) {
                $resultProduct['K1'] = $orderQuantityBefore[$product['id']];
            }
            if (isset($orderQuantityAfter[$product['id']])) {
                $resultProduct['K2'] = $orderQuantityAfter[$product['id']];
            }
            if (isset($addingToCartQuantity[$product['id']])) {
                $resultProduct['K3'] = $addingToCartQuantity[$product['id']];
            }
            if (isset($afterAprilOrders[$product['id']])) {
                $resultProduct['K3'] -= $afterAprilOrders[$product['id']];
            }
            if (isset($addingCartOldQuantity[$product['id']])) {
                $resultProduct['K31'] = $addingCartOldQuantity[$product['id']];
            }
            if (isset($addingFavouritesQuantity[$product['id']])) {
                $resultProduct['K4'] = $addingFavouritesQuantity[$product['id']];
            }
            $resultProduct['N5'] = $product['manufacturer_rating'];

            eval($formulas['$N6']);
            $resultProduct['N6'] = $N6;

            if (isset($sizesQuantity[$product['id']])) {
                switch ($sizesQuantity[$product['id']]) {
                    case 1:
                        $resultProduct['S'] = 10;
                        break;
                    case 2:
                        $resultProduct['S'] = 5;
                        break;
                    case 3:
                        $resultProduct['S'] = 1.5;
                        break;
                    default:
                        $resultProduct['S'] = 1;
                        break;
                }
            } else {
                continue;
            }

            $resultProduct['N7'] = $product['additional_rating_parameter'];
            if ($resultProduct['N7'] > 0 && strtotime($product['additional_rating_parameter_date_change']) < strtotime($additionalParameterDeleteDate)) {
                $resultProduct['N7'] = 0;
                Product::updateAll(['additional_rating_parameter' => 0, 'additional_rating_parameter_date_change' => null], ['id' => $product['id']]);
            }

            if (empty($resultProduct['N7'])) {
                $resultProduct['N7'] = 0;
            }

            $resultProducts[$product['id']] = $resultProduct;
        }

        return $resultProducts;
    }

    /**
     * Перерасчитывает рейтинг у заданных товаров
     * @param $products
     * @param bool $isGetLog
     */
    public static function changeRating($products, $isGetLog = false)
    {
        $formulas = \Yii::$app->categoryService->getRatingFormulas();
        $totalProductsRating = [];

        $P1 = 0;
        $P2 = 0;
        $P3 = 0;
        $P31 = 0;
        $P4 = 0;
        eval($formulas['$P1']);
        eval($formulas['$P2']);
        eval($formulas['$P3']);
        eval($formulas['$P31']);
        eval($formulas['$P4']);

        foreach ($products as $key => $product) {
            $D = $product['D']; // D=D2-D1+1
            $K1 = $product['K1']; // количество заказов с датой D0 старше значения А
            $K2 = $product['K2']; // количество заказов с датой D0 младше значения А
            $K3 = $product['K3']; // количество добавлений товара в корзину
            $K31 = $product['K31']; // количество добавлений товара в корзину
            $K4 = $product['K4']; // количество добавлений товара в избранное

            $N1 = 0; // взвешенное количество старых заказов
            $N2 = 0; // взвешенное число новых заказов
            $N3 = 0; // взвешенное количество добавлений в корзину
            $N31 = 0; // взвешенное количество добавлений в корзину
            $N4 = 0; // взвешенное количество добавлений в избранное
            $N5 = $product['N5'];
            $N6 = $product['N6'];
            $N7 = $product['N7'];

            $S = $product['S'];

            $R = 0;


            eval($formulas['$N1']);
            eval($formulas['$N2']);
            eval($formulas['$N3']);
            eval($formulas['$N31']);
            eval($formulas['$N4']);
            eval($formulas['$R']);

            Product::updateAll(['tmp_rating' => $R], ['id' => $key]);

            if ($isGetLog) {
                $products[$key]['P1'] = $P1;
                $products[$key]['P2'] = $P2;
                $products[$key]['P3'] = $P3;
                $products[$key]['P31'] = $P31;
                $products[$key]['P4'] = $P4;
                $products[$key]['N1'] = $N1;
                $products[$key]['N2'] = $N2;
                $products[$key]['N3'] = $N3;
                $products[$key]['N4'] = $N4;
                $products[$key]['R'] = $R;
            }
        }

        if ($isGetLog) {
            return $products;
        }
    }

    /**
     * Возвращает категорию товара по ID товара
     */
    public static function getProductCategoryById($productId)
    {
        $product = Product::findOne($productId);

        return $product->category_id;
    }

    /**
     * Перерасчет рейтинга для одного товара
     * @param $productId
     */
    public static function changeOneRating($productId)
    {
        $productInfo = self::getProductsForRating(null, $productId);
        self::changeRating($productInfo);
    }

    public function getDimensions()
    {
        return ProdsCatsParamValue::getParamValue($this->id, 16);
    }

    public function getPrev($img){
        if($img) {
            $tmpImg = explode('/', $img);
            if (in_array('pict1.loc', $tmpImg)) {
                $endPath = array_slice($tmpImg, -2);
                $img = 'http://pict1.loc/cached-images/' . $endPath[0] . '/' . $endPath[1];
                $urlHeaders = @get_headers($img);
                if (strpos($urlHeaders[0], '200') === false) {
                    //из облака
                    $img = 'https://s3.eu-west-2.amazonaws.com/pict1.beauti-full.ru/' . $endPath[0] . '/' . $endPath[1];
                    $urlHeaders2 = @get_headers($img);
                    if (strpos($urlHeaders2[0], '200') === false) {
                        $img = 'https://beauti-full.ru/display-images-cache/default/238x360_static_ffffff_0/default.jpg';
                    }
                }
            } else {
                if(in_array('beauti-full.ru', $tmpImg)){
                    $img = str_replace('documents','uploads', $img);
                }else{
                    $img = 'http://beautifull.loc'.str_replace('documents','uploads', $img);
                }
            }
        }else{
            $img = 'https://beauti-full.ru/display-images-cache/default/238x360_static_ffffff_0/default.jpg';
        }
        return $img;
    }

    public function getOrigin($img){
        if($img) {
            $img = str_replace('documents','uploads', $img);
            $tmpImg = explode('/', $img);
            if (in_array('pict1.loc', $tmpImg)) {
                $endPath = array_slice($tmpImg, -2);
                $img = 'http://pict1.loc/original/' . $endPath[0] . '/' . $endPath[1];
                $urlHeaders = @get_headers($img);
                if (strpos($urlHeaders[0], '200') === false) {
                    //из облака
                    $img = 'https://s3.eu-west-2.amazonaws.com/pict1.beauti-full.ru/' . $endPath[0] . '/' . $endPath[1];
                    $urlHeaders2 = @get_headers($img);
                    if (strpos($urlHeaders2[0], '200') === false) {
                        $img = 'https://beauti-full.ru/display-images-cache/default/434x655_static_ffffff_0/default.jpg';
                    }
                }
            }else{
                $img = str_replace('documents','uploads', $img);
            }
        }else{
            $img = 'https://beauti-full.ru/display-images-cache/default/434x655_static_ffffff_0/default.jpg';
        }
        return $img;
    }

    public static function clearProductCache($productId)
    {
        Yii::$app->cache->delete('product:' . $productId);
        Yii::$app->cache->delete('product-mini:' . $productId);
        Yii::$app->cache->delete('admin:product:' . $productId);
    }

    public static function getLabels($productsIds){
        $labels = [];
        $sql="select prods_cats_params_values.*, tags.label_text, tags.label_color, tags.url FROM prods_cats_params_values 
                LEFT JOIN tags ON tags.id = cast(prods_cats_params_values.value as int4)
                WHERE prod_id IN (".$productsIds.")
                AND tags.show_in_wholesale = true
                AND (param_id = '9')";
        $_arr_labels = Yii::$app->db->createCommand($sql)->queryAll();
        if(count($_arr_labels)>0){
            foreach ($_arr_labels as $it) {
                if(!empty($it['label_text']) && !empty($it['label_color'])){
                    $labels[$it['prod_id']][$it['value']] = $it;
                }
            }
        }
        //vd($labels);
        return $labels;
    }
}
