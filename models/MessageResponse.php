<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "message_response".
 *
 * @property integer $id
 * @property integer $message_id
 * @property string $mail
 * @property string $subject
 * @property string $body
 * @property string $date_send
 */
class MessageResponse extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message_response';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message_id'], 'integer'],
            [['mail', 'subject', 'body'], 'required'],
            [['mail', 'subject', 'body'], 'string'],
            [['date_send'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'message_id' => 'Номер сообщения',
            'mail' => 'E-mail получателя',
            'subject' => 'Тема',
            'body' => 'Сообщеие',
            'date_send' => 'Дата отправки',
        ];
    }
}
