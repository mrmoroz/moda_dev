<?php

namespace app\models;

use Yii;
use yii\base\Object;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "user_favourites".
 *
 * @property integer $id
 * @property string $favourites_token
 * @property integer $product_id
 * @property string $add_date
 */
class UserFavourite extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_favourites';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['favourites_token'], 'string'],
            [['product_id'], 'integer'],
            [['add_date'], 'safe'],
        ];
    }

    /**
     * Возвращает товары из избранного
     * @return array
     */
    public static function getFavouriteProducts()
    {
        if (Yii::$app->session->get('favourites') != null) {
            $favouriteProductsIds = Yii::$app->session->get('favourites');
        } else {
            $favouritesToken = $_COOKIE['favourites_token'];

            $favouriteProductsIds = UserFavourite::getFavouriteProductsIdsByFavouritesToken($favouritesToken);
            Yii::$app->session->set('favourites', $favouriteProductsIds);
        }

        return $favouriteProductsIds;
    }

    /**
     * Обновление избранного
     * @param $productId
     * @return object
     */
    public static function updateFavourite($productId)
    {
        $favouriteProducts = self::getFavouriteProducts();

        if (in_array($productId, $favouriteProducts)) {
            self::removeProductFromFavourite($productId);
            $result['status'] = 0;
            $result['count'] = count($favouriteProducts) - 1;
        } else {
            self::addProductToFavourite($productId);
            $result['status'] = 1;
            $result['count'] = count($favouriteProducts) + 1;
        }

        $favouritesToken = $_COOKIE['favourites_token'];
        setcookie("favourites_token", $favouritesToken, time() + 60 * 60 * 24 * 30 * 6, '/');

        return (object) $result;
    }

    /**
     * Возвращает количество товаров в избранном
     * @return int
     */
    public static function getQuantityFavourites()
    {
        return count(self::getFavouriteProducts());
    }

    /**
     * Удалить товар из избранного
     * @param int $productId
     * @return int
     */
    static function removeProductFromFavourite($productId)
    {
        $favouritesToken = $_COOKIE['favourites_token'];

        UserFavourite::deleteAll(['favourites_token' => $favouritesToken, 'product_id' => $productId]);
        $favouriteProductsIds = UserFavourite::getFavouriteProductsIdsByFavouritesToken($favouritesToken);
        Yii::$app->session->set('favourites', $favouriteProductsIds);

        return count($favouriteProductsIds);
    }


    /**
     * Добавить товар в избранное
     * @param int $productId
     * @return int
     */
    static function addProductToFavourite($productId)
    {
        $favouritesToken = $_COOKIE['favourites_token'];

        if (Yii::$app->user->isGuest) {
            $token = $favouritesToken;
        } else {
            $token = Yii::$app->user->id;
        }

        UserFavourite::deleteAll(['favourites_token' => $favouritesToken, 'product_id' => $productId]);
        $userFavourite = new UserFavourite();
        $userFavourite->favourites_token = $favouritesToken;
        $userFavourite->product_id = $productId;
        $userFavourite->add_date = date("Y-m-d H:i:s");
        $userFavourite->save();

        $favouriteProductsIds = UserFavourite::getFavouriteProductsIdsByFavouritesToken($favouritesToken);
        Yii::$app->session->set('favourites', $favouriteProductsIds);

        if (UserFavouritesAddingLog::checkProduct($productId, Yii::$app->user->isGuest, $token) == 0) {
            $favouritesAddingLog = new UserFavouritesAddingLog([
                'product_id' => $productId,
                'add_date' => date('Y-m-d H:i:s')
            ]);

            if (Yii::$app->user->isGuest) {
                $favouritesAddingLog->cart_token = $token;
            } else {
                $favouritesAddingLog->user_id = Yii::$app->user->id;
            }

            $favouritesAddingLog->save();
            Product::changeOneRating($productId);
        }

        return count($favouriteProductsIds);
    }

    /**
     *
     * Возвращает массив id избранных пользователем товаров
     * @param $favouritesToken
     * @return array
     */
    public static function getFavouriteProductsIdsByFavouritesToken($favouritesToken)
    {
        return ArrayHelper::getColumn(self::find()->select('product_id')
                                                  ->where(['favourites_token' => $favouritesToken])->all(), 'product_id');
    }
}
