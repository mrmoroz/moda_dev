<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "page_block".
 *
 * @property integer $id
 * @property integer $page_id
 * @property string $title
 * @property string $body
 * @property integer $position
 * @property integer $active
 * @property integer $has_gallery
 * @property integer $has_body
 */
class PageBlock extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page_block';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_id', 'position', 'has_gallery', 'has_body'], 'integer'],
            [['body'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['active'],'safe']
        ];
    }

    public function getGallery()
    {
        return $this->hasOne(Gallery::className(), ['entity_id' => 'id'])->where(['entity_class' => get_class($this)]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('interface','ID'),
            'page_id' => Yii::t('interface','Page ID'),
            'title' => Yii::t('interface','Title'),
            'body' => Yii::t('interface','Body'),
            'position' => Yii::t('interface','Position'),
            'active' => Yii::t('interface','Active'),
            'has_body' => Yii::t('interface','Наличие текстового блока'),
            'has_gallery' => Yii::t('interface','Наличие галереи'),
        ];
    }
}
