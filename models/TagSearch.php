<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Tags;
use yii\db\Expression;

/**
 * TagsSearch represents the model behind the search form about `app\models\Tags`.
 */
class TagSearch extends Tag
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'position'], 'integer'],
            [['name', 'menu_title', 'html_title', 'h1_page', 'top_text', 'bottom_text', 'text'], 'safe'],
            [['show_in_wholesale', 'show_in_retail', 'on_top', 'on_bottom', 'visible', 'sys_language', 'highlight_in_red'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tag::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'show_in_wholesale' => $this->show_in_wholesale,
            'show_in_retail' => $this->show_in_retail,
            'on_top' => $this->on_top,
            'on_bottom' => $this->on_bottom,
            'highlight_in_red' => $this->highlight_in_red,
            'position' => $this->position,
            'visible' => $this->visible,
            'sys_language' => $this->sys_language,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'menu_title', $this->menu_title])
            ->andFilterWhere(['like', 'html_title', $this->html_title])
            ->andFilterWhere(['like', 'h1_page', $this->h1_page])
            ->andFilterWhere(['like', 'top_text', $this->top_text])
            ->andFilterWhere(['like', 'bottom_text', $this->bottom_text])
            ->orderBy([new Expression('position IS NULL ASC, position ASC')]);

        return $dataProvider;
    }
}
