<?php

namespace app\models;

use app\helpers\RtHelper;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tags".
 *
 * @property integer $id
 * @property string $name
 * @property string $menu_title
 * @property string url
 * @property string $html_title
 * @property string $h1_page
 * @property string $top_text
 * @property string $bottom_text
 * @property boolean $highlight_in_red
 * @property boolean $show_in_wholesale
 * @property boolean $show_in_retail
 * @property boolean $on_top
 * @property boolean $on_bottom
 * @property integer $position
 * @property boolean $visible
 * @property boolean $sys_language
 * @property string $h1_title
 */
class Tag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tags';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['top_text', 'bottom_text', 'url', 'h1_title'], 'string'],
            [['show_in_wholesale', 'show_in_retail', 'on_top', 'on_bottom', 'visible', 'sys_language', 'highlight_in_red'], 'boolean'],
            [['position'], 'integer'],
            [['name', 'html_title', 'h1_page', 'menu_title', 'url'], 'string', 'max' => 100],
        ];
    }

    public function beforeSave($insert)
    {
        if(empty($this->url)){
            $this->url = strtolower(RtHelper::urlToTranslit($this->name));
        }

        return true;
    }

    public function getUrl()
    {
        $url = "/catalog/tag/" . $this->id;

        return $url;
    }

    public static function findOneByUrl($url) {
        return self::find()
            ->where(['url' => $url])
            ->one();
    }

    public static function listAll($keyField = 'id', $valueField = 'name', $asArray = true)
    {
        $query = static::find()->orderBy('name ASC');
        if ($asArray) {
            $query->select([$keyField, $valueField])->asArray();
        }

        return ArrayHelper::map($query->all(), $keyField, $valueField);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'menu_title' => 'Название для меню',
            'html_title' => 'Html Title',
            'url' => 'URL',
            'h1_page' => 'Заголовок',
            'top_text' => 'Вступительный текст',
            'bottom_text' => 'Текст внизу страницы',
            'highlight_in_red' => 'Выделять красным',
            'show_in_wholesale' => 'Опт',
            'show_in_retail' => 'Розница',
            'on_top' => 'Показывать над категориями (Модаоптом)',
            'on_bottom' => 'Показывать под категориями (Модаоптом)',
            'position' => 'Позиция',
            'visible' => 'Актив',
            'sys_language' => 'Sys Language',
        ];
    }

    /**
     * Возвращает наименьшую стоимость товара, привязанного к товарам выбранной метки
     */
    public function getMinProductPrice()
    {
        $prices = Product::findBySql("SELECT min(P.price) as price, min(P.discount_price) as discount_price FROM products P
                                      JOIN prods_cats_params_values PV ON PV.prod_id = P.id
                                      WHERE PV.param_id = 9
                                      AND P.is_active = TRUE
                                      AND P.is_archive = FALSE
                                      AND PV.value = {$this->id}::VARCHAR
                                      AND P.price > 0
                                      AND P.discount_price > 0")->asArray()->all()[0];

        if ($prices['price'] < $prices['discount_price']) {
            return $prices['price'];
        } else {
            return $prices['discount_price'];
        }
    }

    /**
     * Возвращает наибольшую стоимость товара, привязанного к товарам выбранной метки
     */
    public function getMaxProductPrice()
    {
        $prices = Product::findBySql("SELECT max(P.price) as price, max(P.discount_price) as discount_price FROM products P
                                      JOIN prods_cats_params_values PV ON PV.prod_id = P.id
                                      WHERE PV.param_id = 9
                                      AND P.is_active = TRUE
                                      AND P.is_archive = FALSE
                                      AND PV.value = {$this->id}::VARCHAR
                                      AND P.price > 0
                                      AND P.discount_price > 0")->asArray()->all()[0];

        if ($prices['price'] > $prices['discount_price']) {
            return $prices['price'];
        } else {
            return $prices['discount_price'];
        }
    }

    /**
     * Возвращает все размеры товаров
     */
    public function getSizes()
    {
        return ArrayHelper::map(Size::findBySql("SELECT S.id, S.name FROM sizes S
                         JOIN stock_position SP ON SP.size_id = S.id
                         JOIN products P ON P.id = SP.product_id
                         JOIN prods_cats_params_values PV ON PV.prod_id = P.id
                         WHERE S.active = TRUE
                         AND PV.param_id = 9
                         AND PV.value = {$this->id}::VARCHAR
                         AND P.is_active = TRUE
                         AND P.is_archive = FALSE
                         GROUP BY S.id
                         ORDER BY S.name")->asArray()->all(), 'id', 'name');
    }

    /**
     * @return array
     * Возвращает метки категорий, привязанные к товарам выбранной метки
     */
    public function getCategoryTags()
    {
        return ArrayHelper::map(CategoryTag::findBySql("SELECT PCT.id, PCT.client_name FROM product_categories_tags PCT
                            JOIN product_category_product_category_tags PCPCT ON PCPCT.product_category_tag_id = PCT.id
                            JOIN product_categories PC ON PC.id = PCPCT.product_category_id
                            JOIN products P ON P.category_id = PC.id
                            JOIN prods_cats_params_values PV ON PV.prod_id = P.id
                            WHERE PV.param_id = 17
                            AND PV.value = PCT.id::VARCHAR
                            AND P.is_active = TRUE
                            AND P.is_archive = FALSE
                            AND P.id IN (SELECT P.id FROM products P 
                                JOIN prods_cats_params_values PV ON PV.prod_id = P.id
                                WHERE PV.param_id = 9
                                AND PV.value = {$this->id}::VARCHAR
                                AND P.is_active = TRUE
	                            AND P.is_archive = FALSE)
                            GROUP BY PCT.id
                            ORDER BY PCT.position")->asArray()->all(), 'id', 'client_name');
    }

    /**
     * Возвращает производителей, привязанных к товарам выбранной метки
     */
    public function getManufacturers()
    {
        return ArrayHelper::map(ClothForSearch::findBySql("SELECT M.client_name, M.id
                                   FROM manufacturer M
                                   JOIN prods_cats_params_values PV ON PV.value = M.id::VARCHAR
                                   JOIN products P ON PV.prod_id = P.id
                                   WHERE PV.param_id = 5
                                   AND P.id IN (SELECT P.id FROM products P 
                                       JOIN prods_cats_params_values PV ON PV.prod_id = P.id
                                       WHERE PV.param_id = 9
                                       AND PV.value = {$this->id}::VARCHAR
                                       AND P.is_active = TRUE
                                       AND P.is_archive = FALSE
                                   )
                                   GROUP BY M.id
                                   ORDER BY M.client_name")->asArray()->all(), 'id', 'client_name');
    }

    /**
     * Возвращает типы тканей, привязанные к товарам выбранной метки
     */
    public function getTextureTypes()
    {
        return ArrayHelper::map(ClothForSearch::findBySql("SELECT CS.name, CS.id
                                   FROM cloth_for_search CS
                                   JOIN prods_cats_params_values PV ON PV.value = CS.id::VARCHAR
                                   JOIN products P ON PV.prod_id = P.id
                                   WHERE PV.param_id = 26
                                   AND P.id IN (SELECT P.id FROM products P 
                                       JOIN prods_cats_params_values PV ON PV.prod_id = P.id
                                       WHERE PV.param_id = 9
                                       AND PV.value = {$this->id}::VARCHAR
                                       AND P.is_active = TRUE
                                       AND P.is_archive = FALSE
                                   )
                                   GROUP BY CS.id
                                   ORDER BY CS.name")->asArray()->all(), 'id', 'name');
    }

    /**
     * Возвращает цвета для поиска, привязанные к товарам выбранной метки
     */
    public function getColorForSearch()
    {
        return ArrayHelper::map(ClothForSearch::findBySql("SELECT SC.name, SC.id
                                                           FROM search_colors SC
                                                           JOIN prods_cats_params_values PV ON PV.value = SC.id::VARCHAR
                                                           JOIN products P ON PV.prod_id = P.id
                                                           WHERE PV.param_id = 31
                                                           AND P.id IN (SELECT P.id FROM products P 
                                                               JOIN prods_cats_params_values PV ON PV.prod_id = P.id
                                                               WHERE PV.param_id = 9
                                                               AND PV.value = {$this->id}::VARCHAR
                                                               AND P.is_active = TRUE
                                                               AND P.is_archive = FALSE
                                                           )
                                                           GROUP BY SC.id
                                                           ORDER BY SC.name")->asArray()->all(), 'id', 'name');
    }
}
