<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DeferredMultipleOperation;

/**
 * DeferredmMultipleOperationSearch represents the model behind the search form about `app\models\DeferredMultipleOperation`.
 */
class DeferredMultipleOperationSearch extends DeferredMultipleOperation
{
    public $creatorName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'creator_id'], 'integer'],
            [['operation_mode', 'date', 'mode_parameter_value', 'creatorName'], 'safe'],
            [['date'], 'date', 'format'=>'yyyy-MM-dd',],
            [['is_completed'], 'boolean']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DeferredMultipleOperation::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'creator_id' => $this->creator_id,
            'is_completed' => $this->is_completed
        ]);

        $query->andFilterWhere(['like', "text(date)", $this->date]);


        if ($this->creatorName) {
            $query->leftJoin(User::tableName(), User::tableName() . '.id = ' . DeferredMultipleOperation::tableName() . '.creator_id')
                ->andWhere(['like', User::tableName() . '.login', $this->creatorName]);
        }

        $query->andFilterWhere(['like', 'operation_mode', $this->operation_mode])
            ->andFilterWhere(['like', 'mode_parameter_value', $this->mode_parameter_value]);

        $query->orderBy('date DESC');

        return $dataProvider;
    }
}
