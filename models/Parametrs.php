<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "parametrs".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $value
 */
class Parametrs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parametrs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description', 'value'], 'string', 'max' => 255],
            ['visible','safe']
        ];
    }

    public static function getPreviewImageConfig()
    {
        $widthConfig = Parametrs::find()->where(['title' => 'preview_image_width'])->one();
        $heightConfig = Parametrs::find()->where(['title' => 'preview_image_height'])->one();
        $colorConfig = Parametrs::find()->where(['title' => 'preview_image_color'])->one();

        $previewImageConfig = array();

        if (!empty($widthConfig)) {
            $width = $widthConfig->value;
        } else {
            $width = 100;
        }
        $previewImageConfig['width'] = $width;

        if (!empty($heightConfig)) {
            $height = $heightConfig->value;
        } else {
            $height = 120;
        }
        $previewImageConfig['height'] = $height;

        if (!empty($colorConfig)) {
            $color = $colorConfig->value;
        } else {
            $color = "ffffff";
        }
        $previewImageConfig['color'] = $color;

        return $previewImageConfig;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('interface','ID'),
            'title' => Yii::t('interface','Title'),
            'description' => Yii::t('interface','Description'),
            'value' => 'Значение',
            'visible'=>'Видимость'
        ];
    }
}
