<?php

namespace app\models;

use FormulaParser\FormulaParser;
use SimpleXMLElement;
use tebazil\runner\ConsoleCommandRunner;
use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "synchronization".
 *
 * @property integer $id
 * @property integer $manufacturer_id
 * @property string $last_synchronize_date
 */
class Synchronization extends \yii\db\ActiveRecord
{
    const DEFAULT_SIZE_ID = 864;
    const DEFAULT_COLOR_ID = 2455;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'synchronization';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['manufacturer_id'], 'integer'],
            [['last_synchronize_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'manufacturer_id' => 'Производитель',
            'last_synchronize_date' => 'Дата последней синхронизации',
        ];
    }

    /**
     *
     * Возвращает поставщика, с которым проводилась синхронизация
     */
    public function getManufacturer()
    {
        return $this->hasOne(Manufacturer::className(), ['id' => 'manufacturer_id']);
    }

    public static function findModelByManufacturer($manufacturerId)
    {
        return self::find()->where(['manufacturer_id' => $manufacturerId])->one();
    }

    /**
     * Синхронизация с Седжо
     */
    public static function synchronizeSejo()
    {
        $synchronization = self::findModelByManufacturer(50);
        if (empty($synchronization)) {
            $synchronization = new Synchronization();
            $synchronization->manufacturer_id = 50;
        }

        $manufacturer = Manufacturer::findOne(50);

        $resultSynchronization = new \stdClass();
        $successSynchronization = [];
        $errorsSynchronization = [];
        $categories = [];
        
        $request = 'key=aj3Ll4Di5EHl61ZboFEgnIV5GtmJRz2cSksAb6bN&';

        $products = Product::find()->select(['id', 'article', 'category_id'])
                           ->where(['like', 'article', 'S-%', false])
                           ->andWhere(['is_archive' => false])->all();

        foreach ($products as $product) {
            if (!in_array($product->category_id, $categories)) {
                $categories[] = $product->category_id;
            }
        }

        $products = ArrayHelper::map($products, 'article', 'id');

        foreach ($products as $productArticle => $productId) {
            $request .= 'articles[]=' . str_replace('S-', '', $productArticle).'&';
        }
        $request = rtrim($request, '&');

        if ($curl = curl_init('http://sejo.ru/exportProdSizes.php')) {
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
            $out = curl_exec($curl);
            curl_close($curl);

            $result = json_decode($out);
            //vd($result->prods);
            foreach ($result->prods as $article => $sizes) {
                $article = 'S-' . $article;

                $resultProduct = new \stdClass();
                $resultProduct->article = $article;

                if (!empty($products[$article])) {
                    $currentTime = date('Y-m-d H:i:s');
                    Product::updateAll(['sync_info' => $currentTime], ['id' => $products[$article]]);
                    StockPosition::deleteAll(['product_id' => $products[$article], 'stock_id' => 1]);

                    foreach ($sizes as $sizeNum => $size) {
                        if ($sizeNum != 'price') {
                            $resultProduct->sizes[] = $size->name;
                            $sizeId = ArrayHelper::getValue(Size::find()->select('id')->where(['name' => $size->name])->one(), 'id');

                            if (!empty($sizeId)) {
                                $newStockPosition = new StockPosition();
                                $newStockPosition->product_id = $products[$article];
                                $newStockPosition->size_id = $sizeId;
                                $newStockPosition->color_id = self::DEFAULT_COLOR_ID;
                                $newStockPosition->stock_id = 1;
                                $newStockPosition->quantity = 1;
                                $newStockPosition->save();
                            }
                        } else {
                            $price = $size;
                            if ($manufacturer->formula_retail) {
                                $formula = str_replace('price', 'x', $manufacturer->formula_retail);
                                $parser = new FormulaParser($formula);
                                $parser->setVariables(['x' => $price]);
                                $newPrice = $parser->getResult();
                                if ($newPrice[0] == "done") {
                                    $newPrice = $newPrice[1];
                                } else {
                                    $newPrice = $price;
                                }
                            } else {
                                $newPrice = $price;
                            }

                            $product = Product::findOne($products[$article]);
                            if ($product->price != $newPrice) {
                                $resultProduct->oldPrice = $product->price;
                                $resultProduct->newPrice = $newPrice;
                                if ($product->discount) {
                                    $priceWithDiscount = round($newPrice - ($newPrice * $product->discount / 100));
                                    $resultProduct->discount = $product->discount;
                                    $resultProduct->newPriceWithDiscount = $priceWithDiscount;
                                } else {
                                    $priceWithDiscount = 0;
                                }

                                $product->price = $newPrice;
                                $product->discount_price = $priceWithDiscount;
                            }

                            if ($manufacturer->formula_wholesale) {
                                $formula = str_replace('price', 'x', $manufacturer->formula_wholesale);
                                $parser = new FormulaParser($formula);
                                $parser->setVariables(['x' => $price]);
                                $newPrice = $parser->getResult();
                                if ($newPrice[0] == "done") {
                                    $newPrice = $newPrice[1];
                                } else {
                                    $newPrice = $price;
                                }
                            } else {
                                $newPrice = $price;
                            }

                            if ($product->wholesale_price != $newPrice) {
                                $resultProduct->oldWholesalePrice = $product->wholesale_price;
                                $resultProduct->newWholesalePrice = $newPrice;
                                if ($product->wholesale_discount) {
                                    $priceWithDiscount = round($newPrice - ($newPrice * $product->wholesale_discount / 100));
                                    $resultProduct->wholesaleDiscount = $product->wholesale_discount;
                                    $resultProduct->newWholesalePriceWithDiscount = $priceWithDiscount;
                                } else {
                                    $priceWithDiscount = 0;
                                }

                                $product->wholesale_price = $newPrice;
                                $product->wholesale_discount_price = $priceWithDiscount;
                            }

                            $product->save();
                        }
                    }

                    if (!StockPosition::checkSizesInStock($products[$article])) {
                        Product::updateAll(['is_active' => false, 'is_wholesale_active' => false], ['id' => $products[$article]]);
                        $resultProduct->is_visible = 0;
                        $resultProduct->is_wholesale_visible = 0;
                    } else {
                        Product::updateAll(['is_active' => true, 'is_wholesale_active' => true], ['id' => $products[$article]]);
                        $resultProduct->is_visible = 1;
                        $resultProduct->is_wholesale_visible = 1;
                    }
                    // здесь очистка кэшированной карточки товара
                    $successSynchronization[] = $resultProduct;
                }
            }

            foreach ($result->errors as $article => $message) {
                $article = 'S-' . $article;

                $resultProduct = new \stdClass();
                $resultProduct->article = $article;
                $resultProduct->message = $message;

                if (!empty($products[$article])) {
                    $currentTime = date('Y-m-d H:i:s');
                    Product::updateAll(['sync_info' => $currentTime], ['id' => $products[$article]]);
                    StockPosition::deleteAll(['product_id' => $products[$article], 'stock_id' => 1]);

                    if (!StockPosition::checkSizesInStock($products[$article])) {
                        Product::updateAll(['is_active' => false, 'is_wholesale_active' => false], ['id' => $products[$article]]);
                        $resultProduct->is_visible = 0;
                        $resultProduct->is_wholesale_visible = 0;
                    }
                    // здесь очистка кэшированной карточки товара
                }
                
                $errorsSynchronization[] = $resultProduct;
            }

            // Сохраняем синхронизацию
            $synchronization->last_synchronize_date = date('Y-m-d H:i:s');
            $synchronization->save();
        }

        $resultSynchronization->success = $successSynchronization;
        $resultSynchronization->errors = $errorsSynchronization;

        $runner = new ConsoleCommandRunner();
        $categories = implode(',', $categories);
        $runner->run('deferred-operation/background-calculate-rating', [$categories]);

        return $resultSynchronization;
    }

    /**
     * Синхронизация с Прима Линия
     */
    public static function synchronizePrimaLinia()
    {
        $resultSynchronization = new \stdClass();
        $successSynchronization = [];
        $errorsSynchronization = [];

        $synchronization = self::findModelByManufacturer(1);
        if (empty($synchronization)) {
            $synchronization = new Synchronization();
            $synchronization->manufacturer_id = 1;
        }

        $categories = [];
        $products = Product::find()->where(['like', 'article', 'ПЛ-%', false])->andWhere(['is_archive' => false])->all();
        $manufacturer = Manufacturer::findOne(1);

        if ($curl = curl_init()) {
            curl_setopt($curl, CURLOPT_URL, 'http://primalinea.ru/export/catalog.xml');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $out = curl_exec($curl);
            curl_close($curl);
            $result = new SimpleXMLElement($out);

            $resultProds = array();

            $offers = array('offers' => array());
            if (!empty($result->shop)) {
                $offers = (array)$result->shop->offers;
            }
            foreach ($offers['offer'] as $offer) {
                $sizes = array();
                foreach ($offer->param as $param) {
                    /**
                     * @var SimpleXMLElement $param
                     */
                    foreach ($param->attributes() as $attrName => $attrValue) {
                        if ($attrName === 'name' && (string) $attrValue === 'Размеры') {
                            $sizes = array_map('trim', explode(',', $param));
                        }
                    }
                }
                $resultProds['ПЛ-' . $offer->vendorCode] = array(
                    'price' => (int)$offer->price,
                    'sizes' => $sizes,
                    'categoryId' => (int) $offer->categoryId
                );
            }

            foreach ($products as $product) {
                /**
                 * @var Product $product
                 */
                if (!in_array($product->category_id, $categories)) {
                    $categories[] = $product->category_id;
                }

                $resultProduct = new \stdClass();
                $resultProduct->article = $product->article;

                if (!array_key_exists($product->article, $resultProds)) {
                    $resultProduct->message = "Не найдены размеры на сайте поставщика";
                    StockPosition::deleteAll(['product_id' => $product->id, 'stock_id' => 1]);

                    if (!StockPosition::checkSizesInStock($product->id)) {
                        Product::updateAll(['is_active' => false, 'is_wholesale_active' => false], ['id' => $product->id]);
                        $resultProduct->is_visible = 0;
                    } else {
                        $product->is_active = true;
                        $resultProduct->is_visible = 1;
                        if ($manufacturer->show_in_wholesale) {
                            $product->is_wholesale_active = true;
                            $resultProduct->is_wholesale_visible = 1;
                        }
                    }
                    // ОЧИСТКА КЭША ТОВАРА
                    $errorsSynchronization[] = $resultProduct;
                } else {
                    if ($resultProds[$product->article]['categoryId'] === 52) {
                        $resultProduct->article .= ' - аксессуар';
                    } else {
                        StockPosition::deleteAll(['product_id' => $product->id, 'stock_id' => 1]);
                        foreach ($resultProds[$product->article]['sizes'] as $size) {
                            $resultProduct->sizes[] = $size;
                            $sizeId = ArrayHelper::getValue(Size::find()->select('id')->where(['name' => $size])->one(), 'id');

                            if (!empty($sizeId)) {
                                $newStockPosition = new StockPosition();
                                $newStockPosition->product_id = $product->id;
                                $newStockPosition->size_id = $sizeId;
                                $newStockPosition->color_id = self::DEFAULT_COLOR_ID;
                                $newStockPosition->quantity = 1;
                                $newStockPosition->stock_id = 1;
                                $newStockPosition->save();
                            }
                        }

                        if (!StockPosition::checkSizesInStock($product->id)) {
                            $product->is_active = false;
                            $product->is_wholesale_active = false;
                            $resultProduct->is_visible = 0;
                            $resultProduct->is_wholesale_visible = 0;
                        } else {
                            $product->is_active = true;
                            $product->is_wholesale_active = true;
                            $resultProduct->is_visible = 1;
                            $resultProduct->is_wholesale_visible = 1;
                        }
                        // ОЧИСТКА КЭША ТОВАРА
                    }

                    if ($manufacturer->formula_retail) {
                        $formula = str_replace('price', 'x', $manufacturer->formula_retail);
                        $parser = new FormulaParser($formula);
                        $parser->setVariables(['x' => $resultProds[$product->article]['price']]);
                        $newPrice = $parser->getResult();
                        if ($newPrice[0] == "done") {
                            $newPrice = $newPrice[1];
                        } else {
                            $newPrice = $resultProds[$product->article]['price'];
                        }
                    } else {
                        $newPrice = $resultProds[$product->article]['price'];
                    }

                    if ($product->price != $newPrice) {
                        $resultProduct->oldPrice = $product->price;
                        $resultProduct->newPrice = $newPrice;
                        if ($product->discount) {
                            $priceWithDiscount = round($newPrice - ($newPrice * $product->discount / 100));
                            $resultProduct->discount = $product->discount;
                            $resultProduct->newPriceWithDiscount = $priceWithDiscount;
                        } else {
                            $priceWithDiscount = 0;
                        }

                        $product->price = $newPrice;
                        $product->discount_price = $priceWithDiscount;
                    }

                    if ($manufacturer->formula_wholesale) {
                        $formula = str_replace('price', 'x', $manufacturer->formula_wholesale);
                        $parser = new FormulaParser($formula);
                        $parser->setVariables(['x' => $resultProds[$product->article]['price']]);
                        $newPrice = $parser->getResult();
                        if ($newPrice[0] == "done") {
                            $newPrice = $newPrice[1];
                        } else {
                            $newPrice = $resultProds[$product->article]['price'];
                        }
                    } else {
                        $newPrice = $resultProds[$product->article]['price'];
                    }

                    if ($product->wholesale_price != $newPrice) {
                        $resultProduct->oldWholesalePrice = $product->wholesale_price;
                        $resultProduct->newWholesalePrice = $newPrice;
                        if ($product->wholesale_discount) {
                            $priceWithDiscount = round($newPrice - ($newPrice * $product->wholesale_discount / 100));
                            $resultProduct->wholesaleDiscount = $product->wholesale_discount;
                            $resultProduct->newWholesalePriceWithDiscount = $priceWithDiscount;
                        } else {
                            $priceWithDiscount = 0;
                        }

                        $product->wholesale_price = $newPrice;
                        $product->wholesale_discount_price = $priceWithDiscount;
                    }

                    $successSynchronization[] = $resultProduct;
                }

                $currentTime = date('Y-m-d H:i:s');
                $product->sync_info = $currentTime;
                $product->save();
            }

            // Сохраняем синхронизацию
            $synchronization->last_synchronize_date = date('Y-m-d H:i:s');
            $synchronization->save();

            $resultSynchronization->success = $successSynchronization;
            $resultSynchronization->errors = $errorsSynchronization;

            $runner = new ConsoleCommandRunner();
            $categories = implode(',', $categories);
            $runner->run('deferred-operation/background-calculate-rating', [$categories]);

            return $resultSynchronization;
        }
    }

    /**
     * Синхронизация по CSV файлу
     */
    public static function synchronizeCSV($lines, $manufacturerId)
    {
        $synchronization = self::findModelByManufacturer($manufacturerId);
        if (empty($synchronization)) {
            $synchronization = new Synchronization();
            $synchronization->manufacturer_id = $manufacturerId;
        }

        $manufacturer = Manufacturer::findOne($manufacturerId);

        $prefix = $manufacturer->prefix;
        $showInOpt = $manufacturer->show_in_wholesale;

        $sizeIds = array();
        $articles = array();
        $categories = array();

        $resultSynchronization = new \stdClass();
        $successSynchronization = [];
        $errorsSynchronization = [];

        if ($prefix) {
            $productQuery = Product::find()->select('id')->where(['like', 'article', $prefix . '%', false])->andWhere(['is_archive' => false]);
            $products = Product::find()->where(['like', 'article', $prefix . '%', false])->andWhere(['is_archive' => false])->all();

            // Сохраняем данные на случай восстановления
            if (!empty(Yii::$app->cache->get("synchronization-backup" . $manufacturerId))) {
                Yii::$app->cache->delete("synchronization-backup" . $manufacturerId);
            }
            $oldSizesBackup = StockPosition::find()->where(['stock_id' => 1])->andWhere(['product_id' => $productQuery])->all();
            $oldProductsBackup = $products;
            $synchronizationBackup = new \stdClass();
            $synchronizationBackup->sizes = $oldSizesBackup;
            $synchronizationBackup->products = $oldProductsBackup;
            Yii::$app->cache->set("synchronization-backup" . $manufacturerId, $synchronizationBackup);

            StockPosition::deleteAll(['stock_id' => 1, 'product_id' => $productQuery]);
            $csvProds = array();

            foreach ($lines as $line_num => $line) {
                $productInfo = explode(";", $line);
                $countRows = count($productInfo);

                if ($countRows == 4) {
                    list($item1, $item2, $item3, $item4) = $productInfo;
                } elseif ($countRows == 5) {
                    list($item1, $item2, $item3, $item4, $item5) = $productInfo;
                } else {
                    list($item1, $item2, $item3) = $productInfo;
                }


                if (!empty($item1)) {
                    $sourceSizes = preg_split('/\/|,|-/', trim($item3, " \r\n\t\""), -1, PREG_SPLIT_NO_EMPTY);
                    if (empty($sourceSizes)) {
                        $articles[trim(iconv("CP1251", "UTF-8", $item2), "\t\r\n\" ")] = array();
                    } else {
                        foreach ($sourceSizes as $srcSize) {
                            $articles[trim(iconv("CP1251", "UTF-8", $item2), "\t\r\n\" ")][] = $srcSize;
                        }
                        if (!empty($item5)) {
                            $price = preg_split('/\/|,|-/', trim($item5, " \r\n\t\""), -1, PREG_SPLIT_NO_EMPTY);
                            if (!empty($price[0])) {
                                $articles[trim(iconv("CP1251", "UTF-8", $item2), "\t\r\n\" ")]['price'] = $price[0];
                            }
                        }

                        if (!empty($item4)) {
                            $wholesalePrice = preg_split('/\/|,|-/', trim($item4, " \r\n\t\""), -1, PREG_SPLIT_NO_EMPTY);
                            if (!empty($wholesalePrice[0])) {
                                $articles[trim(iconv("CP1251", "UTF-8", $item2), "\t\r\n\" ")]['wholesale-price'] = $wholesalePrice[0];

                                if (empty($item5)) {
                                    $articles[trim(iconv("CP1251", "UTF-8", $item2), "\t\r\n\" ")]['price'] = $wholesalePrice[0];
                                }
                            }
                        }
                    }
                }
            }
            $accessCache = array();

            foreach ($articles as $article => $sizes) {
                $csvProds[mb_strtolower($prefix . $article, 'UTF-8')] = true;
                $resultProduct = new \stdClass();
                $resultProduct->article = $article;

                /**
                 * @var Product $product
                 */
                $product = Product::find()->where(['article' => $prefix . $article])->andWhere(['is_archive' => false])->with('category')->one();
                if (isset($product)) {
                    if (!in_array($product->category_id, $categories)) {
                        $categories[] = $product->category_id;
                    }
                    $price = $sizes['price'];
                    $wholesalePrice = $sizes['wholesale-price'];

                    $sizes = array_unique($sizes);
                    foreach ($sizes as $sizeId => $size) {
                        if ($sizeId != 'price' && $sizeId != 'wholesale-price') {
                            $size = trim($size);
                            if (!empty($size)) {
                                if (isset($sizeIds[$size])) {
                                    $sizeId = $sizeIds[$size];
                                } else {
                                    $sizeId = $sizeIds[$size] = ArrayHelper::getValue(Size::find()->select('id')->where(['name' => $size])->one(), 'id');
                                }
                                if ($sizeId) {
                                    $newStockPosition = new StockPosition();
                                    $newStockPosition->product_id = $product->id;
                                    $newStockPosition->size_id = $sizeId;
                                    $newStockPosition->color_id = self::DEFAULT_COLOR_ID;
                                    $newStockPosition->stock_id = 1;
                                    $newStockPosition->quantity = 1;
                                    $newStockPosition->save();
                                } else {
                                    $size .= '<b style="color: red">- неизвестный размер</b>';
                                }

                                $resultProduct->sizes[] = $size;
                            }
                        }

                        if ($manufacturer->formula_retail) {
                            $formula = str_replace('price', 'x', $manufacturer->formula_retail);
                            $parser = new FormulaParser($formula);
                            $parser->setVariables(['x' => $price]);
                            $newPrice = $parser->getResult();
                            if ($newPrice[0] == "done") {
                                $newPrice = $newPrice[1];
                            } else {
                                $newPrice = $price;
                            }
                        } else {
                            $newPrice = $price;
                        }

                        if ($product->price != $newPrice) {
                            $resultProduct->oldPrice = $product->price;
                            $resultProduct->newPrice = $newPrice;
                            if ($product->discount) {
                                $priceWithDiscount = round($newPrice - ($newPrice * $product->discount / 100));
                                $resultProduct->discount = $product->discount;
                                $resultProduct->newPriceWithDiscount = $priceWithDiscount;
                            } else {
                                $priceWithDiscount = 0;
                            }

                            $product->price = $newPrice;
                            $product->discount_price = $priceWithDiscount;
                        }

                        if ($manufacturer->formula_wholesale) {
                            $formula = str_replace('price', 'x', $manufacturer->formula_wholesale);
                            $parser = new FormulaParser($formula);
                            $parser->setVariables(['x' => $wholesalePrice]);
                            $newPrice = $parser->getResult();
                            if ($newPrice[0] == "done") {
                                $newPrice = $newPrice[1];
                            } else {
                                $newPrice = $wholesalePrice;
                            }
                        } else {
                            $newPrice = $wholesalePrice;
                        }

                        if ($product->wholesale_price != $newPrice) {
                            $resultProduct->oldWholesalePrice = $product->wholesale_price;
                            $resultProduct->newWholesalePrice = $newPrice;
                            if ($product->wholesale_discount) {
                                $priceWithDiscount = round($newPrice - ($newPrice * $product->wholesale_discount / 100));
                                $resultProduct->wholesaleDiscount = $product->wholesale_discount;
                                $resultProduct->newWholesalePriceWithDiscount = $priceWithDiscount;
                            } else {
                                $priceWithDiscount = 0;
                            }

                            $product->wholesale_price = $newPrice;
                            $product->wholesale_discount_price = $priceWithDiscount;
                        }
                    }
                    // ОЧИСТКА КЭША ТОВАРА

                    if (!isset($accessCache[$product->category_id])) {
                        $accessCache[$product->category_id] = $product->category->access;
                    }
                    $checkProdSize = StockPosition::checkSizesInStock($product->id);
                    if ($accessCache[$product->category_id] || $checkProdSize) {
                        $product->is_active = true;
                        $resultProduct->is_visible = 1;
                    }
                    if (($accessCache[$product->category_id] || $checkProdSize) && $showInOpt) {
                        $product->is_wholesale_active = true;
                        $resultProduct->is_wholesale_visible = 1;
                    }
                    $currentTime = date('Y-m-d H:i:s');
                    $product->sync_info = $currentTime;

                    $successSynchronization[] = $resultProduct;
                    $product->save();
                } else {
                    $resultProduct->message = 'Товар не найден.';
                    $errorsSynchronization[] = $resultProduct;
                }
            }

            foreach ($products as $product) {
                if (!isset($csvProds[mb_strtolower($product->article, 'UTF-8')])) {
                    $resultProduct = new \stdClass();
                    $resultProduct->is_visible = 1;

                    if (!isset($accessCache[$product->category_id])) {
                        $accessCache[$product->category_id] = $product->category->access;
                    }
                    $resultProduct->article = $product->article;

                    StockPosition::deleteAll(['stock_id' => 1, 'product_id' => $product->id]);
                    if (!$accessCache[$product->category_id] && !StockPosition::checkSizesInStock($product->id)) {
                        $product->is_wholesale_active = false;
                        $product->is_active = false;
                        $resultProduct->is_visible = 0;
                        $resultProduct->is_wholesale_visible = 0;
                        $resultProduct->message = 'Размеры не найдены';
                        $product->save();

                        $errorsSynchronization[] = $resultProduct;
                    }
                }
            }
        }

        $resultSynchronization->success = $successSynchronization;
        $resultSynchronization->errors = $errorsSynchronization;

        // Сохраняем синхронизацию
        $synchronization->last_synchronize_date = date('Y-m-d H:i:s');
        $synchronization->save();

        $runner = new ConsoleCommandRunner();
        $categories = implode(',', $categories);
        $runner->run('deferred-operation/background-calculate-rating', [$categories]);

        return $resultSynchronization;
    }
}
