<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ml_cfg".
 *
 * @property integer $id
 * @property string $title
 * @property string $emailto
 * @property string $emailfrom
 * @property string $esubj
 * @property string $ecode
 * @property integer $etype
 * @property string $ebody_txt
 * @property string $ebody_html
 * @property string $mnemo
 * @property integer $pos
 * @property integer $visible
 */
class MlCfg extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ml_cfg';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['etype', 'pos', 'visible'], 'integer'],
            [['ebody_txt', 'ebody_html'], 'string'],
            [['title', 'emailto', 'emailfrom', 'esubj', 'ecode', 'mnemo'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'emailto' => 'Emailto',
            'emailfrom' => 'Emailfrom',
            'esubj' => 'Esubj',
            'ecode' => 'Ecode',
            'etype' => 'Etype',
            'ebody_txt' => 'Ebody Txt',
            'ebody_html' => 'Ebody Html',
            'mnemo' => 'Mnemo',
            'pos' => 'Pos',
            'visible' => 'Visible',
        ];
    }
}
