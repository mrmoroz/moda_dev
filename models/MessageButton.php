<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "message_button".
 *
 * @property integer $id
 * @property integer $form_type
 * @property string $name
 */
class MessageButton extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message_button';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['form_type'], 'integer'],
            [['name'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'form_type' => 'Form ID',
            'name' => 'Название',
        ];
    }
}
