<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "stock_position".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $color_id
 * @property integer $size_id
 * @property integer $stock_id
 * @property integer $quantity
 */
class StockPosition extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stock_position';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id'], 'required'],
            [['product_id', 'color_id', 'size_id', 'stock_id', 'quantity'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'color_id' => 'Color ID',
            'size_id' => 'Size ID',
            'stock_id' => 'Stock ID',
            'quantity' => 'Quantity',
        ];
    }

    /**
     * Проверка наличия размеров на складе у поставщика
     * @param $productId
     * @param bool $isManufacturer
     * @return bool
     */
    public static function checkSizesInStock($productId, $isManufacturer = false)
    {
        $query = self::find()->where(['product_id' => $productId]);

        if ($isManufacturer) {
            $query = $query->innerJoin(Stock::tableName(), 'stocks.id = stock_position.stock_id')
                           ->andWhere(['stocks.is_manufacturer' => true]);
        }

        if ($query->count() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public static function getStockdata($product_id, $color_id, $size_id, $stocks_str)
    {
        return Yii::$app->db->createCommand("
                SELECT stock_position.*, stocks.name, stocks.position FROM stock_position
                LEFT JOIN stocks ON stocks.id = stock_position.stock_id 
                WHERE stock_position.product_id = '{$product_id}'
                AND stock_position.color_id = '{$color_id}'
                AND stock_position.size_id = '{$size_id}'
                AND stock_position.stock_id IN($stocks_str)
                AND stock_position.quantity > 0
                ORDER BY stocks.position DESC
        ")->queryOne();
    }

    public static function changeStockData($quantity, $stock_id)
    {
        if ($quantity == 0) {
            Yii::$app->db->createCommand("DELETE FROM stock_position  WHERE id='{$stock_id}'")->execute();
        } else {
            Yii::$app->db->createCommand("UPDATE stock_position SET quantity='{$quantity}' WHERE id='{$stock_id}'")->execute();
        }
    }

    public static function checkStocks($product_id, $id_order){
        $sql = "SELECT stock_position.id FROM stock_position WHERE stock_position.product_id = '{$product_id}' AND stock_position.quantity > 0";
        $stocks = Yii::$app->db->createCommand($sql)->queryAll();
        if(count($stocks) == 0){
            Yii::$app->db->createCommand("UPDATE products SET is_active = false WHERE id='{$product_id}'")->execute();
        }
    }
}
