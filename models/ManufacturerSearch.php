<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Manufacturer;

/**
 * ManufacturerSearch represents the model behind the search form about `app\models\Manufacturer`.
 */
class ManufacturerSearch extends Manufacturer
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'name_mo', 'menu_title', 'html_title', 'html_description', 'html_keywords', 'top_text', 'bottom_text'], 'safe'],
            [['show_in_retail', 'show_in_wholesale', 'show_in_nsk'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Manufacturer::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'show_in_retail' => $this->show_in_retail,
            'show_in_wholesale' => $this->show_in_wholesale,
            'show_in_nsk' => $this->show_in_nsk,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'name_mo', $this->name_mo])
            ->andFilterWhere(['like', 'menu_title', $this->menu_title])
            ->andFilterWhere(['like', 'html_title', $this->html_title])
            ->andFilterWhere(['like', 'html_description', $this->html_description])
            ->andFilterWhere(['like', 'html_keywords', $this->html_keywords])
            ->andFilterWhere(['like', 'top_text', $this->top_text])
            ->andFilterWhere(['like', 'bottom_text', $this->bottom_text]);

        return $dataProvider;
    }
}
