<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "geo_base".
 *
 * @property integer $long_ip1
 * @property integer $long_ip2
 * @property string $ip1
 * @property string $ip2
 * @property string $country
 * @property integer $city_id
 */
class GeoBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'geo_base';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['long_ip1', 'long_ip2', 'city_id'], 'integer'],
            [['ip1', 'ip2'], 'string', 'max' => 16],
            [['country'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'long_ip1' => 'Long Ip1',
            'long_ip2' => 'Long Ip2',
            'ip1' => 'Ip1',
            'ip2' => 'Ip2',
            'country' => 'Country',
            'city_id' => 'City ID',
        ];
    }
}
