<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_changelog".
 *
 * @property integer $type
 * @property integer $product_id
 * @property string $operation_date
 * @property integer $order_id
 * @property integer $user_id
 */
class ProductChangelog extends \yii\db\ActiveRecord
{
    const DISABLE_PRODUCT = 1;
    const EDIT_RETAIL_PRICE = 4;
    const EDIT_WHOLESALE_PRICE = 5;
    const EDIT_PRODUCT_INFO = 6;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_changelog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'product_id', 'operation_date'], 'required'],
            [['type', 'product_id', 'order_id', 'user_id'], 'integer'],
            [['operation_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'type' => 'Type',
            'product_id' => 'Product ID',
            'operation_date' => 'Operation Date',
            'order_id' => 'Order ID',
            'user_id' => 'User ID',
        ];
    }

    public function getUser()
    {
        return UserProfile::findOneByUserId($this->user_id);
    }
}
