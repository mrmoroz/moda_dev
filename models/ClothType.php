<?php

namespace app\models;

use app\helpers\RtHelper;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "cloth_types".
 *
 * @property integer $id
 * @property string $name
 * @property string $menu_title
 * @property string url
 * @property string $html_title
 * @property string $h1_page
 * @property string $top_text
 * @property string $bottom_text
 * @property boolean $highlight_in_red
 * @property boolean $show_in_wholesale
 * @property boolean $show_in_retail
 * @property boolean $on_top
 * @property boolean $on_bottom
 * @property integer $position
 * @property boolean $visible
 * @property boolean $sys_language
 */
class ClothType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cloth_types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cloth_for_search_ids'], 'each', 'rule' => ['integer']],
            [['top_text', 'bottom_text', 'url'], 'string'],
            [['show_in_wholesale', 'show_in_retail', 'on_top', 'on_bottom', 'visible', 'sys_language', 'highlight_in_red'], 'boolean'],
            [['position'], 'integer'],
            [['name', 'html_title', 'h1_page', 'menu_title', 'url'], 'string', 'max' => 100],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => \voskobovich\behaviors\ManyToManyBehavior::className(),
                'relations' => [
                    'cloth_for_search_ids' => 'clothForSearch'
                ],
            ],
        ];
    }

    public function getClothForSearch()
    {
        return $this->hasMany(ClothForSearch::className(), ['id' => 'cloth_for_search_id'])
            ->viaTable('{{%cloth_type_cloth_for_search}}', ['cloth_type_id' => 'id'])
            ->where(['is_active' => true])
            ->orderBy('position');

    }

    public function beforeSave($insert)
    {
        if(empty($this->url)){
            $this->url = strtolower(RtHelper::urlToTranslit($this->name));
        }

        return true;
    }

    public function getUrl()
    {
        $url = "/tag/";

        if (!empty($this->url)) {
            $url .= $this->url;
        } else {
            $url .= $this->id;
        }

        return $url;
    }

    public static function findOneByUrl($url) {
        return self::find()
            ->where(['url' => $url])
            ->one();
    }

    public static function listAll($keyField = 'id', $valueField = 'name', $asArray = true)
    {
        $query = static::find()->where(['visible' => true]);
        if ($asArray) {
            $query->select([$keyField, $valueField])->asArray();
        }

        return ArrayHelper::map($query->all(), $keyField, $valueField);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'menu_title' => 'Название для меню',
            'html_title' => 'Html Title',
            'url' => 'URL',
            'h1_page' => 'Заголовок',
            'top_text' => 'Вступительный текст',
            'bottom_text' => 'Текст внизу страницы',
            'highlight_in_red' => 'Выделять красным',
            'show_in_wholesale' => 'Опт',
            'show_in_retail' => 'Розница',
            'on_top' => 'Показывать над категориями (Модаоптом)',
            'on_bottom' => 'Показывать под категориями (Модаоптом)',
            'position' => 'Позиция',
            'visible' => 'Актив',
            'sys_language' => 'Sys Language',
            'cloth_for_search_ids' => 'Соответствующие ткани для поиска'
        ];
    }
}
