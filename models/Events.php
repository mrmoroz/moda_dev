<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "events".
 *
 * @property integer $id
 * @property string $create_date
 * @property string $url
 * @property string $title
 * @property string $teaser
 * @property string $img
 * @property string $body
 * @property boolean $show_in_retail
 * @property boolean $show_in_wholesale
 * @property boolean $for_employee
 * @property integer $position
 * @property boolean $is_active
 */
class Events extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'events';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['create_date'], 'safe'],
            [['url', 'title'], 'required'],
            [['url', 'title', 'teaser', 'img', 'body'], 'string'],
            [['show_in_retail', 'show_in_wholesale', 'for_employee', 'is_active'], 'boolean'],
            [['position'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'create_date' => 'Create Date',
            'url' => 'Url',
            'title' => 'Title',
            'teaser' => 'Teaser',
            'img' => 'Img',
            'body' => 'Body',
            'show_in_retail' => 'Show In Retail',
            'show_in_wholesale' => 'Show In Wholesale',
            'for_employee' => 'For Employee',
            'position' => 'Position',
            'is_active' => 'Is Active',
        ];
    }
}
