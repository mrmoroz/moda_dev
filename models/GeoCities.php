<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "geo_cities".
 *
 * @property integer $city_id
 * @property string $city
 * @property string $region
 * @property string $district
 * @property double $lat
 * @property double $lng
 */
class GeoCities extends \yii\db\ActiveRecord
{
    public $country;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'geo_cities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lat', 'lng'], 'number'],
            [['city'], 'string', 'max' => 128],
            [['region', 'district'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'city_id' => 'City ID',
            'city' => 'City',
            'region' => 'Region',
            'district' => 'District',
            'lat' => 'Lat',
            'lng' => 'Lng',
        ];
    }
}
