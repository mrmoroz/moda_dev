<?php

namespace app\models;

use app\helpers\RtHelper;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "manufacturer".
 *
 * @property integer $id
 * @property string $name
 * @property string $short_name
 * @property string $client_name
 * @property string $name_mo
 * @property string $url
 * @property string $menu_title
 * @property string $html_title
 * @property string $html_description
 * @property string $html_keywords
 * @property string $top_text
 * @property string $bottom_text
 * @property boolean $show_in_retail
 * @property boolean $show_in_wholesale
 * @property boolean $show_in_nsk
 * @property string $formula_retail
 * @property string $formula_wholesale
 * @property string $prefix
 * @property integer $slot_quantity
 * @property integer $rating
 * @property string $h1_title
 * @property boolean $show_to_client
 */
class Manufacturer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'manufacturer';
    }

    public function beforeSave($insert)
    {
        if(empty($this->url)){
            $this->url = strtolower(RtHelper::urlToTranslit($this->name));
        }

        return true;
    }

    public function getUrl()
    {
        $url = "/catalog/collection/" . $this->id;

        return $url;
    }

    public static function findOneByUrl($url) {
        return self::find()
            ->where(['url' => $url])
            ->one();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['html_description', 'html_keywords', 'top_text', 'bottom_text', 'url', 'prefix', 'short_name',
                'client_name', 'h1_title'], 'string'],
            [['show_in_retail', 'show_in_wholesale', 'show_in_nsk', 'show_to_client'], 'boolean'],
            [['name', 'name_mo', 'menu_title', 'html_title', 'url'], 'string', 'max' => 100],
            [['name'], 'unique'],
            [['days_to_pdo', 'slot_quantity', 'rating'],'integer'],

            [['formula_retail', 'formula_wholesale'], 'validateFormula']
        ];
    }

    public function validateFormula($attribute, $params)
    {
        $attr = $this->$attribute;

        if (substr_count($attr, 'price') == 0) {
            $this->addError($attribute, 'Необходимо указать price!');
        } else {
            $attr = str_replace('price', '', $attr);
            if (!preg_match("/^[0-9-+*\/]/", $attr) && !empty($attr)) {
                $this->addError($attribute, 'Недопустимые символы!');
            }
        }
    }

    public static function listAll($keyField = 'id', $valueField = 'name', $asArray = true)
    {
        $query = static::find()->orderBy('name ASC');
        if ($asArray) {
            $query->select([$keyField, $valueField])->asArray();
        }

        return ArrayHelper::map($query->all(), $keyField, $valueField);
    }

    /**
     * Возвращает количество товаров в слоте по каждому производителю
     */
    public static function getSlotQuantity()
    {
        return ArrayHelper::map(self::find()->select('id, slot_quantity')->all(), 'id', 'slot_quantity');
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'name_mo' => 'Название для опта',
            'url' => 'URL',
            'menu_title' => 'Название для меню',
            'html_title' => 'Html Title',
            'html_description' => 'Html Description',
            'html_keywords' => 'Html Keywords',
            'top_text' => 'Вступительный текст',
            'bottom_text' => 'Текст внизу страницы',
            'show_in_retail' => 'Показывать на сайте',
            'show_in_wholesale' => 'Показывать на МодаОптом',
            'show_in_nsk' => 'Показывать в Новосибирске',
            'formula_retail' => 'Формула для розничной цены',
            'formula_wholesale' => 'Формула для оптовой цены',
            'prefix' => 'Префикс',
            'days_to_pdo'=>'ПДО',
            'slot_quantity' => 'Количество товаров в слоте',
            'rating' => 'Рейтинг производителя для расчета рейтинга',
            'short_name' => 'Краткое название',
            'client_name' => 'Краткое название для покупателей',
            'h1_title' => 'h1 Title'
        ];
    }

    /**
     * Возвращает все размеры товаров
     */
    public function getSizes()
    {
        return ArrayHelper::map(Size::findBySql("SELECT S.id, S.name FROM sizes S
                         JOIN stock_position SP ON SP.size_id = S.id
                         JOIN products P ON P.id = SP.product_id
                         JOIN prods_cats_params_values PV ON PV.prod_id = P.id
                         WHERE S.active = TRUE
                         AND PV.param_id = 5
                         AND PV.value = {$this->id}::VARCHAR
                         AND P.is_active = TRUE
                         AND P.is_archive = FALSE
                         GROUP BY S.id
                         ORDER BY S.name")->asArray()->all(), 'id', 'name');
    }

    /**
     * @return array
     * Возвращает метки категорий, привязанные к товарам выбранного производителя
     */
    public function getCategoryTags()
    {
        return ArrayHelper::map(CategoryTag::findBySql("SELECT PCT.id, PCT.client_name FROM product_categories_tags PCT
                            JOIN product_category_product_category_tags PCPCT ON PCPCT.product_category_tag_id = PCT.id
                            JOIN product_categories PC ON PC.id = PCPCT.product_category_id
                            JOIN products P ON P.category_id = PC.id
                            JOIN prods_cats_params_values PV ON PV.prod_id = P.id
                            WHERE PV.param_id = 17
                            AND PV.value = PCT.id::VARCHAR
                            AND P.is_active = TRUE
                            AND P.is_archive = FALSE
                            AND P.id IN (SELECT P.id FROM products P 
                                JOIN prods_cats_params_values PV ON PV.prod_id = P.id
                                WHERE PV.param_id = 5
                                AND PV.value = {$this->id}::VARCHAR
                                AND P.is_active = TRUE
	                            AND P.is_archive = FALSE)
                            GROUP BY PCT.id
                            ORDER BY PCT.position")->asArray()->all(), 'id', 'client_name');
    }

    /**
     * Возвращает наименьшую стоимость товара, привязанного к товарам выбранного производителя
     */
    public function getMinProductPrice()
    {
        $prices = Product::findBySql("SELECT min(P.price) as price, min(P.discount_price) as discount_price FROM products P
                                      JOIN prods_cats_params_values PV ON PV.prod_id = P.id
                                      WHERE PV.param_id = 5
                                      AND P.is_active = TRUE
                                      AND P.is_archive = FALSE
                                      AND PV.value = {$this->id}::VARCHAR
                                      AND P.price > 0
                                      AND P.discount_price > 0")->asArray()->all()[0];

        if ($prices['price'] < $prices['discount_price']) {
            return $prices['price'];
        } else {
            return $prices['discount_price'];
        }
    }

    /**
     * Возвращает наибольшую стоимость товара, привязанного к товарам выбранного производителя
     */
    public function getMaxProductPrice()
    {
        $prices = Product::findBySql("SELECT max(P.price) as price, max(P.discount_price) as discount_price FROM products P
                                      JOIN prods_cats_params_values PV ON PV.prod_id = P.id
                                      WHERE PV.param_id = 5
                                      AND P.is_active = TRUE
                                      AND P.is_archive = FALSE
                                      AND PV.value = {$this->id}::VARCHAR
                                      AND P.price > 0
                                      AND P.discount_price > 0")->asArray()->all()[0];

        if ($prices['price'] > $prices['discount_price']) {
            return $prices['price'];
        } else {
            return $prices['discount_price'];
        }
    }

    /**
     * Возвращает типы тканей, привязанные к товарам выбранного производителя
     */
    public function getTextureTypes()
    {
        return ArrayHelper::map(ClothForSearch::findBySql("SELECT CS.name, CS.id
                                   FROM cloth_for_search CS
                                   JOIN prods_cats_params_values PV ON PV.value = CS.id::VARCHAR
                                   JOIN products P ON PV.prod_id = P.id
                                   WHERE PV.param_id = 26
                                   AND P.id IN (SELECT P.id FROM products P 
                                       JOIN prods_cats_params_values PV ON PV.prod_id = P.id
                                       WHERE PV.param_id = 5
                                       AND PV.value = {$this->id}::VARCHAR
                                       AND P.is_active = TRUE
                                       AND P.is_archive = FALSE
                                   )
                                   GROUP BY CS.id
                                   ORDER BY CS.name")->asArray()->all(), 'id', 'name');
    }

    /**
     * Возвращает цвета для поиска, привязанные к товарам выбранного производителя
     */
    public function getColorForSearch()
    {
        return ArrayHelper::map(ClothForSearch::findBySql("SELECT SC.name, SC.id
                                                           FROM search_colors SC
                                                           JOIN prods_cats_params_values PV ON PV.value = SC.id::VARCHAR
                                                           JOIN products P ON PV.prod_id = P.id
                                                           WHERE PV.param_id = 31
                                                           AND P.id IN (SELECT P.id FROM products P 
                                                               JOIN prods_cats_params_values PV ON PV.prod_id = P.id
                                                               WHERE PV.param_id = 5
                                                               AND PV.value = {$this->id}::VARCHAR
                                                               AND P.is_active = TRUE
                                                               AND P.is_archive = FALSE
                                                           )
                                                           GROUP BY SC.id
                                                           ORDER BY SC.name")->asArray()->all(), 'id', 'name');
    }
}
