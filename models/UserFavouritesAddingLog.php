<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "user_favourites_adding_log".
 *
 * @property integer $product_id
 * @property integer $user_id
 * @property string $cart_token
 * @property string $add_date
 */
class UserFavouritesAddingLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_favourites_adding_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'user_id'], 'integer'],
            [['cart_token'], 'string'],
            [['add_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product ID',
            'user_id' => 'User ID',
            'cart_token' => 'Cart Token',
            'add_date' => 'Add Date',
        ];
    }

    /**
     * @param $productId
     * @param $isGuest
     * @param $cartToken
     * @return int|string
     *
     * Проверяет существование записи о добавлении в избранное конкретного товара конкретным пользователем
     */
    public static function checkProduct($productId, $isGuest, $cartToken)
    {
        $query = self::find()->where(['product_id' => $productId]);

        if ($isGuest) {
            $query = $query->andWhere(['cart_token' => $cartToken]);
        } else {
            $query = $query->andWhere(['user_id' => $cartToken]);
        }

        return $query->count();
    }

    public static function getProductFavouritesAdding($addCategories, $productId)
    {
        $productIdAdd = '';

        if ($productId) {
            $productIdAdd = 'AND P.id = ' . $productId;
        }

        $productsFavouritesAddingCount = ArrayHelper::map(self::findBySql("SELECT P.id AS product_id, COUNT(FL.product_id) as qty
                                             FROM user_favourites_adding_log FL
                                             JOIN products P ON FL.product_id = P.id
                                             WHERE P.category_id {$addCategories} 
                                             {$productIdAdd}
                                             GROUP BY P.id")
            ->asArray()->all(), 'product_id', 'qty');

        $productsFavouritesAddingCountDuplicates =  ArrayHelper::map(self::findBySql("SELECT P.parent_product_id AS product_id, COUNT(FL.product_id) as qty
                                                   FROM user_favourites_adding_log FL
                                                   JOIN products P ON FL.product_id = P.id
                                                   WHERE P.parent_product_id IN (
                                                      SELECT id FROM products WHERE category_id {$addCategories}
                                                   )
                                                   GROUP BY P.parent_product_id")->asArray()->all(), 'product_id', 'qty');

        $parentIds = Product::getParentIds($addCategories);

        if (!empty($parentIds)) {
            $parentsFavouritesAddingCount = ArrayHelper::map(self::findBySql("SELECT CP.id AS child_id, COUNT(FL.product_id) as qty
                                                                    FROM user_favourites_adding_log FL
                                                                    JOIN products P ON FL.product_id = P.id
                                                                    JOIN products CP ON P.id = CP.parent_product_id
                                                                    GROUP BY P.id, CP.id")->where(['IN', 'P.id', $parentIds])
                ->asArray()->all(), 'child_id', 'qty');
        }

        if (!empty($parentsFavouritesAddingCount)) {
            foreach ($parentsFavouritesAddingCount as $childId => $parentQuantity) {
                if (isset($productsFavouritesAddingCount[$childId])) {
                    $productsFavouritesAddingCount[$childId] += $parentQuantity;
                }
            }
        }

        foreach ($productsFavouritesAddingCountDuplicates as $productId => $duplicateQuantity) {
            if (isset($productsFavouritesAddingCount[$productId])) {
                $productsFavouritesAddingCount[$productId] += $duplicateQuantity;
            }
        }

        unset($productsFavouritesAddingCountDuplicates);
        unset($parentIds);
        unset($parentsFavouritesAddingCount);

        return $productsFavouritesAddingCount;
    }
}
