<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "prods_cats_params".
 *
 * @property integer $id
 * @property string $name
 * @property string $desc
 * @property integer $type
 * @property integer $source
 * @property integer $position
 * @property boolean $visible
 * @property integer $sys_language
 */
class ProdsCatsParam extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prods_cats_params';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'desc', 'type'], 'required'],
            [['desc'], 'string'],
            [['type', 'position', 'sys_language', 'source'], 'integer'],
            [['visible'], 'boolean'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'desc' => 'Описание',
            'type' => 'Тип',
            'source' => 'Источник',
            'position' => 'Позиция',
            'visible' => 'Актив',
            'sys_language' => 'Sys Language',
        ];
    }
}
