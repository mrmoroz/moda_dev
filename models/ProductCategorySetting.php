<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_category_setting".
 *
 * @property integer $id
 * @property integer $cat_id
 * @property integer $param_id
 */
class ProductCategorySetting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_category_setting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cat_id', 'param_id'], 'required'],
            [['cat_id', 'param_id'], 'integer'],
        ];
    }

    public static function getSettingsByCategoryId ($id)
    {
        return self::find()->select(['param_id'])->where(['cat_id' => $id])->all();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cat_id' => 'Cat ID',
            'param_id' => 'Param ID',
        ];
    }
}
