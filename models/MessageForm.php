<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "message_form".
 *
 * @property integer $id
 * @property string $entity_type
 * @property integer $entity_id
 * @property integer $form_type
 */
class MessageForm extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message_form';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entity_type'], 'required'],
            [['entity_type'], 'string'],
            [['entity_id', 'form_type'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'entity_type' => 'Entity Type',
            'entity_id' => 'Entity ID',
            'form_type' => 'Form Type',
        ];
    }
    
    public function getFormSettings()
    {
        return $this->hasOne(MessageFormSettings::className(), ['type' => 'form_type'])->with('fields')->with('buttons');
    }
}
