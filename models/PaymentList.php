<?php

namespace app\models;

use Yii;

/**
 * @property integer $id
 * @property string $code
 * @property double $discount
 * @property string $foru
 * @property string $title
 * @property string $descript
 * @property integer $pos
 * @property integer $visible
 * @property integer $sys_language
 */
class PaymentList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['discount'], 'number'],
            [['descript'], 'string'],
            [['pos', 'visible', 'sys_language'], 'integer'],
            [['code', 'foru', 'title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Код',
            'discount' => 'Скидка',
            'foru' => 'Для жителей',
            'title' => 'Название',
            'descript' => 'Описание',
            'pos' => 'Позиция',
            'visible' => 'Видимость',
            'sys_language' => 'Sys Language',
        ];
    }
}
