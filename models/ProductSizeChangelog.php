<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_sizes_changelog".
 *
 * @property integer $type
 * @property integer $product_id
 * @property integer $size_id
 * @property integer $color_id
 * @property integer $user_id
 * @property integer $order_id
 * @property integer $stock_id
 * @property integer $qty
 * @property string $date_operation
 * @property boolean $status
 */
class ProductSizeChangelog extends \yii\db\ActiveRecord
{
    public function ProductSizeChangelog($productId, $stockId, $colorId, $sizeId, $type)
    {

    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_sizes_changelog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'product_id', 'size_id', 'color_id', 'stock_id'], 'required'],
            [['type', 'product_id', 'size_id', 'color_id', 'user_id', 'order_id', 'stock_id','qty'], 'integer'],
            [['status'], 'boolean'],
            [['date_operation'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'type' => 'Type',
            'product_id' => 'Product ID',
            'size_id' => 'Size ID',
            'color_id' => 'Color ID',
            'user_id' => 'User ID',
            'order_id' => 'Order ID',
            'date_operation' => 'Date Operation',
        ];
    }

    public static function create($product_id, $color_id, $size_id, $stock_id, $status, $order_id, $qty, $type=1)
    {
        $log = new ProductSizeChangelog();
        $log->product_id = $product_id;
        $log->color_id = $color_id;
        $log->size_id = $size_id;
        $log->stock_id = $stock_id;
        $log->status = $status;
        $log->order_id = $order_id;
        $log->type = $type;
        $log->date_operation = date('Y-m-d H:i:s');
        $log->qty = $qty;

        return $log;
    }
}
