<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "message_request_field".
 *
 * @property integer $id
 * @property integer $message_id
 * @property integer $field_id
 * @property integer $value
 */
class MessageRequestField extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message_request_field';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message_id', 'field_id'], 'integer',],
            [['value'], 'string']
        ];
    }

    public function getField()
    {
        return $this->hasOne(MessageField::className(), ['id' => 'field_id']);
    }

    public function isPublished()
    {
        return MessageField::findOne($this->field_id)->published;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'message_id' => 'Message ID',
            'field_id' => 'Field ID',
            'value' => 'Value',
        ];
    }
}
