<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_address".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $country_id
 * @property integer $region_id
 * @property string $city
 * @property string $address
 * @property string $post_index
 * @property string $fio
 * @property boolean $default
 *
 * @property User $user
 */
class UserAddress extends \yii\db\ActiveRecord
{
    public $country_name;
    public $region_name;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'city','address','post_index'],'required'],
            [['user_id', 'country_id', 'region_id'], 'integer'],
            [['address'], 'string'],
            [['default'], 'boolean'],
            [['city', 'post_index', 'fio'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'country_id' => 'Страна',
            'region_id' => 'Регион',
            'city' => 'Город',
            'address' => 'Адрес',
            'post_index' => 'Почтовый индекс',
            'fio' => 'Fio',
            'default' => 'Default',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
