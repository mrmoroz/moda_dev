<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MessageField;
use yii\db\Expression;

/**
 * MessageFieldSearch represents the model behind the search form about `app\models\MessageField`.
 */
class MessageFieldSearch extends MessageField
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'form_type', 'type'], 'integer'],
            [['name'], 'safe'],
            [['required', 'for_list_messages'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MessageField::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'required' => $this->required,
            'form_type' => $this->form_type,
            'for_list_messages' => $this->for_list_messages,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        $query->orderBy([new Expression('position IS NULL ASC, position ASC')]);

        return $dataProvider;
    }
}
