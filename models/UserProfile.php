<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_profile".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $first_name
 * @property string $last_name
 * @property string $second_name
 * @property string $mobile_phone
 * @property string $phone_code
 * @property string $phone
 * @property string $bust
 * @property string $waist
 * @property string $hip
 * @property string $grow
 * @property string $avatar
 * @property User $user
 */
class UserProfile extends \yii\db\ActiveRecord
{
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['first_name', 'last_name', 'second_name', 'mobile_phone', 'phone_code', 'phone', 'bust', 'waist', 'hip', 'grow'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            //[['first_name', 'last_name', 'second_name','mobile_phone'],'required','on' => 'accdata2'],
            [['file'], 'file', 'extensions' => 'jpg, gif, png, jpeg', 'maxSize' => 1048576] //1Mb
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'second_name' => 'Отчество',
            'mobile_phone' => 'Мобильный телефон',
            'phone_code' => 'Phone Code',
            'phone' => 'Phone',
            'bust' => 'Bust',
            'waist' => 'Waist',
            'hip' => 'Hip',
            'grow' => 'Grow',
            'avatar' => 'Аватар'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function findOneByUserId ($userId)
    {
        $userProfile = self::find()->where(['user_id' => $userId])->one();

        return $userProfile;
    }
}
