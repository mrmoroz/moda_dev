<?php
$mailForm = $tpl->ebody_html;
$consultant = "";
$prod_table = "";
$date = date("d.m.Y H:i",strtotime($order->cdate));
if($profile_manager){
    $consultant = "Ваш персональный консультант, ".$profile_manager->last_name." ".$profile_manager->first_name;
}

if($order_items){
    $prod_table.= "<table style='width: 100%; border: 1px solid #333333; border-collapse: collapse; margin-bottom: 30px;'>
        <tr>
            <td style='border-right: 1px solid #333333;'>Название</td>
            <td style='border-right: 1px solid #333333;'>Артикул</td>
            <td style='border-right: 1px solid #333333;'>Размер</td>
            <td style='border-right: 1px solid #333333;'>Цвет</td>
            <td style='border-right: 1px solid #333333;'>Цена</td>
            <td>Кол-во</td>
        </tr>";
    foreach ($order_items as $it){
        $prod_table.="<tr>";
        $prod_table.="<td style=\"border-right: 1px solid #333333; border-top: 1px solid #333333;\">".$it->name."</td>";
        $prod_table.="<td style=\"border-right: 1px solid #333333; border-top: 1px solid #333333;\">".$it->article."</td>";
        if(!empty($it->size_y)){
            $size = preg_replace('/[^0-9]/', '', $it->size_y);
        }elseif (!empty($it->size_n)){
            $stemp = explode(',',$it->size_n);
            $size = preg_replace('/[^0-9]/', '', $stemp[0]);
        }
        if($size==0){
            $size = 'Базовый';
        }
        $prod_table.="<td style=\"border-right: 1px solid #333333; border-top: 1px solid #333333;\">".$size."</td>";
        $prod_table.="<td style=\"border-right: 1px solid #333333; border-top: 1px solid #333333;\">".$it->color."</td>";
        if(!empty((int)$it->new_price)){
            $price = $it->new_price;
        }else{
            $price = $it->price;
        }
        $prod_table.="<td style=\"border-right: 1px solid #333333; border-top: 1px solid #333333;\">".$price."</td>";
        $prod_table.="<td style=\"border-top: 1px solid #333333;\">".$it->qty."</td>";
        $prod_table.="</tr>";
    }
    $prod_table.="</table>";
}

$message = str_replace([
    '{name2}',
    '{name3}',
    '{consultant}',
    '{order_id}',
    '{order_date}',
    '{prod_table}'
],
    [
        $profile->last_name,
        $profile->first_name,
        $consultant,
        $order->id,
        $date,
        $prod_table
    ],
    $mailForm
);

echo $message;
?>