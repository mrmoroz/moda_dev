<h2>Личные данные</h2>

<table style='width: 100%; border: 1px solid #333333; border-collapse: collapse; margin-bottom: 30px;'>
    <tr>
        <th style='border: 1px solid #333333;'>Адрес доставки</th>
        <th style='border: 1px solid #333333;'>Электронная почта</th>
        <th style='border: 1px solid #333333;'>Фамилия</th>
        <th style='border: 1px solid #333333;'>Имя</th>
        <th style='border: 1px solid #333333;'>Отчество</th>
        <th style='border: 1px solid #333333;'>Индекс</th>
        <th style='border: 1px solid #333333;'>Мобильный телефон</th>
        <th style='border: 1px solid #333333;'>Телефон</th>
    </tr>
    <tr>
        <td style='border: 1px solid #333333;'><?=$address->address?></td>
        <td style='border: 1px solid #333333;'><?=$user->login?></td>
        <td style='border: 1px solid #333333;'><?=$profile->last_name?></td>
        <td style='border: 1px solid #333333;'><?=$profile->first_name?></td>
        <td style='border: 1px solid #333333;'><?=$profile->second_name?></td>
        <td style='border: 1px solid #333333;'><?=$address->post_index?></td>
        <td style='border: 1px solid #333333;'><?=$mobile_phone?></td>
        <td style='border: 1px solid #333333;'><?="(".$profile->phone_code.") ".$profile->phone?></td>
    </tr>
</table>

<h2>Данные покупателя</h2>

<table style='width: 100%; border: 1px solid #333333; border-collapse: collapse; margin-bottom: 30px;'>
    <tr>
        <th style='border: 1px solid #333333;'>Объем груди</th>
        <th style='border: 1px solid #333333;'>Объем талии</th>
        <th style='border: 1px solid #333333;'>Объем бедер</th>
        <th style='border: 1px solid #333333;'>Рост</th>
    </tr>
    <tr>
        <td style='border: 1px solid #333333;'><?=$profile->bust?></td>
        <td style='border: 1px solid #333333;'><?=$profile->waist?></td>
        <td style='border: 1px solid #333333;'><?=$profile->hip?></td>
        <td style='border: 1px solid #333333;'><?=$profile->grow?></td>
    </tr>
</table>

<p><b>Способ доставки:</b> <?=$del_method->title?></p>
<p><b>Способ оплаты:</b> <?=$pay_method->title?></p>

<p>Новый заказ №<?=$id_order?> от <?=date("d-m-Y")?></p>

<table style="width: 100%; border: 1px solid #333333; border-collapse: collapse; margin-bottom: 30px;">
    <tr>
        <th style='border: 1px solid #333333;'>Изделие</th>
        <th style='border: 1px solid #333333;'>Артикул</th>
        <th style='border: 1px solid #333333;'>Размер</th>
        <th style='border: 1px solid #333333;'>Цвет</th>
        <th style='border: 1px solid #333333;'>Кол-во</th>
        <th style='border: 1px solid #333333;'>Цена</th>
        <th style='border: 1px solid #333333;'>Цена со скидкой</th>
    </tr>
    <?php foreach ($prod as $key=>$item):?>
        <tr>
            <td style='border: 1px solid #333333;'><?=$item['prod']->title;?></td>
            <td style='border: 1px solid #333333;'><?=$item['prod']->article;?></td>
            <td style='border: 1px solid #333333;'>
                <?php if(!empty($item['size_y']) || !empty($item['size_n'])):?>
                    <?=$item['size']->name;?>
                <?php else:?>
                    нет в наличии
                <?php endif;?>
            </td>
            <td style='border: 1px solid #333333;'><?=$item['color']->name?></td>
            <td style='border: 1px solid #333333;'><?=$item['qty']?></td>
            <td style='border: 1px solid #333333;'><?=$item['prod']->price?></td>
            <td style='border: 1px solid #333333;'><?=$item['prod']->discount_price?></td>
        </tr>
        <?php if(isset($item['gifts'])):?>
            <?php foreach ($item['gifts'] as $igf):?>
                <tr>
                    <td style='border: 1px solid #333333;'><?=$igf['prod']->title?> (подарок)</td>
                    <td style='border: 1px solid #333333;'><?=$igf['prod']->article?></td>
                    <td style='border: 1px solid #333333;'>-</td>
                    <td style='border: 1px solid #333333;'>-</td>
                    <td style='border: 1px solid #333333;'><?=$item['qty']?></td>
                    <td style='border: 1px solid #333333;'>0</td>
                    <td style='border: 1px solid #333333;'>0</td>
                </tr>
            <?php endforeach;?>
        <?php endif;?>
    <?php endforeach;?>
    <tr>
        <td colspan='7' style='border: 1px solid #333333;'>
            Скидка <?=$products_info['ecomomy']?> руб.
        </td>
    </tr>
    <tr>
        <td colspan='7' style='border: 1px solid #333333;'>
            Итого: <?=$products_info['to_be_paid']?> руб.
        </td>
    </tr>
    <?php if(!empty($comment)):?>
        <tr>
            <td  style='border: 1px solid #333333;'>Комментарий</td>
            <td colspan='6' style='border: 1px solid #333333;'><?=$comment?></td>
        </tr>
    <?php endif;?>
</table>


