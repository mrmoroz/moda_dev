<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 28.02.2017
 * Time: 11:20
 */
use yii\helpers\Html;


echo Html::a('Для смены пароля перейдите по этой ссылке',Yii::$app->urlManager->createAbsoluteUrl(['/site/reset-password','key'=>$user->secret_key]));