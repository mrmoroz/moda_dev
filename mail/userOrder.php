<h2>Здравствуйте, <?=$profile->last_name." ".$profile->first_name;?></h2>
<p>Ваш заказ №<?=$id_order?> от <?=date("d-m-Y")?> принят для обработки в службу доставки нашего интернет-магазина. Спасибо за Ваш выбор!</p>
<p>С уважением, "Beauti-full"</p>
<table style="width: 100%; border: 1px solid #333333; border-collapse: collapse; margin-bottom: 30px;">
    <tr>
        <th style='border: 1px solid #333333;'>Изделие</th>
        <th style='border: 1px solid #333333;'>Артикул</th>
        <th style='border: 1px solid #333333;'>Размер</th>
        <th style='border: 1px solid #333333;'>Цвет</th>
        <th style='border: 1px solid #333333;'>Кол-во</th>
        <th style='border: 1px solid #333333;'>Цена</th>
        <th style='border: 1px solid #333333;'>Цена со скидкой</th>
    </tr>
    <?php foreach ($prod as $key=>$item):?>
        <tr>
            <td style='border: 1px solid #333333;'><?=$item['prod']->title;?></td>
            <td style='border: 1px solid #333333;'><?=$item['prod']->article;?></td>
            <td style='border: 1px solid #333333;'>
                <?php if(!empty($item['size_y']) || !empty($item['size_n'])):?>
                    <?=$item['size']->name;?>
                <?php else:?>
                    нет в наличии
                <?php endif;?>
            </td>
            <td style='border: 1px solid #333333;'><?=$item['color']->name?></td>
            <td style='border: 1px solid #333333;'><?=$item['qty']?></td>
            <td style='border: 1px solid #333333;'><?=$item['prod']->price?></td>
            <td style='border: 1px solid #333333;'><?=$item['prod']->discount_price?></td>
        </tr>
        <?php if(isset($item['gifts'])):?>
            <?php foreach ($item['gifts'] as $igf):?>
                <tr>
                    <td style='border: 1px solid #333333;'><?=$igf['prod']->title?> (подарок)</td>
                    <td style='border: 1px solid #333333;'><?=$igf['prod']->article?></td>
                    <td style='border: 1px solid #333333;'>-</td>
                    <td style='border: 1px solid #333333;'>-</td>
                    <td style='border: 1px solid #333333;'><?=$item['qty']?></td>
                    <td style='border: 1px solid #333333;'>0</td>
                    <td style='border: 1px solid #333333;'>0</td>
                </tr>
            <?php endforeach;?>    
        <?php endif;?>
    <?php endforeach;?>
    <tr>
        <td colspan='7' style='border: 1px solid #333333;'>
            Скидка <?=$products_info['ecomomy']?> руб.
        </td>
    </tr>
    <tr>
        <td colspan='7' style='border: 1px solid #333333;'>
            Итого: <?=$products_info['to_be_paid']?> руб.
        </td>
    </tr>
    <?php if(!empty($comment)):?>
        <tr>
            <td  style='border: 1px solid #333333;'>Ваш комментарий</td>
            <td colspan='6' style='border: 1px solid #333333;'><?=$comment?></td>
        </tr>
    <?php endif;?>
</table>


<p>В случае возникновения вопросов свяжитесь с Администратором нашего магазина: <br> - по телефону: 8-800-234-04-93 c 9-00 до 21-00 (Новосибирск); <br> - по электронной почте: <a href=\"mailto:beauti-full@mail.ru\"> beauti-full@mail.ru </a>; <br> - через Skype, наш логин 'bfull.ru'; <br> - по ICQ 640438667; <br> - присоединившись в нашу группу <a href='http://www.odnoklassniki.ru/group/50664980021470'> Женская одежда Beauti-Full. Интернет-магазин.Клуб </a> на сайте Одноклассники.ru.</p>
