<?php

use yii\db\Migration;

/**
 * Handles the creation of table `opt_orders`.
 */
class m170807_065131_create_opt_orders_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('opt_orders', [
            'id' => $this->primaryKey(),
            'user_id'=>$this->integer(11),
            'cdate'=>$this->dateTime(),
            'total_qty'=>$this->integer(11),
            'total_weight'=>$this->money(10,2),
            'total_cost'=>$this->money(10,2),
            'deliv_cost'=>$this->money(10,2),
            'discount'=>$this->money(10,2),
            'off_ac'=>$this->money(10,2),
            'to_be_paid'=>$this->money(10,2),
            'status'=>$this->integer(11)->defaultValue(13),
            'region'=>$this->integer(11),
            'city'=>$this->string(255),
            'zip'=>$this->string(255),
            'city_post'=>$this->integer(11),
            'fio'=>$this->string(255),
            'id_post'=>$this->string(255),
            'ip'=>$this->string(255),
            'first'=>$this->integer(11)->defaultValue(0),
            'g_first'=>$this->integer(11)->defaultValue(0),
            'comment'=>$this->text(),
            'comment_k'=>$this->text(),
            'note'=>$this->text(),
            'manager'=>$this->integer(11),
            'planed'=>$this->integer(11),
            'call_date'=>$this->dateTime(),
            'pos'=>$this->integer(2)->defaultValue(1),
            'visible'=>$this->integer(2)->defaultValue(1),
            'sys_language'=>$this->integer(11)->defaultValue(1)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('opt_orders');
    }
}
