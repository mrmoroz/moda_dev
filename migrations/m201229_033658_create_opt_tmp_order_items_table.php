<?php

use yii\db\Migration;

/**
 * Handles the creation of table `opt_tmp_order_items`.
 */
class m201229_033658_create_opt_tmp_order_items_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('opt_tmp_order_items', [
            'id' => $this->primaryKey(),
            'order_id'=>$this->integer(),
            'product_id'=>$this->integer(),
            'name'=>$this->string(),
            'article'=>$this->string(),
            'size_y'=>$this->string(),
            'size_n'=>$this->string(),
            'color'=>$this->string(),
            'price'=>$this->money(10,2),
            'new_price'=>$this->money(10,2),
            'qty'=>$this->integer(),
            'send'=>$this->integer()->defaultValue(0),
            'not_in_stock'=>$this->integer()->defaultValue(0),
            'gift'=>$this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('opt_tmp_order_items');
    }
}
