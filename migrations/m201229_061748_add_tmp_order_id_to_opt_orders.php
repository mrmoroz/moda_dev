<?php

use yii\db\Migration;

class m201229_061748_add_tmp_order_id_to_opt_orders extends Migration
{
    public function up()
    {
        $this->addColumn(\app\modules\cart\models\OptOrders::tableName(), 'tmp_order_id', $this->integer()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn(\app\modules\cart\models\OptOrders::className(), 'tmp_order_id');
    }

}
