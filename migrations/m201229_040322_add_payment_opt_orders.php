<?php

use yii\db\Migration;

class m201229_040322_add_payment_opt_orders extends Migration
{
    public function up()
    {
        $this->addColumn(\app\modules\cart\models\OptOrders::tableName(), 'payment', $this->integer()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn(\app\modules\cart\models\OptOrders::className(), 'payment');
    }


}
