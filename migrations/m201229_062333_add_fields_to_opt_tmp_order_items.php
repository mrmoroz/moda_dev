<?php

use yii\db\Migration;

class m201229_062333_add_fields_to_opt_tmp_order_items extends Migration
{
    public function up()
    {
        $this->addColumn(\app\models\OptTmpOrderItems::tableName(), 'stock_id', $this->integer()->defaultValue(0));
        $this->addColumn(\app\models\OptTmpOrderItems::tableName(), 'color_id', $this->integer()->defaultValue(0));
        $this->addColumn(\app\models\OptTmpOrderItems::tableName(), 'size_id', $this->integer()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn(\app\models\OptTmpOrderItems::className(), 'stock_id');
        $this->dropColumn(\app\models\OptTmpOrderItems::className(), 'color_id');
        $this->dropColumn(\app\models\OptTmpOrderItems::className(), 'size_id');
    }
}
