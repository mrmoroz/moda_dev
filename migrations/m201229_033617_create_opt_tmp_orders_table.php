<?php

use yii\db\Migration;

/**
 * Handles the creation of table `opt_tmp_orders`.
 */
class m201229_033617_create_opt_tmp_orders_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('opt_tmp_orders', [
            'id' => $this->primaryKey(),
            'user_id'=>$this->integer(),
            'cdate'=>$this->dateTime(),
            'total_qty'=>$this->integer(),
            'total_weight'=>$this->money(10,2),
            'total_cost'=>$this->money(10,2),
            'deliv_cost'=>$this->money(10,2),
            'discount'=>$this->money(10,2),
            'off_ac'=>$this->money(10,2),
            'to_be_paid'=>$this->money(10,2),
            'status'=>$this->integer()->defaultValue(13),
            'region'=>$this->integer(),
            'city'=>$this->string(),
            'zip'=>$this->string(),
            'city_post'=>$this->integer(),
            'fio'=>$this->text(),
            'id_post'=>$this->string(),
            'ip'=>$this->string(),
            'first'=>$this->integer()->defaultValue(0),
            'g_first'=>$this->integer()->defaultValue(0),
            'comment'=>$this->text(),
            'comment_k'=>$this->text(),
            'note'=>$this->text(),
            'manager'=>$this->integer(),
            'planed'=>$this->integer(),
            'call_date'=>$this->dateTime(),
            'pos'=>$this->integer()->defaultValue(1),
            'visible'=>$this->integer()->defaultValue(1),
            'sys_language'=>$this->integer()->defaultValue(1),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('opt_tmp_orders');
    }
}
