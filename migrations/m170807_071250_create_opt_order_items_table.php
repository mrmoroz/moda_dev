<?php

use yii\db\Migration;

/**
 * Handles the creation of table `opt_order_items`.
 */
class m170807_071250_create_opt_order_items_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('opt_order_items', [
            'id' => $this->primaryKey(),
            'order_id'=>$this->integer(11),
            'product_id'=>$this->integer(11),
            'name'=>$this->string(255),
            'article'=>$this->string(255),
            'size_y'=>$this->string(255),
            'size_n'=>$this->string(255),
            'color'=>$this->string(255),
            'price'=>$this->money(10,2),
            'new_price'=>$this->money(10,2),
            'qty'=>$this->integer(11),
            'send'=>$this->integer(10)->defaultValue(0),
            'not_in_stock'=>$this->integer(11)->defaultValue(0),
            'gift'=>$this->integer(11)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('opt_order_items');
    }
}
