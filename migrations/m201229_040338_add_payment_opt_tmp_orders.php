<?php

use yii\db\Migration;

class m201229_040338_add_payment_opt_tmp_orders extends Migration
{
    public function up()
    {
        $this->addColumn(\app\models\OptTmpOrders::tableName(), 'payment', $this->integer()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn(\app\models\OptTmpOrders::className(), 'payment');
    }

}
