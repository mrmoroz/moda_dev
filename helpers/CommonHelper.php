<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 02.12.2020
 * Time: 14:29
 */

namespace app\helpers;


use app\models\MlCfg;
use app\models\Parametrs;
use Yii;

class CommonHelper
{
    public static function SortSizes($sizes)
    {
        $numbers = [];
        $chars_numbers = [];
        $chars = [];
        if(count($sizes)>0) {
            foreach ($sizes as $k => $it) {
                if ( strpos($it, '/') != false || strpos($it, '-') != false ) {
                    continue;
                }
                if (ctype_digit($it)) {
                    $numbers[$k] = $it;
                } elseif (preg_match('/^[^0-9][a-zA-Zа-яА-Я]{0,}/ui', str_replace(' ', '', $it))) {
                    $chars[$k] = $it;
                } else {
                    $chars_numbers[$k] = $it;
                }
            }

            $numbers = CommonHelper::digitSort($numbers);
            $chars = CommonHelper::digitWordSort($chars);
            $chars_numbers = CommonHelper::digitWordSort2($chars_numbers);
        }
        return $numbers+$chars_numbers+$chars;
    }

    public static function digitSort($_arr)
    {
        if(count($_arr)>0) {
            uasort($_arr, function ($a, $b) {
                $res = 0;
                // сравниваем числа
                $inta = intval($a);
                $intb = intval($b);
                if ($inta != $intb) {
                    return ($inta > $intb) ? 1 : -1;
                }
                return $res;
            });
        }
        return $_arr;
    }

    public static function digitWordSort($_arr)
    {
        if(count($_arr)>0) {
            uasort($_arr, function ($a, $b) {
                $res = 0;

                // сравниваем числа
                $inta = intval($a);
                $intb = intval($b);
                if ($inta != $intb) {
                    return ($inta > $intb) ? 1 : -1;
                }

                // сравниваем буквы
                $var1 = preg_replace('~[0-9]+~', '', $a);
                $var2 = preg_replace('~[0-9]+~', '', $b);
                $compare = strcmp($var1, $var2); // А считается меньше Б
                if ($compare !== 0) {
                    return ($compare > 0) ? 1 : -1;
                }

                return $res;
            });
        }

        return $_arr;
    }

    public static function digitWordSort2($_arr)
    {
        if(count($_arr)>0) {
            uasort($_arr, function ($a, $b) {
                $res = 0;
                $lenA = mb_strlen(str_replace(' ','', $a), 'UTF-8');
                $lenB = mb_strlen(str_replace(' ','', $b), 'UTF-8');
                if ($lenA != $lenB) {
                    return ($lenA > $lenB) ? 1 : -1;
                }

                // сравниваем числа
                $inta = intval($a);
                $intb = intval($b);
                if ($inta != $intb) {
                    return ($inta > $intb) ? 1 : -1;
                }

                $var1 = preg_replace('~[0-9]+~', '', $a);
                $var2 = preg_replace('~[0-9]+~', '', $b);
                $compare = strcmp($var1, $var2); // А считается меньше Б
                if ($compare !== 0) {
                    return ($compare > 0) ? 1 : -1;
                }

                return $res;
            });
        }

        return $_arr;
    }

    public static function sendEmails($prod, $user, $order)
    {

        $tpl = MlCfg::findOne(['mnemo'=>'order_user_opt']);
        $tpl_adm = MlCfg::findOne(['mnemo'=>'order_admin_opt']);
        $adminEmails = Parametrs::findOne(2);
        $user_data = "";
        $product_table = self::prodTable($prod);
        $message = str_replace([
            '{name}',
            '{name2}',
            '{order_id}',
            '{order_date}',
            '{prod_table}'
        ],
            [
                $user->f_name,
                $user->l_name,
                $order->id,
                $order->cdate,
                $product_table
            ],
            $tpl->ebody_html
        );

        $user_data.="<table style='width: 100%; border: 1px solid #333333; border-collapse: collapse; margin-bottom: 30px;'>
                            <tr>
                                <th style='border: 1px solid #333333;'>Город</th>
                                <th style='border: 1px solid #333333;'>Электронная почта</th>
                                <th style='border: 1px solid #333333;'>Фамилия</th>
                                <th style='border: 1px solid #333333;'>Имя</th>
                                <th style='border: 1px solid #333333;'>Отчество</th>
                                <th style='border: 1px solid #333333;'>Телефон</th>
                            </tr>
                            <tr>
                                <td style='border: 1px solid #333333;'>".$user->city_name."</td>
                                <td style='border: 1px solid #333333;'>".$user->email."</td>
                                <td style='border: 1px solid #333333;'>".$user->l_name."</td>
                                <td style='border: 1px solid #333333;'>".$user->f_name."</td>
                                <td style='border: 1px solid #333333;'>".$user->t_name."</td>
                                <td style='border: 1px solid #333333;'>".$user->phone."</td>
                            </tr>
                        </table>";

        $message_adm = str_replace([
            '{user_data}',
            '{order_id}',
            '{order_date}',
            '{prod_table}'
        ],
            [
                $user_data,
                $order->id,
                $order->cdate,
                $product_table
            ],
            $tpl_adm->ebody_html
        );
        if($adminEmails && !empty($adminEmails->value)) {
            $emails = explode(',', $adminEmails->value);

            Yii::$app->mailer->compose()
                ->setFrom([Yii::$app->params['supportEmail'] => 'Отправлено с сайта moda-optom.ru'])
                ->setTo($emails)
                ->setSubject($tpl_adm->esubj)
                ->setHtmlBody($message_adm)
                ->send();

        }

        Yii::$app->mailer->compose()
            ->setFrom([Yii::$app->params['supportEmail'] => 'Отправлено с сайта moda-optom.ru'])
            ->setTo($user->email)
            ->setSubject($tpl->esubj)
            ->setHtmlBody($message)
            ->send();
    }

    public static function prodTable($prod){
        $date = date("d.m.Y");
        $prod_table = "";
        $itogo = 0;
        if($prod){
            $prod_table .= "<table style=\"width: 100%; border: 1px solid #333333; border-collapse: collapse; margin-bottom: 30px;\">
                            <tr>
                                <th style='border: 1px solid #333333;'>Изделие</th>
                                <th style='border: 1px solid #333333;'>Артикул</th>
                                <th style='border: 1px solid #333333;'>Размер</th>
                                <th style='border: 1px solid #333333;'>Цвет</th>
                                <th style='border: 1px solid #333333;'>Кол-во</th>
                                <th style='border: 1px solid #333333;'>Цена, руб.</th>
                                <th style='border: 1px solid #333333;'>Цена со скидкой, руб.</th>
                            </tr>";
            foreach ($prod as $key => $item) {
                $prod_table .= "<tr>";
                $prod_table .= "<td style='border: 1px solid #333333;'>" . $item['product']->title . "</td>";
                $prod_table .= "<td style='border: 1px solid #333333;'>" . $item['product']->article . "</td>";
                if (!empty($item['size'])) {
                    $size = $item['size']->name;
                } else {
                    $size = "нет в наличии";
                }
                $prod_table .= "<td style='border: 1px solid #333333;'>" . $size . "</td>";
                $prod_table .= "<td style='border: 1px solid #333333;'>" . $item['color'] . "</td>";
                $prod_table .= "<td style='border: 1px solid #333333;'>" . $item['cnt'] . "</td>";
                $prod_table .= "<td style='border: 1px solid #333333;'>" . $item['product']->wholesale_price . "</td>";
                $prod_table .= "<td style='border: 1px solid #333333;'>" . $item['product']->wholesale_discount_price . "</td>";
                $prod_table .= "</tr>";
                if(!empty($item['product']->wholesale_discount_price)){
                    $itogo += $item['product']->wholesale_discount_price * $item['cnt'];
                }else{
                    $itogo += $item['product']->wholesale_price * $item['cnt'];
                }
            }


            $prod_table .= "<tr>";
            $prod_table .= "<td colspan='7' style='border: 1px solid #333333;'>Итого: " . $itogo . " руб.</td>";
            $prod_table .= "</tr>";
        }
        return $prod_table;
    }

    public static function clearStr($str){
        return addslashes(strip_tags(trim($str)));
    }

    public static function abcImg($img)
    {
        if(stripos($img, 'https://')===false){
            return 'https://beauti-full.ru'.$img;
        }
        return $img;
    }
}