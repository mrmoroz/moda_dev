<?php
namespace app\modules\cart\components;

class Helper
{
    public static function getAddressRPOperation($historyData)
    {
        $addressArray = explode(' ', $historyData->operation_address_description);
        if (count($addressArray) > 1) {
            $last = array_pop($addressArray);
            if (is_numeric($last)) {
                return implode(' ', $addressArray);
            }
        }
        return $historyData->operation_address_description;

    }

    public static function getTitleRPOperation($historyData)
    {
        if ($historyData->operation_type_id == 1) {
            return 'Принято в отделении связи';
        } else if ($historyData->operation_type_id == 8) {
            return $historyData->operation_attr_name;
        } else {
            return ($historyData->operation_attr_name) ? $historyData->operation_type_name . ' / ' . $historyData->operation_attr_name : $historyData->operation_type_name;
        }
    }

    public static function getIndexRPOperation($historyData)
    {
        return $historyData->operation_address_index;
    }
}