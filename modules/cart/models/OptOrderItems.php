<?php

namespace app\modules\cart\models;

use Yii;

/**
 * This is the model class for table "opt_order_items".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $product_id
 * @property string $name
 * @property string $article
 * @property string $size_y
 * @property string $size_n
 * @property string $color
 * @property string $price
 * @property string $new_price
 * @property integer $qty
 * @property integer $send
 * @property integer $not_in_stock
 * @property integer $gift
 */
class OptOrderItems extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'opt_order_items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'product_id', 'qty', 'send', 'not_in_stock', 'gift'], 'integer'],
            [['price', 'new_price'], 'number'],
            [['name', 'article', 'size_y', 'size_n', 'color'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'product_id' => 'Product ID',
            'name' => 'Name',
            'article' => 'Article',
            'size_y' => 'Size Y',
            'size_n' => 'Size N',
            'color' => 'Color',
            'price' => 'Price',
            'new_price' => 'New Price',
            'qty' => 'Qty',
            'send' => 'Send',
            'not_in_stock' => 'Not In Stock',
            'gift' => 'Gift',
        ];
    }

    public static function create($article, $color, $name, $new_price, $price, $qty, $order_id, $product_id, $size_y, $size_n)
    {
        $orderItem = new OptOrderItems();
        $orderItem->article = $article;
        $orderItem->color = $color;
        $orderItem->name = $name;
        $orderItem->new_price = $new_price;
        $orderItem->price = $price;
        $orderItem->qty = $qty;
        $orderItem->order_id = $order_id;
        $orderItem->product_id = $product_id;
        $orderItem->size_y = $size_y;
        $orderItem->size_n = $size_n;

        return $orderItem;
    }
}
