<?php

namespace app\modules\cart\models;

use Yii;

/**
 * This is the model class for table "deliv_list".
 *
 * @property integer $id
 * @property string $code
 * @property string $title
 * @property string $descript
 * @property integer $pos
 * @property integer $visible
 * @property integer $sys_language
 */
class DelivList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'deliv_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['descript'], 'string'],
            [['pos', 'visible', 'sys_language'], 'integer'],
            [['code', 'title'], 'string', 'max' => 255],
            [['price'],'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Код',
            'title' => 'Название',
            'descript' => 'Описание',
            'pos' => 'Позиция',
            'visible' => 'Видимость',
            'sys_language' => 'Sys Language',
            'price'=>'Стоимость'
        ];
    }
}
