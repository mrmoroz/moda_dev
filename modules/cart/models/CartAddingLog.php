<?php

namespace app\modules\cart\models;

use app\models\Product;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "cart_adding_log".
 *
 * @property integer $product_id
 * @property integer $user_id
 * @property string $cart_token
 * @property string $add_date
 */
class CartAddingLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cart_adding_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'user_id'], 'integer'],
            [['cart_token'], 'string'],
            [['add_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product ID',
            'user_id' => 'User ID',
            'cart_token' => 'Cart Token',
            'add_date' => 'Add Date',
        ];
    }

    public static function checkProduct($productId, $isGuest, $cartToken)
    {
        $query = self::find()->where(['product_id' => $productId]);

        if ($isGuest) {
            $query = $query->andWhere(['cart_token' => $cartToken]);
        } else {
            $query = $query->andWhere(['user_id' => $cartToken]);
        }

        return $query->count();
    }

    /**
     * @param $addCategories
     * @param null $date
     * @param bool $isBefore
     * @return array Возвращает количество добавлений товара в корзину с учетом его дубликатов
     *
     * Возвращает количество добавлений товара в корзину с учетом его дубликатов
     */
    public static function getProductCartAdding($addCategories, $date = null, $isBefore = true, $productId)
    {
        $productIdAdd = '';

        if ($productId) {
            $productIdAdd = 'AND P.id = ' . $productId;
        }

        if ($date) {
            if ($isBefore) {
                $addDate = "CL.add_date <= to_timestamp({$date})";
            } else {
                $addDate = "CL.add_date > to_timestamp({$date})";
            }
        } else {
            $addDate = "CL.add_date > to_timestamp(0000-00-00)";
        }

        $productsCartAddingCount = ArrayHelper::map(self::findBySql("SELECT P.id AS product_id, COUNT(CL.product_id) as qty
                                             FROM cart_adding_log CL
                                             JOIN products P ON CL.product_id = P.id
                                             WHERE P.category_id {$addCategories} 
                                             AND {$addDate}
                                             {$productIdAdd}
                                             GROUP BY P.id")
            ->asArray()->all(), 'product_id', 'qty');

        $productsCartAddingCountDuplicates =  ArrayHelper::map(self::findBySql("SELECT P.parent_product_id AS product_id, COUNT(CL.product_id) as qty
                                                   FROM cart_adding_log CL
                                                   JOIN products P ON CL.product_id = P.id
                                                   WHERE P.parent_product_id IN (
                                                      SELECT id FROM products WHERE category_id {$addCategories}
                                                   )
                                                   AND {$addDate}
                                                   GROUP BY P.parent_product_id")->asArray()->all(), 'product_id', 'qty');

        $parentIds = Product::getParentIds($addCategories);

        if (!empty($parentIds)) {
            $parentsCartAddingCount = ArrayHelper::map(self::findBySql("SELECT CP.id AS child_id, COUNT(CL.product_id) as qty
                                                                    FROM cart_adding_log CL
                                                                    JOIN products P ON CL.product_id = P.id
                                                                    JOIN products CP ON P.id = CP.parent_product_id
                                                                    WHERE {$addDate}
                                                                    GROUP BY P.id, CP.id")->where(['IN', 'P.id', $parentIds])
                ->asArray()->all(), 'child_id', 'qty');
        }

        if (!empty($parentsCartAddingCount)) {
            foreach ($parentsCartAddingCount as $childId => $parentQuantity) {
                if (isset($productsCartAddingCount[$childId])) {
                    $productsCartAddingCount[$childId] += $parentQuantity;
                }
            }
        }

        foreach ($productsCartAddingCountDuplicates as $productId => $duplicateQuantity) {
            if (isset($productsCartAddingCount[$productId])) {
                $productsCartAddingCount[$productId] += $duplicateQuantity;
            }
        }

        unset($productsCartAddingCountDuplicates);
        unset($parentIds);
        unset($parentsCartAddingCount);

        return $productsCartAddingCount;
    }
}
