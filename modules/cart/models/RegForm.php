<?php
/**
 * Регистрация в заказе
 * User: max
 * Date: 17.05.17
 * Time: 10:29
 */

namespace app\modules\cart\models;

use Yii;
use yii\base\Model;
use app\models\User;
use app\models\UserProfile;

class RegForm extends Model
{
    public $login;
    public $password;
    public $password_confirm;
    public $first_name;

    public function rules()
    {
        return [
            [['first_name', 'login', 'password', 'password_confirm'], 'required'],
            ['login','email'],
            ['login', 'unique', 'targetClass'=> 'app\models\User'],
            ['password_confirm', 'compare', 'compareAttribute' => 'password', 'message' => 'Неверное подтверждение пароля'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'login' => 'E-mail',
            'password' => 'Пароль',
            'password_confirm' => 'Повторите пароль',
            'first_name' => 'Ваше имя'
        ];
    }

    public function reguser(){
        $password = $this->password;
        $modelUser = new User();
        $modelUser->login = $this->login;
        $modelUser->setPassword($password);
        $modelUser->generateAuthKey();
        $modelUser->status = 1;
        $modelUser->created_at = $modelUser->updated_at = (new \DateTime())->format('Y-m-d H:i:s');
        if ($modelUser->save()) {
            Yii::$app->authManager->assign(Yii::$app->authManager->getRole('User'), $modelUser->id);
            $profile = new UserProfile();
            $profile->first_name = $this->first_name;
            $profile->user_id = $modelUser->id;
            $profile->save(false);
            Yii::$app->user->login($modelUser);
            //$this->sendEmails($password,$modelUser);
        }
    }


}