<?php
namespace app\modules\cart\models;
use app\models\Country;
use app\models\PaymentList;
use app\models\ProductSelection;
use app\models\User;
use app\models\UserAddress;
use app\models\UserProfile;
use app\modules\cart\models\Orders;
use app\modules\cart\models\OrderItems;
use app\models\Product;
use app\models\Color;
use app\models\Size;
use app\models\Stock;
use app\models\StockPosition;
use app\models\CabEmail;
use app\modules\cart\models\Cart;
use Yii;
use yii\base\Model;
use app\models\Parametrs;
use yii\helpers\ArrayHelper;
use app\models\ProductChangelog;
use app\models\ProductSizeChangelog;
use app\modules\admin\models\MlCfg;
use app\components\SMSMessage;
use app\modules\cart\models\LogAc;

class CheckoutForm extends Model
{
    public $mobile_phone;
    public $address;
    public $country;
    public $region_id;
    public $city;
    public $street;
    public $house;
    public $post_index;
    public $flat;
    public $delivery;
    public $first_name;
    public $last_name;
    public $email;
    public $ignore_mail_list = true;
    public $comment;
    public $promocode;
    public $pay_method_list;
    public $log_data = "";
    public $middle_name;


    public function rules()
    {
        return [
            [['mobile_phone','city','street','house','post_index','first_name','last_name','country'], 'required'],
            ['middle_name', 'required', 'on'=>'middlename'],
            ['email','email'],
            [['middle_name','log_data','region_id','address','delivery','ignore_mail_list','comment','promocode','pay_method_list','flat'],'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'mobile_phone' => 'Мобильный телефон',
            'address'=>'Ваши адреса',
            'city'=>'Город',
            'street'=>'Улица',
            'house'=>'Дом',
            'flat'=>'Квартира',
            'post_index'=>'Почтовый индекс',
            'last_name' => 'Фамилия',
            'first_name' => 'Имя',
            'email' => 'Электронная почта',
            'country'=>'Страна',
            'region_id'=>'Регион',
            'middle_name'=>'Отчество'
        ];
    }


    public function saveOrder($ac){
        //Работа с адресом
        $address = $this->getSelectedAdress();
        //метод доставки
        $delivery_method = $this->attributes['delivery'];
        //способ платежа
        $pay_method = PaymentList::findOne($this->attributes['pay_method_list']);
        $del_method = DelivList::findOne(['code'=>$delivery_method]);
        //первый платеж или нет
        $first_payment = $this->checkPayments();
        //обработка товаров
        //лог товаров
        $this->logProducts($address->country_id);
        $products_info = $this->getProductsInfo($pay_method,$address->country_id,$ac);
        //сохранение заказа в бд
        $id_order = $this->saveOrderInBD($address,$delivery_method,$pay_method,$first_payment,$products_info);
        if($id_order) {
            //сохранение продуктов к заказу
            $prod = $this->saveProductItems($id_order);
            //отправка писем
            if ($prod) {
                $order = Orders::findOne($id_order);
                $profile = UserProfile::findOne(['user_id'=>$order->user_id]);
                if(isset($profile->mobile_phone) && empty($profile->mobile_phone)){
                    $profile->mobile_phone = $this->attributes['mobile_phone'];
                    $profile->save(false);
                }
                $this->sendEmailUser($prod, Yii::$app->user->getIdentity(), $id_order, $profile, $products_info, $this->attributes['comment'],$address);
                $this->sendEmailAdmin($del_method, $pay_method, $this->attributes['mobile_phone'], $prod, Yii::$app->user->getIdentity(), $id_order, $profile, $products_info, $this->attributes['comment'], $address);
                //отправка sms
                $smsMessage = new SMSMessage();
                $smsMessage->sendSMS(1, $profile, $order);
                //проверка складов
                $this->checkStocks($prod,$id_order);
                //Комментарий к заказу на служебную почту пользователя
                if (!empty($this->attributes['comment'])) {
                    $mCabEmail = new CabEmail();
                    $mCabEmail->page_id = 0;
                    $mCabEmail->user_id = Yii::$app->user->id;
                    $mCabEmail->cdate = date("Y-m-d H:i:s");
                    $mCabEmail->message = "Комментарий к заказу $id_order: ".$this->attributes['comment'];
                    $mCabEmail->from11 = 1;
                    $mCabEmail->visible = 1;
                    $mCabEmail->sys_language = 1;
                    $mCabEmail->save(false);
                }
                //Удаление корзины из БД
                $cartModel = new  Cart();
                $cartModel->removeCart();
                //удаление сессии и перенаправление
                if (isset($_SESSION['cart'])) {
                    unset($_SESSION['cart']);
                }
                return $id_order;
            }
        }
        return false;
    }

    public function logProducts($country){
        $userDta = User::findOne(Yii::$app->user->id);
        $prodPriceSumm = 0;
        foreach ($_SESSION['cart'] as $k=>$it){
            if($k!='allqty' && $k!='sum' && $k!='allsum') {
                $prod = Product::findOne($it['id']);
                $this->log_data .= '<b>Товар- ' . $prod->article . '</b>. Цена: ' . $prod->price .' '.$this->getManySymbol($country) .' / Цена новая:' . $prod->discount_price .' '.$this->getManySymbol($country). ' Размер скидки (без производителя): ' . $prod->discount . '%<br>';
                $prodPriceWithDiscount = round(($prod->discount_price) ? $prod->discount_price : $prod->price);
                $this->log_data .= '- Выбрана цена: ' . $prodPriceWithDiscount .' '.$this->getManySymbol($country). '<br>';
                if ((int)$prodPriceWithDiscount == (int)$prod->price) {
                    $prodPriceWithDiscount = $prodPriceWithDiscount - round($prodPriceWithDiscount * $userDta->discount_user / 100);
                    $this->log_data .= '- Персональная скидка:' . $userDta->discount_user . '%. Цена с персональной скидкой: ' . $prodPriceWithDiscount .' '.$this->getManySymbol($country). '<br>';
                }
                $this->log_data .= '- Расчет итоговой стоимости корзины: текущая сумма: ' . $prodPriceSumm .' '.$this->getManySymbol($country). ' Цена на товар: ' . $prodPriceWithDiscount .' '.$this->getManySymbol($country). ' Количество товаров: ' . $it['qty'] . '<br>';
                $prodPriceSumm += $prodPriceWithDiscount * $it['qty'];
                $this->log_data .= '-- Новая стоимость корзины: ' . $prodPriceSumm .' '.$this->getManySymbol($country). '<br>';
            }
        }
    }

    public function getManySymbol($country){
        if($country){
            if($country==80){
                return "т.";
            }
        }
        return "р.";
    }

    /**
     * Проверка доступности товара на складах
     * @param $prod
     * @throws \yii\db\Exception
     */
    private function checkStocks($prod,$id_order){
        foreach ($prod as $it){
            $sql = "SELECT stock_position.id FROM stock_position
                        WHERE stock_position.product_id = '{$it['prod']->id}'
                        AND stock_position.quantity > 0
            ";
            $stocks = Yii::$app->db->createCommand($sql)->queryAll();
            if(count($stocks) == 0){
                $newChangeLog = new ProductChangelog([
                    'type' => 1,
                    'product_id' => $it['prod']->id,
                    'operation_date' => date('Y-m-d H:i:s'),
                    'order_id' => $id_order
                ]);
                $newChangeLog->save();

                Yii::$app->db->createCommand("UPDATE products SET is_active = false WHERE id='{$it['prod']->id}'")->execute();
                ProductSelection::clearQueryCacheByProductId($it['prod']->id);
            }
        }
    }

    /**
     * Отправка письма о заказе покупателю
     * @param $prod
     * @param $user
     * @param $id_order
     * @param $profile
     * @param $products_info
     * @param $comment
     */
    public function sendEmailUser($prod, $user, $id_order, $profile,$products_info,$comment,$address){
        $tpl = MlCfg::findOne(['mnemo'=>'order_user']);
        $message = $this->setUserMessage($prod, $user, $id_order, $profile, $products_info, $comment, $tpl,$address);
        
        $validator = new yii\validators\EmailValidator();
        if ($validator->validate($user->login, $error)) {
            if($tpl->etype=='1') {
                Yii::$app->mailer->compose()
                    ->setFrom([Yii::$app->params['supportEmail'] => 'Отправлено с сайта beauti-full.ru'])
                    ->setTo($user->login)
                    ->setSubject($tpl->esubj)
                    ->setHtmlBody($message)
                    ->send();
            }
        } else {
            if(!empty($this->attributes['email'])){
                if($tpl->etype=='1') {
                    Yii::$app->mailer->compose()
                        ->setFrom([Yii::$app->params['supportEmail'] => 'Отправлено с сайта beauti-full.ru'])
                        ->setTo($this->attributes['email'])
                        ->setSubject($tpl->esubj)
                        ->setHtmlBody($message)
                        ->send();
                }
            }
        }
    }
    
    private function setUserMessage($prod, $user, $id_order, $profile, $products_info, $comment, $tpl,$address)
    {
        $mailForm = $tpl->ebody_html;
        $date = date("d.m.Y");
        $prod_table = "";

        if($prod){
            $prod_table.="<table style=\"width: 100%; border: 1px solid #333333; border-collapse: collapse; margin-bottom: 30px;\">
                                <tr>
                                    <th style='border: 1px solid #333333;'>Изделие</th>
                                    <th style='border: 1px solid #333333;'>Артикул</th>
                                    <th style='border: 1px solid #333333;'>Размер</th>
                                    <th style='border: 1px solid #333333;'>Цвет</th>
                                    <th style='border: 1px solid #333333;'>Кол-во</th>
                                    <th style='border: 1px solid #333333;'>Цена</th>
                                    <th style='border: 1px solid #333333;'>Цена со скидкой</th>
                                </tr>";
            foreach ($prod as $key=>$item){
                $prod_table.="<tr>";
                $prod_table.="<td style='border: 1px solid #333333;'>".$item['prod']->title."</td>";
                $prod_table.="<td style='border: 1px solid #333333;'>".$item['prod']->article."</td>";
                if(!empty($item['size_y']) || !empty($item['size_n'])){
                    $size = $item['size']->name;
                }else{
                    $size = "нет в наличии";
                }
                $prod_table.="<td style='border: 1px solid #333333;'>".$size."</td>";
                $prod_table.="<td style='border: 1px solid #333333;'>".$item['color']->name."</td>";
                $prod_table.="<td style='border: 1px solid #333333;'>".$item['qty']."</td>";
                $prod_table.="<td style='border: 1px solid #333333;'>".$item['prod']->price."</td>";
                $prod_table.="<td style='border: 1px solid #333333;'>".$item['prod']->discount_price."</td>";
                $prod_table.="</tr>";
                if(isset($item['gifts'])){
                    foreach ($item['gifts'] as $igf){
                        $prod_table.="<tr>";
                        $prod_table.="<td style='border: 1px solid #333333;'>".$igf['prod']->title." (подарок)</td>";
                        $prod_table.="<td style='border: 1px solid #333333;'>".$igf['prod']->article."</td>";
                        $prod_table.="<td style='border: 1px solid #333333;'>-</td>";
                        $prod_table.="<td style='border: 1px solid #333333;'>-</td>";
                        $prod_table.="<td style='border: 1px solid #333333;'>".$item['qty']."</td>";
                        $prod_table.="<td style='border: 1px solid #333333;'>0</td>";
                        $prod_table.="<td style='border: 1px solid #333333;'>0</td>";
                        $prod_table.="</tr>";
                    }
                }
            }
            $prod_table.="<tr>";
            $prod_table.="<td colspan='7' style='border: 1px solid #333333;'>Скидка ".$products_info['ecomomy']." руб.</td>";
            $prod_table.="</tr>";

            if(isset($products_info['summa_spis']) && (int)$products_info['summa_spis']!=0){
                $prod_table.="<tr>";
                $prod_table.="<td colspan='7' style='border: 1px solid #333333;'>Списано с вашего лицевого счета: ".$products_info['summa_spis']." руб.</td>";
                $prod_table.="</tr>";
            }

            $prod_table.="<tr>";
            $prod_table.="<td colspan='7' style='border: 1px solid #333333;'>Итого: ".$products_info['to_be_paid']." руб.</td>";
            $prod_table.="</tr>";
            if(!empty($comment)){
                $prod_table.="<tr>";
                $prod_table.="<td  style='border: 1px solid #333333;'>Ваш комментарий</td>";
                $prod_table.="<td colspan='6' style='border: 1px solid #333333;'>".$comment."</td>";
                $prod_table.="</tr>";
            }

        }

        $message = str_replace([
                '{name}',
                '{name2}',
                '{order_id}',
                '{order_date}',
                '{prod_table}'
            ],
            [
                $address->surname,
                $address->name,
                $id_order,
                $date,
                $prod_table
            ],
            $mailForm
        );
        return $message;
    }

    /**
     * Отправка письма о заказе администратору
     * @param $del_method
     * @param $pay_method
     * @param $mobile_phone
     * @param $prod
     * @param $user
     * @param $id_order
     * @param $profile
     * @param $products_info
     * @param $comment
     * @param $address
     */
    public function sendEmailAdmin($del_method,$pay_method,$mobile_phone,$prod, $user, $id_order, $profile,$products_info,$comment,$address){
        $tpl = MlCfg::findOne(['mnemo'=>'order_admin']);
        $message = $this->setAdminMessage($del_method,$pay_method,$mobile_phone,$prod, $user, $id_order, $profile,$products_info,$comment,$address, $tpl);
        $adminEmails = Parametrs::findOne(2);
        if($adminEmails && !empty($adminEmails->value)) {
            $emails = explode(',', $adminEmails->value);
            if($tpl->etype=='1') {
                Yii::$app->mailer->compose()
                    ->setFrom([Yii::$app->params['supportEmail'] => 'Отправлено с сайта beauti-full.ru'])
                    ->setTo($emails)
                    ->setSubject($tpl->esubj)
                    ->setHtmlBody($message)
                    ->send();
            }
        }
    }

    private function setAdminMessage($del_method,$pay_method,$mobile_phone,$prod, $user, $id_order, $profile,$products_info,$comment,$address, $tpl)
    {
        $mailForm = $tpl->ebody_html;
        $date = date("d.m.Y");
        $prod_table = "";
        $user_data = "";
        $data = "";

        if($profile){
            $user_data.="<table style='width: 100%; border: 1px solid #333333; border-collapse: collapse; margin-bottom: 30px;'>
                            <tr>
                                <th style='border: 1px solid #333333;'>Адрес доставки</th>
                                <th style='border: 1px solid #333333;'>Электронная почта</th>
                                <th style='border: 1px solid #333333;'>Фамилия</th>
                                <th style='border: 1px solid #333333;'>Имя</th>
                                <th style='border: 1px solid #333333;'>Отчество</th>
                                <th style='border: 1px solid #333333;'>Индекс</th>
                                <th style='border: 1px solid #333333;'>Мобильный телефон</th>
                                <th style='border: 1px solid #333333;'>Телефон</th>
                            </tr>
                            <tr>
                                <td style='border: 1px solid #333333;'>".$address->address."</td>
                                <td style='border: 1px solid #333333;'>".$user->login."</td>
                                <td style='border: 1px solid #333333;'>".$address->surname."</td>
                                <td style='border: 1px solid #333333;'>".$address->name."</td>
                                <td style='border: 1px solid #333333;'>".$address->middle_name."</td>
                                <td style='border: 1px solid #333333;'>".$address->post_index."</td>
                                <td style='border: 1px solid #333333;'>".$mobile_phone."</td>
                                <td style='border: 1px solid #333333;'>".$profile->phone_code." ".$profile->phone."</td>
                            </tr>
                        </table>";
        }

        $data.= "
            <table style='width: 100%; border: 1px solid #333333; border-collapse: collapse; margin-bottom: 30px;'>
                <tr>
                    <th style='border: 1px solid #333333;'>Объем груди</th>
                    <th style='border: 1px solid #333333;'>Объем талии</th>
                    <th style='border: 1px solid #333333;'>Объем бедер</th>
                    <th style='border: 1px solid #333333;'>Рост</th>
                </tr>
                <tr>
                    <td style='border: 1px solid #333333;'>".$profile->bust."</td>
                    <td style='border: 1px solid #333333;'>".$profile->waist."</td>
                    <td style='border: 1px solid #333333;'>".$profile->hip."</td>
                    <td style='border: 1px solid #333333;'>".$profile->grow."</td>
                </tr>
            </table>
        ";

        if($prod){
            $prod_table.="<table style=\"width: 100%; border: 1px solid #333333; border-collapse: collapse; margin-bottom: 30px;\">
                                <tr>
                                    <th style='border: 1px solid #333333;'>Изделие</th>
                                    <th style='border: 1px solid #333333;'>Артикул</th>
                                    <th style='border: 1px solid #333333;'>Размер</th>
                                    <th style='border: 1px solid #333333;'>Цвет</th>
                                    <th style='border: 1px solid #333333;'>Кол-во</th>
                                    <th style='border: 1px solid #333333;'>Цена</th>
                                    <th style='border: 1px solid #333333;'>Цена со скидкой</th>
                                </tr>";
            foreach ($prod as $key=>$item){
                $prod_table.="<tr>";
                $prod_table.="<td style='border: 1px solid #333333;'>".$item['prod']->title."</td>";
                $prod_table.="<td style='border: 1px solid #333333;'>".$item['prod']->article."</td>";
                if(!empty($item['size_y']) || !empty($item['size_n'])){
                    $size = $item['size']->name;
                }else{
                    $size = "нет в наличии";
                }
                $prod_table.="<td style='border: 1px solid #333333;'>".$size."</td>";
                $prod_table.="<td style='border: 1px solid #333333;'>".$item['color']->name."</td>";
                $prod_table.="<td style='border: 1px solid #333333;'>".$item['qty']."</td>";
                $prod_table.="<td style='border: 1px solid #333333;'>".$item['prod']->price."</td>";
                $prod_table.="<td style='border: 1px solid #333333;'>".$item['prod']->discount_price."</td>";
                $prod_table.="</tr>";
                if(isset($item['gifts'])){
                    foreach ($item['gifts'] as $igf){
                        $prod_table.="<tr>";
                        $prod_table.="<td style='border: 1px solid #333333;'>".$igf['prod']->title." (подарок)</td>";
                        $prod_table.="<td style='border: 1px solid #333333;'>".$igf['prod']->article."</td>";
                        $prod_table.="<td style='border: 1px solid #333333;'>-</td>";
                        $prod_table.="<td style='border: 1px solid #333333;'>-</td>";
                        $prod_table.="<td style='border: 1px solid #333333;'>".$item['qty']."</td>";
                        $prod_table.="<td style='border: 1px solid #333333;'>0</td>";
                        $prod_table.="<td style='border: 1px solid #333333;'>0</td>";
                        $prod_table.="</tr>";
                    }
                }
            }
            $prod_table.="<tr>";
            $prod_table.="<td colspan='7' style='border: 1px solid #333333;'>Скидка ".$products_info['ecomomy']." руб.</td>";
            $prod_table.="</tr>";

            if(isset($products_info['summa_spis']) && (int)$products_info['summa_spis']!=0){
                $prod_table.="<tr>";
                $prod_table.="<td colspan='7' style='border: 1px solid #333333;'>Списано с лицевого счета: ".$products_info['summa_spis']." руб.</td>";
                $prod_table.="</tr>";
            }

            $prod_table.="<tr>";
            $prod_table.="<td colspan='7' style='border: 1px solid #333333;'>Итого: ".$products_info['to_be_paid']." руб.</td>";
            $prod_table.="</tr>";
            if(!empty($comment)){
                $prod_table.="<tr>";
                $prod_table.="<td  style='border: 1px solid #333333;'>Ваш комментарий</td>";
                $prod_table.="<td colspan='6' style='border: 1px solid #333333;'>".$comment."</td>";
                $prod_table.="</tr>";
            }

        }

        $message = str_replace([
            '{order_id}',
            '{order_date}',
            '{prod_table}',
            '{user_data}',
            '{data}',
            '{delivery}',
            '{payment}'
        ],
            [
                $id_order,
                $date,
                $prod_table,
                $user_data,
                $data,
                $del_method->title,
                $pay_method->title
            ],
            $mailForm
        );
        return $message;
    }

    /**
     * Сохранение товаров к заказу
     * @param $id_order
     * @return array|bool
     */
    private function saveProductItems($id_order){
        $prod = [];
        $prod_id = [];
        $stocks =  $this->getStocks(); //наши скдады
        $man_stocks = $this->getManStocks(); //склады поставщиков
        foreach ($_SESSION['cart'] as $k=>$it){
            if($k!='allqty' && $k!='sum' && $k!='allsum'){
                $prod[$it['id']]['color'] = Color::findOne($it['attr']['color']);
                $prod[$it['id']]['size'] = Size::findOne($it['attr']['size']);
                $prod[$it['id']]['prod'] = Product::findOne($it['id']);
                $prod[$it['id']]['img'] = $it['img'];
                $prod[$it['id']]['qty'] = $it['qty'];
                if(isset($it['gifts'])){
                    foreach ($it['gifts'] as $gf){
                        $prod[$it['id']]['gifts'][$gf['gift_id']]['prod'] = Product::findOne($gf['gift_id']);
                        $prod[$it['id']]['gifts'][$gf['gift_id']]['parent'] = $it['id'];
                    }
                }
                $stock_tmp = Yii::$app->db->createCommand("
                        SELECT stock_position.*, stocks.name, stocks.position FROM stock_position
                        LEFT JOIN stocks ON stocks.id = stock_position.stock_id 
                        WHERE stock_position.product_id = '{$it['id']}'
                        AND stock_position.color_id = '{$it['attr']['color']}'
                        AND stock_position.size_id = '{$it['attr']['size']}'
                        AND stock_position.stock_id IN($stocks)
                        AND stock_position.quantity > 0
                        ORDER BY stocks.position DESC
                ")->queryOne();
                $prod[$it['id']]['size_y'] = "";
                if($stock_tmp){
                    $prod[$it['id']]['size_y'] = $stock_tmp['name']." - ".$prod[$it['id']]['size']->name;
                    $prod[$it['id']]['stock_id'] = $stock_tmp['stock_id'];

                    $newSizeLog = new ProductSizeChangelog([
                        'product_id' => $it['id'],
                        'color_id' => $it['attr']['color'],
                        'size_id' => $it['attr']['size'],
                        'stock_id' => $stock_tmp['stock_id'],
                        'status' => false,
                        'order_id' => $id_order,
                        'type' => 1,
                        'date_operation' => date('Y-m-d H:i:s'),
                    ]);
                    $newSizeLog->save();
                    Yii::$app->db->createCommand("UPDATE stock_position SET quantity=0 WHERE id='{$stock_tmp['id']}'")->execute();
                }
                $prod[$it['id']]['size_n'] = "";
                $man_stock_tmp = Yii::$app->db->createCommand("
                        SELECT stock_position.*, stocks.name, stocks.position FROM stock_position
                        LEFT JOIN stocks ON stocks.id = stock_position.stock_id 
                        WHERE stock_position.product_id = '{$it['id']}'
                        AND stock_position.color_id = '{$it['attr']['color']}'
                        AND stock_position.size_id = '{$it['attr']['size']}'
                        AND stock_position.stock_id IN($man_stocks)
                        AND stock_position.quantity > 0
                        ORDER BY stocks.position DESC
                ")->queryAll();
                if($man_stock_tmp){
                    foreach ($man_stock_tmp as $mst){
                        $prod[$it['id']]['size_n'].=$mst['name']." - ".$prod[$it['id']]['size']->name.", ";
                    }
                    $prod[$it['id']]['stock_id'] = $man_stock_tmp[0]['stock_id'];
                }
            }
        }

        if(count($prod)>0){
            foreach ($prod as $key=>$item){
                $itemsModel = new OrderItems();
                $itemsModel->order_id = $id_order;
                $itemsModel->name = $item['prod']->title;
                $itemsModel->article = $item['prod']->article;
                $itemsModel->size_y = $item['size_y'];
                $itemsModel->size_n = rtrim($item['size_n'],', ');
                $itemsModel->color = $item['color']->name;
                $itemsModel->price = $item['prod']->price;
                $itemsModel->new_price = $item['prod']->discount_price;
                $itemsModel->qty = $item['qty'];
                $itemsModel->img = $item['img'];
                $itemsModel->prod_id = $item['prod']->id;
                $itemsModel->manufacturer = $item['prod']->manufacturer;
                $itemsModel->stock_id = $item['stock_id'];
                $itemsModel->save(false);
                Product::changeOneRating($item['prod']->id);
                if(isset($item['gifts'])){
                    foreach ($item['gifts'] as $igf){
                        $itemsModel = new OrderItems();
                        $itemsModel->order_id = $id_order;
                        $itemsModel->name = $igf['prod']->title;
                        $itemsModel->article = $igf['prod']->article;
                        $itemsModel->qty = $item['qty'];
                        $itemsModel->gift = $igf['parent'];
                        $itemsModel->save(false);
                    }
                }
            }

            return $prod;
        }
        return false;
    }

    public function getStocks(){
        $stock_str = "";
        $stock = ArrayHelper::getColumn(Stock::findAll(['is_manufacturer'=>false]),'id');
        if($stock){
            foreach ($stock as $it){
                $stock_str.="'".$it."',";
            }
            $stock_str = rtrim($stock_str,",");
        }
        return $stock_str;
    }

    public function getManStocks(){
        $stock_str = "";
        $stock = ArrayHelper::getColumn(Stock::findAll(['is_manufacturer'=>true]),'id');
        if($stock){
            foreach ($stock as $it){
                $stock_str.="'".$it."',";
            }
            $stock_str = rtrim($stock_str,",");
        }
        return $stock_str;
    }

    /**
     * Сохранение заказа
     * @param $address
     * @param $delivery_method
     * @param $pay_method
     * @param $first_payment
     * @param $products_info
     * @return bool|int
     */
    private function saveOrderInBD($address,$delivery_method,$pay_method,$first_payment,$products_info){
        $userInfo = Yii::$app->user->getIdentity();
        $order = new Orders();
        $order->planed = 1;
        $order->user_id = Yii::$app->user->id;
        $order->cdate = date('Y-m-d H:i:s');
        $order->total_qty = $products_info['total_qty'];
        $order->total_cost = $products_info['total_cost'];
        $order->discount = $products_info['discount'];
        $order->off_ac = $products_info['summa_spis'];
        $order->to_be_paid = $products_info['to_be_paid'];
        $order->economy = $products_info['ecomomy'];
        $order->status = 13;
        $order->pay_type = $pay_method->code;
        $order->deliv_addr = $address->address;
        $order->deliv_type = $delivery_method;
        $order->country = $address->country_id;
        $order->region = $address->region_id;
        $order->city = $address->city;
        $order->zip = $address->post_index;
        $order->fio = $address->fio;
        $order->first = $first_payment;
        $order->log_data = $this->log_data;
        $order->user_discount = $userInfo['discount_user'];
        $order->comment = $this->attributes['comment'];
        $order->mobile_phone = $this->attributes['mobile_phone'];
        if($order->save(false)){
            //обновим данные пользователя
            $user = User::findOne(Yii::$app->user->id);
            $user->last_order_date = $order->cdate;
            $user->not_orders = 0;
            $user->delivery_method = $delivery_method;
            $user->pay_method = $pay_method->code;
            if($this->attributes['ignore_mail_list']=='0'){
                $user->ignore_mail_list = '1';
            }else{
                $user->ignore_mail_list = '0';
            }
            $user->ac = $user->ac - $products_info['summa_spis'];
            $user->save(false);

            //Лог списания лицевого счета
            if((int) $products_info['summa_spis'] != 0){
                $lmodel = new LogAc();
                $lmodel->user_id = $user->id;
                $lmodel->summa = $products_info['summa_spis'];
                $lmodel->current_sum = $user->ac;
                $lmodel->type = 2;
                $lmodel->id_order = $order->id;
                $lmodel->create_date = date("Y-m-d H:i:s");
                $lmodel->save();
            }

            return $order->id;
        }
        return false;
    }

    /**
     * Информация из корзины для заказа
     * @param $pay_method
     * @return array
     */
    private function getProductsInfo($pay_method,$country,$ac){
        $products = [];
        $products['total_qty'] = $_SESSION['cart']['allqty'];
        $products['total_cost'] = $_SESSION['cart']['allsum'];
        $paymentDiscount = ($pay_method && $pay_method->discount > 0) ? round($_SESSION['cart']['sum']*$pay_method->discount / 100) : 0;
        $products['discount'] = $paymentDiscount;
        $products['ecomomy'] = $_SESSION['cart']['allsum'] - $_SESSION['cart']['sum'] + $paymentDiscount;
        if((int)$ac != 0){
            $itog = $_SESSION['cart']['sum'] - $paymentDiscount;
            if((float)$ac >= $itog){
                $products['summa_spis'] = $itog;
                $products['to_be_paid'] = 0;
            }else{
                $products['summa_spis'] = $ac;
                $products['to_be_paid'] = $itog - $ac;
            }
        }else{
            $products['summa_spis'] = 0;
            $products['to_be_paid'] = $_SESSION['cart']['sum'] - $paymentDiscount;
        }
        $this->log_data.= "Выбрано ".$products['total_qty']." ".$this->plural($products['total_qty'], 'товар', 'товара', 'товаров')." на сумму: ".$products['total_cost']." ".$this->getManySymbol($country)." <br>";
        $this->log_data.= "Скидка от способа оплаты: ".$pay_method->discount."% - ".$paymentDiscount." ".$this->getManySymbol($country)."<br>";
        $this->log_data.= "Общая экономия по заказу: ".$products['ecomomy']." ".$this->getManySymbol($country). "<br>";
        if((int)$ac != 0){
            $this->log_data.= "С лицевого счета списано ".$products['summa_spis']." ".$this->getManySymbol($country)."<br>";
        }
        $this->log_data.= "Итого: ".$products['to_be_paid']." ".$this->getManySymbol($country)."<br>";
        return $products;
    }

    /**
     * Проверка на первый платеж
     * @return string
     */
    public function checkPayments(){
        $orders = Orders::findAll(['user_id'=>Yii::$app->user->id]);
        if($orders){
            return "0";
        }
        return "1";
    }

    /**
     * Определение какой адрес вернуть
     * @return CheckoutForm|bool|static
     */
    private  function getSelectedAdress(){
        if($this->attributes['city']=='.' && !empty($this->attributes['address'])){
            $address = UserAddress::findOne($this->attributes['address']);
            $address->surname = $this->attributes['last_name'];
            $address->name = $this->attributes['first_name'];
            if(isset($this->attributes['middle_name']) && !empty($this->attributes['middle_name'])){
                $address->middle_name = $this->attributes['middle_name'];
            }
            $address->mphone = $this->attributes['mobile_phone'];
            if(isset($this->attributes['middle_name']) && !empty($this->attributes['middle_name'])){
                $address->fio = $this->attributes['last_name'].' '.$this->attributes['first_name'].' '.$this->attributes['middle_name'];
            }else{
                $address->fio = $this->attributes['last_name'].' '.$this->attributes['first_name'].' '.$address->middle_name;
            }
            $address->save(false);
            return $address;
        }else{
            return $this->addNewAddress();
        }
    }

    /**
     * Добавление нового адреса
     * @return bool|static
     */
    private function addNewAddress(){
        $country = $this->getCountry($this->attributes['country']);
        UserAddress::updateAll(['default'=>false],['user_id'=>Yii::$app->user->id]);
        $addrModel = new UserAddress();
        $addrModel->user_id = Yii::$app->user->id;
        $addrModel->country_id = $this->attributes['country'];
        $addrModel->region_id = $this->attributes['region_id'];
        $addrModel->city = $this->attributes['city'];
        $addrModel->address = $country->name.' '.$this->attributes['city'].' '.$this->attributes['street'].' '.$this->attributes['house'].' '.$this->attributes['flat'];
        $addrModel->post_index = $this->attributes['post_index'];
        if(isset($this->attributes['middle_name']) && !empty($this->attributes['middle_name'])){
            $addrModel->fio = $this->attributes['last_name'].' '.$this->attributes['first_name'].' '.$this->attributes['middle_name'];
        }else{
            $addrModel->fio = $this->attributes['last_name'].' '.$this->attributes['first_name'];
        }

        $addrModel->default = true;
        $addrModel->street = $this->attributes['street'];
        $addrModel->house = $this->attributes['house'];
        $addrModel->flat = $this->attributes['flat'];
        $addrModel->surname = $this->attributes['last_name'];
        $addrModel->name = $this->attributes['first_name'];
        if(isset($this->attributes['middle_name']) && !empty($this->attributes['middle_name'])){
            $addrModel->middle_name = $this->attributes['middle_name'];
        }
        $addrModel->mphone = $this->attributes['mobile_phone'];
        if($addrModel->save(false)){
            return UserAddress::findOne($addrModel->id);
        }
        return false;
    }

    private function getCountry($country_id){
        return Country::findOne($country_id);
    }

    /**
     * Множественное окончание слов
     * @param $n
     * @param $form1
     * @param $form2
     * @param $form5
     * @return mixed
     */
    function plural($n, $form1, $form2, $form5) {
        $n  = abs($n) % 100;
        $n1 = $n % 10;
        if ($n > 10 && $n < 20){
            return $form5;
        }else if ($n1 > 1 && $n1 < 5) {
            return $form2;
        }else if ($n1 == 1){
            return $form1;
        }
        return $form5;
    }
}