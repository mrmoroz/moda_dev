<?php

namespace app\modules\cart\models;

use Yii;

/**
 * This is the model class for table "order_items".
 *
 * @property integer $id
 * @property integer $order_id
 * @property string $name
 * @property string $article
 * @property string $size_y
 * @property string $size_n
 * @property string $color
 * @property string $weight
 * @property string $price
 * @property string $new_price
 * @property integer $qty
 * @property integer $send
 * @property integer $not_in_stock
 * @property integer $main_prod
 * @property string $labels
 * @property integer $gift
 * @property string $pi_number
 * @property integer $man_approve
 */
class OrderItems extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'qty', 'send', 'not_in_stock', 'main_prod', 'gift', 'man_approve'], 'integer'],
            [['prod_id','price', 'new_price'], 'number'],
            [['manufacturer','img','name', 'article', 'size_y', 'size_n', 'color', 'weight', 'labels', 'pi_number'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'name' => 'Name',
            'article' => 'Article',
            'size_y' => 'Size Y',
            'size_n' => 'Size N',
            'color' => 'Color',
            'weight' => 'Weight',
            'price' => 'Price',
            'new_price' => 'New Price',
            'qty' => 'Qty',
            'send' => 'Send',
            'not_in_stock' => 'Not In Stock',
            'main_prod' => 'Main Prod',
            'labels' => 'Labels',
            'gift' => 'Gift',
            'pi_number' => 'Pi Number',
            'man_approve' => 'Man Approve',
        ];
    }
}
