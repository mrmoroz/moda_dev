<?php

namespace app\modules\cart\models;

use app\models\UserFavourite;
use Yii;
use app\models\ProdsCatsParamValue;
use app\models\Product;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "cart".
 *
 * @property integer $id
 * @property integer $user_id
 * @property boolean $guest
 * @property string $cartitem
 * @property string $create_date
 */
class Cart extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cart';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['guest'], 'boolean'],
            [['cartitem'], 'string'],
            [['create_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'guest' => 'Статус авторизации',
            'cartitem' => 'Корзина',
            'create_date' => 'Дата создания',
        ];
    }

    /**
     * Добавить в корзину
     * @param $product
     * @param $qty
     * @param array $attr
     */
    public function addToCart($product, $qty, $kurs, $gifts_products, $attr=[]){
        $product_key = $this->createKey($product->id,$attr);
        // скидка на товар
        $discount_price = $this->discountPrice($product, $kurs);
        if(isset($_SESSION['cart'][$product_key])){
            $_SESSION['cart'][$product_key]['qty'] += $qty;
        }else{
            $_SESSION['cart'][$product_key] = [
                'id'=> $product->id,
                'img'=>$product->getFirstImage(),
                'article'=>$product->article,
                'name' => $product->title,
                'price' => $product->getPrice($product->price,$kurs),//$product->price,
                'discount_price'=>$discount_price,
                'attr'=>$attr,
                'qty'=>$qty
            ];
            if($gifts_products){
                foreach ($gifts_products as $it){
                    $_SESSION['cart'][$product_key]['gifts'][$it->id] = [
                        'gift_id'=>$it->id,
                        'gift_img'=>$it->getFirstImage(),
                        'gift_article'=>$it->article,
                        'gift_name'=>$it->title
                    ];
                }
            }
        }
        $_SESSION['cart']['allqty'] = isset($_SESSION['cart']['allqty']) ? $_SESSION['cart']['allqty'] + $qty : $qty;
        $_SESSION['cart']['sum'] = isset($_SESSION['cart']['sum']) ? $_SESSION['cart']['sum'] + $qty * $discount_price : $qty * $discount_price;
        $_SESSION['cart']['allsum'] = isset($_SESSION['cart']['allsum']) ? $_SESSION['cart']['allsum'] + $qty * $product->getPrice($product->price,$kurs) : $qty * $product->getPrice($product->price,$kurs);

        //Добавить корзину в БД
        $this->saveCartIntoDB(true);
    }

    /**
     * Получить подарки
     * @param $product_id
     * @return array|bool
     */
    public function getGifts($product_id){
        $artStr = "";
        $params = ProdsCatsParamValue::findAll(['param_id'=>21,'prod_id'=>$product_id]);
        if(count($params)>0){
            $art = [];
            foreach ($params as $itm){
                $art[] = str_replace(',',"','",str_replace(' ','',$itm->value));
            }
            if(count($art)>0){
                $productArr = Product::find()->where(['IN','article',$art])->all();
                if($productArr){
                    return $productArr;
                }
            }
        }
        return false;
    }

    public function discountPrice($product, $kurs){
        $price = round((!empty($product->discount_price)) ? $product->discount_price : $product->price);
        if($price == $product->price){
            if(!Yii::$app->user->isGuest) {
                $user = Yii::$app->user->getIdentity();
                $discount_user = $user['discount_user'];
                if($discount_user){
                    $price = $price - round($price * $discount_user / 100);
                }
            }
        }
        return $product->getPrice($price,$kurs);
    }

    /**
     * Создание ключа для товара в корзине
     * @param $id
     * @param $attr
     * @return string
     */
    private function createKey($id,$attr){
        if(count($attr)>0){
            return $id."|".implode("|",$attr);
        }
        return $id;
    }

    /**
     * Пересчет корзины после удаления товара
     * @param $id
     * @return bool
     */
    public function recalc($id){
        if(!isset($_SESSION['cart'][$id])) return false;
        $qtyMinus = $_SESSION['cart'][$id]['qty'];
        $sumMinus = $_SESSION['cart'][$id]['qty'] * $_SESSION['cart'][$id]['discount_price'];
        $sumMinusAll = $_SESSION['cart'][$id]['qty'] * $_SESSION['cart'][$id]['price'];
        $_SESSION['cart']['allqty'] -= $qtyMinus;
        $_SESSION['cart']['sum'] -= $sumMinus;
        $_SESSION['cart']['allsum'] -= $sumMinusAll;
        unset($_SESSION['cart'][$id]);
        if($_SESSION['cart']['sum']==0){
            unset($_SESSION['cart']['allqty']);
            unset($_SESSION['cart']['sum']);
            unset($_SESSION['cart']['allsum']);
        }

        if(count($_SESSION['cart'])>0){
            $this->saveCartIntoDB();
        }else{
            if(!Yii::$app->user->isGuest){
                $this->removeCart();
            }else{
                $this->removeCookiesCartInBD();
            }
        }

    }
    
    public function plus($id, $cnt){
        if(!isset($_SESSION['cart'][$id])) return false;
        $sum = $_SESSION['cart'][$id]['discount_price'];
        $sumAll = $_SESSION['cart'][$id]['price'];
        $_SESSION['cart'][$id]['qty']=(int)$cnt;
        $_SESSION['cart']['allqty'] += 1;
        $_SESSION['cart']['sum'] += $sum;
        $_SESSION['cart']['allsum'] += $sumAll;
        $this->saveCartIntoDB();
    }

    public function minus($id, $cnt){
        if(!isset($_SESSION['cart'][$id])) return false;
        $sum = $_SESSION['cart'][$id]['discount_price'];
        $sumAll = $_SESSION['cart'][$id]['price'];
        $_SESSION['cart'][$id]['qty']=(int)$cnt;
        $_SESSION['cart']['allqty'] -= 1;
        $_SESSION['cart']['sum'] -= $sum;
        $_SESSION['cart']['allsum'] -= $sumAll;
        $this->saveCartIntoDB();
    }

    /**
     * Сохранить корзину в БД
     * @param bool $isAddedProduct
     */
    public function saveCartIntoDB($isAddedProduct = false) {
        if (!empty($_SESSION['cart'])) {
            $cartInfo = base64_encode(serialize($_SESSION['cart']));
            if(!Yii::$app->user->isGuest){
                $cart = $this->findCart(Yii::$app->user->id,false);
                $cartToken = Yii::$app->user->id;
                if($cart){
                    $this->updateCart($cart,$cartInfo);
                }else{
                    $this->insertCart(Yii::$app->user->id,$cartInfo,false);
                }
            }else{
                //Неавторизованный пользователь
                if (isset($_COOKIE['cart_token'])){
                    $cartToken = $_COOKIE['cart_token'];
                    setcookie('cart_token', $cartToken, time() + 60 * 60 * 24 * 30 * 6, '/');
                }else{
                    $cartToken = md5(time() . uniqid('_', 'true') . rand(0, 100) * 8 * rand(0, 50));
                    setcookie('cart_token', $cartToken, time() + 60 * 60 * 24 * 30 * 6, '/');
                    $_COOKIE['cart_token'] = $cartToken;
                }
                $cart = $this->findCart($_COOKIE['cart_token'],true);
                if($cart){
                    $this->updateCart($cart,$cartInfo);
                }else{
                    $this->insertCart($_COOKIE['cart_token'],$cartInfo,true);
                }
            }

            if ($isAddedProduct && CartAddingLog::checkProduct(Yii::$app->request->get('id_product'), Yii::$app->user->isGuest, $cartToken) == 0) {
                $cartAddingLog = new CartAddingLog([
                    'product_id' => Yii::$app->request->get('id_product'),
                    'add_date' => date('Y-m-d H:i:s')
                ]);

                if (Yii::$app->user->isGuest) {
                    $cartAddingLog->cart_token = $cartToken;
                } else {
                    $cartAddingLog->user_id = Yii::$app->user->id;
                }

                $cartAddingLog->save();
                Product::changeOneRating(Yii::$app->request->get('id_product'));
            }
        }
    }

    /**
     * Поднять корзину при авторизации
     */
    public function tryToRestoreCartFromDB(){
        if (!empty($_SESSION['cart'])) {
            $this->saveCartIntoDB();
            $this->removeCookiesCartInBD();
            $this->removeCookies();
        }else{
            $this->upUserCartInBd();
        }
    }
    
    /**
     * Поднять корзину из куки
     */
    public function upCartInCookie(){
        if (isset($_COOKIE['cart_token'])) {
            $cart = $this->findCart($_COOKIE['cart_token'],true);
            if ($cart && !empty($cart->cartitem)) {
                $_SESSION['cart'] = unserialize(base64_decode($cart->cartitem));
            }
        }
    }

    /**
     * Удаление ключа корзины
     */
    public function removeCookies(){
        if(isset($_COOKIE['cart_token'])){
            setcookie("cart_token", "", time() - 60 * 60 * 24 * 30 * 7, '/');
            unset($_COOKIE['cart_token']);
        }
    }

    /**
     * Удаление корзины пользователя
     */
    public function removeCart(){
        $cart = $this->findCart(Yii::$app->user->id,false);
        if($cart){
            $cart->delete();
        }
    }

    /**
     * Удаление временной корзины
     */
    public function removeCookiesCartInBD(){
        if (isset($_COOKIE['cart_token'])) {
            $cart = $this->findCart($_COOKIE['cart_token'],true);
            if ($cart) {
                $cart->delete();
            }
        }
    }

    /**
     * Получение корзины из БД авторизованого пользователя
     */
    public function upUserCartInBd(){
        $cart = $this->findCart(Yii::$app->user->id,false); // получаем корзину
        if($cart && !empty($cart->cartitem)) {
            $_SESSION['cart'] = unserialize(base64_decode($cart->cartitem));
        }
    }

    /**
     * Найти корзину
     * @param $user_id
     * @param $guest
     * @return static
     */
    public function findCart($user_id,$guest){
        return self::findOne(['user_id'=>$user_id,'guest'=>$guest]);
    }

    /**
     * Обновить корзину
     * @param Cart $cart
     * @param $cartInfo
     */
    private function updateCart(Cart $cart, $cartInfo){
        $cart->cartitem = $cartInfo;
        $cart->create_date = date("Y-m-d H:i:s");
        $cart->save(false);
    }

    /**
     * Добавить корзину
     * @param $user_id
     * @param $cartInfo
     * @param $guest
     */
    private function insertCart($user_id, $cartInfo, $guest){
        $cart = new Cart();
        $cart->user_id = $user_id;
        $cart->guest = $guest;
        $cart->cartitem = $cartInfo;
        $cart->create_date = date("Y-m-d H:i:s");
        $cart->save(false);
    }
}
