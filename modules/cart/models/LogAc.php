<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 25.05.17
 * Time: 15:05
 */

namespace app\modules\cart\models;


class LogAc extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_ac';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id','type','id_order','manager_id'], 'integer'],
            [['summa','current_sum'], 'number'],
            [['create_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [

        ];
    }
}