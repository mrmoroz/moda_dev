<?php

namespace app\modules\cart\models;

use Yii;

/**
 * This is the model class for table "opt_orders".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $cdate
 * @property integer $total_qty
 * @property string $total_weight
 * @property string $total_cost
 * @property string $deliv_cost
 * @property string $discount
 * @property string $off_ac
 * @property string $to_be_paid
 * @property integer $status
 * @property integer $region
 * @property string $city
 * @property string $zip
 * @property integer $city_post
 * @property string $fio
 * @property string $id_post
 * @property string $ip
 * @property integer $first
 * @property integer $g_first
 * @property string $comment
 * @property string $comment_k
 * @property string $note
 * @property integer $manager
 * @property integer $planed
 * @property string $call_date
 * @property integer $pos
 * @property integer $visible
 * @property integer $sys_language
 * @property integer $payment
 */
class OptOrders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'opt_orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'total_qty', 'status', 'region', 'city_post', 'first', 'g_first', 'manager', 'planed', 'pos', 'visible', 'sys_language','payment'], 'integer'],
            [['cdate', 'call_date'], 'safe'],
            [['total_weight', 'total_cost', 'deliv_cost', 'discount', 'off_ac', 'to_be_paid'], 'number'],
            [['comment', 'comment_k', 'note'], 'string'],
            [['city', 'zip', 'fio', 'id_post', 'ip'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'cdate' => 'Cdate',
            'total_qty' => 'Total Qty',
            'total_weight' => 'Total Weight',
            'total_cost' => 'Total Cost',
            'deliv_cost' => 'Deliv Cost',
            'discount' => 'Discount',
            'off_ac' => 'Off Ac',
            'to_be_paid' => 'To Be Paid',
            'status' => 'Status',
            'region' => 'Region',
            'city' => 'City',
            'zip' => 'Zip',
            'city_post' => 'City Post',
            'fio' => 'Fio',
            'id_post' => 'Id Post',
            'ip' => 'Ip',
            'first' => 'First',
            'g_first' => 'G First',
            'comment' => 'Comment',
            'comment_k' => 'Comment K',
            'note' => 'Note',
            'manager' => 'Manager',
            'planed' => 'Planed',
            'call_date' => 'Call Date',
            'pos' => 'Pos',
            'visible' => 'Visible',
            'sys_language' => 'Sys Language',
        ];
    }

    public static function create($comment, $city, $fio, $region_id, $user_id, $first, $payment)
    {
        $order = new OptOrders();
        $order->cdate = date('Y-m-d H:i:s');
        $order->comment = $comment;
        $order->city = $city;
        $order->fio = $fio;
        $order->region = $region_id;
        $order->user_id = $user_id;
        $order->first = $first;
        $order->planed = 1;
        $order->payment = (int) $payment;
        return $order;
    }
}
