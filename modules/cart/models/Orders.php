<?php

namespace app\modules\cart\models;

use app\models\Country;
use app\models\Product;
use app\models\Regions;
use app\models\User;
use app\modules\admin\models\OrdersStatusesHistory;
use app\modules\admin\models\OrdstatList;
use app\modules\admin\models\Sales;
use Yii;
use yii\helpers\ArrayHelper;


class Orders extends \yii\db\ActiveRecord
{
    public $email;
    public $country_name;
    public $region_name;
    
    
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tmp_id', 'user_id', 'important', 'is_odnk', 'total_qty', 'cod_checked', 'aff', 'status', 'payed_summ', 'payment', 'country', 'region', 'city_post', 'first', 'g_first', 'manager', 'manager_s', 'planed', 'repeat_request_count', 'pos', 'visible', 'sys_language'], 'integer'],
            [['cdate', 'deliv_date', 'pdo'], 'safe'],
            [['total_weight', 'total_cost', 'deliv_cost', 'discount', 'off_ac', 'to_be_paid', 'economy'], 'number'],
            [['mobile_phone','user_info', 'deliv_addr', 'comment', 'comment_k', 'note', 'stock_info', 'user_msg', 'admin_msg', 'log_data'], 'string'],
            [['fact_paid', 'pay_type', 'deliv_type', 'city', 'zip', 'fio', 'id_post', 'ip', 'pi_number', 'next_orders', 'user_discount'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Номер заказа',
            'tmp_id' => 'Tmp ID',
            'user_id' => 'ИД заказчика',
            'important' => 'Important',
            'is_odnk' => 'Is Odnk',
            'cdate' => 'Дата создания',
            'total_qty' => 'Кол-во',
            'total_weight' => 'Total Weight',
            'total_cost' => 'Total Cost',
            'deliv_cost' => 'Deliv Cost',
            'discount' => 'Discount',
            'off_ac' => 'Off Ac',
            'to_be_paid' => 'Сумма заказа',
            'economy' => 'Economy',
            'fact_paid' => 'Fact Paid',
            'cod_checked' => 'Cod Checked',
            'user_info' => 'User Info',
            'aff' => 'Aff',
            'status' => 'Статус',
            'payed_summ' => 'Payed Summ',
            'payment' => 'Payment',
            'pay_type' => 'Pay Type',
            'deliv_addr' => 'Deliv Addr',
            'deliv_type' => 'Deliv Type',
            'deliv_date' => 'Deliv Date',
            'country' => 'Страна',
            'region' => 'Регион',
            'city' => 'Город',
            'zip' => 'Zip',
            'city_post' => 'City Post',
            'fio' => 'Ф.И.О заказчика',
            'id_post' => 'Id Post',
            'ip' => 'Ip',
            'first' => 'First',
            'g_first' => 'G First',
            'comment' => 'Comment',
            'comment_k' => 'Comment K',
            'note' => 'Note',
            'stock_info' => 'Stock Info',
            'manager' => 'Manager',
            'manager_s' => 'Manager S',
            'pdo' => 'ПДО',
            'planed' => 'Planed',
            'repeat_request_count' => 'Repeat Request Count',
            'user_msg' => 'User Msg',
            'admin_msg' => 'Admin Msg',
            'pi_number' => 'Pi Number',
            'log_data' => 'Log Data',
            'next_orders' => 'Next Orders',
            'user_discount' => 'User Discount',
            'pos' => 'Pos',
            'visible' => 'Visible',
            'sys_language' => 'Sys Language',
            'ot_cdate'=>'Начало периода',
            'do_cdate'=>'Конец периода',
            'last_name'=>'Фамилия',
            'first_name'=>'Имя',
            'second_name'=>'Отчество',
            'article'=>'Артикул',
            'size'=>'Размер',
            'visible'=>'Видимость',
            'p_r'=>'Красный',
            'p_o'=>'Оранжевый',
            'p_b'=>'Синий',
            'p_w'=>'Без ПДО',
            'c_order'=>'Дата заказа',
            'c_pdo'=>'ПДО'
        ];
    }

    public function getRegiondata()
    {
        return $this->hasOne(Regions::className(), ['id' => 'region']);
    }

    public function getCustomer()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    public function getCountryname()
    {
        return $this->hasOne(Country::className(), ['id' => 'country']);
    }
    public function getStatusname()
    {
        return $this->hasOne(OrdstatList::className(), ['id' => 'status']);
    }
    public function getSales()
    {
        return $this->hasOne(Sales::className(), ['order_id' => 'id']);
    }


    public function statusLog($order)
    {
        $oldAttr = $order->getOldAttributes();
        $model = new OrdersStatusesHistory();
        $model->order_id = $order->id;
        $model->old_status = $oldAttr['status'];
        $model->new_status = $order->status;
        $model->user_id = Yii::$app->user->id;
        $model->pdo = $order->pdo;
        $model->created_at = date("Y-m-d H:i:s");
        $model->save(false);
    }

    public function PdoAutoLog($order)
    {
        $model = new OrdersStatusesHistory();
        $model->order_id = $order->id;
        $model->pdo = $order->pdo;
        $model->created_at = date("Y-m-d H:i:s");
        $model->save(false);
    }

    public function PdoLog($order)
    {
        $model = new OrdersStatusesHistory();
        $model->order_id = $order->id;
        $model->pdo = $order->pdo;
        $model->user_id = Yii::$app->user->id;
        $model->created_at = date("Y-m-d H:i:s");
        $model->save(false);
    }

    /**
     * @param $productCategories
     * @param null $productId
     * @return array|\yii\db\ActiveRecord[] Возвращает массив с датами заказов
     * Возвращает массив с датами заказов
     */
    public static function getOrdersDatesForRatingByProductCategory($productCategories, $productId = null)
    {
        $productIdAdd = '';
        if ($productId) {
            $productIdAdd = 'AND P.id = ' . $productId;
        }

        $ordersDates = ArrayHelper::map(self::findBySql("SELECT P.id AS product_id, O.cdate
                                      FROM order_items OI
                                      JOIN products P ON OI.article=P.article OR OI.prod_id = P.id
                                      JOIN orders O ON O.id= OI.order_id
                                      WHERE P.category_id {$productCategories}
                                      {$productIdAdd}
                                      GROUP BY O.cdate, P.id
                                      ORDER BY O.cdate DESC")->asArray()->all(), 'product_id', 'cdate');

        $productOrdersDuplicates =  ArrayHelper::map(self::findBySql("SELECT P.parent_product_id AS product_id, O.cdate
                                                   FROM order_items OI
                                                   JOIN products P ON OI.article=P.article OR OI.prod_id = P.id
                                                   JOIN orders O ON O.id= OI.order_id
                                                   WHERE P.parent_product_id IN (
                                                      SELECT id FROM products WHERE category_id {$productCategories}
                                                   )
                                                   {$productIdAdd}
                                                   GROUP BY O.cdate, P.parent_product_id
                                                   ORDER BY O.cdate DESC")->asArray()->all(), 'product_id', 'cdate');

        $parentIds = ArrayHelper::getColumn(Product::findBySql("SELECT parent_product_id FROM products
                                            WHERE category_id {$productCategories}
                                            AND parent_product_id IS NOT NULL")->asArray()->all(), 'parent_product_id');
        if (!empty($parentIds)) {
            $parentOrderDates = ArrayHelper::map(self::findBySql("SELECT CP.id AS child_id, O.cdate
                                                                    FROM order_items OI
                                                                    JOIN products P ON OI.article=P.article OR OI.prod_id = P.id
                                                                    JOIN orders O ON O.id= OI.order_id
                                                                    JOIN products CP ON P.id = CP.parent_product_id
                                                                    GROUP BY O.cdate, P.id, CP.id
                                                                    ORDER BY O.cdate DESC")->where(['IN', 'P.id', $parentIds])
                                                        ->asArray()->all(), 'child_id', 'cdate');
        }

        if (!empty($parentOrderDates)) {
            foreach ($parentOrderDates as $childId => $parentOrderDate) {
                if (isset($ordersDates[$childId]) && strtotime($ordersDates[$childId]) < strtotime($parentOrderDate)) {
                    $ordersDates[$childId] = $parentOrderDate;
                }
            }
        }

        foreach ($productOrdersDuplicates as $productId => $duplicateOrderDate) {
            if (isset($ordersDates[$duplicateOrderDate]) && strtotime($ordersDates[$duplicateOrderDate]) < strtotime($duplicateOrderDate)) {
                $ordersDates[$duplicateOrderDate] = $duplicateOrderDate;
            }
        }

        unset($productOrdersDuplicates);
        unset($parentIds);
        unset($parentOrderDates);

        return $ordersDates;
    }

    /**
     * Возвращает массив с количеством заказов с учетом дубликатов
     * @param $productCategories
     * @param $date
     * @param bool $isBefore
     * @return array
     */
    public static function getProductOrdersCount($productCategories, $date, $isBefore = true, $productId = null)
    {
        $productIdAdd = '';

        if ($productId) {
            $productIdAdd = 'AND P.id = ' . $productId;
        }

        if ($isBefore) {
            $andWhereDate = 'O.cdate >= to_timestamp(' . $date . ')';
        } else {
            $andWhereDate = 'O.cdate < to_timestamp(' . $date . ')';
        }

        $productOrdersCount = ArrayHelper::map(OrderItems::findBySql("SELECT P.id AS product_id, COUNT(OI.article) AS qty 
                                      FROM order_items OI 
                                      JOIN products P ON OI.article = P.article OR OI.prod_id = P.id
                                      JOIN orders O ON O.id = OI.order_id 
                                      WHERE P.category_id {$productCategories}
                                      AND {$andWhereDate}
                                      {$productIdAdd}
                                      GROUP BY P.id, qty")->asArray()->all(), 'product_id', 'qty');

        // Данные по дубликатам
        $productOrdersDuplicates =  ArrayHelper::map(self::findBySql("SELECT P.parent_product_id AS product_id, COUNT(OI.article) AS qty
                                                   FROM order_items OI
                                                   JOIN products P ON OI.article=P.article OR OI.prod_id = P.id
                                                   JOIN orders O ON O.id= OI.order_id
                                                   WHERE P.parent_product_id IN (
                                                      SELECT id FROM products WHERE category_id {$productCategories}
                                                   )
                                                   AND {$andWhereDate}
                                                   GROUP BY qty, P.parent_product_id")->asArray()->all(), 'product_id', 'qty');

        $parentIds = ArrayHelper::getColumn(Product::findBySql("SELECT parent_product_id FROM products
                                            WHERE category_id {$productCategories}
                                            AND parent_product_id IS NOT NULL")->asArray()->all(), 'parent_product_id');

        if (!empty($parentIds)) {
            $parentOrdersQuantity = ArrayHelper::map(self::findBySql("SELECT CP.id AS child_id, COUNT(OI.article) AS qty
                                                                    FROM order_items OI
                                                                    JOIN products P ON OI.article=P.article OR OI.prod_id = P.id
                                                                    JOIN orders O ON O.id = OI.order_id
                                                                    JOIN products CP ON P.id = CP.parent_product_id
                                                                    WHERE {$andWhereDate}
                                                                    GROUP BY qty, P.id, CP.id")->where(['IN', 'P.id', $parentIds])
                                                         ->asArray()->all(), 'child_id', 'cdate');
        }

        if (!empty($parentOrdersQuantity)) {
            foreach ($parentOrdersQuantity as $childId => $parentQuantity) {
                if (isset($productOrdersCount[$childId])) {
                    $productOrdersCount[$childId] += $parentQuantity;
                }
            }
        }

        foreach ($productOrdersDuplicates as $productId => $duplicateQuantity) {
            if (isset($productOrdersCount[$productId])) {
                $productOrdersCount[$productId] += $duplicateQuantity;
            }
        }

        unset($productOrdersDuplicates);
        unset($parentIds);
        unset($parentOrdersQuantity);

        return $productOrdersCount;
    }
}
