<?php
namespace app\modules\cart\services;
use app\models\Color;
use app\models\OptTmpOrderItems;
use app\models\OptTmpOrders;
use app\models\Product;
use app\models\ProductSizeChangelog;
use app\models\Size;
use app\models\StockPosition;
use app\modules\cart\models\OptOrderItems;
use app\modules\cart\models\OptOrders;
use Yii;

class CartService
{
    public function addOrder($user, $comment, $payment)
    {
        $userOrderCnt = OptOrders::find()->where(['user_id'=>$user->id])->count();
        $fio = $user->f_name . ' ' . $user->l_name;
        $first = ($userOrderCnt > 0) ? 0 : 1;
        if($payment=='1'){
            $order = OptTmpOrders::create($comment, $user->city_name, $fio, $user->region_id, $user->id, $first, $payment);
        }else{
            $order = OptOrders::create($comment, $user->city_name, $fio, $user->region_id, $user->id, $first, $payment);
        }

        $order->save(false);

        return $order;
    }


    public function addOrderItems($cart, $order, $stocks, $payment)
    {
        $positions = array();
        $key = 0;
        $totalAmount = 0;
        $totalCnt = 0;
        $prod = [];

        foreach ($cart as $itemId => $itemInfo) {
            $product = Product::findOne(['id'=>$itemId,'is_active'=>true]);
            if ($product) {
                $prod[$product->id] = $product;
                foreach ($itemInfo as $colorId => $sizeArray) {
                    $colorInfo = Color::find()->where(['id'=>$colorId])->one();
                    $colorName = ($colorInfo) ? $colorInfo->name : 'Базовый';
                    foreach ($sizeArray as $sizeId => $count) {
                        $sizeInfo = Size::find()->where(['id'=>$sizeId])->one();
                        if ($sizeInfo) {
                            $positions[$key]['product'] = $product;
                            $positions[$key]['color'] = $colorName;
                            $positions[$key]['color_id'] = $colorId;
                            $positions[$key]['size'] = $sizeInfo;
                            $positions[$key]['cnt'] = $count;
                            $price = $product->getPrice($product->wholesale_price,1);
                            if(!empty($product->wholesale_discount_price)){
                                $price = $product->getPrice($product->wholesale_discount_price,1);
                            }
                            $totalAmount += $count * $price;
                            $totalCnt += $count;

                            $stock_tmp = StockPosition::getStockdata($product->id, $colorId, $sizeId, $stocks);
                            $stocks_id = $stock_tmp['stock_id'] ?: 1;
                            $size_y = '';
                            $size_n = '';
                            if($stock_tmp){
                                $size_y = $sizeInfo->name;
                                if($payment != '1') {

                                    $quantity = (int)$stock_tmp['quantity'] - $count;
                                    $status = true;

                                    if ($quantity < 0 || $quantity == 0) {
                                        $quantity = 0;
                                        $status = false;
                                    }

                                    $newSizeLog = \app\models\ProductSizeChangelog::create($product->id, $colorId, $sizeId, $stock_tmp['stock_id'], $status, $order->id, $count);
                                    $newSizeLog->save(false);

                                    StockPosition::changeStockData($quantity, $stock_tmp['id']);
                                }
                            }else{
                                $size_n = $sizeInfo->name;
                            }

                            if($payment == '1'){
                                $orderItem = OptTmpOrderItems::create(
                                    $product->article,
                                    $colorName,
                                    $product->title,
                                    $product->wholesale_discount_price,
                                    $product->wholesale_price,
                                    $count,
                                    $order->id,
                                    $product->id,
                                    $size_y,
                                    $size_n,
                                    $stocks_id,
                                    $colorId,
                                    $sizeId
                                );
                            }else{
                                $orderItem = OptOrderItems::create(
                                    $product->article,
                                    $colorName,
                                    $product->title,
                                    $product->wholesale_discount_price,
                                    $product->wholesale_price,
                                    $count,
                                    $order->id,
                                    $product->id,
                                    $size_y,
                                    $size_n
                                );
                            }

                            $orderItem->save(false);

                            $key++;
                        }
                    }
                }

                if($payment!='1') {
                    \app\models\Product::clearProductCache($product->id);
                    StockPosition::checkStocks($product->id, $order->id);
                }
            }
        }

        $order->total_cost = $totalAmount;
        $order->total_qty = $totalCnt;
        $order->save(false);

        return $positions;
    }
}