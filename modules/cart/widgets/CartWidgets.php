<?php
namespace app\modules\cart\widgets;
use yii\base\Widget;
//use yii\web\Session;
use Yii;
use app\modules\cart\models\Cart;
use app\models\PaymentList;
use app\models\Product;


class CartWidgets extends Widget {

    public $count;
    public $cartCost;
    public $product;

    public function init(){
        parent::init();

        $session = Yii::$app->session;
        if (!$session->isActive){
            $session->open();
        }
        $ac = 0;
        if(!Yii::$app->user->isGuest){
            $user = Yii::$app->user->identity;
            $ac = $user->ac;
            if(!empty($user->pay_method)){
                $paymentMethod = PaymentList::findOne(['code'=>$user->pay_method]);
            }
        }

        $count = isset($_SESSION['cart']['allqty'])?$_SESSION['cart']['allqty']:'0';
        $paymentDiscount = (isset($paymentMethod->discount) && $paymentMethod->discount > 0) ? round($_SESSION['cart']['sum']*$paymentMethod->discount / 100) : 0;
        $cartSumm = isset($_SESSION['cart']['sum'])?$_SESSION['cart']['sum']-$paymentDiscount:0;

        if((int)$ac != 0){
            if((float)$ac >= $cartSumm){
                $cartSumm = 0;
            }else{
                $cartSumm = $cartSumm - $ac;
            }
        }

        $this->count = $count;
        $this->cartCost = $cartSumm;
        $this->product = new Product();
    }

    public function run(){
        return $this->render('cart',[
            'count'=>$this->count,
            'cartCost'=>$this->cartCost,
            'product' => $this->product
        ]);
    }
}
