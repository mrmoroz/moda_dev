<?php
use pavlinter\display\DisplayImage;
use app\models\Color;
use app\models\Size;
//vd($product,false);
//vd($attr,false);
//vd($session_cart,false);
?>
<?php if($product):?>
<div class="tabl-added" style="width: 100%;">
    <div class="tr">
        <div class="added-left" style="width: 35%;">
            <h4><?=$product->title?></h4>
            <div class="added-image">
                <?= DisplayImage::widget([
                    'width' => 120,
                    'height' => 180,
                    'image' => $product->getFirstImage(),
                    'category' => 'all',
                ]);?>
            </div>
            <div class="tabl-param">
                <div class="tr">
                    <div class="td">Артикул:</div>
                    <div class="td"><strong><?=$product->article;?></strong></div>
                </div>
                <?php if(isset($attr['color'])):?>
                <div class="tr">
                    <div class="td">Цвет:</div>
                    <div class="td">
                        <strong><?= Color::getName($attr['color']) ?></strong>
                    </div>
                </div>
                <?php endif;?>
                <?php if(isset($attr['size'])):?>
                <div class="tr">
                    <div class="td">Размер:</div>
                    <div class="td">
                        <ul class="catalog-item-sizes clearfix">
                            <li><a href="#"><?= Size::getName($attr['size']) ?></a></li>
                        </ul>
                    </div>
                </div>
                <?php endif;?>
                <div class="tr">
                    <div class="td">Количество:</div>
                    <div class="td"><strong><?=$qty?></strong></div>
                </div>
                <div class="tr">
                    <div class="td">Цена:</div>
                    <div class="td"><strong><?=round(($product->discount_price) ? $product->getPrice($product->discount_price,$this->params['kurs_many']) : $product->getPrice($product->price,$this->params['kurs_many']));?> <?=$product->getSymbol($this->params['country'])?></strong></div>
                </div>
            </div>
            <div class="add-to-cart-block" style="text-align:center; margin-top: 10px;">
                <a href="#" class="add-to-cart-button-2" data-dismiss="modal">Продолжить покупки</a>
            </div>
        </div>
        <div class="added-center" style="width: 5%;"></div>
        <div class="added-right" style="width: 60%;">
            <h4>Содержимое вашей корзины:</h4>
            <div class="tabl total" style="padding-top:30px;">
                <div class="tr">
                    <div class="td" style="border-bottom:none;">Количество товаров</div>
                    <div class="td digits"><strong><?=$session_cart['allqty']?></strong></div>
                </div>
                <div class="tr" style="color:red">
                    <div class="td" style="border-bottom:none;">Общая экономия по заказу</div>
                    <div class="td digits"><strong><?=$session_cart['allsum'] - $session_cart['sum']?> <?=$product->getSymbol($this->params['country'])?></strong></div>
                </div>
                <div class="tr">
                    <div class="td" style="border-bottom:none;">Стоимость товаров</div>
                    <div class="td digits"><strong><?=$session_cart['sum']?> <?=$product->getSymbol($this->params['country'])?></strong></div>
                </div>
            </div>
            <div class="add-to-cart-block" style="text-align:center; padding-top:50px;">
                <a href="/cart" class="add-to-cart-button">Оформить заказ</a>
            </div>
        </div>
    </div>
</div>
<?php endif;?>


