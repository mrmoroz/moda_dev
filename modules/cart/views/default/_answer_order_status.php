<?php
use app\modules\cart\components\Helper;
?>
<?php if($order):?>
    Статус заказа <?= $order->id ?>:<br>
    <b><?= $status->name ?></b><br>
    <?php if ($orderManager): ?>
        <br>Ваш персональный консультант:<b> <?= $orderManager->last_name ?> <?= $orderManager->first_name ?></b><br>
    <?php endif; ?>
    <?php if ($order->comment_k): ?>
        <div style="background: #f5ecda;padding: 15px;width: 80%;margin: 0 auto;">
            <b>Комментарий консультанта:</b><br>
            <?= $order->comment_k ?>
            <br>
        </div>
    <?php endif; ?>
    <?php if ($orderSale): ?>
        <br>Почтовый идентификатор: <b><?= $orderSale->id_post ?></b>
        <br><br>
        <table style="margin:15px auto">
        <?php if ($orderRPInfo['item']): ?>
            <tr>
                <td colspan="4" style="text-align:center">
                    <b><?= $orderRPInfo['item']->ComplexItemName ?></b>
                    <?php if ($orderRPInfo['item']->Mass) : ?>
                        &nbsp; / &nbsp;<?= round($orderRPInfo['item']->Mass / 1000, 2) ?> кг.
                    <?php endif; ?>
                    <br>
                </td>
            </tr>
        <?php endif; ?>
        <?php if($orderOperationHistory):?>
            <?php foreach ($orderOperationHistory as $historyData):?>
                <tr style="border-bottom:1px solid grey">
                    <td style="padding:5px"><?= date('d.m', strtotime($historyData->operation_date)) ?></td>
                    <td style="padding:5px"><?=Helper::getAddressRPOperation($historyData) ?></td>
                    <td style="padding:5px"><?=Helper::getTitleRPOperation($historyData) ?></td>
                    <td style="padding:5px"><?=Helper::getIndexRPOperation($historyData) ?></td>
                </tr>
            <?php endforeach;?>
        <?php endif;?>
        <?php if ($orderRPInfo['finance']): ?>
            <tr>
                <td colspan="4" style="text-align:center; padding-top:10px">
                    <?php $totalPayment = $orderRPInfo['finance']->Payment + $orderRPInfo['finance']->Rate + $orderRPInfo['finance']->CustomDuty ?>
                    <b>К оплате при получении <?= round($totalPayment / 100) ?> руб.</b>
                    <?php if ($orderRPInfo['finance']->Payment): ?>
                        <br> - наложенный платеж: <?= round($orderRPInfo['finance']->Payment / 100) ?> руб.
                    <?php endif; ?>
                    <?php if ($orderRPInfo['finance']->CustomDuty): ?>
                        <br> - таможенный платеж: <?= round($orderRPInfo['finance']->CustomDuty / 100) ?> руб.
                    <?php endif; ?>
                    <?php if ($orderRPInfo['finance']->Rate): ?>
                        <br> - дополнительный тарифный сбор: <?= round($orderRPInfo['finance']->Rate / 100) ?> руб.
                    <?php endif; ?>
                    <br><br>
                </td>
            </tr>
        <?php endif; ?>
        </table>
    <?php endif; ?>
<?php else:?>
    <b style="color:red">Заказ не найден</b>
<?php endif;?>
<?php if (Yii::$app->user->isGuest): ?>
    <br>
    Остались вопросы?<br>
    <a href="#" onclick="$('a#callBackShowButton').click();return false;">Заказать звонок</a>
<?php endif; ?>
