<div class="tr region-block" style="display: table-row;">
    <div class="th">Регион</div>
    <div class="td">
        <div class="form-group field-checkoutform-region_id">
            <select id="checkoutform-region_id" class="form-control" name="CheckoutForm[region_id]">
                <?php foreach ($regions as $it):?>
                    <?php
                        if($region_geo){
                            if($it->id==$region_geo->id){
                                $checked = "selected";
                            }else{
                                $checked = "";
                            }

                        }
                    ?>
                    <option value="<?=$it->id?>" <?=$checked?>><?=$it->name?></option>
                <?php endforeach;?>
            </select>
            <div class="help-block"></div>
        </div>
    </div>
</div>