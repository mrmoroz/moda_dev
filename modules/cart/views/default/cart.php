<?php
use yii\helpers\Html;
$this->title = "Корзина";
?>
<div class="columns-group clearfix">
    <div class="left-column">
        <div class="content">
            <h1><?=$this->title?></h1>
        </div>
    </div>
    <div class="right-column">
        <div class="content">
            <div class="article-content">
                <h1>Ваш заказ</h1>
                <?php if (!empty($positions)): ?>
                <table class="cart-items">
                    <tbody>
                    <tr>
                        <th>Артикул</th>
                        <th>Наименование</th>
                        <th>Цвет</th>
                        <th>Размер</th>
                        <th>Количество</th>
                        <th style="text-align: right;">Цена за единицу</th>
                        <th style="text-align: right;">Общая цена</th>
                        <th></th>
                    </tr>
                    <?php foreach ($positions as $position): ?>
                        <tr>
                            <td>
                                <a href="<?= $position['product']->getUrl() ?>"><?= $position['product']->article ?></a>
                            </td>
                            <td>
                                <a href="<?= $position['product']->getUrl() ?>"><?= $position['product']->title ?></a>
                            </td>
                            <td><?= $position['color'] ?></td>
                            <td><?= $position['size']->name ?></td>
                            <td><?= $position['cnt'] ?></td>
                            <td class="price-column">
                                <?php
                                    $price = $position['product']->getPrice($position['product']->wholesale_price,1);
                                    if(!empty($position['product']->wholesale_discount_price)){
                                        $price = $position['product']->getPrice($position['product']->wholesale_discount_price,1);
                                    }
                                    echo $price.' р.';
                                ?>
                            </td>
                            <td class="price-column">
                                <?=$position['cnt'] * $price.' р.';?>
                            </td>
                            <td style="padding-right: 0px; text-align: right;">
                                <a href="/cart/deleteitem?pid=<?=$position['product']->id?>&cid=<?=$position['color_id']?>&sid=<?=isset($position['size']->id) ? $position['size']->id : ''?>">Удалить</a>
                            </td>
                        </tr>
                    <?php endforeach;?>
                    <tr>
                        <td class="price-column-2" colspan="7"><b><?= $totalAmount ?> р.</b></td>
                    </tr>
                    </tbody>
                </table>
                <div class="registration-form">
                    <form method="post" id="send-comment-form" action="/cart/order">
                        <?php echo Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, \Yii::$app->getRequest()->getCsrfToken(), []);?>
                        <dl>
                            <dt>Оплата на расчетный счет:</dt>
                            <dd>
                                <input type="radio" name="payment" value="0" checked>
                            </dd>
                            <div class="clear"></div>
                        </dl>
                        <dl>
                            <dt>Оплата банковской картой:</dt>
                            <dd>
                                <input type="radio" name="payment" value="1">
                            </dd>
                            <div class="clear"></div>
                        </dl>
                        <dl>
                            <dt>Комментарий:</dt>
                            <dd>
                                <textarea name="comment" style="width: 250px;height: 65px;"></textarea>
                            </dd>
                            <div class="clear"></div>
                        </dl>
                    </form>
                </div>
                <a href="/catalog" class="order-link">Продолжить покупки</a>
                <a href="#" onclick="$('#send-comment-form').submit(); return false;" class="order-link">Заказать</a>
                <?php else:?>
                    Товары отсутствуют
                    <div>
                        <a href="/catalog" class="order-link">Продолжить покупки</a>
                    </div>
                <?php endif;?>
                <div class="footer-block"></div>
            </div>
        </div>
    </div>

</div>