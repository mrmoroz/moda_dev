<?php
use pavlinter\display\DisplayImage;
use app\models\Color;
use app\models\Size;
use app\components\widgets\ShowLoadingWidgets;
use yii\helpers\Html;
use app\models\Product;

$product = new Product();
$this->title="Корзина";
echo ShowLoadingWidgets::widget(['loadingType' => 1]);
?>
<div class="content-cart">
    <h1><?= $this->title; ?></h1>
    <?php if(count($session_cart)>0):?>
        <div class="tabl highlight">
            <div class="tr">
                <div class="th">Изделие</div>
                <div class="th">Товар</div>
                <div class="th">Размер</div>
                <div class="th">Цвет</div>
                <div class="th">Кол-во</div>
                <div class="th">Цена</div>
                <div class="th">Цена со скидкой</div>
                <div class="th">Итого</div>
                <div class="th"> </div>
            </div>
            <?php foreach ($session_cart as $key => $item):?>
                <?php if($key!='allqty' && $key!='sum' && $key!='allsum'):?>
                    <div data-prod="<?= $item['id'] ?>" data-color="<?= $item['attr']['color'] ?>" data-size="<?= $item['attr']['size'] ?>" class="tr tr-cart">
                        <div class="td"><strong><?=$item['name']?></strong><br><?=$item['article']?></div>
                        <div class="td">
                            <a href="<?=$item['img']?>" class="cart-img-group">
                                <?= DisplayImage::widget([
                                    'width' => 50,
                                    'height' => 75,
                                    'image' => $item['img'],
                                    'category' => 'all',
                                ]);?>
                            </a>
                        </div>
                        <div class="td"><?= Size::getName($item['attr']['size']);?></div>
                        <div class="td"><?= Color::getName($item['attr']['color']);?></div>
                        <div class="td amount">
                            <div class="add-to-cart-amount">
                                <a class="icon-minus" data-key="<?=$key;?>" href="#">&ndash;</a>
                                <div class="cart-amount-value"><input type="text" class="input_cnt" data-key="<?=$key;?>" name="product_cnt" style="width: 16px; text-align: center; border: none" value="<?= $item['qty']?>" readonly></div>
                                <a class="icon-plus" data-key="<?=$key;?>" href="#">+</a>
                            </div>
                        </div>
                        <div class="td digits">
                            <?php if(!empty($item['discount_price']) && $item['discount_price'] != $item['price']):?>
                                <s><?= $item['price']?></s>
                            <?php else:?>
                                <?= $item['price']?>
                            <?php endif;?>
                        </div>
                        <div class="td digits">
                            <?= isset($item['discount_price']) ? $item['discount_price'] : $item['price'] ?>
                        </div>
                        <div class="td digits">
                            <? if (isset($item['discount_price'])) {
                                echo $item['discount_price'] * $item['qty'];
                            } else {
                                echo $item['price'] * $item['qty'];
                            }
                            ?>
                        </div>
                        <div class="td"><a href="#" class="del-from-cart" data-key="<?=$key;?>"><img src="<?= \app\assets\HtmlAsset::register($this)->baseUrl ?>/img/icon-del.png" width="16" height="16" title="Удалить"></a></div>
                    </div>
                    <?php if(isset($item['gifts']) && count($item['gifts'])>0):?>
                        <?php foreach ($item['gifts'] as $gift):?>
                        <div class="tr tr-cart">
                            <div class="td"><strong><?=$gift['gift_name']?></strong> (подарок) <br><?=$gift['gift_article']?></div>
                            <div class="td">
                                <?= DisplayImage::widget([
                                    'width' => 50,
                                    'height' => 75,
                                    'image' => $gift['gift_img'],
                                    'category' => 'all',
                                ]);?>
                            </div>
                            <div class="td"> - </div>
                            <div class="td"> - </div>
                            <div class="td amount" style="text-align: center;"><?=$item['qty']?></div>
                            <div class="td digits"> - </div>
                            <div class="td digits"> - </div>
                            <div class="td digits"> - </div>
                            <div class="td"></div>
                        </div>
                        <?php endforeach;?>
                    <?php endif;?>
                <?php endif;?>
            <?php endforeach;?>
        </div>
        <div class="tabl total">
            <div class="tr">
                <div class="td" style="border-bottom:none;">Стоимость товаров</div>
                <div class="td digits" id="cart-price"><?= $session_cart['allsum'] ?> <?=$product->getSymbol($this->params['country'])?></div>
            </div>
            <div class="tr">
                <?php
                    $paymentDiscount = ($paymentMethod && $paymentMethod->discount > 0) ? round($session_cart['sum']*$paymentMethod->discount / 100) : 0;
                ?>
                <div class = "td"  style = "border-bottom:none;">
                    Сумма скидки при выбранном способе оплаты (<?=($paymentMethod?(int)$paymentMethod->discount:'0')?>%)
                </div>
                <div class = "td digits"><?= $paymentDiscount ?> <?=$product->getSymbol($this->params['country'])?></div>
            </div>
            <div class="tr">
                <div class="td" style="border-bottom:none;">Общая экономия по заказу</div>
                <div class="td digits" id="cart-price"><?= $session_cart['allsum'] - $session_cart['sum'] + $paymentDiscount ?> <?=$product->getSymbol($this->params['country'])?></div>
            </div>
            <?php if((int)$ac != 0):?>
                <?php
                    $itogo = $session_cart['sum'] - $paymentDiscount;
                    if((float)$ac >= $itogo){
                        $ostatok_ac = $ac - $itogo;
                        $summa_spis = $itogo;
                        $itogo = 0;
                    }else{
                        $ostatok_ac = 0;
                        $summa_spis = $ac;
                        $itogo = $itogo - $ac;
                    }
                ?>
                <div class="tr">
                    <div class = "td" style = "border-bottom:none;">С вашего лицевого счета будет списано</div>
                    <div class = "td digits"><strong id = "cart-total-price"><?=$summa_spis?> <?=$product->getSymbol($this->params['country'])?></strong></div>
                </div>
                <div class = "tr">
                    <div class = "td" style = "border-bottom:none;">Итого</div>
                    <div class = "td digits"><strong id = "cart-total-price"><?= $itogo ?> <?=$product->getSymbol($this->params['country'])?></strong></div>
                </div>
            <?php else:?>
                <div class = "tr">
                    <div class = "td" style = "border-bottom:none;">Итого</div>
                    <div class = "td digits"><strong id = "cart-total-price"><?= $session_cart['sum'] - $paymentDiscount ?> <?=$product->getSymbol($this->params['country'])?></strong></div>
                </div>
            <?php endif;?>

        </div>
        <?php if(!Yii::$app->user->isGuest):?>
            <div class="btn-wrap-ch">
                <a href="/cart/checkout" class="add-to-cart-button">Оформить заказ</a>
            </div>
        <?php else:?>
            <div class="btn-wrap-ch">
                <a href="#Authorization" class="add-to-cart-button up-login-form">Оформить заказ</a>
            </div>
        <?php endif;?>
    <?php else:?>
        <h2>В корзине нет товаров</h2>
    <?php endif;?>
</div>


<?php echo $this->render('_login_form',['model'=>$model]);?>
<?php echo $this->render('_reg_form',['modelReg'=>$modelReg,'paramRegForm'=>$paramRegForm]);?>
<?php echo $this->render('_skip_reg_form',['modelSkip'=>$modelSkip]);?>

<?php
$script = <<<JS
;$(function(){
    $(".up-login-form").fancybox({
		'transitionIn'	:	'fade',
		'transitionOut'	:	'fade',
		'speedIn'		:	200, 
		'speedOut'		:	200, 
	});
	$(".up_reg_form").fancybox({
		'transitionIn'	:	'fade',
		'transitionOut'	:	'fade',
		'speedIn'		:	200, 
		'speedOut'		:	200, 
	});
	$(".up_skip_reg").fancybox({
		'transitionIn'	:	'fade',
		'transitionOut'	:	'fade',
		'speedIn'		:	200, 
		'speedOut'		:	200, 
	});
	$("a.cart-img-group").fancybox({
        'transitionIn'	:	'elastic',
        'transitionOut'	:	'elastic',
        'speedIn'		:	600, 
        'speedOut'		:	200, 
        'overlayShow'	:	false
    });
	
});
JS;
$this->registerJS($script);
?>

