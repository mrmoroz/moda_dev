<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<div class="PopUp" id="skipReg">
    <h3>Пропустить регистрацию</h3>
    <?php
        $form = ActiveForm::begin([
            'id' => 'skipreg-form-in-cart',
            'action'=>'/cart/skipreg',
            'options' => ['class' => 'popup-submit-form-in-cart'],
            'enableAjaxValidation' => false,
        ]);
    ?>
    <?= $form->field($modelSkip, 'first_name', ['template' => '{input}{error}'])->textInput(['class' => 'my-form-control','placeholder'=>'Ваше имя']) ?>
    <?= $form->field($modelSkip, 'phone', ['template' => '{input}{error}'])->textInput(['class' => 'my-form-control','placeholder'=>'Телефон']) ?>
    <?= $form->field($modelSkip, 'email', ['template' => '{input}{error}'])->textInput(['class' => 'my-form-control email-skip','placeholder'=>'E-mail']) ?>
    <?= $form->field($modelSkip, 'address', ['template' => '{input}{error}'])->textarea(['class' => 'my-form-control','placeholder'=>'Адрес доставки']) ?>
    <?= $form->field($modelSkip, 'comment', ['template' => '{input}{error}'])->textarea(['class' => 'my-form-control','placeholder'=>'Комментарий']) ?>
    <div style="text-align: center;">
        <?= \yii\bootstrap\Html::submitButton('Заказать', ['class'=>'btn btn-danger btn-lg']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php
$script = <<<JS
;$(function(){
    var email;
    $('#skipform-email').focusout(function() {
        email = $(this).val();
        if(email.length > 7){
            $('.email-skip').parent().after("<div class='tr is_acc-skip'><div class='th'>Создать \"Личный кабинет\"</div><div class='td'>"+
            "<div class='form-group field-skipform-is_account'>"+
            "<input name='SkipForm[is_account]' value='0' type='hidden'><label><input id='skipform-is_account' name='SkipForm[is_account]' value='1' type='checkbox'></label>"+
            "</div>"+
            "</div></div>");
        }else{
            if($('div').hasClass('is_acc-skip')){
                $('.is_acc-skip').remove();
            }
        }
    });
    
    $("#skipform-phone").inputmask("+9 999 999-99-99", {showMaskOnHover: false});
    
});
JS;

//$this->registerJS($script);
?>


