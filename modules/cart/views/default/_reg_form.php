<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
$assetBundle = \app\assets\HtmlAsset::register($this);

?>
<div class="PopUp" id="Registration">
    <h3>Регистрация</h3>

    <?php
        $form = ActiveForm::begin([
            'id' => 'reg-form-in-cart',
            'action'=>'/cart/signup',
            'options' => ['class' => 'popup-submit-form-in-cart'],
            'enableAjaxValidation' => true,
        ]);
    ?>
    <div class="form-content">
        <?php if($paramRegForm):?>
            <p class="text-param-reg-form"><?=$paramRegForm->value?></p>
        <?php endif;?>
        <?= $form->field($modelReg,'login')->textInput(['class'=>'my-form-control','placeholder'=>'E-mail'])->label(false)?>

        <?= $form->field($modelReg,'password')->passwordInput(['class'=>'my-form-control','placeholder'=>'Пароль'])->label(false)?>

        <?= $form->field($modelReg,'password_confirm')->passwordInput(['class'=>'my-form-control','placeholder'=>'Повторите пароль'])->label(false)?>

        <?= $form->field($modelReg,'first_name')->textInput(['class'=>'my-form-control','placeholder'=>'Ваше имя'])->label(false)?>

        <div style="text-align: center;">
            <?= \yii\bootstrap\Html::submitButton('Зарегистрироваться', ['class'=>'btn btn-danger btn-lg']) ?>
        </div>
    </div>
    <p>Нажимая на кнопку "Зарегистрироваться", я соглашаюсь с условиями <a href="#">Публичной оферты</a></p>
    <div class="icon-block">
        <a href="#"><?php echo Html::img($assetBundle->baseUrl.'/img/FB.png') ?></a>
        <a href="#"><?php echo Html::img($assetBundle->baseUrl.'/img/mail.png') ?></a>
        <a href="#"><?php echo Html::img($assetBundle->baseUrl.'/img/Odnoklasniki.png') ?></a>
        <a href="#"><?php echo Html::img($assetBundle->baseUrl.'/img/VK.png') ?></a>
    </div>

    <?php ActiveForm::end(); ?>
</div>    