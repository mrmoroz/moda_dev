<?php
//vd($payment_methods_list,false);
//vd($DefAddress,false);
if($payment_methods){
    $user = Yii::$app->user->getIdentity();
    $adId = 0;
    foreach ($payment_methods as $item){
        if($item->code==$user->pay_method){
            $adId = $item->id;
        }
    }
}
?>
<?php if(count($payment_methods_list)>0):?>
<div class="form-group field-checkoutform-pay_method_list">
    <select id="checkoutform-pay_method_list" class="form-control" name="CheckoutForm[pay_method_list]">
        <?php foreach ($payment_methods_list as $key=>$it):?>
            <?php if($key==$adId):?>
                <?php $selected = "delected"?>
            <?php else:?>
                <?php $selected = ""?>
            <?php endif;?>
            <option value="<?=$key?>" <?=$selected?>><?=$it?></option>
        <?php endforeach;?>
    </select><div class="help-block"></div>
</div>
<?php endif;?>