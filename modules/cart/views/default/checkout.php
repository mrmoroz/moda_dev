<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use pavlinter\display\DisplayImage;
use app\components\widgets\ShowLoadingWidgets;
use app\models\Product;
use app\models\Regions;
use app\models\Color;
use app\models\Size;
use app\modules\cart\models\DelivList;

$product = new Product();

$this->title="Оформить заказ";
echo ShowLoadingWidgets::widget(['loadingType' => 1]);

if(!empty($user->delivery_method)){
    $model->delivery = $user->delivery_method;
}else{
    $model->delivery = 'post';
}
if($DefAddress){
    $model->address = $DefAddress->id;
}
if($this->params['country']=='KZ'){
    $model->country = 80;
}elseif($this->params['country']=='RU'){
    $model->country = 157;
}
if(isset($geo_data->region) && !empty($geo_data->region) && $model->country==157){
    $region_geo = Regions::find()->where(['like','name_geo',$geo_data->region])->one();
    if($region_geo){
        $model->region_id = $region_geo->id;
    }
}
?>

<div class="checkout-wrap">
    <div class="bg-round">
        <h3>Оформление заказа</h3>
        <p>Уважаемый покупатель! Перед оформление заказа ознакомьтесь, пожалуйста, с <a href="#">Правилами Интернет-магазина</a></p>
        <hr>
    <?php
        $form = ActiveForm::begin([
            'id' => 'checkout-form',
            'action'=>'/cart/checkout',
            'enableAjaxValidation' => false,
        ]);
    ?>
    <div class="tabl-form">
        <div class="tr">
            <div class="th">Мобильный телефон <span class="star">*</span></div>
            <div class="td">
                <?php
                    $mobile = $profile->mobile_phone;
                    if($DefAddress){
                        $mobile = $DefAddress->mphone;
                    }
                ?>
                <?= $form->field($model, 'mobile_phone', ['template' => '{input}{error}'])->textInput(['value' => $mobile]) ?>
            </div>
        </div>
        <?php if(count($formAddress)>0):?>
            <div class="tr old-address">
                <div class="th">Ваши адреса</div>
                <div class="td">
                    <?= $form->field($model, 'address', ['template' => '{input}{error}'])->dropDownList($formAddress) ?>
                </div>
            </div>
            <div class="tr">
                <div class="th"></div>
                <div class="td">
                    <button type="button" class="add-to-cart-button add-new-address">Новый адрес</button>
                </div>
            </div>
            <div class="tr new-address country-block">
                <div class="th">Страна</div>
                <div class="td">
                    <?= $form->field($model, 'country', ['template' => '{input}{error}'])->dropDownList($list_country,['data-cl'=>'new']); ?>
                </div>
            </div>
            <?php if($model->country==157):?>
                <div class="tr new-address region-block">
                    <div class="th">Регион</div>
                    <div class="td">
                        <?= $form->field($model, 'region_id', ['template' => '{input}{error}'])->dropDownList($list_regions); ?>
                    </div>
                </div>
            <?php endif;?>
            <div class="tr new-address">
                <div class="th">Город</div>
                <div class="td">
                    <?= $form->field($model, 'city', ['template' => '{input}{error}'])->textInput(['placeholder'=>'Город','value'=>'.']); ?>
                </div>
            </div>
            <div class="tr new-address">
                <div class="th">Улица</div>
                <div class="td">
                    <?= $form->field($model, 'street', ['template' => '{input}{error}'])->textInput(['placeholder'=>'Улица','value'=>'.']); ?>
                </div>
            </div>
            <div class="tr new-address">
                <div class="th">Дом</div>
                <div class="td">
                    <?= $form->field($model, 'house', ['template' => '{input}{error}'])->textInput(['placeholder'=>'Дом','value'=>'.']); ?>
                </div>
            </div>
            <div class="tr new-address">
                <div class="th">Квартира</div>
                <div class="td">
                    <?= $form->field($model, 'flat', ['template' => '{input}{error}'])->textInput(['placeholder'=>'Квартира','value'=>'.']); ?>
                </div>
            </div>
            <div class="tr new-address">
                <div class="th">Почтовый индекс</div>
                <div class="td">
                    <?= $form->field($model, 'post_index', ['template' => '{input}{error}'])->textInput(['placeholder'=>'Почтовый индекс','value'=>'.']); ?>
                </div>
            </div>
        <?php else:?>
            <div class="tr country-block">
                <div class="th">Страна</div>
                <div class="td">
                    <?= $form->field($model, 'country', ['template' => '{input}{error}'])->dropDownList($list_country,['data-cl'=>'first']); ?>
                </div>
            </div>
            <?php if($model->country==157):?>
            <div class="tr region-block">
                <div class="th">Регион</div>
                <div class="td">
                    <?= $form->field($model, 'region_id', ['template' => '{input}{error}'])->dropDownList($list_regions,['prompt'=>'Выберете регион']); ?>
                </div>
            </div>
            <?php endif;?>
            <div class="tr">
                <div class="th">Город</div>
                <div class="td">
                    <?= $form->field($model, 'city', ['template' => '{input}{error}'])->textInput(['placeholder'=>'Город']); ?>
                </div>
            </div>
            <div class="tr">
                <div class="th">Улица</div>
                <div class="td">
                    <?= $form->field($model, 'street', ['template' => '{input}{error}'])->textInput(['placeholder'=>'Улица']); ?>
                </div>
            </div>
            <div class="tr">
                <div class="th">Дом</div>
                <div class="td">
                    <?= $form->field($model, 'house', ['template' => '{input}{error}'])->textInput(['placeholder'=>'Дом']); ?>
                </div>
            </div>
            <div class="tr">
                <div class="th">Квартира</div>
                <div class="td">
                    <?= $form->field($model, 'flat', ['template' => '{input}{error}'])->textInput(['placeholder'=>'Квартира']); ?>
                </div>
            </div>
            <div class="tr">
                <div class="th">Почтовый индекс</div>
                <div class="td">
                    <?= $form->field($model, 'post_index', ['template' => '{input}{error}'])->textInput(['placeholder'=>'Почтовый индекс']); ?>
                </div>
            </div>
        <?php endif;?>
        <div class="tr">
            <div class="th">Способ доставки </div>
            <div class="td">
                <?= $form->field($model, 'delivery', ['template' => '{input}{error}'])->dropDownList($deliv_list); ?>
            </div>
        </div>
        <div class="tr">
            <div class="th">Получатель</div>
            <div class="td">
                
            </div>
        </div>
        <div class="tr">
            <div class="th">Имя <span class="star">*</span></div>
            <div class="td">
                <?php
                $nname = $profile->first_name;
                if($DefAddress){
                    $nname = $DefAddress->name;
                }
                ?>
                <?= $form->field($model, 'first_name', ['template' => '{input}{error}'])->textInput(['placeholder'=>'Имя','value'=>$nname]); ?>
            </div>
        </div>
        <div class="tr last_name">
            <div class="th">Фамилия <span class="star">*</span></div>
            <div class="td">
                <?php
                    $surname = $profile->last_name;
                    if($DefAddress){
                        $surname = $DefAddress->surname;
                    }
                ?>
                <?= $form->field($model, 'last_name', ['template' => '{input}{error}'])->textInput(['placeholder'=>'Фамилия','value'=>$surname]); ?>
            </div>
        </div>
        <?php if($model->delivery=='post'):?>
            <div class="tr midname">
                <div class="th">Отчество <span class="star">*</span></div>
                <div class="td">
                    <?php
                        $middle_name = $profile->second_name;
                        if($DefAddress){
                            $middle_name = $DefAddress->middle_name;
                        }
                    ?>
                    <?= $form->field($model, 'middle_name', ['template' => '{input}{error}'])->textInput(['placeholder'=>'Отчество','value'=>$middle_name]); ?>
                </div>
            </div>
        <?php endif;?>
        <div class="tr">
            <div class="th">Электронная почта</div>
            <div class="td">
                <?= $form->field($model, 'email', ['template' => '{input}{error}'])->textInput(['placeholder'=>'Электронная почта','value'=>$user->login]); ?>
            </div>
        </div>
        <div class="tr">
            <div class="th"></div>
            <div class="td">
                <?=$form->field($model, 'ignore_mail_list')->checkbox(['label' => 'Подписаться на новости и скидки']);?>
            </div>
        </div>
        <div class="tr">
            <div class="th">Комментарий к заказу</div>
            <div class="td">
                <?=$form->field($model, 'comment',['template' => '{input}{error}'])->textarea(['style'=>'width:100%;']);?>
            </div>
        </div>
    </div>
    <div class="wrap-result-checkout">
        <hr>
        <?php
            foreach ($payment_methods as $it){
                if($it->code==$user->pay_method){
                    $paymentDisc = $it->discount;
                    $model->pay_method_list = $it->id;
                    break;
                }
            }
            $paymentDiscount = (isset($paymentDisc) && $paymentDisc > 0) ? round($_SESSION['cart']['sum']*$paymentDisc / 100) : 0;
        ?>
        <?php if((int)$ac != 0):?>
            <?php
            $itogo = $_SESSION['cart']['sum'] - $paymentDiscount;
            if((float)$ac >= $itogo){
                $itogo = 0;
            }else{
                $itogo = $itogo - $ac;
            }
            ?>
            <h3>Итого: <?=$_SESSION['cart']['allqty']?> <?=$model->plural($_SESSION['cart']['allqty'], 'товар', 'товара', 'товаров')?> на сумму <span class="itog"><?=$itogo?></span> <?=$product->getSymbol($this->params['country'])?></h3>
        <?php else:?>
            <h3>Итого: <?=$_SESSION['cart']['allqty']?> <?=$model->plural($_SESSION['cart']['allqty'], 'товар', 'товара', 'товаров')?> на сумму <span class="itog"><?=$_SESSION['cart']['sum'] - $paymentDiscount?></span> <?=$product->getSymbol($this->params['country'])?></h3>
        <?php endif;?>
        <div class="wrap-data-checkout">
            <div class="promo-code">
                <div class="wr-pr">
                    <div class="l-pr"><?=$form->field($model, 'promocode', ['template' => '{input}{error}'])->textInput(['placeholder'=>'Промокод'])?></div>
                    <div class="r-pr"><button type="button" class="btn-promo add-promo">Применить</button></div>
                </div>
                <div class="wr-pr">
                    <div class="l-op">Способ оплаты</div>
                    <div class="r-op"><?=$form->field($model,'pay_method_list',['template' => '{input}{error}'])->dropDownList($payment_methods_list,['style'=>'width:100%;'])?></div>
                </div>
            </div>
            <div class="res-checkout">
                <div class="tabl total">
                    <div class="tr">
                        <div class="td" style="border-bottom:none;">Стоимость товаров</div>
                        <div class="td digits" id="cart-price"><?= $_SESSION['cart']['allsum'] ?> <?=$product->getSymbol($this->params['country'])?></div>
                    </div>
                    <div class="tr">
                        <div class = "td"  style = "border-bottom:none;">Сумма скидки при выбранном способе оплаты (<span class="dis"><?=(isset($paymentDisc)?$paymentDisc:0)?></span>%)</div>
                        <div class = "td digits"><span class="discountSum"><?= $paymentDiscount ?></span> <?=$product->getSymbol($this->params['country'])?></div>
                    </div>
                    <div class="tr">
                        <div class="td" style="border-bottom:none;">Общая экономия по заказу</div>
                        <div class="td digits" id="cart-price"><span class="alleconom"><?= $_SESSION['cart']['allsum'] - $_SESSION['cart']['sum'] + $paymentDiscount ?></span> <?=$product->getSymbol($this->params['country'])?></div>
                    </div>
                    <div class="tr">
                        <div class="td" style="border-bottom:none;">Доставка (стоимость)</div>
                        <div class = "td digits delivery">
                            <?php if((int)$_SESSION['cart']['allsum'] >= 3000 && ($model->delivery == 'post' || $model->delivery=='cpcr')):?>
                                0 <?=$product->getSymbol($this->params['country'])?>
                            <?php else:?>
                                <?php
                                    $text = DelivList::findOne(['code'=>$model->delivery]);
                                    echo $text->price;
                                ?>
                            <?php endif;?>
                        </div>
                    </div>
                    <?php if((int)$ac != 0):?>
                        <?php
                        $itogo = $_SESSION['cart']['sum'] - $paymentDiscount;
                        if((float)$ac >= $itogo){
                            $ostatok_ac = $ac - $itogo;
                            $summa_spis = $itogo;
                            $itogo = 0;
                        }else{
                            $ostatok_ac = 0;
                            $summa_spis = $ac;
                            $itogo = $itogo - $ac;
                        }
                        ?>
                        <div class="tr">
                            <div class = "td" style = "border-bottom:none;">С вашего лицевого счета будет списано</div>
                            <div class = "td digits"><strong id = "cart-total-price"><span class="itog-sp"><?=$summa_spis?></span> <?=$product->getSymbol($this->params['country'])?></strong></div>
                        </div>
                        <div class = "tr">
                            <div class = "td" style = "border-bottom:none;">Итого</div>
                            <div class = "td digits"><strong id = "cart-total-price"><span class="itog"><?= $itogo ?></span> <?=$product->getSymbol($this->params['country'])?></strong></div>
                        </div>
                    <?php else:?>
                        <div class = "tr">
                            <div class = "td" style = "border-bottom:none;">Итого</div>
                            <div class = "td digits"><strong id = "cart-total-price"><span class="itog"><?= $_SESSION['cart']['sum'] - $paymentDiscount ?></span> <?=$product->getSymbol($this->params['country'])?></strong></div>
                        </div>
                    <?php endif;?>
                </div>
            </div>
        </div>
    </div>
    <div style="text-align: right; margin-top: 10px;">
        <button type="submit" class="add-to-cart-button">Отправить заказ</button>
    </div>
    <?php ActiveForm::end(); ?>
    </div>
    <h3>Товары</h3>
    <?php if($user):?>
        <p>Размер вашей накопительной скидки: <?=$user->discount_user?> %. Накопительная скидка не применяется на товары с другими скидками</p>
    <?php endif;?>
    <div class="tabl highlight">
        <div class="tr">
            <div class="th">Изделие</div>
            <div class="th">Товар</div>
            <div class="th">Размер</div>
            <div class="th">Цвет</div>
            <div class="th">Кол-во</div>
            <div class="th">Цена</div>
            <div class="th">Цена со скидкой</div>
            <div class="th">Итого</div>
        </div>
        <?php foreach ($_SESSION['cart'] as $key => $items): ?>
            <?php if($key!='allqty' && $key!='sum' && $key!='allsum'):?>
                <?php
                    $prod = Product::findOne($items['id']);
                ?>
                <?php if($prod):?>
                    <div class="tr">
                        <div class="td"><strong><?=$items['name']?></strong><br><?=$items['article']?></div>
                        <div class="td">
                            <a href="<?=$prod->getUrl();?>" target="_blank">
                                <?= DisplayImage::widget([
                                    'width' => 50,
                                    'height' => 75,
                                    'image' => $items['img'],
                                    'category' => 'all',
                                ]);?>
                            </a>
                        </div>
                        <div class="td"><?= Size::getName($items['attr']['size']);?></div>
                        <div class="td"><?= Color::getName($items['attr']['color']);?></div>
                        <div class="td amount"><?= $items['qty']?></div>
                        <div class="td digits">
                            <?php if(!empty($items['discount_price'])):?>
                                <s><?= $items['price']?> <?=$product->getSymbol($this->params['country'])?></s>
                            <?php else:?>
                                <?= $items['price']?> <?=$product->getSymbol($this->params['country'])?>
                            <?php endif;?>
                        </div>
                        <div class="td digits"><?= $items['discount_price']?> <?=$product->getSymbol($this->params['country'])?></div>
                        <div class="td digits"><?= $items['discount_price']*$items['qty']?> <?=$product->getSymbol($this->params['country'])?></div>
                    </div>
                    <?php if(isset($items['gifts']) && count($items['gifts'])>0):?>
                        <?php foreach ($items['gifts'] as $gift):?>
                            <div class="tr">
                                <div class="td"><strong><?=$gift['gift_name']?></strong> (подарок) <br><?=$gift['gift_article']?></div>
                                <div class="td">
                                    <?= DisplayImage::widget([
                                        'width' => 50,
                                        'height' => 75,
                                        'image' => $gift['gift_img'],
                                        'category' => 'all',
                                    ]);?>
                                </div>
                                <div class="td"> - </div>
                                <div class="td"> - </div>
                                <div class="td"><?=$items['qty']?></div>
                                <div class="td">0 <?=$product->getSymbol($this->params['country'])?></div>
                                <div class="td">0 <?=$product->getSymbol($this->params['country'])?></div>
                                <div class="td">0 <?=$product->getSymbol($this->params['country'])?></div>
                            </div>
                        <?php endforeach;?>
                    <?php endif;?>
                <?php endif;?>
            <?php endif;?>
        <?php endforeach;?>
    </div>
</div>

<?php
$script = <<<JS
$( ".new-address" ).hide();
;$(function(){
    
    var token = "395c6a6d5d99946f9756ac8d1b5d5abaa4306aa8";
    var city = $(document).find('#checkoutform-city');
    var street = $(document).find('#checkoutform-street');
    var house = $(document).find('#checkoutform-house');
    var plugin, plugin2, plugin3;
    
    $( ".add-new-address" ).click(function() {
        $('#checkoutform-city').val('');
        $('#checkoutform-street').val('');
        $('#checkoutform-house').val('');
        $('#checkoutform-flat').val('');
        $('#checkoutform-post_index').val('');
        
        if($('.new-address:visible').length){
            $('.new-address').hide(function(){
                $(this).children('.td').children().children('input').val('.');
            });
        }else{
            $('.new-address').show();  
        }
         
    });
    
    $(document).on('change',"#checkoutform-pay_method_list",function(event) {
        var code = $(this).val();
        var cd_deliv = $('#checkoutform-delivery').val();
        $.ajax({
            url: '/cart/change-post-method',
            data: {code: code},
            type: 'POST',
            beforeSend: function(){
                $('.wrap-result-checkout').showLoading();
            },
            success: function(res){
                $('.wrap-result-checkout').hideLoading();
                var response = JSON.parse(res);
                if(!response) alert('Ошибка!');
                if(response){
                    if(typeof response.ok != 'undefined'){
                        $('.dis').text(response.data.discount);
                        $('.discountSum').text(response.data.discountSum);
                        $('.alleconom').text(response.data.alleconom);
                        $('.itog').text(response.data.itog);
                        $('.itog-sp').text(response.data.summa_spis);
                        changeDelivery(cd_deliv,response.data.itog);
                    }
                }
            },
        });
        return false;
    });
    
    $(document).on('change',"#checkoutform-delivery",function(event) {
        var code = $(this).val();
        var price = $('#cart-total-price span.itog').text();
        changeDelivery(code,price);
        if(code == 'post'){
            $('.last_name').after('<div class="tr midname"><div class="th">Отчество <span class="star">*</span></div><div class="td"><div class="form-group field-checkoutform-middle_name"><input id="checkoutform-middle_name" class="form-control" name="CheckoutForm[middle_name]" placeholder="Отчество" type="text"><div class="help-block"></div></div></div></div>');
        }else{
            $('.midname').remove();
        }
        return false;
    });
    
    
    function changeDelivery(code,price) {
        $.ajax({
            url: '/cart/change-deliv-method',
            data: {code: code, price:price},
            type: 'POST',
            beforeSend: function(){
                $('.wrap-result-checkout').showLoading();
            },
            success: function(res){
                $('.wrap-result-checkout').hideLoading();
                var response = JSON.parse(res);
                if(!response) alert('Ошибка!');
                if(response){
                    if(typeof response.ok != 'undefined'){
                        $('.delivery').text(response.data.delivery);
                    }
                }
            },
        });
    }
    
    
    $("#checkoutform-address").on('change',function(event) {
        var id = $(this).val();
        $.ajax({
            url: '/cart/change-address',
            data: {id: id},
            type: 'POST',
            beforeSend: function(){
                $('.tabl-form').showLoading();
            },
            success: function(res){
                $('.tabl-form').hideLoading();
                var response = JSON.parse(res);
                if(!response) alert('Ошибка!');
                if(response){
                    if(typeof response.ok != 'undefined'){
                        $('.r-op').html(response.view);
                        $('#checkoutform-mobile_phone').val(response.defaddress_phone);
                        $('#checkoutform-first_name').val(response.defaddress_name);
                        $('#checkoutform-last_name').val(response.defaddress_surname);
                        $('#checkoutform-middle_name').val(response.defaddress_middle_name);
                    }
                }
            },
        });
        return false;
    });
    
    
    $.ajax({
        url: '/cart/getgeo',
        type: 'POST',
        success: function(res){
            var response = JSON.parse(res);
            if(response){
                if(typeof response.ok != 'undefined' && response.ok=='RU'){
                    plugin = city.suggestions({
                        token: token,
                        type: "ADDRESS",
                        hint: false,
                        bounds: "city",
                        geoLocation: false,
                        onSelect: showPostalCode,
                        onSelectNothing: clearPostalCode
                    }).suggestions();
                    plugin2 = street.suggestions({
                        token: token,
                        type: "ADDRESS",
                        hint: false,
                        bounds: "street",
                        constraints: city,
                        onSelect: showPostalCode,
                        onSelectNothing: clearPostalCode
                    }).suggestions();
                    plugin3 = house.suggestions({
                          token: token,
                          type: "ADDRESS",
                          hint: false,
                          bounds: "house",
                          constraints: street,
                          onSelect: showPostalCode,
                          onSelectNothing: clearPostalCode
                    }).suggestions();
                    
                    var phone_in = $("#checkoutform-mobile_phone");
                    if(phone_in.val()!=''){
                        phone_in.inputmask("+9 999 999-99-99", {showMaskOnHover: false});
                    }else{
                        phone_in.inputmask("+7 999 999-99-99", {showMaskOnHover: false});
                    }
                }else{
                    $("#checkoutform-mobile_phone").inputmask("+9 999 999-99-99", {showMaskOnHover: false});
                }
            }
        },
    });
    
    $(document).on('change','#checkoutform-country', function(){
        var id = $(this).val();
        var cl = $(this).data('cl');
        if(id==157){
            $.ajax({
                url: '/cart/change-country',
                data: {id: id},
                type: 'POST',
                beforeSend: function(){
                    $('.tabl-form').showLoading();
                },
                success: function(res){
                    $('.tabl-form').hideLoading();
                    var response = JSON.parse(res);
                    if(!response) alert('Ошибка!');
                    if(response){
                        if(typeof response.ok != 'undefined'){
                            $('.country-block').after(response.view);
                            if(cl=='new'){
                                $(document).find('.region-block').addClass('new-address');
                            }
                        }
                    }
                },
            });
            plugin.enable();
            plugin2.enable();
            plugin3.enable();
            $("#checkoutform-mobile_phone").inputmask("+9 999 999-99-99", {showMaskOnHover: false});
        }else{
            $(document).find('.region-block').remove();
            plugin.disable();
            plugin2.disable();
            plugin3.disable();
            $("#checkoutform-mobile_phone").inputmask("+9 999 999-99-99", {showMaskOnHover: false});
        }
    });
    
    
    
});

/**
 * Показывает индекс в отдельном поле
 */
function showPostalCode(suggestion) {
    $(document).find("#checkoutform-post_index").val(suggestion.data.postal_code);
}
/**
 * Очищает индекс
 */
function clearPostalCode(suggestion) {
    $(document).find("#checkoutform-post_index").val("");
}
JS;
$this->registerJS($script);
?>

