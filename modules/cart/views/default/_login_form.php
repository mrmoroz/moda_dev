<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
$model->rememberMe = 1;
?>
<div class="PopUp" id="Authorization">
    <h3>Вход</h3>
    <p>Если Вы у нас впервые, <a href="#Registration" class="up_reg_form">зарегистрируйтесь</a>. Регистрация позволит Вам участвовать в наших акциях, а также получать <b >накопительные скидки</b> и приятные сюрпризы. </p>
    <p>После регистрации Вы получите доступ в личный кабинет - Ваш персональный раздел на нашем сайте, где Вы можете сохранить адреса и способы доставки, увидеть состояние Ваших текущих заказов и историю заказов, сделанных ранее. В данном разделе Вы можете посмотреть личные скидки, а также состояние Вашего лицевого счета. Регистрация не обязательна, Вы можете пропустить этот шаг.</p>
    <div>
        <a href="#skipReg" class="up_skip_reg">Пропустить регистрацию</a>
    </div>
    <p>Если Вы зарегистрированный пользователь нашего магазина, для доступа в личный кабинет пройдите авторизацию ниже.</p>
    <?php
    $form = ActiveForm::begin([
        'id' => 'login-form-in-cart',
        'action'=>'/cart/login',
        'options' => ['class' => 'popup-submit-form-in-cart'],
        'enableAjaxValidation' => false,
    ]);
    ?>
    <?= $form->field($model, 'login')->textInput(['class'=>'my-form-control','placeholder'=>'E-mail'])->label(false)?>
    <?= $form->field($model, 'password')->passwordInput(['class'=>'my-form-control','placeholder'=>'Пароль'])->label(false)?>
    <?= $form->field($model, 'rememberMe')->checkbox() ?>
    <?=Html::a('Забыли пароль?',['/site/send-email']);?>
    <div style="text-align: center; margin-top: 20px; margin-bottom: 10px;">
        <?= \yii\bootstrap\Html::submitButton('Войти', ['class'=>'btn btn-danger btn-lg']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <div style=" display:block; float:right">
        <a href="#Registration" class="up_reg_form">Ещё не зарегестрированы?</a>
    </div>


</div>