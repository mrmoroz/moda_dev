<?php
$this->title = "Ошибка";
?>
<div style="margin-top: 50px; padding-left: 200px;">
    <h1>Ошибка оплаты</h1>
    <div style="color: red; font-weight: bold; text-transform: uppercase">
        Оплата банковской картой не произведена, попробуйте еще раз, либо выберите другой способ оплаты<br><br>
    </div>
    <br>
    <a href="/cart">ПЕРЕЙТИ В КОРЗИНУ</a>
</div>