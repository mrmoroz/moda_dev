<?php

namespace app\modules\cart\controllers;

use app\components\BaseController;
use app\helpers\CommonHelper;
use app\models\Country;
use app\models\OptTmpOrders;
use app\models\PostOperationHistory;
use app\models\Regions;
use app\models\User;
use app\models\UserFavourite;
use app\modules\admin\models\OrdstatList;
use app\modules\admin\models\Sales;
use app\modules\cart\models\Cart;
use app\modules\cart\models\OptOrderItems;
use app\modules\cart\models\OptOrders;
use app\modules\cart\models\Orders;
use app\modules\cart\models\RegForm;
use yii\debug\models\search\Debug;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use Yii;
use app\models\Product;
use app\models\PaymentList;
use app\forms\LoginForm;
use app\models\UserProfile;
use app\models\UserAddress;
use app\modules\cart\models\CheckoutForm;
use app\modules\cart\models\DelivList;
use app\forms\SkipForm;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\Parametrs;
use app\models\StockPosition;
use app\models\Size;
use app\models\Color;
use app\models\ProductSelection;
use app\models\MlCfg;

/**
 * Default controller for the `cart` module
 */
class DefaultController extends BaseController
{
    public $attr = [];
    public $cart = [];
    /**
     * Корзина
     */
    public function actionIndex()
    {
        $totalAmount = 0;
        $positions = array();
        $key = 0;

        if(Yii::$app->user->isGuest){
            return $this->redirect('/login');
        }

        $session_cart = [];
        if(isset($_SESSION['cart'])){
            $session_cart = $_SESSION['cart'];
            foreach ($session_cart as $itemId => $itemInfo){
                $product = Product::findOne(['id'=>$itemId,'is_active'=>true]);
                if ($product) {
                    foreach ($itemInfo as $colorId => $sizeArray) {
                        $colorInfo = Color::find()->where(['id'=>$colorId])->one();
                        $colorName = ($colorInfo) ? $colorInfo->name : 'Базовый';
                        foreach ($sizeArray as $sizeId => $count) {
                            $sizeInfo = Size::find()->where(['id'=>$sizeId])->one();
                            if ($sizeInfo) {
                                $positions[$key]['product'] = $product;
                                $positions[$key]['color'] = $colorName;
                                $positions[$key]['color_id'] = $colorId;
                                $positions[$key]['size'] = $sizeInfo;
                                $positions[$key]['cnt'] = $count;
                                $price = $product->getPrice($product->wholesale_price,1);
                                if(!empty($product->wholesale_discount_price)){
                                    $price = $product->getPrice($product->wholesale_discount_price,1);
                                }
                                $totalAmount += $count * $price;
                            } else {
                                unset($session_cart[$itemId][$colorId][$sizeId]);
                            }
                            $key++;
                        }
                    }
                }else{
                    unset($session_cart[$itemId]);
                }
            }
        }

        $_SESSION['cart'] = $session_cart;
        return $this->render('cart',[
            'positions'=>$positions,
            'totalAmount' => $totalAmount
        ]);
    }

    public function actionSignup(){
        $model = new RegForm();
        //ajax валидация
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if($model->load(Yii::$app->request->post())){
            //регистрируем пользователя
            $model->reguser();
            return $this->redirect('/cart');
        }

    }

    /**
     * Заказ без регистрации
     */
    public function actionSkipreg(){
        if(!isset($_SESSION['cart'])){
            return $this->redirect('/');
        }
        $model = new SkipForm();
        if($model->load(Yii::$app->request->post()) && $model->validate()){
            //if($model->is_account){
            if(!empty($model->email)){
                //регистрируем пользователя
                $model->reguser();
            }
            //создаем заказ
            $id_order = $model->saveOrder($this->geo_data);
            if($id_order){
                return $this->redirect('/cart/success?id='.$id_order);
            }
        }
    }

    /**
     * Добавить в корзину
     */
    public function actionAdd(){
        $id = (int)Yii::$app->request->get('id_product');
        $color = (int)Yii::$app->request->get('color');
        $size = (int)Yii::$app->request->get('size');
        $qty = (int)Yii::$app->request->get('qty');
        $qty = !$qty ? 1 : $qty;
        $product = Product::findOne(['id'=>$id,'is_active'=>true]);
        if(empty($product)) return false;
        if(!empty($size)){
            $this->createAttr('size',$size);
        }
        if(!empty($color)){
            $this->createAttr('color',$color);
        }
        $session =Yii::$app->session;
        if (!$session->isActive){
            $session->open();
        }
        $cartModel = new Cart();

        //Подарки
        $gifts_products = $cartModel->getGifts($product->id);
        $cartModel->addToCart($product, $qty, $this->kurs_many,$gifts_products, $this->attr);
        if( !Yii::$app->request->isAjax ){
            return $this->redirect(Yii::$app->request->referrer);
        }
        $this->layout = false;
        $view =  $this->render('cart-modal', [
            'session_cart'=>$_SESSION['cart'],
            'product'=>$product,
            'attr'=>$this->attr,
            'qty'=>$qty,
        ]);
        return json_encode(['view'=>$view,'count'=>$_SESSION['cart']['allqty'],'sum'=>$_SESSION['cart']['sum']]);
    }

    /**
     * Добавить в корзину
     */
    public function actionAdditem()
    {
        $id = (int)Yii::$app->request->post('item_id');
        $color = (int)Yii::$app->request->post('color_id');
        $size = (int)Yii::$app->request->post('size_id');
        $qty = (int)Yii::$app->request->post('items_amount');
        $qty = !$qty ? 1 : $qty;
        $product = Product::findOne(['id'=>$id,'is_active'=>true]);
        if(!$product){
            return $this->redirect('/');
        }

        if ($size != 'all') {
            if (isset($_SESSION['cart'][$id][$color][$size])) {
                $_SESSION['cart'][$id][$color][$size] += $qty;
            } else {
                $_SESSION['cart'][$id][$color][$size] = $qty;
            }
        }else{
            $itemColorSizes = $this->getSizeListArrayByColor($id,$color);
            if($itemColorSizes){
                foreach ($itemColorSizes as $sizeId){
                    if (isset($_SESSION['cart'][$id][$color][$sizeId->size_id])) {
                        $_SESSION['cart'][$id][$color][$sizeId->size_id] += $qty;
                    } else {
                        $_SESSION['cart'][$id][$color][$sizeId->size_id] = $qty;
                    }
                }
            }
        }

        return $this->redirect('/cart');
    }

    private function getSizeListArrayByColor($id,$color)
    {
        $result = StockPosition::find()->distinct('size_id')->select('size_id')->where(['product_id'=>$id])->andWhere(['color_id'=>$color])->all();
        return $result;
    }

    /**
     * Удалить элемент из корзины
     */
    public function actionDeleteitem()
    {
        $pid = (int)Yii::$app->request->get('pid');
        $cid = (int)Yii::$app->request->get('cid');
        $sid = (int)Yii::$app->request->get('sid');
        if (isset($_SESSION['cart'][$pid][$cid][$sid])) {
            unset($_SESSION['cart'][$pid][$cid][$sid]);
            if(empty($_SESSION['cart'][$pid][$cid])){
                unset($_SESSION['cart'][$pid][$cid]);
            }
            if(empty($_SESSION['cart'][$pid])){
                unset($_SESSION['cart'][$pid]);
            }
        }
        return $this->redirect('/cart');
    }

    /**
     * Заказ
     */
    public function actionOrder()
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect('/login');
        }
        if(isset($_SESSION['cart']) && !empty(isset($_SESSION['cart']))){
            $this->cart = $_SESSION['cart'];
        }
        $comment = Yii::$app->request->post('comment');
        $payment = Yii::$app->request->post('payment');
        if(!$payment){
            $payment = '0';
        }
        $user_id = Yii::$app->user->id;

        $model = new CheckoutForm();
        $stocks =  $model->getStocks(); //наши скдады
        //$man_stocks = $model->getManStocks(); //склады поставщиков

        if($this->cart){
            //Сохранение заказа
            $order = (new \app\modules\cart\services\CartService())->addOrder(Yii::$app->user->identity, $comment, $payment);

            if($order){
                // Товары в заказе
                $positions = (new \app\modules\cart\services\CartService())->addOrderItems($this->cart, $order, $stocks, $payment);
            }

            if($payment == '1'){
                $order = OptTmpOrders::findOne($order->id);
                if($order){
                    $invoice = \app\modules\sberbank\models\Invoice::addSberbank($order->id, $order->total_cost, null, [], $order->user_id);
                    $this->cart = [];
                    return $this->redirect(['/sberbank/default/create', 'id' => $invoice->id]);
                }
            }


            $userModel = Yii::$app->user->getIdentity();
            $userModel->last_order_date = date('Y-m-d H:i:s');
            $userModel->save();

            //Письмо пользователю и админу
            //$this->sendEmails($positions, $userModel, $order);
            CommonHelper::sendEmails($positions, $userModel, $order);

            unset($_SESSION['cart']);
            $this->cart = [];
            return $this->redirect('/cart/success?id=' . $order->id);
        }else{
            return $this->redirect('/');
        }
    }

    public function actionConfirmFail()
    {
        return $this->render('fail');
    }

    public function sendEmails($prod, $user, $order)
    {

        $tpl = MlCfg::findOne(['mnemo'=>'order_user_opt']);
        $tpl_adm = MlCfg::findOne(['mnemo'=>'order_admin_opt']);
        $adminEmails = Parametrs::findOne(2);
        $user_data = "";
        $product_table = $this->prodTable($prod);
        $message = str_replace([
            '{name}',
            '{name2}',
            '{order_id}',
            '{order_date}',
            '{prod_table}'
            ],
            [
                $user->f_name,
                $user->l_name,
                $order->id,
                $order->cdate,
                $product_table
            ],
            $tpl->ebody_html
        );

        $user_data.="<table style='width: 100%; border: 1px solid #333333; border-collapse: collapse; margin-bottom: 30px;'>
                            <tr>
                                <th style='border: 1px solid #333333;'>Город</th>
                                <th style='border: 1px solid #333333;'>Электронная почта</th>
                                <th style='border: 1px solid #333333;'>Фамилия</th>
                                <th style='border: 1px solid #333333;'>Имя</th>
                                <th style='border: 1px solid #333333;'>Отчество</th>
                                <th style='border: 1px solid #333333;'>Телефон</th>
                            </tr>
                            <tr>
                                <td style='border: 1px solid #333333;'>".$user->city_name."</td>
                                <td style='border: 1px solid #333333;'>".$user->email."</td>
                                <td style='border: 1px solid #333333;'>".$user->l_name."</td>
                                <td style='border: 1px solid #333333;'>".$user->f_name."</td>
                                <td style='border: 1px solid #333333;'>".$user->t_name."</td>
                                <td style='border: 1px solid #333333;'>".$user->phone."</td>
                            </tr>
                        </table>";

        $message_adm = str_replace([
            '{user_data}',
            '{order_id}',
            '{order_date}',
            '{prod_table}'
        ],
            [
                $user_data,
                $order->id,
                $order->cdate,
                $product_table
            ],
            $tpl_adm->ebody_html
        );
        if($adminEmails && !empty($adminEmails->value)) {
            $emails = explode(',', $adminEmails->value);

            Yii::$app->mailer->compose()
                ->setFrom([Yii::$app->params['supportEmail'] => 'Отправлено с сайта moda-optom.ru'])
                ->setTo($emails)
                ->setSubject($tpl_adm->esubj)
                ->setHtmlBody($message_adm)
                ->send();

        }

        Yii::$app->mailer->compose()
            ->setFrom([Yii::$app->params['supportEmail'] => 'Отправлено с сайта moda-optom.ru'])
            ->setTo($user->email)
            ->setSubject($tpl->esubj)
            ->setHtmlBody($message)
            ->send();
    }

    public function prodTable($prod){
        $date = date("d.m.Y");
        $prod_table = "";
        $itogo = 0;
        if($prod){
            $prod_table .= "<table style=\"width: 100%; border: 1px solid #333333; border-collapse: collapse; margin-bottom: 30px;\">
                            <tr>
                                <th style='border: 1px solid #333333;'>Изделие</th>
                                <th style='border: 1px solid #333333;'>Артикул</th>
                                <th style='border: 1px solid #333333;'>Размер</th>
                                <th style='border: 1px solid #333333;'>Цвет</th>
                                <th style='border: 1px solid #333333;'>Кол-во</th>
                                <th style='border: 1px solid #333333;'>Цена, руб.</th>
                                <th style='border: 1px solid #333333;'>Цена со скидкой, руб.</th>
                            </tr>";
            foreach ($prod as $key => $item) {
                $prod_table .= "<tr>";
                $prod_table .= "<td style='border: 1px solid #333333;'>" . $item['product']->title . "</td>";
                $prod_table .= "<td style='border: 1px solid #333333;'>" . $item['product']->article . "</td>";
                if (!empty($item['size'])) {
                    $size = $item['size']->name;
                } else {
                    $size = "нет в наличии";
                }
                $prod_table .= "<td style='border: 1px solid #333333;'>" . $size . "</td>";
                $prod_table .= "<td style='border: 1px solid #333333;'>" . $item['color'] . "</td>";
                $prod_table .= "<td style='border: 1px solid #333333;'>" . $item['cnt'] . "</td>";
                $prod_table .= "<td style='border: 1px solid #333333;'>" . $item['product']->wholesale_price . "</td>";
                $prod_table .= "<td style='border: 1px solid #333333;'>" . $item['product']->wholesale_discount_price . "</td>";
                $prod_table .= "</tr>";
                if(!empty($item['product']->wholesale_discount_price)){
                    $itogo += $item['product']->wholesale_discount_price * $item['cnt'];
                }else{
                    $itogo += $item['product']->wholesale_price * $item['cnt'];
                }
            }


            $prod_table .= "<tr>";
            $prod_table .= "<td colspan='7' style='border: 1px solid #333333;'>Итого: " . $itogo . " руб.</td>";
            $prod_table .= "</tr>";
        }
        return $prod_table;
    }

    /**
     * Проверка доступности товара на складах
     * @param $prod
     * @throws \yii\db\Exception
     */
    private function checkStocks($prod,$id_order){
        foreach ($prod as $it){
            $sql = "SELECT stock_position.id FROM stock_position
                        WHERE stock_position.product_id = '{$it->id}'
                        AND stock_position.quantity > 0
            ";
            $stocks = Yii::$app->db->createCommand($sql)->queryAll();
            if(count($stocks) == 0){
                Yii::$app->db->createCommand("UPDATE products SET is_active = false WHERE id='{$it->id}'")->execute();
                ProductSelection::clearQueryCacheByProductId($it->id);
            }
        }
    }

    private function createAttr($name, $item){
        return $this->attr[$name] = $item;
    }

    /**
     * Удалить элемент из корзины
     */
    public function actionDelItem(){
        $ac = 0;
        if(!Yii::$app->user->isGuest){
            $user = Yii::$app->user->identity;
            $ac = $user->ac;
        }
        $key = Yii::$app->request->get('key');
        $session =Yii::$app->session;
        if (!$session->isActive){
            $session->open();
        }
        $cartModel = new Cart();
        $cartModel->recalc($key);

        $paymentMethod = $this->getPaymentMethod();

        $this->layout = false;
        $view = $this->render('update-cart',['session_cart'=>$_SESSION['cart'],'paymentMethod'=>$paymentMethod,'ac'=>$ac]);
        $count = isset($_SESSION['cart']['allqty'])?$_SESSION['cart']['allqty']:0;
        $paymentDiscount = ($paymentMethod && $paymentMethod->discount > 0) ? round($_SESSION['cart']['sum']*$paymentMethod->discount / 100) : 0;
        $sum = isset($_SESSION['cart']['sum'])?$_SESSION['cart']['sum']-$paymentDiscount:0;

        if((int)$ac != 0){
            if((float)$ac >= $sum){
                $sum = 0;
            }else{
                $sum = $sum - $ac;
            }
        }

        return json_encode(['view'=>$view,'count'=>$count,'sum'=>$sum]);
    }

    public function actionItemPlus(){
        if(Yii::$app->request->isAjax) {
            $ac = 0;
            if(!Yii::$app->user->isGuest){
                $user = Yii::$app->user->identity;
                $ac = $user->ac;
            }
            $key = Yii::$app->request->post('key');
            $cnt = Yii::$app->request->post('valInput');
            $session = Yii::$app->session;
            if (!$session->isActive) {
                $session->open();
            }
            $cartModel = new Cart();
            $cartModel->plus($key,$cnt);
            $paymentMethod = $this->getPaymentMethod();
            $this->layout = false;
            $view = $this->render('update-cart',['session_cart'=>$_SESSION['cart'],'paymentMethod'=>$paymentMethod,'ac'=>$ac]);
            $count = isset($_SESSION['cart']['allqty'])?$_SESSION['cart']['allqty']:0;
            $paymentDiscount = ($paymentMethod && $paymentMethod->discount > 0) ? round($_SESSION['cart']['sum']*$paymentMethod->discount / 100) : 0;
            $sum = isset($_SESSION['cart']['sum'])?$_SESSION['cart']['sum']-$paymentDiscount:0;

            if((int)$ac != 0){
                if((float)$ac >= $sum){
                    $sum = 0;
                }else{
                    $sum = $sum - $ac;
                }
            }

            return json_encode(['view'=>$view,'count'=>$count,'sum'=>$sum]);
        }
    }

    public function actionItemMinus(){
        if(Yii::$app->request->isAjax) {
            $ac = 0;
            if(!Yii::$app->user->isGuest){
                $user = Yii::$app->user->identity;
                $ac = $user->ac;
            }
            $key = Yii::$app->request->post('key');
            $cnt = Yii::$app->request->post('valInput');
            $session = Yii::$app->session;
            if (!$session->isActive) {
                $session->open();
            }
            $cartModel = new Cart();
            $cartModel->minus($key,$cnt);
            $paymentMethod = $this->getPaymentMethod();
            $this->layout = false;
            $view = $this->render('update-cart',['session_cart'=>$_SESSION['cart'],'paymentMethod'=>$paymentMethod,'ac'=>$ac]);
            $count = isset($_SESSION['cart']['allqty'])?$_SESSION['cart']['allqty']:0;
            $paymentDiscount = ($paymentMethod && $paymentMethod->discount > 0) ? round($_SESSION['cart']['sum']*$paymentMethod->discount / 100) : 0;
            $sum = isset($_SESSION['cart']['sum'])?$_SESSION['cart']['sum']-$paymentDiscount:0;

            if((int)$ac != 0){
                if((float)$ac >= $sum){
                    $sum = 0;
                }else{
                    $sum = $sum - $ac;
                }
            }

            return json_encode(['view'=>$view,'count'=>$count,'sum'=>$sum]);
        }
    }

    public function getPaymentMethod(){
        if(!Yii::$app->user->isGuest){
            $user = Yii::$app->user->identity;
            if(!empty($user->pay_method)){
                return PaymentList::findOne(['code'=>$user->pay_method]);
            }
        }
        return false;
    }

    /**
     * Добавление товара в избранное
     */
    public function actionUpdateFavourites()
    {
        $productId = Yii::$app->request->get("productId");

        $result = UserFavourite::updateFavourite($productId);

        return json_encode([
            'status' => $result->status,
            'count' => $result->count
        ]);
    }

    /**
     * Авторизация из корзины
     * @return string
     */
    public function actionLogin(){
        if(Yii::$app->request->isAjax ){
            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                return json_encode(['ok'=>'login']);
            }
            return json_encode(['view'=>$model->errors]);
        }
    }

    /**
     * Подтверждение заказа
     * @return string|\yii\web\Response
     */
    public function actionCheckout()
    {
        if(!isset($_SESSION['cart'])){
            return $this->redirect('/');
        }
        if(Yii::$app->user->isGuest){
            return $this->redirect('/cart');
        }
        //пользователь
        $user = Yii::$app->user->identity;
        $ac = $user->ac;
        //Страны
        $countryModel = new Country();
        $list_country = $countryModel->countryListForForm();
        //Регионы
        $regionModel = new Regions();
        $list_regions = $regionModel->regionsListForForm();
        //адрес
        $address = UserAddress::find()->select('user_address.*')->addSelect('country.name as country_name')->leftJoin('country','country.id = user_address.country_id')->where(['user_address.user_id'=>$user->id])->all();

        if($address){
            $formAddress = $this->addressForForm($address);
        }else{
            $formAddress = [];
        }
        //Возвращает адрес поумолчанию
        $DefAddress = $this->getDefaultAddress($user->id);

        //методы оплаты
        $foru = $this->getFlagCountry();


        if(isset($foru)){
            $payment_methods = PaymentList::find()->where(['visible'=>'1','foru'=>$foru])->orderBy('pos')->all();
        }else{
            $payment_methods = PaymentList::find()->where(['visible'=>'1'])->orderBy('pos')->all();
        }

        $payment_methods_list = ArrayHelper::map($payment_methods,'id','title');
        //профиль
        $profile = UserProfile::findOne(['user_id'=>$user->id]);
        //Способы доставки
        $deliv_list = ArrayHelper::map(DelivList::find()->where(['visible'=>'1'])->orderBy('pos')->all(),'code','title');
        
        $model = new CheckoutForm();

        if($model->load(Yii::$app->request->post())){
            if($model->delivery=='post'){
                $model->scenario = "middlename";
            }
            if($model->validate()) {
                $id_order = $model->saveOrder($ac);
                if ($id_order) {
                    return $this->redirect('/cart/success?id=' . $id_order);
                }
            }
        }


        return $this->render('checkout',[
            'payment_methods'=>$payment_methods,
            'payment_methods_list'=>$payment_methods_list,
            'model'=>$model,
            'formAddress'=>$formAddress,
            'profile'=>$profile,
            'deliv_list'=>$deliv_list,
            'user'=>$user,
            'DefAddress'=>$DefAddress,
            'list_country'=>$list_country,
            'list_regions'=>$list_regions,
            'geo_data' => $this->geo_data,
            'ac'=>$ac
        ]);
    }
    
    public function actionSuccess(){
        if(isset($_GET['id'])){
            $model = OptOrders::findOne(['id'=>$id]);
            if($model){
                $parametrs = Parametrs::findOne(6);
                return $this->render('success',['model'=>$model,'parametrs'=>$parametrs]);
            }
        }
        if(isset($_GET['idt'])){
            $id = (int)$_GET['idt'];
            $model = OptOrders::findOne(['tmp_order_id'=>$id]);
            if($model){
                $parametrs = Parametrs::findOne(6);
                return $this->render('success',['model'=>$model,'parametrs'=>$parametrs]);
            }
        }
        return $this->redirect('/');
    }

    /**
     * Нормализация адреса для формы
     * @param $address
     * @return array
     */
    public function addressForForm($address){
        $arr = [];
        foreach ($address as $it){
            //$arr[$it->id] = $it->country_name." г ".$it->city." ".$it->address;
            $arr[$it->id] = $it->address;
        }
        return $arr;
    }

    /**
     * Возвращает адрес по умалчанию
     * @param $user_id
     * @return static
     */
    public function getDefaultAddress($user_id){
        return UserAddress::findOne(['user_id'=>$user_id,'default'=>true]);
    }

    /**
     * Смена метода оплаты
     * @return string
     */
    public function actionChangePostMethod(){
        if(Yii::$app->request->isAjax){
            if(!Yii::$app->user->isGuest){
                $summa_spis = 0;
                $user = Yii::$app->user->identity;
                $ac = $user->ac;

                $id = (int)Yii::$app->request->post('code');
                $payment_method = PaymentList::findOne($id);

                $userModel = User::findOne(Yii::$app->user->id);
                $userModel->pay_method = $payment_method->code;
                $userModel->save(false);

                $discount = $payment_method->discount;

                if($discount > 0){
                    $discountSum = round($_SESSION['cart']['sum']*$discount / 100);
                    $itog = $_SESSION['cart']['sum']-$discountSum;
                    $alleconom = $_SESSION['cart']['allsum'] - $_SESSION['cart']['sum'] + $discountSum;

                    if((int)$ac != 0){
                        if((float)$ac >= $itog){
                            $summa_spis = $itog;
                            $itog = 0;
                        }else{
                            $summa_spis = $ac;
                            $itog = $itog - $ac;
                        }
                    }

                    return json_encode(["ok"=>"ok","data"=>["summa_spis"=>$summa_spis,"discount"=>$discount,"discountSum"=>$discountSum,"alleconom"=>$alleconom,"itog"=>$itog]]);
                }
                $itog = $_SESSION['cart']['sum'];
                if((int)$ac != 0){
                    if((float)$ac >= $itog){
                        $summa_spis = $itog;
                        $itog = 0;
                    }else{
                        $summa_spis = $ac;
                        $itog = $itog - $ac;
                    }
                }
                return json_encode(['ok'=>'ok',"data"=>["summa_spis"=>$summa_spis,"discount"=>0,"discountSum"=>0,"alleconom"=>$_SESSION['cart']['allsum'] - $_SESSION['cart']['sum'],"itog"=>$itog]]);
            }
        }
    }

    public function actionChangeDelivMethod(){
        if(Yii::$app->request->isAjax){
            $product = new Product();
            $code = Yii::$app->request->post('code');
            $price = Yii::$app->request->post('price');
            $deliv = DelivList::findOne(['code'=>$code]);
            $st_deliv = "0 ".$product->getSymbol($this->geo_data->country);
            if($deliv){
                if((int)$price >= 3000 && ($code=='post' || $code == 'cpcr')){
                    return json_encode(['ok'=>'ok',"data"=>['delivery'=>$st_deliv]]);
                }else{
                    return json_encode(['ok'=>'ok',"data"=>['delivery'=>$deliv->price]]);
                }
            }else{
                return json_encode(['no'=>'ok']);
            }
        }
    }

    public function actionChangeAddress(){
        if(Yii::$app->request->isAjax) {
            if (!Yii::$app->user->isGuest) {
                $id = (int)Yii::$app->request->post('id');
                $address = UserAddress::findOne($id);
                if($address){
                    $allad = UserAddress::findAll(['user_id'=>Yii::$app->user->id]);
                    foreach ($allad as $it){
                        $it->default = false;
                        $it->save(false);
                    }
                    $address->default = true;
                    $address->save(false);

                    $foru = $this->getFlagCountry($address);
                    $payment_methods = PaymentList::find()->where(['visible'=>'1','foru'=>$foru])->orderBy('pos')->all();
                    if($payment_methods){
                        $payment_methods_list = ArrayHelper::map($payment_methods,'id','title');
                        $DefAddress = $this->getDefaultAddress(Yii::$app->user->id);
                        $view = $this->renderAjax('_payment_list',['payment_methods'=>$payment_methods,'payment_methods_list'=>$payment_methods_list,'DefAddress'=>$DefAddress]);
                        return json_encode([
                            "ok"=>"ok",
                            "view"=>$view,
                            'defaddress_phone'=>$DefAddress->mphone,
                            'defaddress_surname'=>$DefAddress->surname,
                            'defaddress_name'=>$DefAddress->name,
                            'defaddress_middle_name'=>$DefAddress->middle_name,
                        ]);
                    }
                }
            }
        }
    }

    private function getFlagCountry(){
        $foru = 'zar';
        if($this->geo_data->country=='RU'){
            $foru = 'rf';
        }
        return $foru;
    }


    public function actionChangeCountry(){
        if(Yii::$app->request->isAjax) {
            $id = (int)Yii::$app->request->post('id');
            if($id==157){
                $regions = Regions::find()->where(['country_id'=>$id])->andWhere(['visible'=>'1'])->all();
                if($regions){
                    $region_geo = Regions::find()->where(['like','name_geo',$this->geo_data->region])->one();
                    $view = $this->renderAjax('_region_list',['regions'=>$regions,'region_geo'=>$region_geo]);
                    return json_encode(["ok"=>"ok","view"=>$view]);
                }
            }
        }
    }

    public function actionGetgeo(){
        if(Yii::$app->request->isAjax){
            return json_encode(['ok'=>$this->geo_data->country]);
        }
    }

    public function actionOrderstatus()
    {
        if(Yii::$app->request->isAjax){
            $orderRPInfo = null;
            $orderOperationHistory = null;
            $order_id = Yii::$app->request->get('order_id');
            $order = Orders::findOne($order_id);
            if($order){
                $status = OrdstatList::findOne($order->status);
                $orderManager = UserProfile::findOne(['user_id'=>$order->manager]);
                $orderSale = Sales::find()->where(['order_id'=>$order->id])->andWhere(['!=','id_post',''])->orderBy('id','DESC')->one();
                if($orderSale){
                    $orderRPInfo = $this->orderRPInfo($orderSale);
                    $orderOperationHistory = PostOperationHistory::findAll(['sale_id'=>$orderSale->id]);
                }
                $view = $this->renderAjax('_answer_order_status',[
                    'order'=>$order,
                    'status'=>$status,
                    'orderManager'=>$orderManager,
                    'orderSale'=>$orderSale,
                    'orderRPInfo'=>$orderRPInfo,
                    'orderOperationHistory'=>$orderOperationHistory,
                ]);
                return json_encode(["view"=>$view]);
            }else{
                $view = $this->renderAjax('_answer_order_status',['order'=>$order]);
                return json_encode(["view"=>$view]);
            }
        }
    }

    public function orderRPInfo($orderSale)
    {
        $result = null;
        if ($orderSale && $orderSale->raw_post_tracking_result) {
            $resultRawArray = unserialize($orderSale->raw_post_tracking_result);
            foreach ($resultRawArray as $resultRaw) {
                if ($resultRaw->FinanceParameters && $resultRaw->FinanceParameters->Payment > 0) {
                    $result['finance'] = $resultRaw->FinanceParameters;
                }
                if ($resultRaw->ItemParameters) {
                    $result['item'] = $resultRaw->ItemParameters;
                }
                if (!empty($result['finance']) && !empty($result['item'])) {
                    break;
                }
            }
        }
        return $result;
    }
}
