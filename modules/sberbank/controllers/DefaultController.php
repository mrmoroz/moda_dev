<?php

namespace app\modules\sberbank\controllers;

use app\modules\sberbank\models\Invoice;
use app\modules\sberbank\SberbankModule;
use Yii;
use yii\base\ErrorException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class DefaultController extends Controller
{
    /* @var \app\modules\sberbank\SberbankModule */
    public $module;

    /**
     * Сюда будет перенаправлен результат оплаты
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionComplete()
    {
        file_put_contents(__DIR__.'/'.date('Y-m-d').'-ekam-complete.txt', date('Y-m-d H:i:s')." ".Yii::$app->request->get('orderId').PHP_EOL, FILE_APPEND);
        /* @var $model Invoice */
        if (is_null(Yii::$app->request->get('orderId'))) {
            throw new NotFoundHttpException();
        }
        $model = Invoice::find()
            ->where([
                'AND',
                ['=', 'status', 'I'],
                ['=', 'orderId', Yii::$app->request->get('orderId')],
            ])
            ->one();
        if (is_null($model)) {
            throw new NotFoundHttpException();
        }
        $result = $this->module->sberbank->complete(Yii::$app->request->get('orderId'));
        file_put_contents(__DIR__.'/'.date('Y-m-d').'-ekam-result.txt', date('Y-m-d H:i:s')." ".Yii::$app->request->get('orderId')." ".json_encode($result).PHP_EOL, FILE_APPEND);
        //Проверяем статус оплаты если всё хорошо обновим инвойс и редерекним
        if (isset($result['OrderStatus']) && ($result['OrderStatus'] == $this->module->sberbank->classRegister->successStatus())) {
            $model->attributes = $this->module->sberbank->classRegister->getDataForUpdate();
            $model->update();
            if ($this->module->successCallback) {
                $callback = $this->module->successCallback;
                $callback($model);
            }
            $this->redirect($this->module->successUrl.'?idt='.$model->order_id);
        } else {
            if ($this->module->failCallback) {
                $callback = $this->module->failCallback;
                $callback($model);
            }
            $this->redirect($this->module->failUrl);
        }
    }


    /**
     * Создание оплаты редеректим в шлюз сберабнка
     * @param $id
     * @return \yii\web\Response
     * @throws ErrorException
     * @throws \Exception
     * @throws \yii\db\StaleObjectException
     * @throws \Throwable
     */
    public function actionCreate($id)
    {
        $model = Invoice::findOne($id);
        $result = $this->module->sberbank->create($model);
        if (array_key_exists('errorCode', $result)) {
            throw new ErrorException($result['errorMessage']);
        }
        $orderId = $result['orderId'];
        $formUrl = $result['formUrl'];
        $model->orderId = $orderId;
        $model->update();
        return $this->redirect($formUrl);
    }
}