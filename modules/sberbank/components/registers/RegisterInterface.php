<?php

namespace app\modules\sberbank\components\registers;

interface RegisterInterface
{
    public function getActionRegister();

    public function successStatus();

    public function getDataForUpdate();
}
