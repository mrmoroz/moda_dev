<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 21.10.2020
 * Time: 14:19
 */

namespace app\modules\sberbank\components;


use app\helpers\CommonHelper;
use app\models\OptTmpOrderItems;
use app\models\OptTmpOrders;
use app\models\OptUsers;
use app\models\Product;
use app\models\StockPosition;
use app\modules\cart\models\OptOrderItems;
use app\modules\cart\models\OptOrders;
use app\models\Size;
use Yii;

class handlerSuccess
{
    private $invoice;

    public function __construct($invoice)
    {
        $this->invoice = $invoice;
    }

    public function handle()
    {
        $paymentCheck = OptTmpOrders::find()->where(['id'=>$this->invoice->order_id])->one();

        if($paymentCheck){

            $user = OptUsers::findOne(['id' => $paymentCheck->user_id]);
            $number = '';
            if(!empty($user->phone)){
                $number = $this->getOrderMobileNumber($user->phone);
            }
            $email = '';
            if(!empty($user->email)){
                $email = $user->email;
            }


            $order = OptOrders::create(
                $paymentCheck->comment,
                $paymentCheck->city,
                $paymentCheck->fio,
                $paymentCheck->region,
                $paymentCheck->user_id,
                $paymentCheck->first,
                $paymentCheck->payment
            );
            $order->total_cost = $paymentCheck->total_cost;
            $order->total_qty = $paymentCheck->total_qty;
            $order->tmp_order_id = $paymentCheck->id;
            $order->save();

            if($order){
                $positions = [];
                $items = OptTmpOrderItems::find()->where(['order_id'=>$paymentCheck->id])->all();
                if($items){

                    foreach ($items as $key => $item){

                        $positions[$key]['product'] = Product::findOne($item->product_id);
                        $positions[$key]['color'] = $item->color;
                        $positions[$key]['color_id'] = $item->color_id;
                        $positions[$key]['size'] = Size::find()->where(['id'=>$item->size_id])->one();;
                        $positions[$key]['cnt'] = $item->qty;

                        $orderItem = OptOrderItems::create(
                            $item->article,
                            $item->color,
                            $item->name,
                            $item->new_price,
                            $item->price,
                            $item->qty,
                            $order->id,
                            $item->product_id,
                            $item->size_y,
                            $item->size_n
                        );
                        $orderItem->save();

                        if(!empty($item->size_y)){
                            $stock_tmp = StockPosition::getStockdata($item->product_id, $item->color_id, $item->size_id, $item->stock_id);
                            if($stock_tmp){
                                $quantity = (int)$stock_tmp['quantity'] - $item->qty;
                                $status = true;

                                if ($quantity < 0 || $quantity == 0) {
                                    $quantity = 0;
                                    $status = false;
                                }

                                $newSizeLog = \app\models\ProductSizeChangelog::create($item->product_id, $item->color_id, $item->size_id, $item->stock_id, $status, $order->id, $item->qty);
                                $newSizeLog->save(false);

                                StockPosition::changeStockData($quantity, $stock_tmp['id']);
                            }
                        }

                        \app\models\Product::clearProductCache($item->product_id);
                        StockPosition::checkStocks($item->product_id, $order->id);
                    }

                    if($items && !empty($email)){
                        $ekam = new \app\components\Ekam();
                        $answer = $ekam->sendReq($order, $email, $number, $items);
                        file_put_contents(__DIR__.'/'.date('Y-m-d').'-ekam.txt', date('Y-m-d H:i:s')." ".$order->id." ".$answer."\n", FILE_APPEND);
                    }

                }


                $userModel = Yii::$app->user->getIdentity();
                $userModel->last_order_date = date('Y-m-d H:i:s');
                $userModel->save();

                CommonHelper::sendEmails($positions, $userModel, $order);
                unset($_SESSION['cart']);

            }
        }
    }

    private function getOrderMobileNumber($number)
    {
        $number = str_replace(array('(', ')', ' ', '+', '-'), '', $number);
        if (strpos($number, '8') === 0) {
            $number = '7' . substr($number, 1);
        }
        return $number;
    }
}