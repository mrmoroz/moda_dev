<?php

use yii\db\Migration;

/**
 * Handles adding remote_id to table `invoice`.
 */
class m180331_054941_add_remote_id_column_to_invoice_table2 extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('opt_invoice', 'remote_id', $this->integer()->null());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('opt_invoice', 'remote_id');
    }
}
