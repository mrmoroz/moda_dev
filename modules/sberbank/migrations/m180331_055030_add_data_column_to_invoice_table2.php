<?php

use yii\db\Migration;

/**
 * Handles adding data to table `invoice`.
 */
class m180331_055030_add_data_column_to_invoice_table2 extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('opt_invoice', 'data', 'JSON NULL DEFAULT NULL');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('opt_invoice', 'data');
    }
}
