<?php

namespace app\modules\account;

use Yii;

/**
 * account module definition class
 */
class AccountModule extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\account\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if(Yii::$app->user->isGuest){
            return $this->redirect('/');
        }
    }

    private function redirect(){
        header('Location: /', true, 301);
        exit;
    }
}
