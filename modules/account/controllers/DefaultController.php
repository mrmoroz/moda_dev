<?php

namespace app\modules\account\controllers;
use app\components\BaseController;
use app\models\CabEmail;
use app\modules\account\models\UserPhotos;
use app\modules\admin\models\Sales;
use app\modules\cart\models\Orders;
use Yii;
use app\models\UserProfile;
use app\models\UserAddress;
use yii\web\UploadedFile;
use app\components\resize\AcImage;
use app\modules\account\models\Changepass;
use yii\data\Pagination;

/**
 * Default controller for the `account` module
 */
class DefaultController extends BaseController
{
    /**
     * Мои данные
     * @return string
     */
    public function actionIndex()
    {
        $user = Yii::$app->user->identity;
        //профиль
        $userProfile = UserProfile::findOne(['user_id'=>$user->id]);
        //фотогалерея пользователя
        $userPhotos = UserPhotos::findAll(['user_id'=>$user->id]);

        $modelImg = new UserPhotos();

        $model = new Changepass();
        //vd($_POST);
        if(isset($_POST['ch-profile'])) {
            if ($userProfile->load(Yii::$app->request->post())) {
                $userProfile->file = UploadedFile::getInstance($userProfile, 'file');
                if ($userProfile->validate()) {
                    if ($userProfile->file) {
                        $img = AcImage::createImage($userProfile->file->tempName);
                        $img->resizeByWidth(100);
                        $img->cropCenter(70, 70);
                        $nameFile = "user_" . $user->id . '.' . $userProfile->file->extension;
                        if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/img/avatar/' . $nameFile)) {
                            unlink($_SERVER['DOCUMENT_ROOT'] . '/img/avatar/' . $nameFile);
                        }
                        $img->save($_SERVER['DOCUMENT_ROOT'] . '/img/avatar/' . $nameFile);
                        $userProfile->avatar = $nameFile;
                    }

                    if ($userProfile->save(false)) {
                        if ($user->load(Yii::$app->request->post())) {
                            $user->save(false);
                            Yii::$app->session->setFlash('acc', 'Данные сохранены');
                            return $this->redirect('/account');
                        }
                    }
                }
            }
        }

        if(isset($_POST['ch-pass'])) {
            if($model->load(Yii::$app->request->post()) && $model->validate()){
                $user = Yii::$app->user->identity;
                $user->setPassword($model->password);
                if($user->save(false)){
                    Yii::$app->session->setFlash('changepass','Пароль успешно изменен');
                    return $this->redirect('/account');
                }
            }
        }

        return $this->render('index',[
            'user'=>$user,
            'userprofile'=>$userProfile,
            'geo'=>$this->geo_data,
            'userPhotos' => $userPhotos,
            'modelImg' => $modelImg,
            'model'=>$model
        ]);
    }

    /**
     * Удаление изображений
     * @return string
     */
    public function actionDelimg()
    {
        if(Yii::$app->request->isAjax){
            $id = (int)Yii::$app->request->post('id');
            $model = UserPhotos::findOne($id);
            if($model){
                $mainPath = Yii::$app->params['uploadPath'];// путь к папке изображений
                if(file_exists($mainPath.$model->url)){
                    unlink($mainPath.$model->url);
                }
                if(file_exists($mainPath.'/thumbs/width_200/'.$model->url)){
                    unlink($mainPath.'/thumbs/width_200/'.$model->url);
                }
                if($model->delete()){
                    return json_encode(array("ok"=>"ok"));
                }
            }
        }
    }

    /**
     * Загрузка галереи
     * @return \yii\web\Response
     */
    public function actionDownload()
    {
        $modelImg = new UserPhotos();
        if ($modelImg->load(Yii::$app->request->post())) {
            $modelImg->file = UploadedFile::getInstance($modelImg, 'file');
            if ($modelImg->file && $modelImg->validate()) {
                $array = $modelImg::thumbnail();// массив папок и размеров (ширина) миниатюр
                $nameFile = Yii::$app->security->generateRandomString();// генерируем имя изображения
                $mainPath = Yii::$app->params['uploadPath'];// путь к папке изображений
                $modelImg->file->saveAs($mainPath.$nameFile.'.'.$modelImg->file->extension);// сохраняем полноразмерное изображение

                if(file_exists($mainPath.$nameFile.'.'.$modelImg->file->extension)){
                    chmod($mainPath.$nameFile.'.'.$modelImg->file->extension,0777);
                }
                if(!file_exists($mainPath.'thumbs')){
                    mkdir($mainPath.'thumbs', 0777);
                }
                // ресайз изображения
                foreach ($array as $folder => $width) {
                    $img = AcImage::createImage($mainPath.$nameFile.'.'.$modelImg->file->extension);
                    $img->resizeByWidth($width);
                    if(!file_exists($mainPath.'thumbs/'.$folder)){
                        mkdir($mainPath.'thumbs/'.$folder, 0777);
                    }
                    $img->save($mainPath.'thumbs/'.$folder.'/'.$nameFile.'.'.$modelImg->file->extension);
                    if(file_exists($mainPath.'thumbs/'.$folder.'/'.$nameFile.'.'.$modelImg->file->extension)){
                        chmod($mainPath.'thumbs/'.$folder.'/'.$nameFile.'.'.$modelImg->file->extension,0777);
                    }
                }
                $modelImg->user_id = Yii::$app->user->id;
                $modelImg->url = $nameFile.'.'.$modelImg->file->extension;
                $modelImg->save(false);
                return $this->redirect('/account');
            }
        }
    }

    /**
     * Смена пароля
     * @return string
     */
    public function actionChPass()
    {
        $model = new Changepass();

        if($model->load(Yii::$app->request->post()) && $model->validate()){
            $user = Yii::$app->user->identity;
            $user->setPassword($model->password);
            if($user->save(false)){
                Yii::$app->session->setFlash('changepass','Пароль успешно изменен');
                return $this->redirect('/account/ch-pass');
            }
        }
        return $this->render('chpass',['model'=>$model]);
    }

    /**
     * Заказы
     * @return string
     */
    public function actionCabHistory()
    {
        $user = Yii::$app->user->identity;
        $count = Yii::$app->db->createCommand("SELECT COUNT(*) FROM orders WHERE user_id='{$user->id}'")->queryScalar(); // колличество
        $pagination = new Pagination(['totalCount' => $count, 'pageSize' => 20, 'forcePageParam' => false, 'pageSizeParam' => false]);
        $sql = "SELECT orders.*, ordstat_list.name as status_name FROM orders
                LEFT JOIN ordstat_list ON ordstat_list.id = orders.status
                WHERE orders.user_id='{$user->id}'  
                OFFSET :offset LIMIT :limit";
        $orders = Yii::$app->db->createCommand($sql)->bindValues([':offset' => $pagination->offset, ':limit' => $pagination->limit])->queryAll();
        $stat_list = Yii::$app->db->createCommand("SELECT * FROM ordstat_list WHERE in_site='1' ORDER BY pos")->queryAll();
        return $this->render('cabhistory',[
            'orders'=>$orders,
            'pagination' => $pagination,
            'stat_list'=>$stat_list
        ]);
    }

    public function actionViewHistory()
    {
        $id = Yii::$app->request->get('id');
        $order = Orders::findOne($id);
        $order_items = Yii::$app->db->createCommand("SELECT * FROM order_items WHERE order_id = :order_id")->bindValues([':order_id'=>$id])->queryAll();
        $sales = Sales::findAll(['order_id'=>$order->id,'visible'=>'1']);
        return $this->render('viewhistory',[
            'order_items'=>$order_items,
            'order'=>$order,
            'sales'=>$sales
        ]);
    }

    /**
     * Служебная почта
     */
    public function actionCabMail()
    {
        $user = Yii::$app->user->identity;
        $userprofile = UserProfile::findOne(['user_id'=>$user->id]);
        $count = Yii::$app->db->createCommand("SELECT COUNT(*) FROM cab_email WHERE user_id='{$user->id}'")->queryScalar(); // колличество
        $pagination = new Pagination(['totalCount' => $count, 'pageSize' => 20, 'forcePageParam' => false, 'pageSizeParam' => false]);
        $sql = "SELECT cab_email.* FROM cab_email 
                WHERE cab_email.user_id='{$user->id}' 
                AND cab_email.visible = '1'
                ORDER BY cab_email.cdate DESC
                OFFSET :offset LIMIT :limit";
        $mess = Yii::$app->db->createCommand($sql)->bindValues([':offset' => $pagination->offset, ':limit' => $pagination->limit])->queryAll();
        $model = new CabEmail();
        $model->scenario = 'acc_emial';

        if($model->load(Yii::$app->request->post()) && $model->validate()){
            $model->user_id = $user->id;
            $model->new_msg = 1;
            $model->cdate = date("Y-m-d H:i:s");
            if($model->save()){
                Yii::$app->session->setFlash('acc_email','Ваше сообщение успешно отправлено!');
                return $this->redirect('/account/cab-mail');
            }
        }

        return $this->render('cabmail',[
            'mess'=>$mess,
            'pagination' => $pagination,
            'userprofile' => $userprofile,
            'model'=>$model
        ]);
    }
}
