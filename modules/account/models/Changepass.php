<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 19.05.17
 * Time: 13:17
 */

namespace app\modules\account\models;


use yii\base\Model;
use app\models\User;
use Yii;

class Changepass extends Model
{
    public $current_password;
    public $password;
    public $confirm_password;

    public function rules()
    {
        return [
            [['current_password', 'password', 'confirm_password'], 'required'],
            ['current_password', 'checkpassword'],
            ['confirm_password', 'compare', 'compareAttribute' => 'password', 'message' => 'Неверное подтверждение пароля'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'current_password' => 'Текущий пароль',
            'password' => 'Новый пароль',
            'confirm_password' => 'Повторите пароль',
        ];
    }

    public function checkpassword($attribute, $params)
    {
        $user = Yii::$app->user->identity;
        if (!Yii::$app->security->validatePassword($this->current_password, $user->password)) {
            $this->addError($attribute, 'Неверный текущий пароль');
        }
    }
}