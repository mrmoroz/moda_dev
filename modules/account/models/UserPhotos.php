<?php

namespace app\modules\account\models;

use Yii;

/**
 * This is the model class for table "user_photos".
 *
 * @property integer $id
 * @property string $title
 * @property integer $user_id
 * @property string $url
 * @property integer $sys_language
 * @property integer $pos
 * @property integer $visible
 */
class UserPhotos extends \yii\db\ActiveRecord
{
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_photos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'sys_language', 'pos', 'visible'], 'integer'],
            [['title', 'url'], 'string', 'max' => 255],
            ['file','required'],
            [['file'], 'file', 'extensions' => 'jpg, gif, png, jpeg', 'maxSize' => 2097152]
        ];
    }

    public static function thumbnail() {
        return array(
            'width_200' => 200,
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок к изображению',
            'user_id' => 'Пользователь',
            'url' => 'Путь',
            'sys_language' => 'Sys Language',
            'pos' => 'Позиция',
            'visible' => 'Видимость',
            'file' => 'Изображение'
        ];
    }
}
