<?php
use app\modules\account\widgets\AccountmenuWidget;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
$this->title = 'Личный кабинет';
?>
<div class="account-content">
    <div class="first-column"><?=AccountmenuWidget::widget();?></div>
    <div class="second-column">
        <div class="account-page">
            <h1>Изменение пароля</h1>
            <p>
                В данном разделе Вы можете в любое время сами сменить пароль, не делая специальных запросов.<br>
                * - Поля, обязательные для заполнения.
            </p>
            <?php
                $form = ActiveForm::begin([
                    'id' => 'acc-user-data',
                    'action'=>'/account/ch-pass',
                    'enableAjaxValidation' => false,
                ]);
            ?>
            <div class="bg-round">
                <?php if(Yii::$app->session->hasFlash('changepass')):?>
                    <div class="alert alert-success"><?=Yii::$app->session->getFlash('changepass')?></div>
                <?php endif;?>
                <div class="tabl-form">
                    <div class="tr">
                        <div class="th">Старый пароль <span class="star">*</span></div>
                        <div class="td">
                            <?=$form->field($model,'current_password')->textInput(['class'=>'w-long'])->label(false)?>
                        </div>
                    </div>
                    <div class="tr">
                        <div class="th">Новый пароль <span class="star">*</span></div>
                        <div class="td">
                            <?=$form->field($model,'password')->textInput(['class'=>'w-long'])->label(false)?>
                        </div>
                    </div>
                    <div class="tr">
                        <div class="th">Подтверждение пароля <span class="star">*</span></div>
                        <div class="td">
                            <?=$form->field($model,'confirm_password')->textInput(['class'=>'w-long'])->label(false)?>
                        </div>
                    </div>
                </div>
            </div>
            <div style="text-align: center">
                <button type="submit" class="add-to-cart-button">Сменить</button>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
