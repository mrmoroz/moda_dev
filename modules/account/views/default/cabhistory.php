<?php
use app\modules\account\widgets\AccountmenuWidget;
use yii\widgets\LinkPager;
$this->title = 'Личный кабинет';
?>
<div class="account-content">
    <div class="first-column"><?=AccountmenuWidget::widget();?></div>
    <div class="second-column">
        <div class="account-page">
            <h1>Мои заказы</h1>
            <p>
                На данной странице Вы можете посмотреть все сделанные Вами заказы, а также на каком этапе обработки они находятся, согласно статуса заказа.
            </p>
            <?php if(count($orders)>0):?>
                <?php //vd($orders,false);?>
                <div class="tabl highlight">
                    <div class="tr">
                        <div class="th">Номер заказа</div>
                        <div class="th">Сумма</div>
                        <div class="th">Почтовый идентификатор</div>
                        <div class="th">Где посылка?</div>
                        <div class="th">Статус заказа</div>
                    </div>
                    <?php foreach ($orders as $it):?>
                        <div class="tr">
                            <div class="td no-wrap"><a href="/account/view-history/<?=$it['id']?>"><strong><?=$it['id']?> от <?=date("d.m.Y",strtotime($it['cdate']))?></strong></a></div>
                            <div class="td digits no-wrap"><?=$it['to_be_paid']?> руб.</div>
                            <div class="td">
                                <?php if(!empty($it['pi_number'])):?>
                                    <?=$it['pi_number']?>
                                <?php else:?>
                                    -
                                <?php endif;?>
                            </div>
                            <div class="td">
                                <a href="http://почтароссии.рф/rp/servise/ru/home/postuslug/trackingpo" target="_blank">на сайте Почты России</a>
                            </div>
                            <div class="td">
                                <?=$it['status_name']?>
                            </div>
                        </div>
                    <?php endforeach;?>
                </div>
                <?php if (!empty($pagination)): ?>
                    <div class="paginationWrap">
                        <?= LinkPager::widget([
                            'options' => ['class' => 'pagination'],
                            'prevPageLabel' => false,
                            'nextPageLabel' => false,
                            'pagination' => $pagination
                        ]); ?>
                    </div>
                <?php  endif; ?>
            <?php endif;?>


            <?php if(count($stat_list)>0):?>
                <div class="border-round">
                    <h2>Стадии (статусы) заказов:</h2>

                    <dl>
                        <?php foreach ($stat_list as $st):?>
                            <dt><?=$st['name']?></dt>
                            <dd><?=$st['descript']?></dd>
                        <?php endforeach;?>
                    </dl>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>
