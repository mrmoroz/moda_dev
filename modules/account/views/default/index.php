<?php
use app\modules\account\widgets\AccountmenuWidget;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
$this->title = 'Личный кабинет';

?>
<div class="account-content">
    <div class="first-column"><?=AccountmenuWidget::widget();?></div>
    <div class="second-column">
        <div class="account-page">
            <h1>Персональные данные</h1>
            <p>
                Вы можете изменить Ваши персональные данные. Согласно данным, указанным в этом разделе, формируются посылки в Ваш адрес. Просьба - очень внимательно заполнять личные данные, чтобы отправленные заказы дошли по адресу!
            </p>
            <h2>ТАКЖЕ В ЭТОМ РАЗДЕЛЕ МОЖНО ПОСМОТРЕТЬ:</h2>
            <ul>
                <li>присвоенную Вам скидку в разделе&nbsp;<a href="/account/cab-history">"История заказов"</a>.</li>
                <li>состояние лицевого счета в нижней части данного текста.</li>
            </ul>
            <p>Обращаем Ваше внимание, что при регистрации - Вам автоматически становятся доступны подписки на новинки, распродажи, скидки, акции, подарки! Все наши предложениями мы высылаем письмами на Ваш эл.адрес. Следите за нашими предложениями, это выгодно и удобно!</p>
            <h3>Состояние лицевого счета: <span class="red"><?=$user->ac?> руб.</span></h3>
            <h3>Размер вашей накопительной скидки: <span class="red"><?=(!empty($user->discount_user)?$user->discount_user:0)?> %</span></h3>
            <?php
            $form = ActiveForm::begin([
                'id' => 'acc-user-data',
                'action'=>'/account',
                'enableAjaxValidation' => false,
                'options' => ['enctype' => 'multipart/form-data']
            ]);
            ?>
            <div class="bg-round">
                <?php if(Yii::$app->session->hasFlash('acc')):?>
                    <div class="alert alert-success"><?=Yii::$app->session->getFlash('acc')?></div>
                <?php endif;?>
                <div class="tabl-form">
                    <div class="tr">
                        <div class="th">Email(логин) </div>
                        <div class="td">
                            <?=$user->login?><br><br>
                        </div>
                    </div>
                    <div class="tr">
                        <div class="th">Фамилия </div>
                        <div class="td">
                            <?=$form->field($userprofile,'last_name')->textInput(['placeholder'=>'Фамилия','class'=>'w-long'])->label(false)?>
                        </div>
                    </div>
                    <div class="tr">
                        <div class="th">Имя </div>
                        <div class="td">
                            <?=$form->field($userprofile,'first_name')->textInput(['placeholder'=>'Имя','class'=>'w-long'])->label(false)?>
                        </div>
                    </div>
                    <div class="tr">
                        <div class="th">Отчество </div>
                        <div class="td">
                            <?=$form->field($userprofile,'second_name')->textInput(['placeholder'=>'Отчество','class'=>'w-long'])->label(false)?>
                        </div>
                    </div>
                    <div class="tr img-tr">
                        <div class="th">Аватар</div>
                        <div class="td">
                            <img src="/img/avatar/<?=$userprofile->avatar?>" width="70" height="70">
                            <p>Заменить</p>
                            <?=$form->field($userprofile,'file')->fileInput(['class'=>'Inp Short'])->label(false);?>
                        </div>
                    </div>
                    <div class="tr">
                        <div class="th">Дата рождения </div>
                        <div class="td">
                            <?=$form->field($user,'bdate')->textInput(['placeholder'=>'Дата рождения','class'=>'w-long'])->label(false)?>
                        </div>
                    </div>
                    <div class="tr">
                        <div class="th">Параметры</div>
                        <div class="td">
                            <input class="w-short" data-toggle="tooltip" data-placement="top" title="Объем груди (см)" name="UserProfile[bust]" placeholder="объем груди" value="<?=$userprofile->bust?>" type="text">
                            <input class="w-short" data-toggle="tooltip" data-placement="top" title="Объем талии (см)" name="UserProfile[waist]" placeholder="объем талии" value="<?=$userprofile->waist?>" type="text">
                            <input class="w-short" data-toggle="tooltip" data-placement="top" title="Объем бедер (см)" name="UserProfile[hip]" placeholder="объем бедер" value="<?=$userprofile->hip?>" type="text">
                        </div>
                    </div>
                    <div class="tr">
                        <div class="th">Рост</div>
                        <div class="td">
                            <?=$form->field($userprofile,'grow')->textInput(['placeholder'=>'Рост','class'=>'w-long'])->label(false)?>
                        </div>
                    </div>
                    <div class="tr">
                        <div class="th">Мобильный телефон </div>
                        <div class="td">
                            <?=$form->field($userprofile,'mobile_phone')->textInput(['placeholder'=>'Мобильный телефон','class'=>'w-long'])->label(false)?>
                        </div>
                    </div>
                    <div class="tr">
                        <div class="th">Телефон</div>
                        <div class="td">
                            <input class="w-short" name="UserProfile[phone_code]" value="<?=$userprofile->phone_code?>" placeholder="код города" type="text">
                            <input name="UserProfile[phone]" value="<?=$userprofile->phone?>" placeholder="номер" style="width:217px;" type="text">
                        </div>
                    </div>
                </div>
            </div>
            <div style="text-align: center;">
                <button class="add-to-cart-button" name="ch-profile" type="submit">Сохранить изменения</button>
            </div>
            <?php ActiveForm::end(); ?>

            <div class="bg-round">
                <h1>Изменение пароля</h1>
                <p>
                    В данном разделе Вы можете в любое время сами сменить пароль, не делая специальных запросов.<br>
                    * - Поля, обязательные для заполнения.
                </p>
                <?php
                $form = ActiveForm::begin([
                    'id' => 'acc-user-data',
                    'action'=>'/account',
                    'enableAjaxValidation' => false,
                ]);
                ?>
                <div class="bg-round">
                    <?php if(Yii::$app->session->hasFlash('changepass')):?>
                        <div class="alert alert-success"><?=Yii::$app->session->getFlash('changepass')?></div>
                    <?php endif;?>
                    <div class="tabl-form">
                        <div class="tr">
                            <div class="th">Старый пароль <span class="star">*</span></div>
                            <div class="td">
                                <?=$form->field($model,'current_password')->passwordInput(['class'=>'w-long'])->label(false)?>
                            </div>
                        </div>
                        <div class="tr">
                            <div class="th">Новый пароль <span class="star">*</span></div>
                            <div class="td">
                                <?=$form->field($model,'password')->passwordInput(['class'=>'w-long'])->label(false)?>
                            </div>
                        </div>
                        <div class="tr">
                            <div class="th">Подтверждение пароля <span class="star">*</span></div>
                            <div class="td">
                                <?=$form->field($model,'confirm_password')->passwordInput(['class'=>'w-long'])->label(false)?>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="text-align: center">
                    <button type="submit" name="ch-pass" class="add-to-cart-button">Сменить</button>
                </div>
                <?php ActiveForm::end(); ?>
            </div>

            <div class="bg-round">
                <h1>Галерея фотографий</h1>
                    <?php if($userPhotos):?>
                        <div class="img-block">
                        <?php foreach ($userPhotos as $it):?>
                            <div class="img-item">
                                <div class="img-block-it">
                                    <a href="/uploads/userphotos/<?=$it->url?>" class="foto-gal"><img src="/uploads/userphotos/thumbs/width_200/<?=$it->url?>" alt=""></a>
                                </div>
                                <div class="edit-photo-block">
                                    <input type="text" name="title" class="edit-in" data-id="<?=$it->id;?>" value="<?=$it->title?>" readonly>
                                    <button type="button" data-id="<?=$it->id;?>" class="add-to-cart-button del-img">Удалить</button>
                                </div>
                            </div>
                        <?php endforeach;?>
                        </div>
                    <?php endif;?>
                    <?php
                        $form = ActiveForm::begin([
                            'action'=>'/account/download',
                            'options' => ['enctype' => 'multipart/form-data']
                        ]);
                    ?>
                    <?= $form->field($modelImg, 'file')->fileInput(); ?>
                    <?= $form->field($modelImg,'title')->textInput(['class'=>'galery-in']);?>
                    <div class="form-group">
                        <?= Html::submitButton('Добавить изображение', ['class' => 'add-to-cart-button']); ?>
                    </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

<?php
$script = <<<JS
    $(function() {
        $("#userprofile-mobile_phone").inputmask("+7 999 999-99-99", {showMaskOnHover: false});
    });
JS;

if(isset($geo->country) && $geo->country=='RU'){
    $this->registerJS($script);
}

$script2 = <<<JS
    $(function() {
        $('[data-toggle="tooltip"]').tooltip();        
        
        $('#user-bdate').datepicker({
            format: 'yyyy-mm-dd',
            language:'ru'
        });
        $("#user-bdate").inputmask("9999-99-99", {showMaskOnHover: false});
        
        $("a.foto-gal").fancybox({
            'transitionIn'	:	'elastic',
            'transitionOut'	:	'elastic',
            'speedIn'		:	600, 
            'speedOut'		:	200, 
            'overlayShow'	:	false
        });
        
        $(".del-img").click(function(){
            var id = $(this).data('id');
            var btn = $(this);
            $.ajax({
                url: '/account/delimg',
                data: {id:id},
                type: 'POST',
                success: function(res){
                    var response = JSON.parse(res);
                    if(!response) alert('Ошибка!');
                    if(typeof response.ok != 'undefined'){
                        btn.parent().parent().remove();
                    }
                },
                error: function(){
                    alert('Error!');
                }
            });
            return false;
        });
    });
JS;
$this->registerJS($script2);

?>
