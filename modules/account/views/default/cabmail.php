<?php
use app\modules\account\widgets\AccountmenuWidget;
use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
$this->title = 'Личный кабинет';
?>
<div class="account-content">
    <div class="first-column"><?=AccountmenuWidget::widget();?></div>
    <div class="second-column">
        <div class="account-page">
            <h1>Отправить сообщение</h1>
            <?php if(Yii::$app->session->hasFlash('acc_email')):?>
                <div class="alert alert-success"><?=Yii::$app->session->getFlash('acc_email')?></div>
            <?php endif;?>
            <?php
                $form = ActiveForm::begin([
                    'id' => 'acc-user-data',
                    'action'=>'/account/cab-mail',
                    'enableAjaxValidation' => false,
                ]);
            ?>
            <?= $form->field($model,'from11')->hiddenInput(['value'=>'1'])->label(false)?>
            <?= $form->field($model,'message')->textarea(['placeholder'=>'Введите ваше сообщение','class'=>'my-form-control'])->label(false);?>
            <br>
            <button type="submit" class="add-to-cart-button">Отправить</button>
            <?php ActiveForm::end(); ?>
            <h1 style="margin-top: 20px;">История переписки</h1>
            <?php if(count($mess)>0):?>
                <?php //vd($mess,false);?>

                <?php foreach ($mess as $m):?>
                    <?php if($m['from11']=='1'):?>
                        <div class="user"  style="clear: both;">
                            <span class="mess_author">
                                <?php
                                    if($userprofile){
                                        echo $userprofile->last_name.' '.$userprofile->first_name;
                                    }
                                ?>
                            </span>&nbsp;&nbsp;
                            <span class="mess_date">(<?= date("d.m.Y H:i",strtotime($m['cdate'])) ?>)</span><br/>
                            <div class="mess_text">
                                <?php
                                if (strpos($m['message'], 'Комментарий к заказу') === 0) {
                                    $message_array = explode(':', $m['message']);
                                    $order_string_array = explode(' ', $message_array[0]);
                                    $order_id = $order_string_array[3];
                                    echo 'Комментарий к заказу <a href="/account/view-history/' . $order_id . '">' . $order_id . '</a>:<br>';
                                    for ($iC = 1; $iC < count($message_array); $iC++) {
                                        echo $message_array[$iC];
                                    }
                                }else{
                                    echo $m['message'];
                                }

                                ?>
                            </div>
                        </div>
                    <?php else:?>
                        <div class="admin" style="clear: both; overflow: auto">
                            <span class="mess_author">Администратор</span>&nbsp;&nbsp;<span class="mess_date">(<?= date("d.m.Y H:i",strtotime($m['cdate'])) ?>)</span>
                            <?php if($m['new_msg']=='1'):?>
                                <b style="color: #ff0000">Новое сообщение!</b>
                            <?php endif;?>
                            <br/>
                            <div class="mess_text">
                                <?= $m['message'] ?>
                            </div>
                        </div>
                    <?php endif;?>
                <?php endforeach;?>

                <?php if (!empty($pagination)): ?>
                    <div class="paginationWrap">
                        <?= LinkPager::widget([
                            'options' => ['class' => 'pagination'],
                            'prevPageLabel' => false,
                            'nextPageLabel' => false,
                            'pagination' => $pagination
                        ]); ?>
                    </div>
                <?php  endif; ?>
            <?php endif;?>
        </div>
    </div>
</div>
