<?php
use app\modules\account\widgets\AccountmenuWidget;
use yii\widgets\LinkPager;
use app\modules\admin\models\SaleItems;
$this->title = 'Личный кабинет';
$saleTypes = array('Отгрузка', 'Возврат', 'Невыкуп');
$saleTypesR = array('отгрузки', 'возврата', 'невыкупа');
?>
<div class="account-content">
    <div class="first-column"><?=AccountmenuWidget::widget();?></div>
    <div class="second-column">
        <div class="account-page">
            <?php if($order):?>
            <h1>Состав заказа <?=$order->id?> от <?=date("d.m.Y",strtotime($order->cdate))?></h1>

            <?php if(count($order_items)>0):?>
                <div class="tabl highlight">
                    <div class="tr">
                        <div class="th">Изделие</div>
                        <div class="th">Товар</div>
                        <div class="th">Размер</div>
                        <div class="th">Цвет</div>
                        <div class="th">Кол-во</div>
                        <div class="th">Цена</div>
                        <div class="th">Цена по акции</div>
                        <div class="th">Итого</div>
                    </div>
                    <?php foreach ($order_items as $it):?>
                        <div class="tr">
                            <div class="td no-wrap"><strong><?=$it['name']?></strong><br><?="Артикул: ".$it['article']?></div>
                            <div class="td digits no-wrap">
                                <?php if(!empty($it['img'])):?>
                                    <a href="<?=$it['img']?>" class="foto-order"><img src="<?=$it['img']?>" alt="" width="50"></a>
                                <?php endif;?>
                            </div>
                            <div class="td">
                                <?php if(!empty($it['size_y'])):?>
                                    <?=$it['size_y']?>
                                <?php else:?>
                                    <?=str_replace('Поставщик - ','',$it['size_n'])?>
                                <?php endif;?>
                            </div>
                            <div class="td"><?=$it['color']?></div>
                            <div class="td"><?=$it['qty']?></div>
                            <div class="td"><?=(int)$it['price']?> руб.</div>
                            <div class="td"><?=(int)$it['new_price']?> руб.</div>
                            <div class="td">
                                <?php
                                    if($it['new_price']!='0.00'){
                                        $itog = $it['new_price']*(int)$it['qty'];
                                    }else{
                                        $itog = $it['price']*(int)$it['qty'];
                                    }
                                    echo $itog." руб.";
                                ?>
                            </div>
                        </div>
                    <?php endforeach;?>
                </div>
                <div class="tabl total">
                    <div class="tr">
                        <div class="td" style="border-bottom:none;">Комментарий:</div>
                        <div class="td digits"><?=$order->comment?></div>
                    </div>
                </div>
                <div class="tabl total">
                    <div class="tr">
                        <div class="td" style="border-bottom:none;">Списано с вашего лицевого счета:</div>
                        <div class="td digits"><?=$order->off_ac?> руб.</div>
                    </div>
                </div>
                <div class="tabl total">
                    <div class="tr">
                        <div class="td" style="border-bottom:none;">Выбрано на сумму:</div>
                        <div class="td digits"><?=$order->to_be_paid?> руб.</div>
                    </div>
                </div>
            <?php endif;?>
            <div class="add-to-cart-block" style="text-align:center; margin-top: 20px;">
                <a href="/account/cab-history" class="add-to-cart-button-2">Вернуться</a>
            </div>
            <?php endif;?>

            <?php if($sales):?>
                <?php $allSsumm = 0; ?>
                <?php foreach ($sales as $sale):?>
                    <?php
                        $sC_summ = 0;
                        $sales_items = SaleItems::findAll(['sale_id'=>$sale->id,'visible'=>'1']);
                    ?>
                    <h3><?= $saleTypes[$sale->type] ?>
                        <?php if ($sale->type == 0): ?>
                            <?= $sale->n_sale ?>
                        <?php endif;?>
                    </h3>
                    <div class="tabl highlight">
                        <div class="tr">
                            <div class="th">Изделие</div>
                            <div class="th">Размер</div>
                            <div class="th">Цвет</div>
                            <div class="th">Кол-во</div>
                            <div class="th">Цена</div>
                            <div class="th">Итого</div>
                        </div>
                        <?php
                        foreach ($sales_items as $sItem):
                            $sC_summ += $sItem->qty * $sItem->price;
                            ?>
                            <div class="tr">
                                <div class="td"><strong><?= $sItem->name ?><br>Артикул: <?= $sItem->article ?></strong></div>
                                <div class="td"><?= $sItem->size ?></div>
                                <div class="td"><?= $sItem->color ?></div>
                                <div class="td digits"><?= $sItem->qty ?></div>
                                <div class="td digits no-wrap"><?= $sItem->price ?> руб.</div>
                                <div class="td digits no-wrap"><?= ($sItem->qty * $sItem->price) ?> руб.</div>
                            </div>

                            <?php
                        endforeach;
                        if ($sale->type == 0) {
                            $allSsumm += $sC_summ;
                        } else {
                            $allSsumm -= $sC_summ;
                        }
                        ?>
                    </div>
                    <?php if ($sale->id_post): ?>
                        <div class="tabl total">
                            <div class="tr">
                                <div class="td"><b>Почтовый идентификатор:</b></div>
                                <div class="td digits"><?= $sale->id_post ?></div>
                            </div>
                        </div>
                    <?php endif;?>
                    <div class="tabl total">
                        <div class="tr">
                            <div class="td" style="border-bottom:none;">Стоимоcть <?=$saleTypesR[$sale->type]?>:</div>
                            <div class="td digits"><?= $sC_summ ?> руб.</div>
                        </div>
                    </div>
                <?php endforeach;?>
                <div class="tabl total">
                    <div class="tr">
                        <div class="td" style="border-bottom:none;"><strong>Общая стоимость отгрузок:</strong></div>
                        <div class="td digits"><strong><?= $allSsumm ?> руб.</strong></div>
                    </div>
                </div>
                
                <div class="add-to-cart-block" style="text-align:center; margin-top: 20px;">
                    <a href="/account/cab-history" class="add-to-cart-button-2">Вернуться</a>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>

<?php
$script = <<<JS
    $(function(){
        $("a.foto-order").fancybox({
            'transitionIn'	:	'elastic',
            'transitionOut'	:	'elastic',
            'speedIn'		:	600, 
            'speedOut'		:	200, 
            'overlayShow'	:	false
        });
    });
JS;
$this->registerJS($script);
?>
