<?php
namespace app\modules\account\widgets;
use yii\base\Widget;
use Yii;
use app\models\UserProfile;

class AccountmenuWidget extends Widget
{
    public $_user;
    public $curUrl;

    public function init()
    {
        parent::init();
        $this->curUrl = Yii::$app->request->getUrl();
        $this->_user = UserProfile::findOne(['user_id'=>Yii::$app->user->id]);
    }

    public function run()
    {
        return $this->render('menu',['user'=>$this->_user,'curUrl'=>$this->curUrl]);
    }
}