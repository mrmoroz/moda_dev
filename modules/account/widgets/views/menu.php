<div class="menu-content">
    <h2>Добро пожаловать, <?=$user->first_name?></h2>
    <h2 class="sub-menu-title" style="margin-top:10px">Личный кабинет</h2>
    <ul class="sub-menu">
        <li><a href="/cart">Текущий заказ</a></li>
        <li><a href="/account" class="<?=($curUrl=='/account'?'active':'')?>">Мои данные</a></li>
        <li><a href="/account/ch-pass" class="<?=($curUrl=='/account/ch-pass'?'active':'')?>">Сменить пароль</a></li>
        <li><a href="/account/cab-history" class="<?=($curUrl=='/account/cab-history'?'active':'')?>">Мои заказы</a></li>
        <li><a href="/account/cab-mail" class="<?=($curUrl=='/account/cab-mail'?'active':'')?>">Служебная почта </a></li>
<!--        <li><a href="/account/cab-dp" class="--><?//=($curUrl=='/account/cab-dp'?'active':'')?><!--">Доставка и оплата</a></li>-->
        <li><a href="/callback/otzyvy">Добавить отзыв</a></li>
<!--        <li><a href="/account/complaints" class="--><?//=($curUrl=='/account/complaints'?'active':'')?><!--">Пожаловаться</a></li>-->
    </ul>
</div>

