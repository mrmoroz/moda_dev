<?php
/**
 * Отказ от регистрации при заказе
 * Created by PhpStorm.
 * User: max
 * Date: 10.05.17
 * Time: 10:33
 */

namespace app\forms;
use Yii;
use yii\base\Model;
use app\models\User;
use app\models\UserProfile;
use app\modules\admin\models\MlCfg;
use app\modules\cart\models\Orders;
use app\modules\cart\models\OrderItems;
use app\models\Product;
use app\models\Color;
use app\models\Size;
use app\models\Stock;
use app\models\StockPosition;
use yii\helpers\ArrayHelper;
use app\models\ProductChangelog;
use app\models\ProductSizeChangelog;
use app\components\SMSMessage;
use app\models\Parametrs;
use app\models\CabEmail;
use app\modules\cart\models\Cart;

class SkipForm extends Model
{
    public $first_name;
    public $phone;
    public $email;
    public $is_account;
    public $address;
    public $comment;
    public $_usr_id = 0;
    public $log_data = "";

    public function rules()
    {
        return [
            [['first_name', 'phone'], 'required'],
            ['email','email'],
            [['email','is_account','address','comment'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'first_name' => 'Ваше имя',
            'phone' => 'Телефон',
            'email' => 'E-mail',
            'is_account' => 'Создать "Личный кабинет"',
            'address' => 'Адрес доставки',
            'comment' => 'Комментарий'
        ];
    }
    //обработка заказа
    public function saveOrder($geodata){
        //первый платеж или нет
        $first_payment = $this->checkPayments();
        $this->logProducts($geodata->country);
        $products_info = $this->getProductsInfo($geodata->country);
        //сохранение заказа в бд
        $id_order = $this->saveOrderInBD($first_payment,$products_info);
        if($id_order){
            //сохранение продуктов к заказу
            $prod = $this->saveProductItems($id_order);
            if($prod){
                $order = Orders::findOne($id_order);
                if(!empty($this->email)) {
                    $this->sendEmailUser($prod, $id_order, $this->first_name, $products_info, $this->comment);
                }
                $this->sendEmailAdmin($this->phone, $prod, $id_order, $this->first_name, $products_info, $this->comment, $this->address);
                //проверка складов
                $this->checkStocks($prod,$id_order);
                //Комментарий к заказу на служебную почту пользователя
                if (!empty($this->comment) && $this->_usr_id!=0) {
                    $mCabEmail = new CabEmail();
                    $mCabEmail->page_id = 0;
                    $mCabEmail->user_id = $this->_usr_id;
                    $mCabEmail->cdate = date("Y-m-d H:i:s");
                    $mCabEmail->message = $this->comment;
                    $mCabEmail->from11 = 1;
                    $mCabEmail->visible = 1;
                    $mCabEmail->sys_language = 1;
                    $mCabEmail->save(false);
                }
                //Удаление корзины из БД
                $cartModel = new  Cart();
                $cartModel->removeCookiesCartInBD();
                //удаление сессии и перенаправление
                if (isset($_SESSION['cart'])) {
                    unset($_SESSION['cart']);
                }
                return $id_order;
            }
        }
        return false;
    }

    /**
     * Проверка доступности товара на складах
     * @param $prod
     * @throws \yii\db\Exception
     */
    private function checkStocks($prod,$id_order){
        foreach ($prod as $it){
            $sql = "SELECT stock_position.id FROM stock_position
                        WHERE stock_position.product_id = '{$it['prod']->id}'
                        AND stock_position.quantity > 0
            ";
            $stocks = Yii::$app->db->createCommand($sql)->queryAll();
            if(count($stocks)==0){
                $newChangeLog = new ProductChangelog([
                    'type' => 1,
                    'product_id' => $it['prod']->id,
                    'operation_date' => date('Y-m-d H:i:s'),
                    'order_id' => $id_order
                ]);
                $newChangeLog->save();

                Yii::$app->db->createCommand("UPDATE products SET is_active = false WHERE id='{$it['prod']->id}'")->execute();
            }
        }
    }

    public function saveOrderInBD($first_payment,$products_info){
        $discount_user = 0;
        if($this->_usr_id!=0){
            $userInfo = User::findOne($this->_usr_id);
            if($userInfo && $userInfo->discount_user){
                $discount_user = $userInfo->discount_user;
            }
        }

        $order = new Orders();
        $order->planed = 1;
        $order->user_id = $this->_usr_id;
        $order->cdate = date('Y-m-d H:i:s');
        $order->total_qty = $products_info['total_qty'];
        $order->total_cost = $products_info['total_cost'];
        $order->discount = $products_info['discount'];
        $order->off_ac = 0;
        $order->to_be_paid = $products_info['to_be_paid'];
        $order->economy = $products_info['ecomomy'];
        $order->status = 13;
        $order->pay_type = 'naloj';
        $order->deliv_addr = $this->address;
        $order->deliv_type = 'post';
        //$order->country = $address->country_id;
        //$order->region = $address->region_id;
        //$order->city = $address->city;
        //$order->zip = $address->post_index;
        $order->fio = $this->first_name;
        $order->first = $first_payment;
        $order->log_data = $this->log_data;
        $order->user_discount = $discount_user;
        $order->comment = $this->comment;
        $order->mobile_phone = $this->phone;
        if($order->save(false)){
            //обновим данные пользователя
            if($this->_usr_id!=0) {
                $user = User::findOne($this->_usr_id);
                $user->last_order_date = $order->cdate;
                $user->not_orders = 0;
                $user->delivery_method = 'post';
                $user->pay_method = 'naloj';
                $user->ignore_mail_list = '0';
                $user->save(false);
            }
            return $order->id;
        }
        return false;
    }

    /**
     * Сохранение товаров к заказу
     * @param $id_order
     * @return array|bool
     */
    private function saveProductItems($id_order){
        $prod = [];
        $prod_id = [];
        $stocks =  $this->getStocks(); //наши скдады
        $man_stocks = $this->getManStocks(); //склады поставщиков

        foreach ($_SESSION['cart'] as $k=>$it){
            if($k!='allqty' && $k!='sum' && $k!='allsum'){
                $prod[$it['id']]['color'] = Color::findOne($it['attr']['color']);
                $prod[$it['id']]['size'] = Size::findOne($it['attr']['size']);
                $prod[$it['id']]['prod'] = Product::findOne($it['id']);
                $prod[$it['id']]['img'] = $it['img'];
                $prod[$it['id']]['qty'] = $it['qty'];
                if(isset($it['gifts'])){
                    foreach ($it['gifts'] as $gf){
                        $prod[$it['id']]['gifts'][$gf['gift_id']]['prod'] = Product::findOne($gf['gift_id']);
                        $prod[$it['id']]['gifts'][$gf['gift_id']]['parent'] = $it['id'];
                    }
                }
                $stock_tmp = Yii::$app->db->createCommand("
                        SELECT stock_position.*, stocks.name, stocks.position FROM stock_position
                        LEFT JOIN stocks ON stocks.id = stock_position.stock_id 
                        WHERE stock_position.product_id = '{$it['id']}'
                        AND stock_position.color_id = '{$it['attr']['color']}'
                        AND stock_position.size_id = '{$it['attr']['size']}'
                        AND stock_position.stock_id IN($stocks)
                        AND stock_position.quantity > 0
                        ORDER BY stocks.position DESC
                ")->queryOne();
                $prod[$it['id']]['size_y'] = "";
                if($stock_tmp){
                    $prod[$it['id']]['size_y'] = $stock_tmp['name']." - ".$prod[$it['id']]['size']->name;

                    $newSizeLog = new ProductSizeChangelog([
                        'product_id' => $it['id'],
                        'color_id' => $it['attr']['color'],
                        'size_id' => $it['attr']['size'],
                        'stock_id' => $stock_tmp['stock_id'],
                        'status' => false,
                        'order_id' => $id_order,
                        'type' => 1,
                        'date_operation' => date('Y-m-d H:i:s'),
                    ]);
                    $newSizeLog->save();
                    Yii::$app->db->createCommand("UPDATE stock_position SET quantity=0 WHERE id='{$stock_tmp['id']}'")->execute();
                }
                $prod[$it['id']]['size_n'] = "";
                $man_stock_tmp = Yii::$app->db->createCommand("
                        SELECT stock_position.*, stocks.name, stocks.position FROM stock_position
                        LEFT JOIN stocks ON stocks.id = stock_position.stock_id 
                        WHERE stock_position.product_id = '{$it['id']}'
                        AND stock_position.color_id = '{$it['attr']['color']}'
                        AND stock_position.size_id = '{$it['attr']['size']}'
                        AND stock_position.stock_id IN($man_stocks)
                        AND stock_position.quantity > 0
                        ORDER BY stocks.position DESC
                ")->queryAll();
                if($man_stock_tmp){
                    foreach ($man_stock_tmp as $mst){
                        $prod[$it['id']]['size_n'].=$mst['name']." - ".$prod[$it['id']]['size']->name.", ";
                    }
                }
            }
        }

        if(count($prod)>0){
            foreach ($prod as $key=>$item){
                $itemsModel = new OrderItems();
                $itemsModel->order_id = $id_order;
                $itemsModel->name = $item['prod']->title;
                $itemsModel->article = $item['prod']->article;
                $itemsModel->size_y = $item['size_y'];
                $itemsModel->size_n = rtrim($item['size_n'],', ');
                $itemsModel->color = $item['color']->name;
                $itemsModel->price = $item['prod']->price;
                $itemsModel->new_price = $item['prod']->discount_price;
                $itemsModel->qty = $item['qty'];
                $itemsModel->img = $item['img'];
                $itemsModel->prod_id = $item['prod']->id;
                $itemsModel->manufacturer = $item['prod']->manufacturer;
                $itemsModel->save(false);
                if(isset($item['gifts'])){
                    foreach ($item['gifts'] as $igf){
                        $itemsModel = new OrderItems();
                        $itemsModel->order_id = $id_order;
                        $itemsModel->name = $igf['prod']->title;
                        $itemsModel->article = $igf['prod']->article;
                        $itemsModel->qty = $item['qty'];
                        $itemsModel->gift = $igf['parent'];
                        $itemsModel->save(false);
                    }
                }
            }

            return $prod;
        }
        return false;
    }

    public function getStocks(){
        $stock_str = "";
        $stock = ArrayHelper::getColumn(Stock::findAll(['is_manufacturer'=>false]),'id');
        if($stock){
            foreach ($stock as $it){
                $stock_str.="'".$it."',";
            }
            $stock_str = rtrim($stock_str,",");
        }
        return $stock_str;
    }

    public function getManStocks(){
        $stock_str = "";
        $stock = ArrayHelper::getColumn(Stock::findAll(['is_manufacturer'=>true]),'id');
        if($stock){
            foreach ($stock as $it){
                $stock_str.="'".$it."',";
            }
            $stock_str = rtrim($stock_str,",");
        }
        return $stock_str;
    }

    /**
     * Информация из корзины для заказа
     * @param $pay_method
     * @return array
     */
    private function getProductsInfo($country){
        $products = [];
        $products['total_qty'] = $_SESSION['cart']['allqty'];
        $products['total_cost'] = $_SESSION['cart']['allsum'];
        $paymentDiscount = 0;//($pay_method && $pay_method->discount > 0) ? round($_SESSION['cart']['sum']*$pay_method->discount / 100) : 0;
        $products['discount'] = $paymentDiscount;
        $products['to_be_paid'] = $_SESSION['cart']['sum'] - $paymentDiscount;
        $products['ecomomy'] = $_SESSION['cart']['allsum'] - $_SESSION['cart']['sum'] + $paymentDiscount;
        $this->log_data.= "Выбрано ".$products['total_qty']." ".$this->plural($products['total_qty'], 'товар', 'товара', 'товаров')." на сумму: ".$products['total_cost']." ".$this->getManySymbol($country)." <br>";
        $this->log_data.= "Скидка от способа оплаты: 0% - ".$paymentDiscount." ".$this->getManySymbol($country)."<br>";
        $this->log_data.= "Общая экономия по заказу: ".$products['ecomomy']." ".$this->getManySymbol($country). "<br>";
        $this->log_data.= "Итого: ".$products['to_be_paid']." ".$this->getManySymbol($country)."<br>";
        return $products;
    }

    public function logProducts($country){
        $userDta = User::findOne(Yii::$app->user->id);
        $prodPriceSumm = 0;
        foreach ($_SESSION['cart'] as $k=>$it){
            if($k!='allqty' && $k!='sum' && $k!='allsum') {
                $prod = Product::findOne($it['id']);
                $this->log_data .= '<b>Товар- ' . $prod->article . '</b>. Цена: ' . $prod->price .' '.$this->getManySymbol($country) .' / Цена новая:' . $prod->discount_price .' '.$this->getManySymbol($country). ' Размер скидки (без производителя): ' . $prod->discount . '%<br>';
                $prodPriceWithDiscount = round(($prod->discount_price) ? $prod->discount_price : $prod->price);
                $this->log_data .= '- Выбрана цена: ' . $prodPriceWithDiscount .' '.$this->getManySymbol($country). '<br>';
                if ((int)$prodPriceWithDiscount == (int)$prod->price) {
                    if($userDta) {
                        $prodPriceWithDiscount = $prodPriceWithDiscount - round($prodPriceWithDiscount * $userDta->discount_user / 100);
                        $this->log_data .= '- Персональная скидка:' . $userDta->discount_user . '%. Цена с персональной скидкой: ' . $prodPriceWithDiscount . ' ' . $this->getManySymbol($country) . '<br>';
                    }
                }
                $this->log_data .= '- Расчет итоговой стоимости корзины: текущая сумма: ' . $prodPriceSumm .' '.$this->getManySymbol($country). ' Цена на товар: ' . $prodPriceWithDiscount .' '.$this->getManySymbol($country). ' Количество товаров: ' . $it['qty'] . '<br>';
                $prodPriceSumm += $prodPriceWithDiscount * $it['qty'];
                $this->log_data .= '-- Новая стоимость корзины: ' . $prodPriceSumm .' '.$this->getManySymbol($country). '<br>';
            }
        }
    }

    public function getManySymbol($country){
        if($country){
            if($country=='KZ'){
                return "т.";
            }
        }
        return "руб.";
    }

    /**
     * Проверка на первый платеж
     * @return string
     */
    public function checkPayments(){
        if($this->_usr_id!=0){
            $orders = Orders::findAll(['user_id'=>$this->_usr_id]);
            if($orders){
                return "0";
            }
        }
        return "1";
    }

    //тихая регистрация пользователя
    public function reguser(){
        if(!empty($this->email)){
            $user = User::findOne(['login'=>$this->email]);
            if($user){
                $this->_usr_id = $user->id;
            }else{
                $password = $this->randomPassword();
                $modelUser = new User();
                $modelUser->login = $this->email;
                $modelUser->setPassword($password);
                $modelUser->generateAuthKey();
                $modelUser->status = 1;
                $modelUser->created_at = $modelUser->updated_at = (new \DateTime())->format('Y-m-d H:i:s');
                if ($modelUser->save()) {
                    $this->_usr_id = $modelUser->id;
                    Yii::$app->authManager->assign(Yii::$app->authManager->getRole('User'), $modelUser->id);
                    $profile = new UserProfile();
                    $profile->first_name = $this->first_name;
                    $profile->mobile_phone = $this->phone;
                    $profile->user_id = $modelUser->id;
                    $profile->save(false);
                    $this->sendEmails($password,$modelUser);
                    \Yii::$app->user->login($modelUser,Yii::$app->params['user.sessionExpire']);
                }
            }
        }
    }

    private function randomPassword($number = 6){
        $arr = array('a','b','c','d','e','f',
            'g','h','i','j','k','l',
            'm','n','o','p','r','s',
            't','u','v','x','y','z',
            'A','B','C','D','E','F',
            'G','H','I','J','K','L',
            'M','N','O','P','R','S',
            'T','U','V','X','Y','Z',
            '1','2','3','4','5','6',
            '7','8','9','0',
            '(',')','[',']','!','*','$'
        );
        // Генерируем пароль
        $pass = "";
        for($i = 0; $i < $number; $i++)
        {
            // Вычисляем случайный индекс массива
            $index = rand(0, count($arr) - 1);
            $pass .= $arr[$index];
        }
        return $pass;
    }

    /**
     * Отправляет письма о регисрации
     * @param $pass
     * @param User $user
     */
    public function sendEmails($pass, User $user){
        $profile = UserProfile::findOne(['user_id'=>$user->id]);
        $tpl = MlCfg::findOne(['mnemo'=>'conf_user']);
        $message_user = $this->setUserMessage($pass, $user, $tpl, $profile);
        $tpl_admin = MlCfg::findOne(['mnemo'=>'user_new']);
        $message_admin = $this->setAdminMessage($pass, $user, $tpl_admin, $profile);

        if($tpl->etype=='1') {
            Yii::$app->mailer->compose()
                ->setFrom([Yii::$app->params['supportEmail'] => 'Отправлено с сайта beauti-full.ru'])
                ->setTo($user->login)
                ->setSubject($tpl->esubj)
                ->setHtmlBody($message_user)
                ->send();
        }

        if($tpl_admin->etype=='1') {
            Yii::$app->mailer->compose()
                ->setFrom([Yii::$app->params['supportEmail'] => 'Отправлено с сайта'])
                ->setTo(Yii::$app->params['adminEmail'])
                ->setSubject($tpl_admin->esubj)
                ->setHtmlBody($message_admin)
                ->send();
        }
    }

    private function setUserMessage($pass, $user, $tpl, $profile){
        $mailForm = $tpl->ebody_html;
        $message = str_replace([
            '{name2}',
            '{name3}',
            '{email}',
            '{pass}'
        ],
            [
                '',
                $profile->first_name,
                $user->login,
                $pass,
            ],
            $mailForm
        );
        return $message;
    }

    private function setAdminMessage($pass, $user, $tpl, $profile)
    {
        $mailForm = $tpl->ebody_html;
        $message = str_replace([
            '{name}',
            '{name2}',
            '{name3}',
            '{email}',
            '{pass}',
            '{city}',
            '{zip}',
            '{addr}',
            '{mob-phone}'
        ],
            [
                '',
                $profile->first_name,
                '',
                $user->login,
                $pass,
                '',
                '',
                '',
                $profile->mobile_phone
            ],
            $mailForm
        );
        return $message;
    }

    /**
     * Множественное окончание слов
     * @param $n
     * @param $form1
     * @param $form2
     * @param $form5
     * @return mixed
     */
    public function plural($n, $form1, $form2, $form5) {
        $n  = abs($n) % 100;
        $n1 = $n % 10;
        if ($n > 10 && $n < 20){
            return $form5;
        }else if ($n1 > 1 && $n1 < 5) {
            return $form2;
        }else if ($n1 == 1){
            return $form1;
        }
        return $form5;
    }

    public function sendEmailUser($prod, $id_order, $name, $products_info,$comment){
        $tpl = MlCfg::findOne(['mnemo'=>'order_user']);
        $message = $this->setUserMessageOrder($prod, $id_order, $name, $products_info, $comment, $tpl);
        $validator = new yii\validators\EmailValidator();
        if ($validator->validate($this->email, $error)) {
            if($tpl->etype=='1') {
                Yii::$app->mailer->compose()
                    ->setFrom([Yii::$app->params['supportEmail'] => 'Отправлено с сайта beauti-full.ru'])
                    ->setTo($this->email)
                    ->setSubject($tpl->esubj)
                    ->setHtmlBody($message)
                    ->send();
            }
        }
    }

    private function setUserMessageOrder($prod, $id_order, $name, $products_info, $comment, $tpl)
    {
        $mailForm = $tpl->ebody_html;
        $date = date("d.m.Y");
        $prod_table = "";

        if($prod){
            $prod_table.="<table style=\"width: 100%; border: 1px solid #333333; border-collapse: collapse; margin-bottom: 30px;\">
                                <tr>
                                    <th style='border: 1px solid #333333;'>Изделие</th>
                                    <th style='border: 1px solid #333333;'>Артикул</th>
                                    <th style='border: 1px solid #333333;'>Размер</th>
                                    <th style='border: 1px solid #333333;'>Цвет</th>
                                    <th style='border: 1px solid #333333;'>Кол-во</th>
                                    <th style='border: 1px solid #333333;'>Цена</th>
                                    <th style='border: 1px solid #333333;'>Цена со скидкой</th>
                                </tr>";
            foreach ($prod as $key=>$item){
                $prod_table.="<tr>";
                $prod_table.="<td style='border: 1px solid #333333;'>".$item['prod']->title."</td>";
                $prod_table.="<td style='border: 1px solid #333333;'>".$item['prod']->article."</td>";
                if(!empty($item['size_y']) || !empty($item['size_n'])){
                    $size = $item['size']->name;
                }else{
                    $size = "нет в наличии";
                }
                $prod_table.="<td style='border: 1px solid #333333;'>".$size."</td>";
                $prod_table.="<td style='border: 1px solid #333333;'>".$item['color']->name."</td>";
                $prod_table.="<td style='border: 1px solid #333333;'>".$item['qty']."</td>";
                $prod_table.="<td style='border: 1px solid #333333;'>".$item['prod']->price."</td>";
                $prod_table.="<td style='border: 1px solid #333333;'>".$item['prod']->discount_price."</td>";
                $prod_table.="</tr>";
                if(isset($item['gifts'])){
                    foreach ($item['gifts'] as $igf){
                        $prod_table.="<tr>";
                        $prod_table.="<td style='border: 1px solid #333333;'>".$igf['prod']->title." (подарок)</td>";
                        $prod_table.="<td style='border: 1px solid #333333;'>".$igf['prod']->article."</td>";
                        $prod_table.="<td style='border: 1px solid #333333;'>-</td>";
                        $prod_table.="<td style='border: 1px solid #333333;'>-</td>";
                        $prod_table.="<td style='border: 1px solid #333333;'>".$item['qty']."</td>";
                        $prod_table.="<td style='border: 1px solid #333333;'>0</td>";
                        $prod_table.="<td style='border: 1px solid #333333;'>0</td>";
                        $prod_table.="</tr>";
                    }
                }
            }
            $prod_table.="<tr>";
            $prod_table.="<td colspan='7' style='border: 1px solid #333333;'>Скидка ".$products_info['ecomomy']." руб.</td>";
            $prod_table.="</tr>";
            $prod_table.="<tr>";
            $prod_table.="<td colspan='7' style='border: 1px solid #333333;'>Итого: ".$products_info['to_be_paid']." руб.</td>";
            $prod_table.="</tr>";
            if(!empty($comment)){
                $prod_table.="<tr>";
                $prod_table.="<td  style='border: 1px solid #333333;'>Ваш комментарий</td>";
                $prod_table.="<td colspan='6' style='border: 1px solid #333333;'>".$comment."</td>";
                $prod_table.="</tr>";
            }

        }

        $message = str_replace([
            '{name}',
            '{name2}',
            '{order_id}',
            '{order_date}',
            '{prod_table}'
        ],
            [
                '',
                $name,
                $id_order,
                $date,
                $prod_table
            ],
            $mailForm
        );
        return $message;
    }

    public function sendEmailAdmin($mobile_phone, $prod, $id_order, $name, $products_info, $comment, $address){
        $tpl = MlCfg::findOne(['mnemo'=>'order_admin']);
        $message = $this->setAdminMessageOrder($mobile_phone, $prod, $id_order, $name, $products_info, $comment, $address, $tpl);

        $adminEmails = Parametrs::findOne(2);
        if($adminEmails && !empty($adminEmails->value)) {
            $emails = explode(',', $adminEmails->value);
            if($tpl->etype=='1') {
                Yii::$app->mailer->compose()
                    ->setFrom([Yii::$app->params['supportEmail'] => 'Отправлено с сайта beauti-full.ru'])
                    ->setTo($emails)
                    ->setSubject($tpl->esubj)
                    ->setHtmlBody($message)
                    ->send();
            }
        }
    }

    private function setAdminMessageOrder($mobile_phone, $prod, $id_order, $name, $products_info, $comment, $address, $tpl)
    {
        $mailForm = $tpl->ebody_html;
        $date = date("d.m.Y");
        $prod_table = "";
        $user_data = "";
        $data = "";


        $user_data.="<table style='width: 100%; border: 1px solid #333333; border-collapse: collapse; margin-bottom: 30px;'>
                        <tr>
                            <th style='border: 1px solid #333333;'>Адрес доставки</th>
                            <th style='border: 1px solid #333333;'>Электронная почта</th>
                            <th style='border: 1px solid #333333;'>Имя</th>
                            <th style='border: 1px solid #333333;'>Мобильный телефон</th>
                        </tr>
                        <tr>
                            <td style='border: 1px solid #333333;'>".$address."</td>
                            <td style='border: 1px solid #333333;'>".$this->email."</td>
                            <td style='border: 1px solid #333333;'>".$this->first_name."</td>
                            <td style='border: 1px solid #333333;'>".$mobile_phone."</td>
                        </tr>
                    </table>";

        if($prod){
            $prod_table.="<table style=\"width: 100%; border: 1px solid #333333; border-collapse: collapse; margin-bottom: 30px;\">
                                <tr>
                                    <th style='border: 1px solid #333333;'>Изделие</th>
                                    <th style='border: 1px solid #333333;'>Артикул</th>
                                    <th style='border: 1px solid #333333;'>Размер</th>
                                    <th style='border: 1px solid #333333;'>Цвет</th>
                                    <th style='border: 1px solid #333333;'>Кол-во</th>
                                    <th style='border: 1px solid #333333;'>Цена</th>
                                    <th style='border: 1px solid #333333;'>Цена со скидкой</th>
                                </tr>";
            foreach ($prod as $key=>$item){
                $prod_table.="<tr>";
                $prod_table.="<td style='border: 1px solid #333333;'>".$item['prod']->title."</td>";
                $prod_table.="<td style='border: 1px solid #333333;'>".$item['prod']->article."</td>";
                if(!empty($item['size_y']) || !empty($item['size_n'])){
                    $size = $item['size']->name;
                }else{
                    $size = "нет в наличии";
                }
                $prod_table.="<td style='border: 1px solid #333333;'>".$size."</td>";
                $prod_table.="<td style='border: 1px solid #333333;'>".$item['color']->name."</td>";
                $prod_table.="<td style='border: 1px solid #333333;'>".$item['qty']."</td>";
                $prod_table.="<td style='border: 1px solid #333333;'>".$item['prod']->price."</td>";
                $prod_table.="<td style='border: 1px solid #333333;'>".$item['prod']->discount_price."</td>";
                $prod_table.="</tr>";
                if(isset($item['gifts'])){
                    foreach ($item['gifts'] as $igf){
                        $prod_table.="<tr>";
                        $prod_table.="<td style='border: 1px solid #333333;'>".$igf['prod']->title." (подарок)</td>";
                        $prod_table.="<td style='border: 1px solid #333333;'>".$igf['prod']->article."</td>";
                        $prod_table.="<td style='border: 1px solid #333333;'>-</td>";
                        $prod_table.="<td style='border: 1px solid #333333;'>-</td>";
                        $prod_table.="<td style='border: 1px solid #333333;'>".$item['qty']."</td>";
                        $prod_table.="<td style='border: 1px solid #333333;'>0</td>";
                        $prod_table.="<td style='border: 1px solid #333333;'>0</td>";
                        $prod_table.="</tr>";
                    }
                }
            }
            $prod_table.="<tr>";
            $prod_table.="<td colspan='7' style='border: 1px solid #333333;'>Скидка ".$products_info['ecomomy']." руб.</td>";
            $prod_table.="</tr>";
            $prod_table.="<tr>";
            $prod_table.="<td colspan='7' style='border: 1px solid #333333;'>Итого: ".$products_info['to_be_paid']." руб.</td>";
            $prod_table.="</tr>";
            if(!empty($comment)){
                $prod_table.="<tr>";
                $prod_table.="<td  style='border: 1px solid #333333;'>Ваш комментарий</td>";
                $prod_table.="<td colspan='6' style='border: 1px solid #333333;'>".$comment."</td>";
                $prod_table.="</tr>";
            }

        }

        $message = str_replace([
            '{order_id}',
            '{order_date}',
            '{prod_table}',
            '{user_data}',
            '{data}',
            '{delivery}',
            '{payment}'
        ],
            [
                $id_order,
                $date,
                $prod_table,
                $user_data,
                $data,
                'Почта России',
                'Наложенным платежом в почтовом отделении'
            ],
            $mailForm
        );
        return $message;
    }
}