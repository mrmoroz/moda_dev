<?php

namespace app\forms;

use app\models\User;
use Yii;
use yii\base\Model;

class LoginForm extends Model
{
    public $email;

    private $_user = null;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['email'], 'email'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => 'Электронная почта',
        ];
    }


    public function login($roleLogin = null)
    {
        if ($this->validate()) {
            $identity = $this->getUser();

            if (!$identity) {
                $this->addError('email', Yii::t('app', 'Нет такого адреса электронной почты'));
            } elseif ($identity && $identity->isBlocked()) {
                $this->addError('email', Yii::t('app', 'Пользователь заблокирован'));
            }
            if (!$this->hasErrors()) {
                return Yii::$app->user->login($identity);
            }
        }
        return false;
    }


    public function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByEmail($this->email);
        }

        return $this->_user;
    }
}