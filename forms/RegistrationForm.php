<?php

namespace app\forms;

use app\models\User;
use yii\base\Model;
use app\models\MlCfg;
use app\models\Parametrs;
use Yii;


class RegistrationForm extends Model
{
    public $email;
    public $f_name;
    public $l_name;
    public $t_name;
    public $phone;
    public $region_id;
    public $city_name;
    public $tk;
    public $realizations;
    public $sog;
    public $ignore_mail_list;
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['email','f_name','l_name','t_name','phone','region_id','city_name','tk','realizations'],'required'],
            ['sog', 'compare', 'compareValue' => 1, 'message' => 'Необходимо ометить "Соглашение на обработку персональных данных!"'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass'=> 'app\models\User'],
            ['ignore_mail_list','safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => 'Электронная почта',
            'f_name' => 'Имя',
            'l_name' => 'Фамилия',
            't_name' => 'Отчество',
            'phone' => 'Телефон',
            'region_id' => 'Выберите регион',
            'city_name' => 'Город',
            'tk' => 'Транспортная компания',
            'realizations' => 'Способ реализации товара',
            'sog'=> 'Я согласен на обработку персональных данных в соответствии с ФЗ РФ от 27.07.2006, №152 Ф3 (ред от 21.07.2004) "О персональных данных"',
            'ignore_mail_list'=>'Быть в курсе скидок и новостей'
        ];
    }


    public function register(){
        $user = new User();
        //$user->setAttributes($this->getAttributes());
        $user->email = htmlentities(strip_tags($this->email));
        $user->l_name = htmlentities(strip_tags($this->l_name));
        $user->f_name = htmlentities(strip_tags($this->f_name));
        $user->t_name = htmlentities(strip_tags($this->t_name));
        $user->phone = htmlentities(strip_tags($this->phone));
        $user->region_id = htmlentities(strip_tags($this->region_id));
        $user->city_name = htmlentities(strip_tags($this->city_name));
        $user->tk = htmlentities(strip_tags($this->tk));
        $user->realizations = htmlentities(strip_tags($this->realizations));
        $user->create_time = date('Y-m-d H:i:s');
        if($this->ignore_mail_list == '1'){
            $user->ignore_mail_list = 0;
        }else{
            $user->ignore_mail_list = 1;
        }
        //$user->ignore_mail_list = 0;
        $user->visible = 1;
        $user->pos = 0;
        //vd($user);
        if($user->save()){
            //отправка пользователю и администратору
            $this->sendEmails($user);
            //аутентификация пользователя
            Yii::$app->user->login($user);
            return true;
        }
    }



    /**
     * Отправляет письма о регисрации
     * @param $pass
     * @param User $user
     */
    public function sendEmails(User $user){
        $tpl = MlCfg::findOne(['mnemo'=>'opt_reg_user']);
        $message_user = $this->setUserMessage($user, $tpl);
        $tpl_admin = MlCfg::findOne(['mnemo'=>'opt_reg_admin']);
        $message_admin = $this->setAdminMessage($user, $tpl_admin);

        if($tpl->etype=='1') {
            Yii::$app->mailer->compose()
                ->setFrom([Yii::$app->params['supportEmail'] => 'Отправлено с сайта moda-optom.ru'])
                ->setTo($user->email)
                ->setSubject($tpl->esubj)
                ->setHtmlBody($message_user)
                ->send();
        }

        if($tpl_admin->etype=='1') {
            $model_config = Parametrs::findOne(['id'=>'2']);
            if (isset($model_config)) {
                if ($model_config->value) {
                    $addresses = array_map('trim', explode(',', $model_config->value));
                    if (!empty($settings->mail)) {
                        $addresses = array_merge($addresses, array_map('trim', explode(',', $settings->mail)));
                    }
                    Yii::$app->mailer->compose()
                        ->setFrom([Yii::$app->params['supportEmail'] => 'Отправлено с сайта moda-optom.ru'])
                        ->setTo($addresses)
                        ->setSubject($tpl_admin->esubj)
                        ->setHtmlBody($message_admin)
                        ->send();
                }
            }
        }
    }

    private function setUserMessage($user, $tpl){
        $mailForm = $tpl->ebody_html;
        $message = str_replace([
            '{email}',
            '{l_name}',
            '{f_name}',
            '{phone}',
            '{city_name}',
            '{tk}'
        ],
            [
                $user->email,
                $user->l_name,
                $user->f_name,
                $user->phone,
                $user->city_name,
                $user->tk
            ],
            $mailForm
        );
        return $message;
    }

    private function setAdminMessage($user, $tpl)
    {
        $mailForm = $tpl->ebody_html;
        $message = str_replace([
            '{email}',
            '{l_name}',
            '{f_name}',
            '{phone}',
            '{city_name}',
            '{tk}'
        ],
            [
                $user->email,
                $user->l_name,
                $user->f_name,
                $user->phone,
                $user->city_name,
                $user->tk
            ],
            $mailForm
        );
        return $message;
    }

    public function getToken()
    {
        if(empty($_SESSION['_token_moda'])){
            $_SESSION['_token_moda'] = uniqid('',true);
        }
        return password_hash($_SESSION['_token_moda'], PASSWORD_DEFAULT);
    }

    public function checkToken($token)
    {
        return password_verify($_SESSION['_token_moda'], $token);
    }
}