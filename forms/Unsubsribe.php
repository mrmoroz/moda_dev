<?php

namespace app\forms;

use yii\base\Model;

class Unsubsribe extends Model
{
    public $email;


    public function rules()
    {
        return [
            [['email'], 'required'],
            [['email'], 'email'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => 'Электронная почта',
        ];
    }

}