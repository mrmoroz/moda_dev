<?php

$params = require(__DIR__ . '/params.php');


$config = [
    'id' => 'basic',
    'language' => 'ru-RU',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            'cookieValidationKey' => 'Nfo0AztJNmvwhTNGn8fBtbaJWsxqu-aU',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                //admin module
                '/admin/reset-password' => '/admin/admin/reset-password',
                '/admin/login' => '/admin/admin/login',
                '/admin/logout' => '/admin/admin/logout',
                '/admin/bad-browser' => '/admin/admin/bad-browser',
                '/admin/<controller\w+>/<action>/<id\d+>' => '/admin/<controller>/<action>',
                '/admin/error' => '/admin/admin/error',
                '/cart/<action>' => '/cart/default/<action>',
                '/account/<action>' => '/account/default/<action>',
                '/account/<action>/<id\d+>' => '/account/default/<action>',
                [
                    'class' => '\app\components\url\CatalogUrlRule'
                ],
                '/page/<url>' => '/page/show/',
                '/tag/<url>' => '/tag/show/',
                '/manufacturer/<url>' => '/manufacturer/show/',
                '/search-color/<url>' => '/search-color/show/',
                '/callback/<url>' => '/callback/show/',
                '/registration' => '/site/registration',
                '/login' => '/site/login',
                '/logout' => '/site/logout',
                '/news/<url>' => '/news/show'
            ]
        ],
        'authManager' => [
            'class' => 'app\components\DbRbacManager',
            'cache' => 'cache',   // this enables RBAC caching
            'defaultRoles' => ['User'],
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'fileMap' => [
                        'app' => 'app.php',
                        'app/error' => 'error.php',
                    ],
                ],
                'interface*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'fileMap' => [
                        'interface' => 'interface.php',
                    ],
                ]
            ],
        ],
        'categoryService' => [
            'class' => app\services\CategoryService::class
        ],
        'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => 'himiklab\yii2\recaptcha\ReCaptcha',
            'siteKey' => $params['siteKey'],
            'secret' => $params['secret'],
        ],
    ],
    'modules' => [
        'admin' => [
            'class' => 'app\modules\admin\AdminModule',
            'loginUrl' => '/admin/login',
            'minRequiredRole' => 'Admin'

        ],
        'cart' => [
            'class' => 'app\modules\cart\CartModule',
        ],
        'sberbank' => [
            'class'=>'app\modules\sberbank\SberbankModule',
            'components' => [
                'sberbank' => [
                    'class' => app\modules\sberbank\components\Sberbank::class,

                    // время жизни инвойса в секундах (по умолчанию 20 минут - см. документацию Сбербанка)
                    // в этом примере мы ставим время 1 неделю, т.е. в течение этого времени покупатель может
                    // произвести оплату по выданной ему ссылке
                    'sessionTimeoutSecs' => 60 * 60 * 24 * 7,

                    // логин api мерчанта
                    'login' => 'T5402576989-api',

                    // пароль api мерчанта
                    'password' => 'T5402576989',

                    // использовать тестовый режим (по умолчанию - нет)
                    'testServer' => true,

                    // использовать двухстадийную оплату (по умолчанию - нет)
                    'registerPreAuth' => false
                ],
            ],
            // страница вашего сайта с информацией об успешной оплате
            'successUrl' => '/cart/success',

            // страница вашего сайта с информацией о НЕуспешной оплате
            'failUrl' => '/cart/confirm-fail',

            // обработчик, вызываемый по факту успешной оплаты
            'successCallback' => function($invoice) {
                $handler = new \app\modules\sberbank\components\handlerSuccess($invoice);
                $handler->handle();
            },

            // обработчик, вызываемый по факту НЕуспешной оплаты
            'failCallback' => function($invoice) {

            },
        ],
        'account' => [
            'class' => 'app\modules\account\AccountModule',
        ],
        'debug'=>[
        		'class' => 'yii\debug\Module',
        		'allowedIPs' => ['193.238.176.230','127.0.0.1', '::1']
        ]
    ],
    'as access' => [
        'class' => 'app\components\AccessControl',
        'allowActions' => [
            'site/*',
            'cart/*',
            'catalog/*',
            'manufacturer/*',
            'news/*',
            'page/*',
            'callback/*',
            'tag/*',
            'search-color/*',
            'admin/admin/login',
            'gii/*',
            'debug/*'
            //'admin/*',

        ]
    ],
    'params' => $params,
];

//if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['193.238.176.230','127.0.0.1', '::1']
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
//}

Yii::$container->set('pavlinter\display\DisplayImage', [
    'bgColor' => 'ffffff',
    'config' => [
        'product-image' => [
            'imagesWebDir' => '@web/uploads',
            'imagesDir' => '@webroot/uploads',
            'defaultWebDir' => '@web/uploads',
            'defaultDir' => '@webroot/uploads',
            'defaultImage' => 'default.jpg',
            'mode' => \pavlinter\display\DisplayImage::MODE_STATIC,
        ],
        'all' => [
            'imagesWebDir' => '@web',
            'imagesDir' => '@webroot',
            'defaultWebDir' => '@web/uploads',
            'defaultDir' => '@webroot/uploads',
            'defaultImage' => 'default.jpg',
            'mode' => \pavlinter\display\DisplayImage::MODE_STATIC,
        ]
    ]
]);

return $config;
