<?php

namespace app\services;

use app\models\ProdsCatsParamValue;
use app\models\Product;
use app\models\ProductCategory;
use app\models\ProductSearch;
use Yii;
use yii\base\Exception;
use yii\db\Expression;

class CategoryService
{
    /**
     *
     * Применяет массовые операции к выборке товаров. Если необходим редирект на общую страницу категорий,
     * возвращает true, иначе false
     * @return boolean
     */
    public function applyMultipleOperation($dataProvider, $mode, $multipleOperationParameters)
    {
        $dataProvider->setPagination(false);
        $multipleOperationProducts = $dataProvider->getKeys();

        switch ($mode) {
            case "to-archive" :
                Product::updateAll(['is_archive' => true], ['id' => $multipleOperationProducts]);
                return true;
                break;
            case "set-discount" :
                $discount = $multipleOperationParameters[0];
                if ($discount == 0) {
                    $discountPrice = 0;
                } else {
                    $discountPrice = new Expression('price - round('. $discount .' * price / 100)');
                }

                Product::updateAll([
                    'discount_price' => $discountPrice,
                    'discount' => $discount
                ], ['id' => $multipleOperationProducts]);
                break;
            case "change-discount" :
                $discount = $multipleOperationParameters[0];
                if ($discount > 0) {
                    Product::updateAll([
                        'discount' => new Expression('discount + ' . $discount),
                        'discount_price' => new Expression('price - round((discount + '. $discount . ') * price / 100)'),
                    ], ['id' => $multipleOperationProducts]);
                }
                break;
            case "set-wholesale-discount" :
                $wholesaleDiscount = $multipleOperationParameters[0];
                if ($wholesaleDiscount == 0) {
                    $wholesaleDiscountPrice = 0;
                } else {
                    $wholesaleDiscountPrice = new Expression('wholesale_price - round(' . $wholesaleDiscount . ' * wholesale_price / 100)');
                }

                Product::updateAll([
                    'wholesale_discount_price' => $wholesaleDiscountPrice,
                    'wholesale_discount' => $wholesaleDiscount
                ], ['id' => $multipleOperationProducts]);
                break;
            case "change-wholesale-discount" :
                $wholesaleDiscount = $multipleOperationParameters[0];
                if ($wholesaleDiscount > 0) {
                    Product::updateAll([
                        'wholesale_discount' => new Expression('wholesale_discount + ' . $wholesaleDiscount),
                        'wholesale_discount_price' => new Expression('wholesale_price - round((wholesale_discount + '. $wholesaleDiscount . ') * wholesale_price / 100)'),
                    ], ['id' => $multipleOperationProducts]);
                }
                break;
            case "set-tag" :
                $tag = $multipleOperationParameters[0];
                foreach ($multipleOperationProducts as $multipleOperationProduct) {
                    $prodsCatsParamsValuesArray[] = [
                        'prod_id' => $multipleOperationProduct,
                        'param_id' => 9,
                        'value' => $tag
                    ];
                }
                if (!empty($prodsCatsParamsValuesArray)) {
                    Yii::$app->db->createCommand()->batchInsert(
                        ProdsCatsParamValue::tableName(),
                        ['prod_id', 'param_id', 'value'],
                        $prodsCatsParamsValuesArray)->execute();
                }
                break;
            case "set-gift" :
                $gift = $multipleOperationParameters[0];
                ProdsCatsParamValue::deleteAll([
                    'prod' => $multipleOperationProducts,
                    'param_id' => 21
                ]);
                foreach ($multipleOperationProducts as $multipleOperationProduct) {
                    $prodsCatsParamsValuesArray[] = [
                        'prod_id' => $multipleOperationProduct,
                        'param_id' => 21,
                        'value' => $gift
                    ];
                }
                Yii::$app->db->createCommand()->batchInsert(
                    ProdsCatsParamValue::tableName(),
                    ['prod_id', 'param_id', 'value'],
                    $prodsCatsParamsValuesArray)->execute();
                break;
            case "delete-gift" :
                ProdsCatsParamValue::deleteAll([
                    'prod' => $multipleOperationProducts,
                    'param_id' => 21
                ]);
                break;
        }

        return false;
    }

    public function getTypeByUrl($url)
    {
        $typeUrls = array_flip(ProductCategory::$categoryTypeUrls);
        if(isset($typeUrls[$url])){
            return $typeUrls[$url];
        }
        return null;
    }

    public function getMenuCategoriesTree()
    {
        $categories = ProductCategory::find()->where(['parent_id' => null])
                    ->orderBy(['type' => SORT_ASC, 'position' => SORT_ASC])->all();
        $tree = $this->processForAdminMenu($categories);

        return $tree;
    }

    public function getCategoriesTree($activeOnly = true, $forDropDown = false, $type = null)
    {
        $categoryQuery = ProductCategory::find()
            ->orderBy(['type' => SORT_ASC, 'parent_id' => SORT_ASC, 'position' => SORT_ASC]);
        if ($activeOnly) {
            $categoryQuery->where('is_active=true');
        }
        if ($type) {
            $categoryQuery->andWhere('type=:type', ['type' => $type]);
        }
        /**
         * @var ProductCategory[] $categories
         */
        $categories = $categoryQuery->all();
        $tree = $this->processForFullList($categories);

        return ($forDropDown) ? $this->processForDropDown($tree) : $tree;
    }

    /**
     * @return ProductCategory[]
     */
    public function getWomanCategories()
    {
        return ProductCategory::find()
            ->where('type=:type', ['type' => ProductCategory::TYPE_WOMAN])
            ->andWhere('parent_id IS null')
            ->andWhere(['is_active' => true])
            ->orderBy(['position' => SORT_ASC])
            ->all();
    }

    public function getDimensionlessWomanCategories()
    {
        return ProductCategory::find()
            ->where('type=:type', ['type' => ProductCategory::TYPE_WOMAN])
            ->andWhere('parent_id IS null')
            ->andWhere(['is_active' => true])
            ->andWhere(['size_chart_id' => '1'])
            ->orderBy(['position' => SORT_ASC])
            ->all();
    }

    public function getBigWomanCategories()
    {
        return ProductCategory::findBySql("SELECT * FROM product_categories"
                              . " WHERE parent_id IS NULL AND type=1 AND is_active='t'"
                              . " AND id IN (SELECT category_id FROM category_sizes WHERE type ='big' GROUP BY category_id)"
                              . " AND size_chart_id != 1"
                              . " ORDER BY position")->all();
    }

    public function getStandardWomanCategories()
    {
        return ProductCategory::findBySql("SELECT * FROM product_categories"
            . " WHERE parent_id IS NULL AND type=1 AND is_active='t'"
            . " AND id IN (SELECT category_id FROM category_sizes WHERE type ='std' GROUP BY category_id)"
            . " AND size_chart_id != 1"
            . " ORDER BY position")->all();

    }

    public function getManCategories()
    {
        return ProductCategory::find()
            ->where('type=:type', ['type' => ProductCategory::TYPE_MAN])
            ->andWhere('parent_id IS null')
            ->andWhere(['is_active' => true])
            ->orderBy(['position' => SORT_ASC])
            ->all();
    }

    public function getChildrenCategories()
    {
        return ProductCategory::find()
            ->where('type=:type', ['type' => ProductCategory::TYPE_CHILDREN])
            ->andWhere('parent_id IS null')
            ->andWhere(['is_active' => true])
            ->orderBy(['position' => SORT_ASC])
            ->all();
    }

    public function getHomeCategories()
    {
        return ProductCategory::find()
            ->where('type=:type', ['type' => ProductCategory::TYPE_HOME])
            ->andWhere('parent_id IS null')
            ->andWhere(['is_active' => true])
            ->orderBy(['position' => SORT_ASC])
            ->all();
    }

    /**
     * @param array $tree
     * @return array
     */
    private function processForDropDown($tree)
    {
        $result = [];
        foreach (ProductCategory::$typesLabes as $labelID => $label) {
            $result[$label] = [];
        }
        foreach ($tree as $typeId => $treeNodes) {
            foreach ($treeNodes as $treeNode) {
                if (isset($treeNode['info']->id)) {
                    $result[ProductCategory::$typesLabes[$typeId]][$treeNode['info']->id] = $treeNode['info']->title;
                }
                if (isset($treeNode['childs'])) {
                    foreach ((array)$treeNode['childs'] as $treeNodeChild) {
                        $result[ProductCategory::$typesLabes[$typeId]][$treeNodeChild->id] = '---' . $treeNodeChild->title;
                    }
                }
            }
        }
        return $result;
    }

    /**
     * @param ProductCategory[] $categories
     * @return array
     */
    private function processForFullList($categories)
    {
        $tree = [];
        foreach (ProductCategory::$typesLabes as $labelID => $label) {
            $tree[$labelID] = [];
        }
        foreach ($categories as $category) {
            if ($category->parent_id == NULL) {
                $tree[$category->type][$category->id]['info'] = $category;
            } else {
                $tree[$category->type][$category->parent_id]['childs'][] = $category;
            }
        }

        return $tree;
    }

    private function getChildrenMenuCategories(ProductCategory $category)
    {
        $newCategory = $this->createAdminCategoryMenuItem($category);

        $childCategories = array();
        foreach($category->getProductCategories()->all() as $childCategory) {
            $newChildCategory = $this->getChildrenMenuCategories($childCategory);
            array_push($childCategories, $newChildCategory);
        }

        $newCategory['children'] = $childCategories;

        return $newCategory;
    }

    /**
     * @param ProductCategory[] $categories
     * @return array
     */
    private function processForAdminMenu($categories)
    {
        $tree = [];
        foreach (ProductCategory::$typesLabes as $labelID => $label) {
            $tree[$labelID] = [];
        }

        $rootCategories = array();

        foreach ($categories as $category) {
            if ($category->parent_id == null) {
                array_push($rootCategories, $category);
            }
        }

        foreach ($rootCategories as $rootCategory) {
            $newCategory = $this->createAdminCategoryMenuItem($rootCategory, true);
            $childCategories = array();
            foreach($rootCategory->getProductCategories()->all() as $childCategory) {
                $newChildCategory = $this->getChildrenMenuCategories($childCategory);
                array_push($childCategories, $newChildCategory);
            }

            $newCategory['children'] = $childCategories;

            $tree[$rootCategory->type][] = $newCategory;
        }

        return $tree;
    }

    private function createAdminCategoryMenuItem(ProductCategory $category, $isRoot = false)
    {
        if ($isRoot) {
            $newCategory['title'] = '<a class="parent-category';
        } else {
            $newCategory['title'] = '<a class="child-category';
        }
        if (!$category->is_active) {
            $newCategory['title'] .= ' non-active';
            $newCategory['icon'] = 'fa fa-circle-o';
        } else {
            $newCategory['icon'] = 'fa fa-circle';
        }
        if (Yii::$app->request->get('id') == $category->id) {
            $newCategory['title'] .= ' active';
        }

        $newCategory['title'] .= '" href="/admin/catalog/categories/' . $category->id . '"';
        $newCategory['title'] .= '>' . $category->title . '</a>';
        $newCategory['title'] .= ' <a href="/admin/catalog/create-category/' . $category->id . '" '
            . 'title="Редактировать категорию ' . $category->title . '">'
            . '<i class="fa fa-pencil hidden"></i></a>';
        $newCategory['key'] = $category->id;

        return $newCategory;
    }

    /**
     * Сохранение значения фильтров каталога в сессию
     */
    public function setFiltersIntoSession(ProductSearch $productSearch)
    {
        Yii::$app->session->set('manufacturer', $productSearch->manufacturer);
        Yii::$app->session->set('tag', $productSearch->tag);
        Yii::$app->session->set('categoryTag', $productSearch->categoryTag);
        Yii::$app->session->set('checkedSizes', $productSearch->checkedSizes);
        Yii::$app->session->set('article', $productSearch->article);
        Yii::$app->session->set('searchCurrentCategory', $productSearch->searchCurrentCategory);
        Yii::$app->session->set('sizeMode', $productSearch->sizeMode);
        Yii::$app->session->set('additionalSizeModes', $productSearch->additionalSizeModes);
    }

    /**
     * Получение значений фильтров каталога из сессии
     */
    public function getFiltersFromSession(ProductSearch $productSearch)
    {
        $productSearch->manufacturer = Yii::$app->session->get('manufacturer');
        $productSearch->tag = Yii::$app->session->get('tag');
        $productSearch->categoryTag = Yii::$app->session->get('categoryTag');
        $productSearch->checkedSizes = Yii::$app->session->get('checkedSizes');
        $productSearch->article = Yii::$app->session->get('article');
        $productSearch->searchCurrentCategory = Yii::$app->session->get('searchCurrentCategory');
        $productSearch->sizeMode = Yii::$app->session->get('sizeMode');
        $productSearch->additionalSizeModes = Yii::$app->session->get('additionalSizeModes');
    }

    /**
     * Возвращает список формул для расчета рейтинга товаров
     */
    public function getRatingFormulas()
    {
        $strings = file($_SERVER['DOCUMENT_ROOT'] . '/ratings.txt', FILE_IGNORE_NEW_LINES);

        $formulas = array();

        foreach ($strings as $string) {
            $variableAndFormula = explode("=", $string);
            $variable = trim($variableAndFormula[0]);

            $formulas[$variable] = $string;
        }

        return $formulas;
    }
}