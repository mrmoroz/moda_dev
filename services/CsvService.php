<?php

namespace app\services;

class CsvService
{
    public static $REALIZACIONS = [
        0 => '---',
        1 => 'Опт',
        2 => 'СП',
        3 => 'Розница',
        4 => 'Интернет магазин',
    ];
    public static $IGNORE_MAIL = [
        0 => 'Нет',
        1 => 'Да',
    ];
    public function reindexDataUsers(Array $data): Array
    {
        $dataCsv[] = ['Фамилия','Имя', 'Отчество', 'Телефон', 'E-mail', 'Способ реализации', 'Отписан от рассылки'];
        if($data) {
            foreach ($data as $item) {
                $realizations = isset(self::$REALIZACIONS[$item['realizations']]) ? self::$REALIZACIONS[$item['realizations']] : '---';
                $dataCsv[] = [
                    $item['l_name'],
                    $item['f_name'],
                    $item['t_name'] ? $item['t_name'] : '---',
                    $item['phone'] ? $item['phone'] : '---',
                    $item['email'],
                    $realizations,
                    self::$IGNORE_MAIL[$item['ignore_mail_list']] ? self::$IGNORE_MAIL[$item['ignore_mail_list']]  : '---'
                ];
            }
        }
        return $dataCsv;
    }

    public function outCSV(Array $data, String $filename=null): void
    {
        header("Content-Type: text/csv; charset=windows-1251");
        if($filename){
            header("Content-Disposition: attachment; filename={$filename}");
        }else{
            header("Content-Disposition: attachment; filename=file.csv");
        }
        header("Cache-Control: no-cache, no-store, must-revalidate");
        header("Pragma: no-cache");
        header("Expires: 0");

        $output = fopen("php://output", "w");
        foreach ($data as $row) {
            foreach ($row as $i=>$cellData){
                $row[$i] = mb_convert_encoding($cellData, 'windows-1251', 'utf-8');
            }
            fputcsv($output, $row,';');
        }
        fclose($output);
    }
}