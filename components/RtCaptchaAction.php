<?php
/**
 * Created by PhpStorm.
 * User: shabanov
 * Date: 21.08.2016
 * Time: 15:43
 */

namespace app\components;


use yii\captcha\CaptchaAction;

class RtCaptchaAction extends CaptchaAction
{
    public $fontFile = "@webroot/assets/ArchivoBlack.ttf";

    /**
     * Generates a new verification code.
     * @return string the generated verification code
     */
    protected function generateVerifyCode()
    {
        if ($this->minLength > $this->maxLength) {
            $this->maxLength = $this->minLength;
        }
        if ($this->minLength < 3) {
            $this->minLength = 3;
        }
        if ($this->maxLength > 20) {
            $this->maxLength = 20;
        }
        $length = mt_rand($this->minLength, $this->maxLength);

        $letters = '79135679135679135679';
        $vowels = '2480';
        $code = '';
        for ($i = 0; $i < $length; ++$i) {
            if ($i % 2 && mt_rand(0, 10) > 2 || !($i % 2) && mt_rand(0, 10) > 9) {
                $code .= $vowels[mt_rand(0, 4)];
            } else {
                $code .= $letters[mt_rand(0, 20)];
            }
        }

        return $code;
    }
}