<?php

namespace app\components\pagination;

use app\models\Manufacturer;
use app\models\ProductSearch;
use app\models\Tag;
use yii\helpers\Html;
use yii\widgets\LinkPager;

class CatalogLinkPager extends LinkPager
{
    public $options = ['class' => 'catalog-pagination'];

    public $prevPageLabel = '<span class="page-catalog-pager prev-enable"></span>';
    public $nextPageLabel = "<span class='page-catalog-pager next-enable'></span>";

    public $maxButtonCount = 7;

    public function renderPageButtons()
    {
        $pageCount = $this->pagination->getPageCount();
        if ($pageCount < 2 && $this->hideOnSinglePage) {
            return '';
        }

        $buttons = [];
        $currentPage = $this->pagination->getPage();



        $buttons[] = Html::tag('li', sprintf('Страница %d из %d | ', $this->pagination->getPage()+1, $this->pagination->getPageCount()), [
            'class' => 'grey-pager'
        ]);

        // prev page
        if ($this->prevPageLabel !== false) {
            if (($page = $currentPage - 1) < 0) {
                $page = 0;
                $this->prevPageLabel = '<span class="page-catalog-pager prev-disable"></span>';
            }
            $buttons[] = $this->renderPageButton($this->prevPageLabel, $page, $this->prevPageCssClass, $currentPage <= 0, false);
        }

        // next page
        if ($this->nextPageLabel !== false) {
            if (($page = $currentPage + 1) >= $pageCount) {
                $page = $pageCount - 1;
                $this->nextPageLabel = '<span class="page-catalog-pager next-disable"></span>';
            }
            $buttons[] = $this->renderPageButton($this->nextPageLabel, $page, $this->nextPageCssClass, $currentPage >= $pageCount - 1, false);
        }

        if ($this->pagination->getPageCount() > 1) {
            $buttons[] = Html::tag('li', sprintf('Показать все'), ['class' => 'show-all-mode']);
        }

        return Html::tag('ul', implode("\n", $buttons), $this->options);
    }

    public function renderPageButton($label, $page, $class, $disabled, $active)
    {
        $options = ['class' => empty($class) ? $this->pageCssClass : $class];
        if ($active) {
            Html::addCssClass($options, $this->activePageCssClass);
        }
        if ($disabled) {
            Html::addCssClass($options, $this->disabledPageCssClass);

            return Html::tag('li', Html::tag('span', $label), $options);
        }
        $linkOptions = $this->linkOptions;
        $linkOptions['data-page'] = $page;

        if (\Yii::$app->request->get('sizeType')) {
            /* @var $category \app\models\ProductCategory */
            $category = \Yii::$app->request->get('category');
            $page +=1 ;

            $sizeType = \Yii::$app->request->get('sizeType');
            if(!empty($category)) {
                if ($sizeType == 'bolshie-razmery') {
                    $url = $category->getBigUrl();
                } elseif ($sizeType == 'bazovie-razmery') {
                    $url = $category->getStandardUrl();
                } else {
                    $url = $category->getUrl();
                }
            }else{
                $url = '/'.\Yii::$app->request->getPathInfo();
            }

            $url .= '?sizeType=' . \Yii::$app->request->get('sizeType')
                 . '&page=' . $page . '&per-page=' . $this->pagination->getPageSize();
        } elseif (\Yii::$app->request->get('tag')) {
            /** @var Tag $tag */
            $tag = \Yii::$app->request->get('tag');
            $page +=1 ;

            $url = $tag->getUrl();

            $url .= '?page=' . $page . '&per-page=' . $this->pagination->getPageSize();
        } elseif (\Yii::$app->request->get('manufacturer')) {
            /** @var Manufacturer $manufacturer */
            $manufacturer = \Yii::$app->request->get('manufacturer');
            $page +=1 ;

            $url = $manufacturer->getUrl();

            $url .= '?page=' . $page . '&per-page=' . $this->pagination->getPageSize();
        } else {
            $url = $this->pagination->createUrl($page);
        }

        $productSearch = new ProductSearch();
        $productSearch->load(\Yii::$app->request->get());
        if ($productSearch->checkCatalogFilters()) {
            $url .= $productSearch->createGetParamsForCatalogFilters();
        }

        return Html::tag('li', Html::a($label, $url, $linkOptions), $options);
    }


}