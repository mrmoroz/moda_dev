<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 17.04.17
 * Time: 10:20
 */

namespace app\components;

use app\components\MTSSmsProvider;
use Yii;
use app\models\UserProfile;
use app\models\UserAddress;
use yii\base\Object;
use app\models\CabEmail;
use app\modules\admin\models\SmsLog;
use app\modules\admin\models\SmsTemplates;
use app\modules\admin\models\SmsHistory;



class SMSMessage extends Object
{
    private $provider;

    public function __construct($provider = null, $config = [])
    {
        if($provider){
            $this->provider = new $provider;
        } else{
            $this->provider = new MTSSmsProvider();
        }
        parent::__construct($config);
    }

    public function send($number, $text = '', $userId = null)
    {
        if(!$userId) {
            $checkMobilePhone = substr(str_replace(array('-', ' '), '', $number), 1);

            $checkUserSQL = "SELECT * FROM user_profile WHERE REPLACE(replace(mobile_phone, '-', ''),' ','') LIKE '%{$checkMobilePhone}%' LIMIT 1";

            $checkUser = Yii::$app->db->createCommand($checkUserSQL)->queryOne();
        }

        if(!empty($checkUser)) {
            $userId = $checkUser['user_id'];
        }


        if($userId){
            $cabModel = new CabEmail();
            $cabModel->user_id = $userId;
            $cabModel->new_msg = 1;
            $cabModel->message = $text;
            $cabModel->cdate = date("Y-m-d H:i:s");
            $cabModel->pos = 0;
            $cabModel->visible = 1;
            $cabModel->sys_language = 1;
            $cabModel->save(false);
        }


        $logModel = new SmsLog();
        $logModel->number = $number;
        $logModel->text = $text;
        $logModel->send_date = date("Y-m-d H:i:s");
        $logModel->save(false);

        try {
            $this->provider->sendMessage($number, $text);
        } catch (Exception $e) {
            return -1;
        }

    }

    public function sendSMS($type, $user, $order = null, $sale = null, $return = false)
    {
        if(isset($type) && !empty($type)){
            $model = SmsTemplates::findOne(['action'=>$type,'visible'=>'1']);
            if($model){
                $text = $this->templateParsing($model->text, $user, $order, $sale);
                $this->saveToHistory($user, $text);
            }
        }

        return false;
    }

    public function templateParsing($text, $user, $order, $sale)
    {
        if ($order && $order->manager) {
            $manager = UserProfile::findOne(['user_id'=>$order->manager]);
        }

        if(is_array($sale) && count($sale)>0) {
            $post_id = $sale['id_post'];
            $text = str_replace('{post_id}', $post_id, $text);
            //$waint_time = $sale['waint_time'];
        }

        $text = str_replace('{n_order}', $order->id, $text);
        $text = str_replace('{name}', $user->first_name, $text);
        $text = str_replace('{surname}', $user->last_name, $text);
        if(isset($manager)) {
            $text = str_replace('{name_cons}', $manager->first_name, $text);
            $text = str_replace('{fname_cons}', $manager->last_name, $text);
        }
        /*if($sale){
            $text = str_replace('{post_id}', $post_id, $text);
            //$text = str_replace('{date_arrival}', $waint_time, $text);
        }*/


        return $text;
    }

    private function saveToHistory($user, $text)
    {
        $region = UserAddress::findOne(['user_id'=>$user->user_id,'default'=>true]);
        $number = $user->mobile_phone;
        $model = new SmsHistory();
        $model->number = $number;
        $model->text = $text;
        $model->region = (isset($region->region_id)?$region->region_id:0);
        $model->user_id = $user->user_id;
        $model->save(false);
    }
}