<?php
namespace app\components\url;

use app\models\Manufacturer;
use app\models\Product;
use app\models\ProductCategory;
use app\models\Tag;
use yii\base\Object;
use yii\web\UrlRuleInterface;

class CatalogUrlRule extends Object implements UrlRuleInterface
{

    public function createUrl($manager, $route, $params)
    {
        if ($route === 'catalog/category') {
            $url = '';
            if(isset($params['category'])){
                /** @var ProductCategory $category */
                $category = $params['category'];
                $url.= ProductCategory::$categoryTypeUrls[$category->type];
                $url.='/'.ProductCategory::$categorySizeUrls[ProductCategory::SIZE_ALL];
                $url.='/'.$category->url;
                unset($params['category']);
            }
            if(isset($params['subCategory'])){
                /** @var ProductCategory $subCategory */
                $subCategory = $params['subCategory'];
                $url.='/'.$subCategory->url;
                unset($params['subCategory']);
            }
            return $url . (($params) ? '?'.http_build_query($params) : '');
        }
        return false;  // данное правило не применимо
    }

    public function parseRequest($manager, $request)
    {
        $params = [];
        $pathInfo = explode('/', $request->getPathInfo());


        if($pathInfo[0]=='novinki' && isset($pathInfo[1]) && !isset($pathInfo[2])){
            $params['categoryType'] = $pathInfo[1];
            $params['currentSizesUrl'] = '/'.$pathInfo[0].'/'.$pathInfo[1];
            //$params['categoryParentType'] = $pathInfo[1];
            if($pathInfo[1]=='zhenskaja'){
                $params['sizeType'] = 'bazovie-razmery';
            }elseif ($pathInfo[1]=='zhenskaja-bolshie-razmery'){
                $params['sizeType'] = 'bolshie-razmery';
            }else{
                $params['sizeType'] = 'vse-razmery';
            }
            return ['/catalog/razdel', $params];
        }

        $checkType = in_array($pathInfo[0], ProductCategory::$categoryTypeUrls);
        if (isset($pathInfo[1])) {
            $checkSize = in_array($pathInfo[1], ProductCategory::$categorySizeUrls);
        } else {
            $checkSize = false;
        }
        if ($checkType && $checkSize && isset($pathInfo[2])) {

            $category = ProductCategory::find()
                ->where(['type' => array_search($pathInfo[0], ProductCategory::$categoryTypeUrls)])
                ->andWhere(['url' => $pathInfo[2]])
                ->andWhere(['is_active' => true])
                ->one();
            $params['sizeType'] = $pathInfo[1];
            if (!$category) {
                return false;
            }
            $params['category'] = $category;
            if (isset($pathInfo[4])) {
                //по урл - точно товар

                $product = Product::find()->with(['category'])->where(['is_active' => 1, 'is_archive'=>false, 'url' => $pathInfo[4]])->one();
                if (!$product || !$product->category) {
                    return false;
                }
                if ($product->category->url == $pathInfo[3] && $product->category->parent_id == $category->id) {
                    $pageType = 'category';
                    $entityId = $pathInfo[2];
                    $sizeType = $pathInfo[1];

                    $params['pageType'] = $pageType;
                    $params['entityId'] = $entityId;
                    $params['sizeType'] = $sizeType;
                    $params['product'] = $product;
                    return ['/catalog/product', $params];
                }
                return false;
            }
            if (isset($pathInfo[3])) {
                //либо подкатегория, либо товар

                $product = Product::find()->with(['category'])->where(['is_active' => 1, 'is_archive'=>false, 'url' => $pathInfo[3]])->one();
                if ($product && $product->category && $product->category->is_active && $product->is_wholesale_active = true
                    && ($product->category->id == $category->id || ($category->checkChildCategories($product->category_id)))) {

                    $pageType = 'category';
                    $sizeType = $pathInfo[1];
                    $entityId = $pathInfo[2];

                    $params['product'] = $product;
                    $params['pageType'] = $pageType;
                    $params['entityId'] = $entityId;
                    $params['sizeType'] = $sizeType;

                    return ['/catalog/product', $params];
                } else {

                    $subCategory = ProductCategory::find()
                        ->where(['type' => array_search($pathInfo[0], ProductCategory::$categoryTypeUrls)])
                        ->andWhere(['url' => $pathInfo[3]])
                        ->andWhere(['is_active' => true])
                        ->with(['parent'])
                        ->one();
                    if ($subCategory) {
                        $parentCategory = $subCategory->parent;
                    }
                    if ($subCategory && $parentCategory && $parentCategory->is_active && $parentCategory->id == $category->id) {
                        $params['subCategory'] = $subCategory;
                        return ['/catalog/category', $params];
                    }
                    return false;
                }
            }

            return ['/catalog/category', $params];
        } elseif (isset($pathInfo[0]) && $pathInfo[0] == 'catalog' && isset($pathInfo[1]) && $pathInfo[1] == 'collection') {
            if (isset($pathInfo[2])) {
                if (isset($pathInfo[3])) {
                    $product = Product::find()->where(['is_active' => 1, 'is_archive'=>false, 'url' => $pathInfo[3]])->one();
                    if ($product && $product->category->is_active && $product->is_wholesale_active = true) {

                        $sizeType = '';
                        $pageType = 'collection';
                        $entityId = $pathInfo[2];

                        $params['product'] = $product;
                        $params['pageType'] = $pageType;
                        $params['entityId'] = $entityId;
                        $params['sizeType'] = $sizeType;

                        return ['/catalog/product', $params];
                    } else {
                        return false;
                    }
                } else {
                    $manufacturerId = $pathInfo[2];
                    $manufacturer = Manufacturer::findOne($manufacturerId);
                    if (!empty($manufacturer)) {
                        $params['manufacturer'] = $manufacturer;

                        return ['/catalog/collection', $params];
                    } else {
                        return false;
                    }
                }
            } else {
                return false;
            }
        } elseif (isset($pathInfo[0]) && $pathInfo[0] == 'catalog' && isset($pathInfo[1]) && $pathInfo[1] == 'tag') {
            if (isset($pathInfo[2])) {
                if (isset($pathInfo[3])) {
                    $product = Product::find()->where(['is_active' => 1,'is_archive'=>false, 'url' => $pathInfo[3]])->one();
                    if ($product && $product->category->is_active && $product->is_wholesale_active = true) {

                        $sizeType = '';
                        $pageType = 'tag';
                        $entityId = $pathInfo[2];

                        $params['product'] = $product;
                        $params['pageType'] = $pageType;
                        $params['entityId'] = $entityId;
                        $params['sizeType'] = $sizeType;

                        return ['/catalog/product', $params];
                    } else {
                        return false;
                    }
                } else {
                    $tagId = $pathInfo[2];
                    $tag = Tag::findOne($tagId);
                    if (!empty($tag)) {
                        $params['tag'] = $tag;

                        return ['/catalog/tag', $params];
                    } else {
                        return false;
                    }
                }
            } else {
                return false;
            }
        }elseif ($pathInfo[0]=='novinki' && isset($pathInfo[2])) {
            $product = Product::find()->with(['category'])->where(['is_wholesale_active' => true, 'is_active' => true, 'is_new' => true, 'is_archive' => false, 'url' => $pathInfo[2]])->one();
            if ($product && $product->category && $product->category->is_active) {

                $pageType = 'category';
                if($pathInfo[1]=='zhenskaja'){
                    $params['sizeType'] = 'bazovie-razmery';
                }elseif ($pathInfo[1]=='zhenskaja-bolshie-razmery'){
                    $params['sizeType'] = 'bolshie-razmery';
                }else{
                    $params['sizeType'] = 'vse-razmery';
                }


                $category = ProductCategory::find()
                    ->where(['id' => $product->category_id])
                    ->andWhere(['is_active' => true])
                    ->one();
                if($category){
                    $params['product'] = $product;
                    $params['pageType'] = $pageType;
                    $params['entityId'] = $category->url;
                    $params['section'] = 'novinki';
                    $params['currentSizesUrl'] = '/'.$pathInfo[0].'/'.$pathInfo[1];

                    return ['/catalog/product', $params];
                }
            }
        }

        return false;
    }

}