<?php

namespace app\components;



class Ekam
{
    private $_token = "96f99835fe1d976807b81094a4db45d858cf0da0f191647f9786fbf5f465";

    public function sendReq( $order, $email, $phone_number, $lines )
    {
        $items = [];
        foreach ($lines as $line){
            $price = (!empty((int)$line->new_price) ? (int)$line->new_price : (int)$line->price);

            $items[] = [
                'price'=> $price,
                'quantity'=>$line->qty,
                'title'=>$line->name,
                'total_price'=> $price * $line->qty,
                'vat_rate'=>null, //ставка НДС: null – без НДС, 0 – 0%, 10 – 10%, 20 – 20%.
                'fiscal_product_type'=> '',
                'payment_case'=> ''
            ];
        }

        /*
         * Важно: сумма полей total_price по всем позициям чека должна равняться
         * сумме cash_amount + electron_amount. Иначе – ошибка 422.
         */

        $data = [
            'order_id'=>$order->id,
            'order_number'=>$order->id,
            'type'=>"SaleReceiptRequest", //тип чека: 'sale' – чек прихода, 'return' – чек возврата прихода.
            'email'=>$email,
            'phone_number'=>$phone_number,
            'should_print'=>true, //печатать (true) или не печатать (false) бумажный чек (по умолчанию – false).
            'cash_amount'=> 0,// итого оплачено наличными.
            'electron_amount'=>(int)$order->total_cost,//итого оплачено электронно.
            'prepaid_amount'=>0, //итого оплачено предоплатой.
            'postpaid_amount'=>0, // итого оплачено постоплатой.
            'counter_offer_amount'=>0, //итого оплачено встречным представлением.
            'cashier_name'=>"moda-optom.ru", //— имя кассира.
            'draft'=>false, //черновик (true) или чек для оформления (false).
            'lines'=> $items
        ];

        //vd($data);

        $data = json_encode($data);

        $out = '';
        if( $curl = curl_init() ) {
            curl_setopt($curl, CURLOPT_URL, 'https://app.ekam.ru/api/online/v2/receipt_requests');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data),
                    'X-Access-Token: ' . $this->_token,
                    'Accept: application/json'
                )
            );
            $out = curl_exec($curl);
            curl_close($curl);
        }
        return $out;
    }

    
}