<?php

namespace app\components;

use yii\web;
use yii\web\User;

/**
 * Class WebUser
 * @package app\modules\user\components\user
 * @property \app\models\User identity
 */
class WebUser extends User
{
    //Base roles
    const ROLE_GUEST = 'Guest';
    const ROLE_USER = 'User';
    const ROLE_ADMIN = 'Admin';
    const ROLE_SUPERADMIN = 'Superadmin';

    protected $_userRoles;

    public static function baseRoles()
    {
        return [
            self::ROLE_GUEST,
            self::ROLE_USER,
            self::ROLE_ADMIN,
            self::ROLE_SUPERADMIN,
        ];
    }

    /**
     * Check assign users to current role (exact matching)
     * For check permissions with parent/child role relations use \Yii::$app->user->can($roleName)
     * @param string $roleName
     * @return bool
     */
    public function checkRole($roleName)
    {
        $userRoles = $this->getUserRoleNamesList();
        return in_array($roleName, $userRoles);

    }

    /**
     * Return list of user`s roles
     * @return \yii\rbac\Role[]
     */
    public function getUserRoles()
    {
        if (empty($this->_userRoles)) {
            $this->_userRoles = \Yii::$app->authManager->getRolesByUser($this->getId());
        }
        return $this->_userRoles;
    }

    /**
     * Return only names of user`s roles (GUI used)
     * @param bool $needSort
     * @return array
     */
    public function getUserRoleNamesList($needSort = false)
    {
        $userRoles = $this->identity->getRolesNamesList();
        if ($needSort) {
            sort($userRoles);
        }

        return $userRoles;
    }
}