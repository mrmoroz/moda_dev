<?php
namespace app\components\widgets;
use yii\base\Widget;
use Yii;

class ShowLoadingWidgets extends Widget
{
    public $loadingType = 1;
    public $path;
    public function init()
    {
        parent::init();
        $this->path = Yii::getAlias('@app');
        if ($this->loadingType == 1) {

            $css = <<<CSS
                 .loading-indicator {
                    height: 80px;
                    width: 80px;
                    background: url( '/img/loading1.gif' );
                    background-repeat: no-repeat;
                    background-position: center center;
                }
                .loading-indicator-overlay {
                    background-color: #FFFFFF;
                    opacity: 0.6;
                    filter: alpha(opacity = 60);
                }               
CSS;
            Yii::$app->view->registerCss($css);
        }
    }
}