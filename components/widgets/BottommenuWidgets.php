<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 09.12.2020
 * Time: 11:56
 */

namespace app\components\widgets;


use yii\base\Widget;

class BottommenuWidgets extends Widget
{
    public $menu;

    public function init(){
        parent::init();
    }

    public function run(){
        return $this->render('bottommenu',[
            'menu'=>$this->menu
        ]);
    }
}