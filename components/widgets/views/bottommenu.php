<?php $children = $menu->children; ?>
<?php if (isset($children)) : ?>
    <p class="links-footer-title"><?=$menu->menuItem->title?></p>
    <ul>
        <?php foreach ($children as $child) : ?>
            <?php $url = $child->getUrl(); ?>
            <?php if ($url) : ?>
                <li>
                    <a href="<?= $url ?>"><?= $child->menuItem->title ?></a>
                </li>
            <?php else:?>
                <li>
                    <a href="/"><?= $child->menuItem->title ?></a>
                </li>
            <? endif; ?>
        <? endforeach; ?>
    </ul>
<? endif; ?>