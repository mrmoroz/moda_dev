<?php

namespace app\components\resize\geometry;

use yii\base\Exception;

/*class FileNotFoundException extends Exception {

	public function __construct() {
		$this->message = 'File not found';
	}
}*/

/*class InvalidFileException extends Exception {

	public function __construct($path) {
		$this->message = 'Invalid file: '.$path;
	}
}*/

class InvalidChannelException extends Exception {

	public function __construct($channelName) {
		$this->message = 'Invalid channel: '.$channelName;
	}
}

class UnsupportedFormatException extends Exception {

	public function __construct($format = '') {
		if (!empty($format)) {
			$message = 'This image format ('.$format.') is not supported by your version of GD library';
		}
		else {
			$message = 'This image format is not supported by your version of GD library';
		}
		$this->message = $message;
	}
}

class GDnotInstalledException extends Exception {

	public function __construct() {
		$this->message = 'The GD library is not installed';
	}
}

class FileAlreadyExistsException extends Exception {

	public function __construct($path) {
		$this->message = 'File: '.$path.' is already exists!';
	}
}

class FileNotSaveException extends Exception {

	public function __construct($path) {
		$this->message = 'File: '.$path.' not saved';
	}
}

class IllegalArgumentException extends Exception {

	public function __construct() {
		$this->message = 'Illegal argument';
	}
}