<?php

namespace app\components\resize;

use app\components\resize\geometry\UnsupportedFormatException;
use app\components\resize\geometry\FileAlreadyExistsException;
use app\components\resize\geometry\FileNotSaveException;

/**
 * Класс, описывающий изображение в формате png
 */
class AcImagePNG extends AcImage {

	/**
	 * Проверяет, поддерживается ли формат png
	 *
	 * @return boolean
	 */
	public static function isSupport() {
		$gdInfo = parent::getGDinfo();
		return (bool)$gdInfo['PNG Support'];
	}

	/**
	 * @param string $filePath - путь к файлу с изображением
	 * @throws UnsupportedFormatException
	 */
	protected function __construct($filePath) {
		if (!self::isSupport()) {
			throw new UnsupportedFormatException('png');
		}

		parent::__construct($filePath);
		$path = parent::getFilePath();
		parent::setResource(@imagecreatefrompng($path));
	}

	/**
	 * @param string $path - путь, по которому будет сохранено изображение
	 * @return AcImage
	 * @throws FileAlreadyExistsException
	 * @throws FileNotSaveException
	 */
	public function save($path) {
		return parent::saveAsPNG($path);
	}

	public static function getQuality() {
		// png quality [0, 9] показывает степень сжатия
		// 0 - лучшее качество (нет сжатия)
		return 9 - round(parent::getQuality() / 10);
	}
}