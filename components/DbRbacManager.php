<?php

namespace app\components;

use Yii;
use yii\rbac\DbManager;

class DbRbacManager extends DbManager
{
    public function init()
    {
        parent::init();
    }
}