<?php
/**
 * Created by PhpStorm.
 * User: shabanov
 * Date: 14.08.2016
 * Time: 22:57
 */

namespace app\components;


use app\models\Menu;
use app\models\MessageForm;
use app\models\MessageFormSettings;
use app\models\UserFavourite;
use Yii;
use yii\db\Expression;
use yii\web\Controller;
use app\modules\cart\models\Cart;
use app\components\Geo;
use app\models\Parametrs;

class BaseController extends Controller
{
    public $geo_data;
    public $kurs_many = null;

    public function init()
    {
        $this->on('beforeAction', function ($event) {

            // запоминаем страницу неавторизованного пользователя, чтобы потом отредиректить его обратно с помощью  goBack()
            if (Yii::$app->getUser()->isGuest) {
                $request = Yii::$app->getRequest();
                // исключаем страницу авторизации или ajax-запросы
                if (!($request->getIsAjax() || strpos($request->getUrl(), 'login') !== false)) {
                    if(!strpos($request->getUrl(), 'documents') !== false){
                        Yii::$app->getUser()->setReturnUrl($request->getUrl());
                    }
                }
            }
        });

        /*if(!empty($_GET['mp'])) {
            if($_GET['mp'] == 'off'){
                setcookie('mp', null, time() - 3600, '/');
                unset($_COOKIE['mp']);
            } elseif ($_GET['mp'] == 2) {
                setcookie('mp', 2, time()+60*60*24*30*6, '/');
                $_COOKIE['mp'] = 2;
            } elseif ($_GET['mp'] == 3) {
                setcookie('mp', 3, time()+60*60*24*30*6, '/');
                $_COOKIE['mp'] = 3;
            } elseif ($_GET['mp'] == 4) {
                setcookie('mp', 4, time()+60*60*24*30*6, '/');
                $_COOKIE['mp'] = 4;
            } elseif ($_GET['mp'] == 5) {
                setcookie('mp', 5, time()+60*60*24*30*6, '/');
                $_COOKIE['mp'] = 5;
            }
        }*/

        /*$session = Yii::$app->session;
        if (!$session->isActive){
            $session->open();
        }

        if (!isset($_COOKIE['favourites_token'])) {
            $favouritesToken = md5(time() . uniqid('_', 'true') . rand(0, 100) * 8 * rand(0, 50));
            setcookie("favourites_token", $favouritesToken, time() + 60 * 60 * 24 * 30 * 6, '/');
        }*/

        /*$cartModel = new Cart();
        if(Yii::$app->user->isGuest && empty($_SESSION['cart'])) {
            $cartModel->upCartInCookie();
        }

        if(!Yii::$app->user->isGuest && empty($_SESSION['cart'])) {
            $cartModel->upUserCartInBd();
        }*/


        /*if(isset($_COOKIE['geobase'])){
            $this->geo_data = unserialize($_COOKIE['geobase']);
        }else{
            //$geo = new Geo(['ip'=>'31.171.160.0']);
            //$ip = Yii::$app->request->getUserIP();
            $geo = new Geo(['ip'=>'212.17.3.93']);
            $data = $geo->get_data($geo->ip);
            if(!$data){
                $data = $geo->get_country($geo->ip);
            }
            $this->geo_data = $data;
            if($data){
                setcookie('geobase', serialize($data), time() + 3600 * 24 * 7, '/');
            }
        }*/

        /*if(!empty($this->geo_data) && isset($this->geo_data->country) && $this->geo_data->country == 'KZ') {
            if (isset($_SESSION['kurs_many'])) {
                $this->kurs_many = $_SESSION['kurs_many'];
            } else {
                $kurs = Parametrs::findOne(4);
                $_SESSION['kurs_many'] = $kurs->value;
                $this->kurs_many = $kurs->value;
            }
        }*/


        $form = MessageFormSettings::find()->where(['type' => 6])->one();
        $formFields = $this->renderPartial('//layouts/callback/form-fields', [
            'callbackForm' => $form
        ]);
        $formButtons = $this->renderPartial('//layouts/callback/form-buttons', [
            'callbackForm' => $form
        ]);

        $mainMenu = Menu::find()->with('menuItem')->where(['menu_id' => '5', 'parent_id' => null])
                        ->orderBy([new Expression('position IS NULL ASC, position ASC')])->all();

        $pathInfo = explode('/', Yii::$app->request->getPathInfo());

        $sizeType = null;
        $categoryType = null;

        if (isset($pathInfo[1])) {
            if ($pathInfo[1] == "bolshie-razmery" || $pathInfo[1] == "bazovie-razmery" || $pathInfo[1] == "vse-razmery") {
                $sizeType = $pathInfo[1];
            }
        }
        if (isset($pathInfo[0])) {
            $categoryType = $pathInfo[0];
        }


        $this->view->params['main-menu'] = $mainMenu;
        $this->view->params['callback-form-buttons'] = $formButtons;
        $this->view->params['callback-form-fields'] = $formFields;
        $this->view->params['category-type'] = $pathInfo[0];
        $this->view->params['size-type'] = $sizeType;
        $this->view->params['category-type'] = $categoryType;
        $this->view->params['source'] = 'http://beautifull.loc';
        //$this->view->params['kurs_many'] = $this->kurs_many;
        /*if(!empty($this->geo_data) && isset($this->geo_data->country)){
            $this->view->params['country'] = $this->geo_data->country;
        }else{
            $this->view->params['country'] = null;
        }*/

        $description = \app\models\Parametrs::findOne(23);
        $title = \app\models\Parametrs::findOne(22);
        if($description){
            $this->view->params['description'] = $description->value;
        }else{
            $this->view->params['description'] = '';
        }
        if($title){
            $this->view->params['title'] = $title->value;
        }else{
            $this->view->params['title'] = '';
        }


        $bottomMenu = Menu::find()
            ->with('menuItem')->where(['menu_id' => '7', 'parent_id' => null])
            ->with('children')
            ->orderBy([new Expression('position IS NULL ASC, position ASC')])
            ->all();

        $this->view->params['bottomMenu'] = $bottomMenu;

    }

}