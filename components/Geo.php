<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 24.04.17
 * Time: 12:56
 */

namespace app\components;

use yii\base\Object;
use Yii;
use app\models\GeoCities;
use app\models\GeoBase;

class Geo extends Object
{
    public $ip;
    public $charset = 'utf-8';

    public function __construct(array $config = [])
    {
        if(isset($config['ip'])){
            $this->ip = $config['ip'];
        }else{
            $this->ip = $this->get_ip();
        }
        if (isset($options['charset']) && is_string($options['charset'])) {
            $this->charset = $options['charset'];
        }
        parent::__construct($config);
    }

    public function get_ip()
    {
        $keys = array('HTTP_X_FORWARDED_FOR', 'HTTP_CLIENT_IP', 'REMOTE_ADDR', 'HTTP_X_REAL_IP');
        foreach ($keys as $key) {
            $ip = trim(strtok(filter_input(INPUT_SERVER, $key), ','));
            if ($this->is_valid_ip($ip)) {
                return filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4);
            }
        }
    }

    public function is_valid_ip($ip)
    {
        return (bool)filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4);
    }

    /**
     * Данные из локальной базы
     * @param $ip
     * @return array|bool|null|\yii\db\ActiveRecord
     */
    public function get_data($ip)
    {
        $data = null;
        if(empty($ip)) return false;
        $long_ip = ip2long($ip);

        $geo_base = GeoBase::find()->where(['<=','long_ip1', $long_ip])->andWhere(['>=','long_ip2',$long_ip])->one();
        if($geo_base){
            if($geo_base->city_id!='0'){
                $data = GeoCities::find()->where(['city_id'=>$geo_base->city_id])->one();
                $data->country = $geo_base->country;
            }
        }

        return $data;
    }

    public function get_country($ip){
        $data = null;
        if(empty($ip)) return false;
        $long_ip = ip2long($ip);
        $data = GeoBase::find()->where(['<=','long_ip1', $long_ip])->andWhere(['>=','long_ip2',$long_ip])->one();
        return $data;
    }


    /**
     * функция получает данные по ip.
     * @return array - возвращает массив с данными
     */
    public function get_geobase_data() {
        // получаем данные по ip
        $ch = curl_init('http://ipgeobase.ru:7020/geo?ip=' . $this->ip);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 3);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        $string = curl_exec($ch);
        // если указана кодировка отличная от windows-1251, изменяем кодировку
        if ($this->charset != 'windows-1251' && function_exists('iconv')) {
            $string = iconv('windows-1251', $this->charset, $string);
        }
        $data = $this->parse_string($string);
        return $data;
    }

    /**
     * функция парсит полученные в XML данные
     * @return array - возвращает массив с данными
     */
    protected function parse_string($string) {
        $params = array('inetnum', 'country', 'city', 'region', 'district', 'lat', 'lng');
        $data = $out = array();
        foreach ($params as $param) {
            if (preg_match('#<' . $param . '>(.*)</' . $param . '>#is', $string, $out)) {
                $data[$param] = trim($out[1]);
            }
        }
        return $data;
    }
}