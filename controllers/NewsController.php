<?php

namespace app\controllers;



use app\models\Events;
use yii\web\NotFoundHttpException;

class NewsController extends \app\components\BaseController
{
    public function actionIndex()
    {
        $news = Events::find()->where(['is_active'=>true,'show_in_wholesale'=>true])->all();

        return $this->render('index',[
            'news'=>$news
        ]);
    }

    public function actionShow($url)
    {
        $news = Events::find()->where(['is_active'=>true,'show_in_wholesale'=>true,'url'=>$url])->one();
        if(!$news){
            throw new NotFoundHttpException('Страница не найдена');
        }
        return $this->render('show',[
            'news'=>$news
        ]);
    }

}
