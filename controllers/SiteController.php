<?php

namespace app\controllers;

use app\components\BaseController;
use app\forms\LoginForm;
use app\forms\RegistrationForm;
use app\models\Country;
use app\models\Parametrs;
use app\models\Product;
use app\models\readModels\UserReadModel;
use app\models\Regions;
//use app\modules\admin\models\DiscountsList;
use app\models\repositories\HomeReadRepository;
use app\models\repositories\NewprodsRepository;
use app\models\repositories\SensationRepository;
use app\modules\cart\models\Cart;
use app\modules\cart\models\Orders;
use app\services\CsvService;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\SendEmailForm;
use app\models\ResetPasswordForm;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use app\components\SMSMessage;
use app\models\UserProfile;
use app\models\CabEmail;
//use app\modules\admin\models\InboxSms;
//use app\modules\admin\models\MlCfg;
use app\models\GeoCities;
use app\models\GeoBase;
use app\components\Geo;


class SiteController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'app\components\RtCaptchaAction',
                'foreColor'=>0xcc0066,
                'transparent'=>true,
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $banners = \app\models\Banners::find()->select(['src','link','position','src_size'])->where(['type'=>13, 'is_active'=>true])->orderBy('id DESC')->asArray()->all();
        usort($banners, function($a,$b){
            return ($a['position']-$b['position']);
        });

        $bannersGallery = \app\models\Banners::find()->select(['src','link','position'])->where(['type'=>14, 'is_active'=>true])->orderBy('id DESC')->asArray()->all();
        usort($bannersGallery, function($a,$b){
            return ($a['position']-$b['position']);
        });

        $newProducts = (new NewprodsRepository())->getNewprods();
        $idsSens = '';
        if(file_exists('../commands/ids.txt')) {
            $idsSens = file_get_contents('../commands/ids.txt');
        }
        $sensations = (new SensationRepository())->getSensation($idsSens);

        $hit = [];
        $isComing = [];
        $ids = [];
        if($sensations){
            foreach ($sensations as $key=>$sens){
                $ids[] = $key;
            }
        }
        if($newProducts){

            foreach ($newProducts as $k=>$item){
                $ids[] = $k;
            }

            $hit = ArrayHelper::getColumn(\app\models\ProdsCatsParamValue::find()->where(['IN','prod_id',$ids])
                ->andWhere(['param_id' => 9])
                ->andWhere(['value' => 35])
                ->all(),'prod_id');
            $isComing = ArrayHelper::getColumn(\app\models\ProdsCatsParamValue::find()->where(['IN','prod_id',$ids])
                ->andWhere(['param_id' => 9])
                ->andWhere(['value' => 3])
                ->all(),'prod_id');
        }

        $titleParam = (new HomeReadRepository())->getParamValue(30);
        $titleParam2 = (new HomeReadRepository())->getParamValue(31);

        return $this->render('index',[
            'banners'=>$banners,
            'bannersGallery'=>$bannersGallery,
            'newProducts'=>$newProducts,
            'isComing' => $isComing,
            'hit'=>$hit,
            'titleParam'=>$titleParam,
            'titleParam2'=>$titleParam2,
            'sensations'=>$sensations
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        unset($_SESSION['ref']);
        return $this->redirect('/');
    }

    /**
     * Авторизация
     * @return string|\yii\web\Response
     */
    public function actionLogin()
    {
        if(!isset($_SESSION['ref']) && empty($_SESSION['ref'])) {
            $ref = Yii::$app->request->referrer;
            $_SESSION['ref'] = $ref;
        }
        //vd($ref,false);
        if(Yii::$app->user->isGuest){
            $model = new LoginForm();

            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                //return $this->goBack();
                return $this->redirect($_SESSION['ref'] ?: Yii::$app->homeUrl);
            }

            return $this->render('login',[
                'model'=>$model
            ]);

        }else{
            return $this->redirect('/');
        }
    }

    public function actionAuthWindow()
    {
        if( !Yii::$app->request->isAjax ){
            return $this->redirect('/');
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $session = Yii::$app->session;
            if (!$session->isActive){
                $session->open();
            }
            //поднимем корзину
//            $cartModel = new Cart();
//            $cartModel->tryToRestoreCartFromDB();
//
//            //подсчет персональной скидки
//            $this->checkDiscount();
//
//            if(isset($_SESSION['cart'])){
//                $count = $_SESSION['cart']['allqty'];
//                $sum = $_SESSION['cart']['sum'];
//            }else{
//                $count = 0; $sum = 0;
//            }

            //return json_encode(['ok'=>'login','count'=>$count, 'sum'=>$sum]);
            return json_encode(['ok'=>'login']);
        }
        $this->layout = false;
        $view =  $this->render('windows/auth', [
            'model' => $model
        ]);
        return json_encode(['view'=>$view]);
    }

    public function checkDiscount()
    {
        $user = Yii::$app->user->identity;
        if($user){
            $roles = Yii::$app->authManager->getRolesByUser($user->id);
            if(count($roles)==1 && isset($roles['User'])){
                $summa = $this->getSumOrders($user->id);
                if($summa > 0){
                    $discount_list = DiscountsList::findAll(['visible'=>'1']);
                    if($discount_list){
                        $user_discount = 0;
                        foreach ($discount_list as $it){
                            if($summa >= $it->summa){
                                $user_discount = $it->persent;
                                break;
                            }
                        }
                        $user->discount_user = $user_discount;
                        $user->save();
                    }
                }
            }
        }
    }

    public function getSumOrders($user_id)
    {
        $sql = "SELECT SUM(total_cost) AS summa FROM sales WHERE order_id IN (SELECT id FROM orders WHERE user_id='{$user_id}') AND type='0'";
        $summa = ArrayHelper::getValue(Yii::$app->db->createCommand($sql)->queryOne(),'summa');
        $sql2 = "SELECT SUM(total_cost) as sr FROM sales WHERE order_id IN (SELECT id FROM orders WHERE user_id='{$user_id}') AND type != '0'";
        $summaReturned = ArrayHelper::getValue(Yii::$app->db->createCommand($sql2)->queryOne(),'sr');
        $summa-= $summaReturned;
        return $summa;
    }

    /**
     * регистрация покупателя
     * @return string
     */
    public function actionRegistration()
    {
        if(!isset($_SESSION['ref']) && empty($_SESSION['ref'])) {
            $ref = Yii::$app->request->referrer;
            $_SESSION['ref'] = $ref;
        }
        if(Yii::$app->user->isGuest) {
            $regionModel = new Regions();
            $regions = $regionModel->regionsListForForm();
            $model = new RegistrationForm();

            if(!empty($_POST)) {
                $token = Yii::$app->request->post('_token_moda') ?? '';
                if (!$model->checkToken($token)) {
                    unset($_SESSION['_token_moda']);
                    return $this->redirect('/');
                }
            }

            if ($model->load(Yii::$app->request->post())) {
                $model->email = mb_strtolower(trim($model->email));
                if($model->validate()) {
                    if ($model->register()) {
                        //return $this->redirect('/');
                        return $this->redirect($_SESSION['ref'] ?: Yii::$app->homeUrl);
                    }
                }
            }

            return $this->render('registration', [
                'regions' => $regions,
                'model'=>$model,
            ]);
        }else{
            return $this->redirect('/');
        }
    }

    public function actionSendEmail(){
        
        $model = new SendEmailForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                if($model->sendEmail()){
                    Yii::$app->session->setFlash('warning','Проверьте емайл, указанный при регистрации');
                    return $this->redirect('/site/send-email');
                }else{
                    Yii::$app->session->setFlash('error','Ошибка! Не возможно сбросить пароль');
                }
            }
        }

        return $this->render('sendEmail', [
            'model' => $model,
        ]);
    }

    public function actionResetPassword($key)
    {
        try{
            $model = new resetPasswordForm($key);
        }catch (InvalidParamException $e){
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate() && $model->resetPassword()) {
                Yii::$app->session->setFlash('success','Ваш пароль успешно изменен!');
                return $this->redirect('/');
            }
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }


    public function actionResiver(){
        $foundUser = "не найден";
        $cdate = date('Y-m-d H:i:s');
        $msid = Yii::$app->request->get('msid');
        $message = Yii::$app->request->get('message');
        if(!empty($msid) && !empty($message)){
            $msid = strip_tags(trim($msid));
            $message = addslashes(strip_tags(trim($message)));
            $checkMobilePhone = substr(str_replace(array('-', ' '), '', $msid), 1);
            $checkUserSQL = "SELECT * FROM user_profile WHERE REPLACE(replace(mobile_phone, '-', ''),' ','') LIKE '%{$checkMobilePhone}%' LIMIT 1";
            $checkUser = Yii::$app->db->createCommand($checkUserSQL)->queryOne();
            if($checkUser){
                $foundUser = $checkUser['last_name'].' '.$checkUser['first_name'].' / id:'.$checkUser['id'];
                $modelCab = new CabEmail();
                $modelCab->user_id = $checkUser['id'];
                $modelCab->new_msg = 1;
                $modelCab->cdate = $cdate;
                $modelCab->message = $message;
                $modelCab->from11 = 1;
                //$modelCab->save(false);
            }
            $model = new InboxSms();
            $model->msid = $msid;
            $model->message = $message;
            $model->cdate = $cdate;
            $model->found_user = $foundUser;
            //$model->save(false);

            $tpl = MlCfg::findOne(['mnemo'=>'sms_in']);
            if($tpl){
                $this->sendMessage($message,$msid,$tpl);
            }
        }
        echo '<?xml version="1.0" encoding="utf-8"?>
                <OperationResult xmlns="http://mcommunicator.ru/M2M/">
                 <code>0</code>
                 <description>OK</description>
                </OperationResult>';
    }

    private function sendMessage($message,$msid,$tpl)
    {
        if($tpl->etype=='1'){
            $message = str_replace(['{message}','{phone}'],[$message,$msid],$tpl->ebody_html);
        }else{
            $message = str_replace(['{message}','{phone}'],[$message,$msid],$tpl->ebody_txt);
        }

        $subj = $tpl->esubj;

        if($tpl->etype=='1'){
            Yii::$app->mailer->compose()
                ->setFrom([Yii::$app->params['supportEmail']=>'Отправлено с сайта beauti-full.ru'])
                ->setTo([Yii::$app->params['supportEmail']])
                ->setSubject($subj)
                ->setHtmlBody($message)
                ->send();
        }
    }

    public function actionImportGeo()
    {
        set_time_limit(0); // указываем, чтобы скрипт не ограничивался временем по умолчанию
        ignore_user_abort(1); // указываем, чтобы скрипт продолжал работать даже при разрыве

        if(file_exists($_SERVER['DOCUMENT_ROOT'].'/geo/cities.txt')){
            Yii::$app->db->createCommand('TRUNCATE TABLE geo_cities')->execute();
            $file = file($_SERVER['DOCUMENT_ROOT'].'/geo/cities.txt');
            $pattern = '#(\d+)\s+(.*?)\t+(.*?)\t+(.*?)\t+(.*?)\s+(.*)#';
            foreach ($file as $row){
                $row = iconv('windows-1251', 'utf-8', $row);
                if(preg_match($pattern, $row, $out)){
                    $model = new GeoCities();
                    $model->city_id = $out[1];
                    $model->city = $out[2];
                    $model->region = $out[3];
                    $model->district = $out[4];
                    $model->lat = $out[5];
                    $model->lng = $out[6];
                    //$model->save(false);
                }
            }
        }

        if(file_exists($_SERVER['DOCUMENT_ROOT'].'/geo/cidr_optim.txt')){
            Yii::$app->db->createCommand('TRUNCATE TABLE geo_base')->execute();
            $file = file($_SERVER['DOCUMENT_ROOT'].'/geo/cidr_optim.txt');
            //vd(count($file));
            $pattern = '#(\d+)\s+(\d+)\s+(\d+\.\d+\.\d+\.\d+)\s+-\s+(\d+\.\d+\.\d+\.\d+)\s+(\w+)\s+(\d+|-)#';
            foreach ($file as $row){
                if(preg_match($pattern, $row, $out)){
                    $model = new GeoBase();
                    $model->long_ip1 = $out[1];
                    $model->long_ip2 = $out[2];
                    $model->ip1 = $out[3];
                    $model->ip2 = $out[4];
                    $model->country = $out[5];
                    $model->city_id = $out[6];
                    $model->save(false);
                }
            }
        }
    }

    public function actionSendSub()
    {
        $email = addslashes(strip_tags(trim(Yii::$app->request->post('email'))));
        $name = addslashes(strip_tags(trim(Yii::$app->request->post('name'))));

        if(Yii::$app->request->isAjax) {
            if (!empty($email)) {
                $validator = new \yii\validators\EmailValidator();
                if ($validator->validate($email, $error)) {
                    $dbEmail = \app\models\OptSubemails::findOne(['email'=>$email]);
                    if($dbEmail){
                        echo json_encode(['error'=>'Спасибо! Вы уже подписаны на рассылку!']);
                        exit;
                    }else{
                        $model = new \app\models\OptSubemails();
                        $model->name = $name;
                        $model->email = $email;
                        $model->create_dt = date('Y-m-d');
                        if($model->save()){
                            $template = \app\models\MlCfg::findOne(['mnemo'=>'optsubemail']);
                            if($template){
                                $from = \app\models\Parametrs::findOne(21);
                                Yii::$app->mailer->compose()
                                    ->setFrom([Yii::$app->params['supportEmail']=>$from->value])
                                    ->setTo($email)
                                    ->setSubject($template->esubj)
                                    ->setHtmlBody($template->ebody_html)
                                    ->send();
                            }
                            echo json_encode(['ok'=>'ok']);
                            exit;
                        }
                    }
                }else{
                    echo json_encode(['error'=>'Невалидный email']);
                    exit;
                }
            }
        }
    }

    public function actionTestInvoice()
    {
        $invoice_id = 12;
        $invoice = \app\modules\sberbank\models\Invoice::findOne(['order_id'=>$invoice_id]);
        $handler = new \app\modules\sberbank\components\handlerSuccess($invoice);
        $handler->handle();
        Yii::$app->request->getUrl();
    }

    public function actionCsvUsers()
    {
        $date = "2021-11-01 00:00:00";
        $data = (new UserReadModel())->getUsersToDate($date);
        $servive = new CsvService();
        $usersForCsv = $servive->reindexDataUsers($data);
        $servive->outCSV($usersForCsv, 'users_20211101.csv');

        exit();
    }


    public function actionExportc()
    {

        $categories = \app\models\ProductCategory::find()->where(['IN', 'id', [
            287, 58, 1, 50, 36, 276, 59, 51, 52, 57, 55, 206, 81, 56, 255, 284, 90, 258, 161, 172, 109, 253
        ]])->orderBy('position')->with(['productCategories'])->all();

        $ids_cat = [];
        if($categories){
            foreach ($categories as $cat){
                $ids_cat[] = $cat->id;
                if(isset($cat->productCategories) && !empty($cat->productCategories)){
                    foreach ($cat->productCategories as $c){
                        $ids_cat[] =  $c->id;
                    }
                }
            }
        }

        $products = [];
        if(count($ids_cat)>0){
            $products = Product::find()
                ->select(['id','wholesale_price','wholesale_discount_price','category_id','title','url'])
                ->where(['IN', 'category_id', $ids_cat])
                ->andWhere(['is_active'=>true])
                ->andWhere(['is_archive'=>false])
                ->andWhere(['is_wholesale_active'=>true])
                ->with('gallery')
                ->with('category')
                ->all();
        }

        $this->layout = null;

        return $this->renderPartial('export_c', [
            'categories' => $categories,
            'products'=>$products
        ]);
    }

}
