<?php
namespace app\controllers;
/**
 * @var SearchColor $searchColorPage
 */

use app\components\BaseController;
use app\components\pagination\CatalogLinkPager;
use app\models\Product;
use app\models\ProductSearch;
use app\models\SearchColor;
use Yii;
use yii\data\Pagination;
use yii\web\NotFoundHttpException;

class SearchColorController extends BaseController
{
    public function actionShow($url = null)
    {
        if (empty($url) || !$searchColorPage = SearchColor::findOneByUrl($url)) {
            throw new NotFoundHttpException('Страница не найдена');
        }

        $showAjaxAddProducts = false;
        $productSearch = new ProductSearch();
        $productSearch->load(Yii::$app->request->get());
        $productSearch->searchColor= $searchColorPage->id;
        $productSearch->is_archive = false;
        $productSearch->is_active = true;
        $dataProvider = $productSearch->search();

        $pagination = new Pagination([
            'totalCount' => $dataProvider->query->count(),
            'pageSize' => 75
        ]);

        $pages = new CatalogLinkPager(['pagination' => $pagination]);

        $priceMin = $searchColorPage->getMinProductPrice();
        $priceMax = $searchColorPage->getMaxProductPrice();

        $actualPriceMin = ($productSearch->minPrice) ? $productSearch->minPrice : $priceMin;
        $actualPriceMax = ($productSearch->maxPrice) ? $productSearch->maxPrice : $priceMax;

        $products = Product::getAlternateSortedProducts($dataProvider->query, $pagination->offset, $pagination->limit,
            'search_color', $searchColorPage->id, $productSearch->checkCatalogFilters($priceMin, $priceMax));

        $remainingProductsCount = $pagination->totalCount - (($pagination->getPage() + 1) * $pagination->getPageSize());
        if ($remainingProductsCount > $pagination->getPageSize()) {
            $remainingProductsCount = $pagination->getPageSize();
        }

        if ($remainingProductsCount > 0) {
            $showAjaxAddProducts = true;
        }

        if ($searchColorPage) {
            if ($searchColorPage->html_title) {
                $this->view->title = $searchColorPage->html_title;
            } else {
                $this->view->title = $searchColorPage->menu_title;
            }

            // Ajax используется для подгрузки товаров
            if (Yii::$app->request->isAjax && Yii::$app->request->get('ajaxAddProducts')) {
                $data['pageCount'] = $pages->pagination->getPageCount();
                $data['products'] = $this->renderPartial('/catalog/category/_products_colls', ['products' => $products]);
                $data['productCount'] = $remainingProductsCount;

                return json_encode($data);
            } else {
                return $this->render('search-color', [
                    'searchColor' => $searchColorPage,
                    'products' => $products,
                    'pages' => $pages,
                    'showAjaxAddProducts' => $showAjaxAddProducts,
                    'priceMin' => $priceMin,
                    'priceMax' => $priceMax,
                    'actualPriceMin' => $actualPriceMin,
                    'actualPriceMax' => $actualPriceMax,
                    'remainingProductsCount' => $remainingProductsCount,
                    'productSearch' => $productSearch,
                ]);
            }

        } else {
            return $this->render('index');
        }
    }
}