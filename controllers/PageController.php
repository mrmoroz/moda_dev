<?php

namespace app\controllers;

use app\components\BaseController;
use app\models\Menu;
use app\models\MenuItem;
use app\models\Message;
use app\models\MessageForm;
use app\models\Page;
use app\models\Parametrs;
use Yii;
use yii\db\Expression;
use yii\web\NotFoundHttpException;
use app\models\MessageRequest;
use app\models\Manufacturer;

class PageController extends BaseController
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionShow($url = null)
    {
        if (empty($url) || !$page = Page::findOneByUrl($url)) {
            throw new NotFoundHttpException('Страница не найдена');
        }
        $previewConfig = Parametrs::getPreviewImageConfig();
        $mess = '';

        if ($page) {
            if ($page->seo_title) {
                $this->view->title = $page->seo_title;
            } else {
                $this->view->title = $page->title;
            }

            $pageForms = $page->getForms();
            $formFields = array();
            $formButtons = array();
            $messages = [];

            if($url==='otzyvy'){
                $messages = MessageRequest::find()->where(['form_type' => 3, 'published' => true])->with('fields')->orderBy('date_send DESC')->limit(20)->all();
            }

            $latChar = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
            $latGroup = [];
            $numberGroup = [];
            $cyrilGroup = [];

            if($url==='brands'){
                $manufacturers = Manufacturer::find()->where(['show_in_wholesale' => true])->andWhere(['is_active' => true])->orderBy('name')->all();
                if($manufacturers){
                    foreach ($latChar as $char){
                        foreach ($manufacturers as $m){
                            if(!empty($m->client_name)){
                                $firstchar = mb_substr(trim($m->client_name),0,1);
                                if(mb_strtolower($firstchar)==$char){
                                    $latGroup[$char][$m->id][] = $m->client_name;
                                    $latGroup[$char][$m->id][] = $m->id;
                                }elseif (ctype_digit ($firstchar)){
                                    $numberGroup['numbers'][$m->id][0] = $m->client_name;
                                    $numberGroup['numbers'][$m->id][1] = $m->id;
                                }elseif (!in_array(mb_strtolower($firstchar), $latChar) && !ctype_digit ($firstchar)){
                                    $cyrilGroup['cyr'][$m->id][0] = $m->client_name;
                                    $cyrilGroup['cyr'][$m->id][1] = $m->id;
                                }
                            }
                        }
                    }
                }
            }



            if (isset($pageForms)) {
                foreach ($pageForms as $pageForm) {

                    $form = MessageForm::find()->where(['entity_type' => 'page', 'form_type' => $pageForm->form_type])->with('formSettings')->one();

                    $fields = $this->renderPartial('//callback/form-fields.php', [
                        'form' => $form,
                    ]);

                    $buttons = $this->renderPartial('//callback/form-buttons.php', [
                        'form' => $form,
                    ]);

                    array_push($formFields, $fields);
                    array_push($formButtons, $buttons);

                    unset($fields);
                    unset($buttons);
                }
            }



            $leftMenu = [];
            $menuTitle = null;

            $leftMenu = Menu::find()
                ->with('menuItem')
                ->where(['menu_id' => '5', 'parent_id' => null])
                ->orderBy([new Expression('position IS NULL ASC, position ASC')])
                ->all();

            return $this->render('page_left_menu', [
                'model_page' => $page,
                'pageForms' => $pageForms,
                'previewConfig' => $previewConfig,
                'leftMenu' => $leftMenu,
                'menuTitle' => $menuTitle,
                'messages'=>$messages,
                'formFields'=>$formFields,
                'formButtons'=>$formButtons,
                'url'=>$url,
                'latChar'=>$latChar,
                'latGroup'=>$latGroup,
                'numberGroup'=>$numberGroup,
                'cyrilGroup'=>$cyrilGroup,
            ]);

        }
    }
}
