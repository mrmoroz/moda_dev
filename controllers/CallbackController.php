<?php

namespace app\controllers;

const MESSAGES_FROM_SITE = 1;
const QUICK_ANSWER = 2;
const RECALLS = 3;
const OFFICIAL_MAIL = 4;

use app\components\BaseController;
use app\models\MessageField;
use app\models\MessageForm;
use app\models\MessageFormSettings;
use app\models\MessageRequest;
use app\models\MessageRequestField;
use app\models\Parametrs;
use app\models\Product;
use app\models\ProductCategory;
use app\models\ProductSearch;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class CallbackController extends BaseController
{
    public $out = ['init'=>FALSE];               //выходные данне
    public $in;                                  //входные данные
    private $secret = '6LcUThEUAAAAAHdGuF9Gzu-5XI0k4HB43_w70hGi';

    public function init(){
        parent::init();

        //отключаем верификацию токена
        $this->enableCsrfValidation = false;

        //получаем данные входные данне in (можно заполнить самостоятельно
        if (Yii::$app->request->post()) {
            if (Yii::$app->request->post('output')) {
                $this->in = json_decode(Yii::$app->request->post('output'));
                $this->in->init = TRUE;
            }
        }
    }

    public function actionAjax(){
        if ($this->in->init) {
            switch ($this->in->controller) {
                case 'sendMessage' :
                    $this->_sendMessage();
                    break;
                default :
                    $this->out=$this->in;
                    break;
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            return $this->out;
        } else {
            return $this->FALSE;
        }
    }
    
    public function _sendMessage()
    {
        $requireArr = [];
        $errorMessages = [];
        $isCaptha = false;

        $formType = $this->in->data->formType;
        $entityType = $this->in->data->entityType;
        if (isset($this->in->data->entityId)) {
            $entityId = $this->in->data->entityId;
        }
        $fields = $this->in->data->fields;

        $captcha = $this->in->data->captcha;
        $this->out['captcha'] = true;
        $this->out['init'] = true;

        $settings = MessageFormSettings::find()->where(['type' => $formType])->with('fields')->one();

        if(isset($settings->fields)){
            foreach ($settings->fields as $fld){
                if($fld->required) {
                    $requireArr[$fld->id] = $fld->name;
                    if($fld->type == 5){
                        $isCaptha = true;
                    }
                }
            }

            foreach ($fields as $fld2){
                $fValue = trim($fld2->value);
                if(isset($requireArr[$fld2->id]) && empty($fValue)){
                    $errorMessages[] = "Необходимо заполнить поле ".$requireArr[$fld2->id];
                }
            }

            if($isCaptha && !$captcha){
                $errorMessages[] = "Необходимо заполнить поле Captcha";
            }

            if($isCaptha && isset($captcha) && empty($captcha->value)){
                $errorMessages[] = "Необходимо заполнить поле Captcha";
            }

            if(count($errorMessages) > 0){
                $this->out['init'] = false;
                $this->out['errorMessages'] = $errorMessages;
                return;
            }
        }
        
        if (isset($captcha)) {
            $recaptcha = new \ReCaptcha\ReCaptcha($this->secret);
            $resp = $recaptcha->verify($captcha->value, $_SERVER['REMOTE_ADDR']);

            if (!$resp->isSuccess()) {
                $this->out['captcha'] = false;
                return;
            }
        }

        

        $request = new MessageRequest();
        $request->entity_type = $entityType;
        if (isset($entityId)) {
            $request->entity_id = $entityId;
        }
        if($formType==13){
            $request->title = $fields[0]->value;
            $this->out['gohome'] = true;
        }
        $request->form_type = $formType;
        $request->status = 0;
        $request->published = 0;
        $request->date_send = date("Y-m-d H:i:s");

        $messageBody = '';

        if ($request->save()) {
            foreach ($fields as $field) {
                $requestField = new MessageRequestField();
                $requestField->field_id = $field->id;
                $requestField->value = addslashes(strip_tags(trim($field->value)));
                $requestField->message_id = $request->id;

                $formField = MessageField::findOne($field->id);
                if (isset($formField->name_field)) {
                    $request->title = $requestField->value;
                    $request->save();
                }

                $requestField->save();

                if(isset($formField->name)){
                    $messageBody .= addslashes(strip_tags(trim($formField->name))) . ": " . addslashes(strip_tags(trim($field->value))) . "<br>";
                }
            }

            if($formType==13){
                $messageBody .= 'Email: '.$request->title;
            }
        }

        $model_config = Parametrs::findOne(['id'=>'2']);
        if (isset($model_config)) {
            if ($model_config->value) {
                //TODO - перенести в сервис
                $addresses = array_map('trim', explode(',', $model_config->value));
                if (!empty($settings->mail)) {
                    $addresses = array_merge($addresses, array_map('trim', explode(',', $settings->mail)));
                }
                Yii::$app->mailer->compose()
                    ->setFrom([Yii::$app->params['supportEmail'] => 'Отправлено с сайта moda-optom.ru'])
                    ->setTo($addresses)
                    ->setSubject($settings->name)
                    ->setHtmlBody($messageBody)
                    ->send();
            }
        }

        Yii::$app->session->setFlash('success_callback', 'Ваше сообщение отправлено');

        $this->out['init'] = true;
    }

    public function actionShow($url = null)
    {
        if (empty($url) || !$callbackPage = MessageFormSettings::findOneByUrl($url)) {
            throw new NotFoundHttpException('Страница не найдена');
        }

        $formFields = Yii::$app->cache->get("callback_form_" .$callbackPage->type . "_fields");
        $formButtons = Yii::$app->cache->get("callback_form_" .$callbackPage->type . "_buttons");

        if (!($formButtons && $formFields)) {
            $form = MessageForm::find()->where(['entity_type' => 'callback', 'form_type' => $callbackPage->type])
                                       ->with('formSettings')->one();

            if (!$formFields) {
                $formFields = $this->renderPartial('form-fields', [
                    'form' => $form,
                    'entityType' => $callbackPage->type,
                    'entityId' => $callbackPage->id,
                ]);

                Yii::$app->cache->set("callback_form_" .$callbackPage->type . "_fields", $formFields);
            }

            if (!$formButtons) {
                $formButtons = $this->renderPartial('form-buttons', [
                    'form' => $form,
                ]);

                Yii::$app->cache->set("callback_form_" .$callbackPage->type . "_buttons", $formButtons);
            }
        }

        $messages = MessageRequest::find()->where(['form_type' => $callbackPage->type, 'published' => true])->with('fields')->all();

        if ($callbackPage) {
            if ($callbackPage->HTML_title) {
                $this->view->title = $callbackPage->HTML_title;
            }

            return $this->render('callback', [
                'callbackPage' => $callbackPage,
                'messages' => $messages,
                'formFields' => $formFields,
                'formButtons' => $formButtons
            ]);

        } else {
            return $this->render('index');
        }
    }
}