<?php

namespace app\controllers;


use app\components\BaseController;
use app\components\pagination\CatalogLinkPager;
use app\models\Manufacturer;
use app\models\MessageRequest;
use app\models\ProdsCatsParamValue;
use app\models\Product;
use app\models\ProductCategory;
use app\models\ProductSearch;
use app\models\SizeChart;
use app\models\Tag;
use app\models\UserFavourite;
use Yii;
use yii\base\Exception;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use ZipArchive;

class CatalogController extends BaseController
{
    public $catalogType;
    public $catalogSize;

    public $tagsListTop = array();
    public $tagsListBottom = array();

    public $collectionList = array();
    /**
     * @var  ProductCategory[] $this->categoriesList
     */
    public $categoriesList = array();

    public function init()
    {
        if (!Yii::$app->request->isAjax) {
            $this->tagsListTop = Tag::find()->where(['visible' => true, 'show_in_wholesale' => 1, 'on_top'=>1])->orderBy('position, name')->all();
            $this->tagsListBottom = Tag::find()->where(['visible' => true, 'show_in_wholesale' => 1, 'on_bottom'=>1])->orderBy('position, name')->all();
            $this->categoriesList = [];
            $this->collectionList = Manufacturer::find()->where(['show_in_wholesale' => true])->andWhere(['is_active' => true])->orderBy('name')->all();
        }

        parent::init();
    }

    public function actionIndex()
    {
        //Search first category
        $category = (isset($this->categoriesList[0])) ? $this->categoriesList[0] : null;
        return $this->redirect('/catalog/tag/35');
        if ($category) {
            $addType = '';
            if($category['type_size']=='std'){
                $addType = '/bazovie-razmery/';
            }else{
                $addType = '/bolshie-razmery/';
            }
            /*if ($category->withStdSizes) {
                $addType = '/bazovie-razmery/';
            } elseif ($category->withBigSizes) {
                $addType = '/bolshie-razmery/';
            }*/

            return $this->redirect(ProductCategory::$categoryTypeUrls[$category['type']] . $addType . $category['url']);

        }
        $tag = (isset($this->tagsListTop)) ? $this->tagsListTop[0] : null;
        if ($tag) {
            return $this->redirect('/catalog/tag/' . $this->tagsListTop[0]->id);
        } else {
            $tag = (isset($this->tagsListBottom)) ? $this->tagsListBottom[0] : null;
            if ($tag) {
                return $this->redirect('/catalog/tag/' . $this->tagsListBottom[0]->id);
            }
        }

        return $this->render('index');
    }

    public function actionSearch()
    {
        $searchRequest = Yii::$app->request->get('articul');
        $searchRequest = trim($searchRequest, ' ');
        $productSearch = new ProductSearch();
        $productSearch->article = $searchRequest;
        $productSearch->title = $searchRequest;
        $productSearch->is_active = true;
        $productSearch->is_archive = false;
        $productSearch->is_wholesale_active = true;
        $titleH1 = 'Поиск по артикулу';

        $dataProvider = $productSearch->search();

        $products = $dataProvider->query->all();

        if($products){
            $ids = [];
            foreach ($products as $product) {
                $ids[] = $product->id;
            }

            $strIds = implode(',',$ids);
            $labels = Product::getLabels($strIds);
        }

        $tagsListTop = null;
        $tagsListBottom = null;
        $collectionList = null;
        $categoriesList = null;
        $tagsListTop = $this->tagsListTop;
        $tagsListBottom = $this->tagsListBottom;
        $collectionList = $this->collectionList;
        $categoriesList = $this->categoriesList;

        return $this->render('category/list', [
            'products' => $products,
            'searchRequest' => $searchRequest,
            'currentSizesUrl' => null,
            'pages' => null,
            'sizeType' => null,
            'titleH1' => $titleH1,
            'categoriesList' => $categoriesList,
            'pageType'=>'search',
            'catalogItem' => null,
            'tagsListTop' => $tagsListTop,
            'tagsListBottom' => $tagsListBottom,
            'collectionList' => $collectionList,
            'labels'=>$labels
        ]);
    }

    public function actionCollection($manufacturer)
    {
        /** @var Manufacturer $manufacturer */

        $showAjaxAddProducts = false;
        $productSearch = new ProductSearch();
        $productSearch->load(Yii::$app->request->get());
        $productSearch->manufacturer = $manufacturer->id;
        $productSearch->is_archive = false;
        $productSearch->is_active = true;
        $productSearch->is_wholesale_active = true;
        $dataProvider = $productSearch->search();

        $pagination = new Pagination([
            'totalCount' => $dataProvider->query->count(),
            'pageSize' => 75
        ]);

        $pages = new CatalogLinkPager(['pagination' => $pagination]);

        $priceMin = $manufacturer->getMinProductPrice();
        $priceMax = $manufacturer->getMaxProductPrice();

        $actualPriceMin = ($productSearch->minPrice) ? $productSearch->minPrice : $priceMin;
        $actualPriceMax = ($productSearch->maxPrice) ? $productSearch->maxPrice : $priceMax;

        $products = Product::getAlternateSortedProducts($dataProvider->query, $pagination->offset, $pagination->limit,
            'manufacturer', $manufacturer->id, $productSearch->checkCatalogFilters($priceMin, $priceMax));

        $new42 = [];
        $new56 = [];
        $hit = [];
        $isComing = [];
        if($products){
            $ids = [];
            foreach ($products as $product) {
                $ids[] = $product->id;
            }
            $strIds = implode(',',$ids);
            $labels = Product::getLabels($strIds);

            $new42 = ArrayHelper::getColumn(ProdsCatsParamValue::find()->where(['IN','prod_id',$ids])
                                            ->andWhere(['param_id' => 9])
                                            ->andWhere(['value' => 1])
                                            ->all(),'prod_id');
            $new56 = ArrayHelper::getColumn(ProdsCatsParamValue::find()->where(['IN','prod_id',$ids])
                                            ->andWhere(['param_id' => 9])
                                            ->andWhere(['value' => 12])
                                            ->all(),'prod_id');
            $hit = ArrayHelper::getColumn(ProdsCatsParamValue::find()->where(['IN','prod_id',$ids])
                                            ->andWhere(['param_id' => 9])
                                            ->andWhere(['value' => 35])
                                            ->all(),'prod_id');
            $isComing = ArrayHelper::getColumn(ProdsCatsParamValue::find()->where(['IN','prod_id',$ids])
                                            ->andWhere(['param_id' => 9])
                                            ->andWhere(['value' => 3])
                                            ->all(),'prod_id');                                                                
            
        }

        $remainingProductsCount = $pagination->totalCount - (($pagination->getPage() + 1) * $pagination->getPageSize());
        if ($remainingProductsCount > $pagination->getPageSize()) {
            $remainingProductsCount = $pagination->getPageSize();
        }

        if ($remainingProductsCount > 0) {
            $showAjaxAddProducts = true;
        }

        if ($manufacturer) {
            $tagsListTop = null;
            $tagsListBottom = null;
            $collectionList = null;
            $categoriesList = null;
            $tagsListTop = $this->tagsListTop;
            $tagsListBottom = $this->tagsListBottom;
            $collectionList = $this->collectionList;
            $categoriesList = $this->categoriesList;

            //vd($manufacturer);

            if ($manufacturer->html_title_mo) {
                $this->view->title = $manufacturer->html_title_mo;
            }

            if ($manufacturer->h1_title_mo) {
                $titleH1 = $manufacturer->h1_title_mo;
            } else {
                $titleH1 = $manufacturer->name_mo;
            }

            // Ajax используется для подгрузки товаров
            if (Yii::$app->request->isAjax && Yii::$app->request->get('ajaxAddProducts')) {
                $data['pageCount'] = $pages->pagination->getPageCount();
                $data['products'] = $this->renderPartial('/catalog/category/_products_colls', ['labels'=>$labels,'products' => $products,'currentSizesUrl' => null,'pageType' => 'collection','catalogItem' => $manufacturer]);
                $data['productCount'] = $remainingProductsCount;

                return json_encode($data);
            } else {
                return $this->render('category/list', [
                    'catalogItem' => $manufacturer,
                    'products' => $products,
                    'pages' => $pages,
                    'showAjaxAddProducts' => $showAjaxAddProducts,
                    'remainingProductsCount' => $remainingProductsCount,
                    'priceMin' => $priceMin,
                    'priceMax' => $priceMax,
                    'actualPriceMin' => $actualPriceMin,
                    'actualPriceMax' => $actualPriceMax,
                    'productSearch' => $productSearch,
                    'pageType' => 'collection',
                    'titleH1' => $titleH1,
                    'tagsListTop' => $tagsListTop,
                    'tagsListBottom' => $tagsListBottom,
                    'collectionList' => $collectionList,
                    'categoriesList' => $categoriesList,
                    'sizeType' => null,
                    'currentSizesUrl' => null,
                    'sizeChartSizes' => $manufacturer->getSizes(),
                    'new42'=>$new42,
                    'new56'=>$new56,
                    'hit'=>$hit,
                    'isComing'=>$isComing,
                    'labels'=>$labels
                ]);
            }

        } else {
            return $this->render('index');
        }
    }

    public function actionTag($tag)
    {
        $productSearch = new ProductSearch();
        $productSearch->load(Yii::$app->request->get());
        $productSearch->tag = $tag->id;
        $productSearch->is_archive = false;
        $productSearch->is_active = true;
        $productSearch->is_wholesale_active = true;
        $dataProvider = $productSearch->search();

//        $pagination = new Pagination([
//            'totalCount' => $dataProvider->query->count(),
//            'pageSize' => 75
//        ]);

        $pagination = $dataProvider->pagination;

        $pages = new CatalogLinkPager(['pagination' => $pagination]);

        $priceMin = $tag->getMinProductPrice();
        $priceMax = $tag->getMaxProductPrice();

        $actualPriceMin = ($productSearch->minPrice) ? $productSearch->minPrice : $priceMin;
        $actualPriceMax = ($productSearch->maxPrice) ? $productSearch->maxPrice : $priceMax;

        /*$products = Product::getAlternateSortedProducts($dataProvider->query, $pagination->offset, $pagination->limit,
            'tag', $tag->id, $productSearch->checkCatalogFilters($priceMin, $priceMax));*/

        $products = $dataProvider->getModels();

        if($products) {
            $ids = [];
            foreach ($products as $product) {
                $ids[] = $product->id;
            }
            $strIds = implode(',', $ids);
            $labels = Product::getLabels($strIds);
        }

        $remainingProductsCount = $pagination->totalCount - (($pagination->getPage() + 1) * $pagination->getPageSize());
        if ($remainingProductsCount > $pagination->getPageSize()) {
            $remainingProductsCount = $pagination->getPageSize();
        }

        if ($remainingProductsCount > 0) {
            $showAjaxAddProducts = true;
        }

        if ($tag) {
            $tagsListTop = null;
            $tagsListBottom = null;
            $collectionList = null;
            $categoriesList = null;
            $tagsListTop = $this->tagsListTop;
            $tagsListBottom = $this->tagsListBottom;
            $collectionList = $this->collectionList;
            $categoriesList = $this->categoriesList;

            if (!empty($tag->html_title_mo)) {
                $title = $tag->html_title_mo;
            } else {
                $title = $tag->name;
            }

            if ($tag->h1_title_mo) {
                $titleH1 = $tag->h1_title_mo;
            } else {
                $titleH1 = $tag->name;
            }

            $this->view->title = strip_tags($title);

            $this->view->registerMetaTag([

            ]);

            // Ajax используется для подгрузки товаров
            if (Yii::$app->request->isAjax && Yii::$app->request->get('ajaxAddProducts')) {
                $data['pageCount'] = $pages->pagination->getPageCount();
                $data['products'] = $this->renderPartial('/catalog/category/_products_colls', ['labels'=>$labels,'products' => $products,'pageType' => 'tag','currentSizesUrl' => null,'catalogItem' => $tag]);
                $data['productCount'] = $remainingProductsCount;

                return json_encode($data);
            } else {
                return $this->render('category/list', [
                    'catalogItem' => $tag,
                    'products' => $products,
                    'pages' => $pages,
                    'showAjaxAddProducts' => $showAjaxAddProducts,
                    'remainingProductsCount' => $remainingProductsCount,
                    'priceMin' => $priceMin,
                    'priceMax' => $priceMax,
                    'actualPriceMin' => $actualPriceMin,
                    'actualPriceMax' => $actualPriceMax,
                    'productSearch' => $productSearch,
                    'pageType' => 'tag',
                    'titleH1' => $titleH1,
                    'tagsListTop' => $tagsListTop,
                    'tagsListBottom' => $tagsListBottom,
                    'collectionList' => $collectionList,
                    'categoriesList' => $categoriesList,
                    'sizeType' => null,
                    'currentSizesUrl' => null,
                    'sizeChartSizes' => $tag->getSizes(),
                    'labels'=>$labels
                ]);
            }

        } else {
            return $this->render('index');
        }

    }

    public function actionCategory($category = null, ProductCategory $subCategory = null)
    {
        $otherSizesUrl = null;
        $showAjaxAddProducts = false;
        $searchCategory = $subCategory ?? $category;
        $productSearch = new ProductSearch();

        $sizeType = Yii::$app->request->get('sizeType');

        if (Yii::$app->request->isAjax && Yii::$app->request->get('ajaxAddProducts')) {
            $productSearch->load(Yii::$app->request->get('productSearch'));
        } else {
            $productSearch->load(Yii::$app->request->get());
        }

        $productSearch->categories = $searchCategory->getSearchCategories();
        $productSearch->is_active = true;
        $productSearch->is_wholesale_active = true;

        if ($sizeType == "bolshie-razmery") {
            //$productSearch->is_big_size = true;
            $productSearch->sizeMode = 'big-sizes';
            //$productSearch->visible_b_mo = true;
            $otherSizesUrl = $searchCategory->getStandardUrl();
            $currentSizesUrl = $searchCategory->getBigUrl();
            /*if ($searchCategory->h1_title_b) {
                $titleH1 = $searchCategory->h1_title_b;
            } else {
                $titleH1 = $searchCategory->title;
            }*/
            $titleH1 = $searchCategory->title.'. Большие размеры';
        } elseif ($sizeType == "bazovie-razmery") {
            //$productSearch->is_std_size = true;
            //$productSearch->visible_s_mo = true;
            $productSearch->sizeMode = 'standard-sizes';
            $otherSizesUrl = $searchCategory->getBigUrl();
            $currentSizesUrl = $searchCategory->getStandardUrl();
            /*if ($searchCategory->h1_title) {
                $titleH1 = $searchCategory->h1_title;
            } else {
                $titleH1 = $searchCategory->title;
            }*/
            $titleH1 = $searchCategory->title;
        } else {
            $currentSizesUrl = $searchCategory->getUrl();
            /*if ($searchCategory->h1_title) {
                $titleH1 = $searchCategory->h1_title;
            } else {
                $titleH1 = $searchCategory->title;
            }*/
            $titleH1 = $searchCategory->title;
        }

        if(isset($searchCategory->moda_title) && !empty($searchCategory->moda_title)){
            $this->view->title = $searchCategory->moda_title;
        }else{
            $this->view->title = $titleH1;
        }

        $dataProvider = $productSearch->search();
        $pagination = new Pagination([
            'totalCount' => $dataProvider->query->count(),
            'pageSize' => 45
        ]);

        $pages = new CatalogLinkPager(['pagination' => $pagination]);

        /*$priceMin = Product::find()->where(['IN', 'category_id', $productSearch->categories])
            ->andWhere(['is_active' => true])
            ->andWhere(['is_archive' => false]);
        $priceMax = Product::find()->where(['IN', 'category_id', $productSearch->categories])
            ->andWhere(['is_active' => true])
            ->andWhere(['is_archive' => false]);

        $discountPriceMin = $priceMin->andWhere(['>', 'discount_price', 0])->min('discount_price');
        $discountPriceMax = $priceMax->andWhere(['>', 'discount_price', 0])->max('discount_price');
        $priceMin = $priceMin->andWhere(['>', 'discount_price', 0])->min('discount_price');
        $priceMax = $priceMax->andWhere(['>', 'discount_price', 0])->max('discount_price');

        if ($priceMin > $discountPriceMin) {
            $priceMin = $discountPriceMin;
        }

        if ($priceMax < $discountPriceMax) {
            $priceMax = $discountPriceMax;
        }

        $actualPriceMin = ($productSearch->minPrice) ? $productSearch->minPrice : $priceMin;
        $actualPriceMax = ($productSearch->maxPrice) ? $productSearch->maxPrice : $priceMax;*/


        /*$products = Product::getAlternateSortedProducts($dataProvider->query, $pagination->offset, $pagination->limit,
            'category', $category->id);*/
        $new42 = [];
        $new56 = [];
        $hit = [];
        $isComing = [];

        $products = $dataProvider->query->offset($pagination->offset)->limit($pagination->limit)->all();

        if($products){
            $ids = [];
            foreach ($products as $product) {
                $ids[] = $product->id;
            }

            $strIds = implode(',',$ids);
            $labels = Product::getLabels($strIds);

            $new42 = ArrayHelper::getColumn(ProdsCatsParamValue::find()->where(['IN','prod_id',$ids])
                ->andWhere(['param_id' => 9])
                ->andWhere(['value' => 1])
                ->all(),'prod_id');
            $new56 = ArrayHelper::getColumn(ProdsCatsParamValue::find()->where(['IN','prod_id',$ids])
                ->andWhere(['param_id' => 9])
                ->andWhere(['value' => 12])
                ->all(),'prod_id');
            $hit = ArrayHelper::getColumn(ProdsCatsParamValue::find()->where(['IN','prod_id',$ids])
                ->andWhere(['param_id' => 9])
                ->andWhere(['value' => 35])
                ->all(),'prod_id');
            $isComing = ArrayHelper::getColumn(ProdsCatsParamValue::find()->where(['IN','prod_id',$ids])
                ->andWhere(['param_id' => 9])
                ->andWhere(['value' => 3])
                ->all(),'prod_id');

        }


        $remainingProductsCount = $pagination->totalCount - (($pagination->getPage() + 1) * $pagination->getPageSize());
        if ($remainingProductsCount > $pagination->getPageSize()) {
            $remainingProductsCount = $pagination->getPageSize();
        }

        if ($searchCategory->type == 1 && $remainingProductsCount > 0) {
            $showAjaxAddProducts = true;
        }

        $categorySizeChart = $category->getSizeChart();

        if ($categorySizeChart == null) {
            $categorySizeChart = SizeChart::find()->where(['name' => 'Без размеров'])->one();
        }
        if($categorySizeChart){
            $sizeChartSizes = ArrayHelper::map($categorySizeChart->getSizes()->all(), 'id', 'name');
        }else{
            $sizeChartSizes = [];
        }
        $categoryTags = $category->getAvailableCategoryTags();

        $tagsListTop = null;
        $tagsListBottom = null;
        $collectionList = null;
        $categoriesList = null;
        $tagsListTop = $this->tagsListTop;
        $tagsListBottom = $this->tagsListBottom;
        $collectionList = $this->collectionList;
        $categoriesList = $this->categoriesList;

        // Ajax используется для подгрузки товаров
        if (Yii::$app->request->isAjax && Yii::$app->request->get('ajaxAddProducts')) {
            $data['pageCount'] = $pages->pagination->getPageCount();
            $data['products'] = $this->renderPartial('category/_products_colls', ['labels'=>$labels, 'products' => $products,'currentSizesUrl' => $currentSizesUrl,'pageType' => 'category']);
            $data['productCount'] = $remainingProductsCount;

            return json_encode($data);
        } else {
            return $this->render('category/list', [
                'sizeChartSizes' => $sizeChartSizes,
                'productSearch' => $productSearch,
                'catalogItem' => $searchCategory,
                'products' => $products,
                'pages' => $pages,
                'sizeType' => $sizeType,
                'otherSizesUrl' => $otherSizesUrl,
                'currentSizesUrl' => $currentSizesUrl,
                'remainingProductsCount' => $remainingProductsCount,
                'showAjaxAddProducts' => $showAjaxAddProducts,
                'priceMin' => $priceMin,
                'priceMax' => $priceMax,
                'actualPriceMin' => $actualPriceMin,
                'actualPriceMax' => $actualPriceMax,
                'categoryTags' => $categoryTags,
                'pageType' => 'category',
                'titleH1' => $titleH1,
                'tagsListTop' => $tagsListTop,
                'tagsListBottom' => $tagsListBottom,
                'collectionList' => $collectionList,
                'categoriesList' => $categoriesList,
                'new42'=>$new42,
                'new56'=>$new56,
                'hit'=>$hit,
                'isComing'=>$isComing,
                'labels'=>$labels
            ]);
        }
    }

   public function actionProduct(Product $product, $pageType, $entityId, $sizeType)
   {
       $tagsListTop = null;
       $tagsListBottom = null;
       $collectionList = null;
       $categoriesList = null;
       $tagsListTop = $this->tagsListTop;
       $tagsListBottom = $this->tagsListBottom;
       $collectionList = $this->collectionList;
       $categoriesList = $this->categoriesList;
       $labels = Product::getLabels($product->id);

       if ($pageType == 'tag') {
           $catalogItem = Tag::findOne($entityId);
           if ($catalogItem->h1_title_mo) {
               $titleH1 = $catalogItem->h1_title_mo;
           } else {
               $titleH1 = $catalogItem->name;
           }
       } elseif ($pageType == 'collection') {
           $catalogItem = Manufacturer::findOne($entityId);
           if ($catalogItem->h1_title_mo) {
               $titleH1 = $catalogItem->h1_title_mo;
           } else {
               if (isset($catalogItem->name_mo)) {
                   $titleH1 = $catalogItem->name_mo;
               } else {
                   $titleH1 = $catalogItem->client_name;
               }
           }
       } else {
           $catalogItem = ProductCategory::findByUrl($entityId);
           $sectionName = '';
           $section = Yii::$app->request->get('section');
           if(isset($section)){
               $sectionName = 'Новинки. ';
           }

           if ($catalogItem->parent) {
               $titleH1 = $catalogItem->parent->title . ' - ' . $catalogItem->title;
           } else {
               $titleH1 = $catalogItem->title;
           }

           /*if ($sizeType == 'bolshie-razmery') {
               if ($catalogItem->h1_title_b) {
                   if ($catalogItem->parent) {
                       $titleH1 = $catalogItem->parent->title . ' - ' . $catalogItem->h1_title_b;
                   } else {
                       $titleH1 = $catalogItem->h1_title_b;
                   }
               } else {
                   if ($catalogItem->parent) {
                       $titleH1 = $catalogItem->parent->title . ' - ' . $catalogItem->h1_title_b;
                   } else {
                       $titleH1 = $catalogItem->title;
                   }
               }
           } else {
               if ($catalogItem->h1_title) {
                   if ($catalogItem->parent) {
                       $titleH1 = $catalogItem->parent->title . ' - ' . $catalogItem->h1_title;
                   } else {
                       $titleH1 = $catalogItem->h1_title;
                   }
               } else {
                   if ($catalogItem->parent) {
                       $titleH1 = $catalogItem->parent->title . ' - ' . $catalogItem->title;
                   } else {
                       $titleH1 = $catalogItem->title;
                   }
               }
           }*/
       }
       
       return $this->render('product/product', [
           'product' => $product,
           'pageType' => $pageType,
           'catalogItem' => $catalogItem,
           'sizeType' => $sizeType,
           'tagsListTop' => $tagsListTop,
           'tagsListBottom' => $tagsListBottom,
           'collectionList' => $collectionList,
           'categoriesList' => $categoriesList,
           'titleH1' => $sectionName.$titleH1,
           'labels'=>$labels
       ]);
   }

   public function actionDownloadZipPhoto()
   {
       $productId = Yii::$app->request->get('prodId');
       /**
        * @var Product $product
        */
       $product = Product::find()->with('gallery')->where(['id' => $productId])->one();
       if ($product === null) {
           throw new Exception(404);
       }
       $zipName = 'ModaOptom_Product_' . $product->id . '.zip';
       $zip = new ZipArchive();
       if (!$zip->open($_SERVER['DOCUMENT_ROOT'] . '/../runtime/' . $zipName, ZipArchive::CREATE)) {

       }

       if (!empty($product->gallery)) {
           foreach ($product->gallery as $number => $photo) {
               $filePath = $_SERVER['DOCUMENT_ROOT'] . '/../../../dev.beauti-full.ru/src/web' . $photo->path;
               if (file_exists($filePath)) {
                   $zip->addFile($filePath, ($number + 1) . '.jpg');
               }
           }

           $zipPath = $zip->filename;
           $zip->close();

           header('Content-type: application/zip');
           header('Content-Disposition: attachment; filename="' . $zipName . '"');

           readfile($zipPath);
           unlink($zipPath);
       } else {
           throw new Exception(404);
       }
   }


   public function actionRazdel()
   {
       $productSearch = new ProductSearch();
       $sizeType = Yii::$app->request->get('sizeType');
       $sectionType = Yii::$app->request->get('categoryType');
       $currentSizesUrl = Yii::$app->request->get('currentSizesUrl');
       $this->view->title = $titleH1 =  $this->getSectionName($sectionType);

       if (Yii::$app->request->isAjax && Yii::$app->request->get('ajaxAddProducts')) {
           $productSearch->load(Yii::$app->request->get('productSearch'));
       }else{
           $productSearch->load(Yii::$app->request->get());
       }
       $productSearch->categories = $this->getSectionCategories($sectionType);
       $productSearch->is_active = true;
       $productSearch->is_wholesale_active = true;
       $productSearch->is_new = true;
       $productSearch->sizeMode = $this->getSizeMode($sizeType);

       $dataProvider = $productSearch->search();

       $pagination = new Pagination([
           'totalCount' => $dataProvider->query->count(),
           'pageSize' => 45
       ]);

       $pages = new CatalogLinkPager(['pagination' => $pagination]);

       $new42 = [];
       $new56 = [];
       $hit = [];
       $isComing = [];

       $products = $dataProvider->query->offset($pagination->offset)->limit($pagination->limit)->all();

       if($products){
           $ids = [];
           foreach ($products as $product) {
               $ids[] = $product->id;
           }

           $strIds = implode(',',$ids);
           $labels = Product::getLabels($strIds);

           $new56 = ArrayHelper::getColumn(ProdsCatsParamValue::find()->where(['IN','prod_id',$ids])
               ->andWhere(['param_id' => 9])
               ->andWhere(['value' => 12])
               ->all(),'prod_id');
           $hit = ArrayHelper::getColumn(ProdsCatsParamValue::find()->where(['IN','prod_id',$ids])
               ->andWhere(['param_id' => 9])
               ->andWhere(['value' => 35])
               ->all(),'prod_id');
           $isComing = ArrayHelper::getColumn(ProdsCatsParamValue::find()->where(['IN','prod_id',$ids])
               ->andWhere(['param_id' => 9])
               ->andWhere(['value' => 3])
               ->all(),'prod_id');

       }

       $remainingProductsCount = $pagination->totalCount - (($pagination->getPage() + 1) * $pagination->getPageSize());
       if ($remainingProductsCount > $pagination->getPageSize()) {
           $remainingProductsCount = $pagination->getPageSize();
       }

       if ($searchCategory->type == 1 && $remainingProductsCount > 0) {
           $showAjaxAddProducts = true;
       }


       $sizeChartSizes = $this->getCategoriesSizes($productSearch->categories, $this->getSectionType($sectionType));

       $tagsListTop = null;
       $tagsListBottom = null;
       $collectionList = null;
       $categoriesList = null;
       $tagsListTop = $this->tagsListTop;
       $tagsListBottom = $this->tagsListBottom;
       $collectionList = $this->collectionList;
       $categoriesList = $this->categoriesList;

       // Ajax используется для подгрузки товаров
       if (Yii::$app->request->isAjax && Yii::$app->request->get('ajaxAddProducts')) {
           $data['pageCount'] = $pages->pagination->getPageCount();
           $data['products'] = $this->renderPartial('category/_products_colls', ['labels'=>$labels,'products' => $products,'currentSizesUrl' => $currentSizesUrl,'pageType' => 'category']);
           $data['productCount'] = $remainingProductsCount;

           return json_encode($data);
       } else {
           return $this->render('category/list', [
               'sizeChartSizes' => $sizeChartSizes,
               'productSearch' => $productSearch,
               'catalogItem' => null,//$searchCategory,
               'products' => $products,
               'pages' => $pages,
               'sizeType' => $sizeType,
               'otherSizesUrl' => null,//$otherSizesUrl,
               'currentSizesUrl' => $currentSizesUrl,
               'remainingProductsCount' => $remainingProductsCount,
               'showAjaxAddProducts' => $showAjaxAddProducts,
               'priceMin' => 0,//$priceMin,
               'priceMax' => 0,//$priceMax,
               'actualPriceMin' => 0,//$actualPriceMin,
               'actualPriceMax' => 0,//$actualPriceMax,
               'categoryTags' => null,//$categoryTags,
               'pageType' => 'category',
               'titleH1' => $titleH1,
               'tagsListTop' => $tagsListTop,
               'tagsListBottom' => $tagsListBottom,
               'collectionList' => $collectionList,
               'categoriesList' => $categoriesList,
               'new42'=>$ids,
               'new56'=>$new56,
               'hit'=>$hit,
               'isComing'=>$isComing,
               'labels'=>$labels
           ]);
       }
   }

   private function getCategoriesSizes($categories, $type){
       $sizes = [];
       if(count($categories) > 0) {
           $catStr = implode(',', $categories);

           if ($type == 0) {
               $type = 1;
           }

           $key_man_sizes = 'sizes_razdel_' . $type;
           $sizes = Yii::$app->cache->get($key_man_sizes);
           if ($sizes === false) {
               $sizes = \yii\helpers\ArrayHelper::map(\app\models\Size::findBySql("SELECT S.id, S.name FROM sizes S
                         JOIN stock_position SP ON SP.size_id = S.id
                         JOIN products P ON P.id = SP.product_id
                         WHERE S.active = TRUE
                         AND P.category_id IN ({$catStr})
                         AND P.is_active = TRUE
                         AND P.is_archive = FALSE
                         GROUP BY S.id
                         ORDER BY S.name")->asArray()->all(), 'id', 'name');
               Yii::$app->cache->set($key_man_sizes, $sizes, 60 * 60 * 24);
           }
           $sizes = \app\helpers\CommonHelper::SortSizes($sizes);
       }
       return $sizes;
   }

   private function getSizeMode($sizeType){
       $mode = null;
       if ($sizeType == "bolshie-razmery") {
           $mode = 'big-sizes';
       }elseif($sizeType == "bazovie-razmery"){
           $mode = 'standard-sizes';
       }
       return $mode;
   }

   private function getSectionName($sectionType)
   {
       $name = 'Новинки. Женщины';
       if(!empty($sectionType)){
           switch ($sectionType){
               case 'zhenskaja':
                   $name = 'Новинки. Женщины';
                   break;
               case 'zhenskaja-bolshie-razmery':
                   $name = 'Новинки. Большие размеры';
                   break;
               case 'muzhskaja':
                   $name = 'Новинки.Мужчины';
                   break;
               case 'dom':
                   $name = 'Новинки. Дом';
                   break;
               case 'detskaja':
                   $name = 'Новинки.Дети';
                   break;
           }
       }
       return $name;
   }

   private function getSectionType($sectionType){
       $type = 1;
       if(!empty($sectionType)){
           switch ($sectionType){
               case 'zhenskaja':
                   $type = 1;
                   break;
               case 'zhenskaja-bolshie-razmery':
                   $type = 1;
                   break;
               case 'muzhskaja':
                   $type = 2;
                   break;
               case 'dom':
                   $type = 4;
                   break;
               case 'detskaja':
                   $type = 3;
                   break;
           }
       }
       return $type;
   }

   private function getSectionCategories($sectionType)
   {
       return ArrayHelper::getColumn(\app\models\ProductCategory::find()->select(['id'])->where(['type'=>$this->getSectionType($sectionType)])->asArray()->all(),'id');
   }
}