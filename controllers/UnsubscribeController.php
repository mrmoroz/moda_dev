<?php

namespace app\controllers;

use app\components\BaseController;
use app\helpers\CommonHelper;
use app\models\MessageForm;
use app\models\Subemails;
use app\models\User;
use Yii;

class UnsubscribeController extends BaseController
{
    public function actionIndex()
    {
        if(Yii::$app->request->isPost){

            $email = '';
            $post = Yii::$app->request->post();

            if(isset($post['Unsubsribe']) && count($post['Unsubsribe']) > 0){
                if(isset($post['Unsubsribe']['email'])){
                    $email = CommonHelper::clearStr($post['Unsubsribe']['email']);
                }
            }

            if(empty($email)){
                return $this->redirect('/');
            }

            User::unsubscribe(1,$email);
            $sunemail = \app\models\OptSubemails::find()->where(['email'=>$email])->one();
            if($sunemail){
                $sunemail->delete();
            }

            $form = MessageForm::find()->where('entity_id IS null')->andWhere(['entity_type' => 'unsubscribe-mo'])
                ->with('formSettings')->one();
                //vd($form);

            $formFields = $this->renderPartial('@app/views/callback/form-fields', [
                'form' => $form
            ]);
            
            $formButtons = $this->renderPartial('@app/views/callback/form-buttons', [
                'form' => $form
            ]);



            return $this->render('index',[
                'activeForm' => $form->formSettings->active,
                'formFields'=>$formFields,
                'formButtons'=>$formButtons,
                'form'=>$form,
                'email'=>$email
            ]);
        }

        return $this->redirect('/');
    }

}
