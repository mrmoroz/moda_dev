<?php
use app\models\Tag;

/**
 * @var Tag $tagPage
 */

namespace app\controllers;


use app\components\BaseController;
use app\components\pagination\CatalogLinkPager;
use app\models\ProdsCatsParamValue;
use app\models\Product;
use app\models\ProductSearch;
use app\models\Tag;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\db\Query;
use yii\web\NotFoundHttpException;

class TagController extends BaseController
{
    public function actionShow($url = null)
    {
        if (empty($url) || !$tagPage = Tag::findOneByUrl($url)) {
            throw new NotFoundHttpException('Страница не найдена');
        }

        $productSearch = new ProductSearch();
        $productSearch->load(Yii::$app->request->get());
        $productSearch->tag = $tagPage->id;
        $productSearch->is_archive = false;
        $productSearch->is_active = true;
        $dataProvider = $productSearch->search();

        $pagination = new Pagination([
            'totalCount' => $dataProvider->query->count(),
            'pageSize' => 75
        ]);

        $pages = new CatalogLinkPager(['pagination' => $pagination]);

        $priceMin = $tagPage->getMinProductPrice();
        $priceMax = $tagPage->getMaxProductPrice();

        $actualPriceMin = ($productSearch->minPrice) ? $productSearch->minPrice : $priceMin;
        $actualPriceMax = ($productSearch->maxPrice) ? $productSearch->maxPrice : $priceMax;

        $products = Product::getAlternateSortedProducts($dataProvider->query, $pagination->offset, $pagination->limit,
            'tag', $tagPage->id, $productSearch->checkCatalogFilters($priceMin, $priceMax));

        $remainingProductsCount = $pagination->totalCount - (($pagination->getPage() + 1) * $pagination->getPageSize());
        if ($remainingProductsCount > $pagination->getPageSize()) {
            $remainingProductsCount = $pagination->getPageSize();
        }

        if ($remainingProductsCount > 0) {
            $showAjaxAddProducts = true;
        }

        if ($tagPage) {
            if ($tagPage->html_title) {
                $title = $tagPage->html_title;
            } elseif ($tagPage->menu_title) {
                $title = $tagPage->menu_title;
            } else {
                $title = $tagPage->name;
            }

            $this->view->title = $title;

            $this->view->registerMetaTag([

            ]);

            // Ajax используется для подгрузки товаров
            if (Yii::$app->request->isAjax && Yii::$app->request->get('ajaxAddProducts')) {
                $data['pageCount'] = $pages->pagination->getPageCount();
                $data['products'] = $this->renderPartial('/catalog/category/_products_colls', ['products' => $products]);
                $data['productCount'] = $remainingProductsCount;

                return json_encode($data);
            } else {
                return $this->render('tag', [
                    'tag' => $tagPage,
                    'products' => $products,
                    'pages' => $pages,
                    'showAjaxAddProducts' => $showAjaxAddProducts,
                    'remainingProductsCount' => $remainingProductsCount,
                    'priceMin' => $priceMin,
                    'priceMax' => $priceMax,
                    'actualPriceMin' => $actualPriceMin,
                    'actualPriceMax' => $actualPriceMax,
                    'productSearch' => $productSearch,
                ]);
            }

        } else {
            return $this->render('index');
        }
    }

    public function actionProduct()
    {

    }
}