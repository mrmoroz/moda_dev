<div class="footer">
    <div class="wrap-form-sub">
        <div class="columns-group">
            <div class="content-form-sub">
                <div class="sub-title">Подписка на модаоптом</div>
                <div class="sub-form-item">
                    <input class="sub-input sub-name" type="text" name="name" value="" placeholder="Ваше имя">
                </div>
                <div class="sub-form-item">
                    <input class="sub-input sub-email" type="email" name="email" value="" placeholder="Ваш email">
                </div>
                <div class="sub-form-item">
                    <button class="btn-sub send-sub" type="button">Подписаться</button>
                </div>
                <div class="error-sub"></div>
            </div>
        </div>
    </div>
    <div class="columns-group">
        <div class="content-wrap-bottom-menu">
            <div>
                <p class="links-footer-title">Наши контакты</p>
                <p class="footer-phone">8 983 510 71 09</p>
                <p class="footer-jobtime">
                    Будни: с 4:00 до 13:00 ч.<br>
                    по московскому времени <br>
                    Выходные: суббота <br>
                    и воскресение
                </p>
                <a class="fancybox open-footer-link" href="#Callback">Заказать звонок</a><br>
                <a class="open-footer-link" href="/page/kontakty-mo#7_top">Обратная связь</a>
            </div>
            <div>
                <?php if(isset($this->params['bottomMenu'][0]) && !empty($this->params['bottomMenu'][0])):?>
                    <?=\app\components\widgets\BottommenuWidgets::widget(['menu' => $this->params['bottomMenu'][0]])?>
                <?php endif;?>
            </div>
            <div>
                <?php if(isset($this->params['bottomMenu'][1]) && !empty($this->params['bottomMenu'][1])):?>
                    <?=\app\components\widgets\BottommenuWidgets::widget(['menu' => $this->params['bottomMenu'][1]])?>
                <?php endif;?>
            </div>
            <div>
                <p class="links-footer-title">Способы оплаты</p>
                <div class="sp-op">
                    <img src="https://beauti-full.ru/img/newstyle/logo_visa_mc_mir.jpg" alt="">
                </div>
                <p class="sp-p-change">Вы можете выбрать другой <br>
                    <a href="/page/zakaz" style="text-decoration: underline;">способ оплаты</a>
                </p>
                <p class="links-footer-title">МЫ В СОЦ. СЕТЯХ</p>
                <div class="wrap-soc index-page">
                    <div class="ok">
                        <a href="http://odnoklassniki.ru/bfull" target="_blank">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.86 31.86">
                                <defs>
                                    <style>.cls-1-ok{fill:none;stroke:#999;stroke-miterlimit:10;}.cls-2-ok{fill:#999;}</style>
                                </defs>
                                <g id="ok-ic" data-name="Layer 1">
                                    <path class="cls-1-ok" d="M15.92,31.36h0A15.42,15.42,0,0,0,31.36,15.93h0A15.43,15.43,0,0,0,15.93.5h0A15.43,15.43,0,0,0,.5,15.93h0A15.42,15.42,0,0,0,15.92,31.36Z"></path>
                                    <g id="ok">
                                        <path class="cls-2-ok" d="M15.87,16.38A4.34,4.34,0,1,0,11.53,12,4.34,4.34,0,0,0,15.87,16.38Zm0-6.13a1.8,1.8,0,1,1-1.8,1.8A1.8,1.8,0,0,1,15.87,10.25Z"></path>
                                        <path class="cls-2-ok" d="M17.63,19.92a8.14,8.14,0,0,0,2.52-1,1.27,1.27,0,0,0-1.35-2.15,5.51,5.51,0,0,1-5.84,0,1.27,1.27,0,0,0-1.35,2.15,8.15,8.15,0,0,0,2.52,1l-2.43,2.43a1.27,1.27,0,0,0,1.8,1.8l2.38-2.38,2.38,2.38a1.27,1.27,0,1,0,1.8-1.8Z"></path>
                                    </g>
                                </g>
                            </svg>
                        </a>
                    </div>
                    <div class="vk">
                        <a href="https://vk.com/bfull_ru" target="_blank">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.86 31.86">
                                <defs>
                                    <style>.cls-1-vk{fill:none;stroke:#999;stroke-miterlimit:10;}.cls-2-vk{fill:#999;fill-rule:evenodd;}</style>
                                </defs>
                                <g id="vk" data-name="Layer 1">
                                    <path class="cls-1-vk" d="M15.92,31.36h0A15.42,15.42,0,0,0,31.36,15.94h0A15.43,15.43,0,0,0,15.93.5h0A15.43,15.43,0,0,0,.5,15.93h0A15.42,15.42,0,0,0,15.92,31.36Z"></path>
                                    <path id="vk_alt" class="cls-2-vk" d="M15.36,22h1.17a1,1,0,0,0,.53-.23.85.85,0,0,0,.16-.51s0-1.57.71-1.8,1.64,1.52,2.62,2.19a1.85,1.85,0,0,0,1.3.4l2.61,0s1.37-.08.72-1.16a8.72,8.72,0,0,0-1.94-2.25c-1.64-1.52-1.42-1.27.55-3.91,1.2-1.6,1.68-2.58,1.53-3s-1-.29-1-.29l-2.94,0a.67.67,0,0,0-.38.07.82.82,0,0,0-.26.31,17.13,17.13,0,0,1-1.09,2.29c-1.31,2.22-1.83,2.34-2,2.2-.5-.32-.37-1.29-.37-2,0-2.16.33-3.06-.64-3.29a5,5,0,0,0-1.38-.14,6.37,6.37,0,0,0-2.44.25c-.33.16-.59.53-.44.55a1.32,1.32,0,0,1,.87.44,2.85,2.85,0,0,1,.29,1.33s.17,2.54-.41,2.85c-.39.22-.94-.23-2.11-2.24a18.78,18.78,0,0,1-1-2.18.89.89,0,0,0-.24-.33,1.23,1.23,0,0,0-.45-.18l-2.8,0s-.42,0-.57.19,0,.5,0,.5S8,17.21,10.5,19.8A6.72,6.72,0,0,0,15.36,22Z"></path>
                                </g>
                            </svg>
                        </a>
                    </div>
                    <div class="facebook">
                        <a href="https://www.facebook.com/groups/224671134558164/" target="_blank">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.86 31.86">
                                <defs>
                                    <style>.cls-1-f{fill:none;stroke:#999;stroke-miterlimit:10;}.cls-2-f{fill:#999;}</style>
                                </defs>
                                <g id="icon_facebook" data-name="Layer 1">
                                    <path class="cls-1-f" d="M15.92,31.36h0A15.42,15.42,0,0,0,31.36,15.94h0A15.43,15.43,0,0,0,15.93.5h0A15.43,15.43,0,0,0,.5,15.93h0A15.42,15.42,0,0,0,15.92,31.36Z"></path>
                                    <path id="facebook" class="cls-2-f" d="M17.38,25.73V17.17h2.87l.43-3.33h-3.3V11.71c0-1,.27-1.62,1.65-1.62H20.8v-3A23.89,23.89,0,0,0,18.23,7c-2.55,0-4.29,1.55-4.29,4.41v2.46H11.06v3.33h2.88v8.55Z"></path>
                                </g>
                            </svg>
                        </a>
                    </div>
                    <div class="instagramm">
                        <a href="https://www.instagram.com/beauti_full.ru/" target="_blank">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.86 31.86">
                                <defs>
                                    <style>.cls-1-i{fill:none;stroke:#999;stroke-miterlimit:10;}.cls-2-i{fill:#999;}</style>
                                </defs>
                                <g id="inst-ic" data-name="Layer 1">
                                    <path class="cls-1-i" d="M15.92,31.36h0A15.42,15.42,0,0,0,31.36,15.94h0A15.43,15.43,0,0,0,15.93.5h0A15.43,15.43,0,0,0,.5,15.93h0A15.42,15.42,0,0,0,15.92,31.36Z"></path>
                                    <g id="instagram">
                                        <path class="cls-2-i" d="M15.93,9.44c2.11,0,2.36,0,3.2,0a4.36,4.36,0,0,1,1.47.27,2.62,2.62,0,0,1,1.5,1.5,4.43,4.43,0,0,1,.27,1.47c0,.83,0,1.08,0,3.2s0,2.36,0,3.2a4.42,4.42,0,0,1-.27,1.47,2.62,2.62,0,0,1-1.5,1.5,4.37,4.37,0,0,1-1.47.27c-.84,0-1.09,0-3.2,0s-2.36,0-3.2,0a4.39,4.39,0,0,1-1.47-.27,2.62,2.62,0,0,1-1.5-1.5,4.35,4.35,0,0,1-.27-1.47c0-.83,0-1.08,0-3.2s0-2.36,0-3.2a4.36,4.36,0,0,1,.27-1.47,2.62,2.62,0,0,1,1.5-1.5,4.38,4.38,0,0,1,1.47-.27c.83,0,1.09,0,3.2,0m0-1.43c-2.15,0-2.42,0-3.26,0a5.81,5.81,0,0,0-1.92.37,4,4,0,0,0-2.32,2.32,5.83,5.83,0,0,0-.37,1.92c0,.84,0,1.11,0,3.26s0,2.42,0,3.26a5.83,5.83,0,0,0,.37,1.92,4,4,0,0,0,2.32,2.31,5.81,5.81,0,0,0,1.92.37c.84,0,1.11,0,3.26,0s2.42,0,3.26,0a5.81,5.81,0,0,0,1.92-.37,4,4,0,0,0,2.32-2.31,5.83,5.83,0,0,0,.37-1.92c0-.84,0-1.11,0-3.26s0-2.42,0-3.26a5.83,5.83,0,0,0-.37-1.92,4,4,0,0,0-2.32-2.32,5.81,5.81,0,0,0-1.92-.37c-.84,0-1.11,0-3.26,0Z"></path>
                                        <path class="cls-2-i" d="M15.93,11.87A4.06,4.06,0,1,0,20,15.93,4.06,4.06,0,0,0,15.93,11.87Zm0,6.7a2.64,2.64,0,1,1,2.64-2.64A2.64,2.64,0,0,1,15.93,18.57Z"></path>
                                        <circle class="cls-2-i" cx="20.16" cy="11.71" r="0.95"></circle>
                                    </g>
                                </g>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="coda">
            <p>МОДАОПТОМ – ОПТОВЫЙ ИНТЕРНЕТ-МАГАЗИН МОДНОЙ ОДЕЖДЫ</p>

            <p>
                Интернет-магазин МОДАОПТОМ – это более 190 крупных поставщиков России и Республики Беларусь в одном месте.
                В оптовом интернет-магазине МОДАОПТОМ представлены коллекции модной женской, мужской, детской одежды и обуви с
                доставкой по всей России, Казахстану и всему миру, а также товары для дома и отдыха. Покупая одежду и обувь, Вы
                гарантированно получаете качественные товары из Европы по привлекательным ценам! Весь товар сертифицирован.
            </p>

            <p>© Copyright 2013-<?=date('Y')?>. <a href="mailto:moda-optom@mail.ru">Модаоптом</a> – оптовый интернет-магазин модной одежды. Все права защищены.</p>
        </div>
    </div>
</div>

<?php
$script = <<<JS
$(function() {
  $('.send-sub').click(function() {
        var email = $('.sub-email').val();
        var name = $('.sub-name').val();
        var csrfToken = $('meta[name="csrf-token"]').attr("content");
        if(email!=''){
            $(this).prop('disabled',true);
            $.ajax({
                data: {email: email, _csrf : csrfToken, name:name},
                url: '/site/send-sub',
                type: "POST",
                success: function (data) {
                    var response = JSON.parse(data);
                    $('.send-sub').prop('disabled',false);
                    if(typeof response.error != 'undefined'){
                        $('.error-sub').text(response.error);
                    }
                    if(typeof response.ok != 'undefined'){
                        $('.sub-title').text('Спасибо за подписку!');
                        $('.sub-form-item').hide();
                        $('.error-sub').text('');
                    }
                }
            });
        }
    });
});
JS;
$this->registerJS($script);
?>