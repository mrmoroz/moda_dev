<?php
    $assetBundle = \app\assets\HtmlAsset::register($this);
?>

<div class="PopUp" id="Callback">
    <h3>Спасибо за обращение!</h3>

    <p>Оставьте, пожалуйста, Ваши контактные данные.<br> Персональный консультант перезвонит Вам в
        ближайшее время.</p>

    <form name="6" data-entity-type="callback-form" id="6_top">
            <div class="tabl-form">
                <? if (isset($this->params['callback-form-fields'])) {
                    echo $this->params['callback-form-fields'];
                } ?>
            </div>
            <? if (isset($this->params['callback-form-buttons'])) {
                echo $this->params['callback-form-buttons'];
            } ?>
    </form>



</div>