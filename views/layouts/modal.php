<div class="modal fade" id="cart-modal" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <a title="Закрыть" class="fancybox-close close" href="#" data-dismiss="modal" aria-label="Close"></a>
                <h1 class="modal-title">Товар успешно добавлен в корзину!</h1>
            </div>
            <div class="modal-body">
                <p>One fine body&hellip;</p>
            </div>
            <!--div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div-->
        </div>
    </div>
</div>