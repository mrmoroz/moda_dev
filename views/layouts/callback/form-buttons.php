<? if (isset($callbackForm)) : ?>
    <? foreach ($callbackForm->buttons as $button) : ?>
        <div>
            <div>
                <button class="callback-button send-callback-form" type="button">
                    <?= $button->name ?>
                </button>
            </div>
        </div>
    <? endforeach; ?>
<? endif; ?>
