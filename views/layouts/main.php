<?php
/* @var $this \yii\web\View */
use yii\bootstrap\Html;

$assetBundle = \app\assets\HtmlAsset::register($this);
$this->registerMetaTag(['name' => 'description', 'content' => $this->params['description']]);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html class="">
<html lang="<?= Yii::$app->language ?>">
<head>
    <?= Html::csrfMetaTags() ?>
    <title>
        <?php if(!empty($this->title)):?>
            <?= Html::encode($this->title) ?>
        <?php else:?>
            <?= $this->params['title'] ?>
        <?php endif;?>
    </title>
    <?php $this->head() ?>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i&amp;subset=cyrillic" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?= $assetBundle->baseUrl ?>/css/cloud-zoom.css">
    <script type="text/javascript" src="<?= $assetBundle->baseUrl ?>/js/cloud-zoom.1.0.3.js"></script>

    <script type="text/javascript">
        $('.cloud-zoom, .cloud-zoom-gallery').CloudZoom();
    </script>
    <style type="text/css">
        .fancybox-margin {
            margin-right: 17px;
        }
    </style>
</head>
<body class="white-bg">
<?= $this->beginBody() ?>
<div id="super-container">
    <div class="block"></div>
    <div id="columns" class="wrapper">
        <div id="header-block">
            <div class="columns-group">
                <div class="left-column">
                    <a class="logo" href="/">
                        <img src="<?= $assetBundle->baseUrl ?>/images/modaoptom-logo.png" alt="МодаОптом.рф">
                    </a>
                </div>
                <div class="right-column">
                    <div class="content">
                        <!-- ВСТАВКА -->
                        <script type="text/javascript">

                            $(document).ready(function () {
                                $(".fancybox").fancybox({
                                    padding: 0,
                                    openEffect: 'fade',
                                    closeEffect: 'fade',
                                    closeClick: false,
                                    closeBtn: true
                                });
                                $('form.cf_form').submit(function(){
                                    var $elem = $(this);
                                    var inputs = $elem.find('input[type=text]');
                                    for(var i=0;i<inputs.length; i++){
                                        if($(inputs[i]).val() == ''){
                                            $(inputs[i]).focus();
                                            event.preventDefault();
                                            return false;
                                        }
                                    }
                                    if($($elem.find('textarea')).val() == ''){
                                        $($elem.find('textarea')).focus();
                                        event.preventDefault();
                                        return false;
                                    }
                                })
                            });
                        </script>
                        <!-- / ВСТАВКА -->

                        <div id="main-menu-container">
                            <?= $this->render('../_colls/main-menu') ?>
                        </div>
                        <!-- start всплывающее окно ОБРАТНЫЙ ЗВОНОК -->
                        <?= $this->render('callback-form') ?>
                        <!-- finish всплывающее окно ОБРАТНЫЙ ЗВОНОК -->
                    </div>
                </div>

                <div class="clear"></div>
                <!-- ВСТАВКА -->
                <div class="container_99" style="display: flex; justify-content: flex-end; padding-top: 20px;">
                    <?php if(Yii::$app->session->hasFlash('success_callback')):?>
                        <p style="padding-right: 40px;"><?= Yii::$app->session->getFlash('success_callback') ?></p>
                    <?php endif;?>
                    <a style="padding-right: 64px;" href="https://moda-optom.ru/page/kak-zakazat-mo">Заказ от 5000 руб.</a>
                    <span class="phone-number" style="padding-left: 0; padding-right: 64px;">+7 983 510 71 09</span>
                    <a style="padding-right: 20px;" class="fancybox" href="#Callback">Заказать звонок</a>
                </div>
                <!-- / ВСТАВКА -->
            </div>
        </div>
        <div id="subheader-block">

            <div class="columns-group">

                <div class="left-column">
                </div>

                <div class="right-column">
                    <div class="content">
                        <?php if (Yii::$app->user->isGuest): ?>
                            <div id="user-block">
                                <div class="column1">
                                    <?php if (Yii::$app->controller->id == 'catalog'): ?>
                                        <div id="search-by-articul-block">
                                            <form action="/catalog/search" method="get" accept-charset="utf-8">
                                                <input type="text" name="articul"
                                                       value="<?= Yii::$app->request->get('articul') ?>">
                                                <a class="button-link" id="articul-search-submit" href="#">Поиск по
                                                    артикулу</a>
                                            </form>
                                            <script type="text/javascript">
                                                jQuery(document).ready(function () {
                                                    $("#articul-search-submit").click(function () {
                                                        $(this).parent().submit();
                                                        return false;
                                                    });
                                                });
                                            </script>

                                        </div>
                                    <?php else:?>
                                        &nbsp;
                                    <?php endif; ?>
                                </div>
                                <div class="column2">
                                    <div id="user-menu">
                                        <a href="/login">Вход</a>
                                        <a href="/registration">Регистрация</a>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        <?php else: ?>
                            <div id="user-block">
                                <div class="column1">
                                    <?php if (Yii::$app->controller->id == 'catalog'): ?>
                                        <div id="search-by-articul-block">
                                            <form action="/catalog/search" method="get" accept-charset="utf-8">
                                                <input type="text" name="articul"
                                                       value="<?= Yii::$app->request->get('articul') ?>">
                                                <a class="button-link" id="articul-search-submit" href="#">Поиск по
                                                    артикулу</a>
                                            </form>
                                            <script type="text/javascript">
                                                jQuery(document).ready(function () {
                                                    $("#articul-search-submit").click(function () {
                                                        $(this).parent().submit();
                                                        return false;
                                                    });
                                                });
                                            </script>

                                        </div>
                                    <?php else: ?>
                                        &nbsp;
                                    <?php endif; ?>
                                </div>
                                <div class="column2">
                                    <?php
                                    //vd($_SESSION['cart'],false);
                                    $count_cart = 0;
                                    if(isset($_SESSION['cart'])){
                                        $count_cart = count($_SESSION['cart']);
                                    }
                                    ?>
                                    <div id="user-menu">
                                    <span id="user-name">
                                        Добро пожаловать, <b><?= Yii::$app->user->identity->f_name ?></b>
                                    </span>
                                        <a id="shopping-cart-link" href="/cart"
                                           title="Оформить заказ">Корзина: <?=$count_cart?></a>
                                        <a href="/logout">Выход</a>

                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="columns-group clearfix">
            <?= $content ?>
        </div>
        <?=$this->render('footer')?>
    </div>
<!--    <a href="#top" class="scrollTop">^наверх^</a>-->
    <div id="super-overlay"></div>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            $(".sizes-filter-link").click(function () {
                if ($("#sizes-filter-list").css('display') != 'none') {
                    $("#sizes-filter-list").fadeOut("fast");
                    $("#super-overlay").hide();
                }
                else {
                    $("#super-overlay").show();
                    $("#sizes-filter-list").fadeIn("fast");
                }
                return false;
            });
            $("#super-overlay").click(function () {
                $("#sizes-filter-list").fadeOut("fast");
                $("#super-overlay").hide();
            });
            $("#size-search-submit").click(function () {
                $(this).parent().submit();
                return false;
            });
            $('body').on('mouseenter', 'ul#catalog-items-list li', function(){
                $(this).find('.prod-card-size-hover').show();
            });
            $('body').on('mouseleave', 'ul#catalog-items-list li', function(){
                $(this).find('.prod-card-size-hover').hide();
            });
            
            $(function(){
                var e = $(".scrollTop");
                var speed = 500;

                function show_scrollTop(){
                    ( $(window).scrollTop()>300 ) ? e.fadeIn(600) : e.hide();
                }
                $(window).scroll( function(){show_scrollTop()} );
                show_scrollTop();
            });
        });
    </script>
</div>
<?= $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>