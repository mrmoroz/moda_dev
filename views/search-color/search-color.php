<?php
/**
 * @var \app\models\SearchColor $searchColor
 * @var \yii\data\ActiveDataProvider $dataProvider
 */
use app\components\pagination\CatalogLinkPager;
use yii\widgets\ListView;

?>
<div id="content-body">
    <div class="content-columns clearfix">
        <div class="content-left-column">
            <?= $this->render('//catalog/_left-column') ?>
        </div>
        <div class="content-right-column">
            <ul id="breadcrumbs" class="clearfix">
                // > > >a
            </ul>
            <h1>
                <?= $searchColor->h1_page ?>
            </h1>
            <? if ($searchColor->top_text) : ?>
                <div class="infoblock">
                    <?= $searchColor->top_text ?>
                </div>
            <? endif; ?>

            <? if (!empty($products)) : ?>
                <? \yii\widgets\Pjax::begin(['timeout' => '4000']) ?>
                <?= $this->render('../catalog/filters', [
                    'productSearch' => $productSearch,
                    'sizeChartSizes' => $searchColor->getSizes(),
                    'categoryTags' => $searchColor->getCategoryTags(),
                    'textureTypes' => $searchColor->getTextureTypes(),
                    'manufacturers' => $searchColor->getManufacturers(),
                    'priceMin' => $priceMin,
                    'priceMax' => $priceMax,
                    'actualPriceMin' => $actualPriceMin,
                    'actualPriceMax' => $actualPriceMax,
                    'showManufacturerFilter' => true,
                    'showSearchColorFilter' => false,
                    'width' => '17.8%'
                ]) ?>
                <div id="top-pagination">
                    <?= CatalogLinkPager::widget([
                        'pagination' => $pages->pagination,
                    ]); ?>
                </div>
                <ul class="catalog-items clearfix">
                    <?= $this->render('/catalog/category/_products_colls', ['products' => $products]) ?>
                </ul>

                <!-- Вывод кнопок внизу -->
                <div class="next-counter-link-holder">
                    <? if ($showAjaxAddProducts) : ?>
                        <a data-href="<?= $searchColor->getUrl() ?>" data-page="<?= $pages->pagination->getPage() + 2 ?>"
                           class="catalog-ajax-button inline-b-links append-catalog-link" style="width: 100%">
                            Показать еще <?= $remainingProductsCount ?> товаров "<?= $searchColor->menu_title ?>"
                        </a>
                    <? endif; ?>
                </div>

                <div id="bottom-pagination">
                    <?= CatalogLinkPager::widget([
                        'pagination' => $pages->pagination,
                    ]); ?>
                </div>
                <? \yii\widgets\Pjax::end(); ?>
            <? else:?>
                <p>Не найдено товаров</p>
            <? endif; ?>

            <? if ($searchColor->bottom_text) : ?>
                <?= $searchColor->bottom_text ?>
            <? endif; ?>
        </div>
    </div>
</div>