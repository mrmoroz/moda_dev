<div id="content">
    <div id="content-body">
        <?php if(Yii::$app->session->hasFlash('success')):?>
            <div class="alert alert-success"><?=Yii::$app->session->getFlash('success');?></div>
        <?php endif;?>

        <?php if(isset($bannersGallery) && count($bannersGallery)>0):?>
            <div id="slider">
                <div class="bxslider">

                    <?php foreach ($bannersGallery as $banner):?>

                        <div class="img-bx-hiddden">
                            <a href="<?=$banner['link']?>">
                                <img src="<?=$this->params['source'].$banner['src']?>">
                            </a>
                        </div>

                    <?php endforeach;?>

                </div>
            </div>
        <?php endif;?>

        <?php if(isset($newProducts) && count($newProducts)>0):?>
            <h2 class="home-title">Самые популярные новинки</h2>
            <div class="slider2 wrap-slider">
                <?php foreach ($newProducts as $k=> $product):?>
                    <div style="position: relative;">
                        <a href="<?=$product['url']?>" style="outline: none;">
                            <img src="<?=$product['img']?>" alt="" style="height: 320px;" style="outline: none;">
                        </a>
                        <div class="supertags-bar" style="top: 255px;">
                            <?php if(!empty($product['disc'])):?>
                                <span class="supertag red">-<?=$product['disc']?>%</span>
                            <?php endif;?>
                            <span class="supertag">Новинка</span>
                            <?php if($hit && in_array($k, $hit)):?>
                                <span class="supertag">Хит продаж</span>
                            <?php endif;?>
                            <?php if($isComing && in_array($k, $isComing)):?>
                                <span class="supertag">Скоро в продаже</span>
                            <?php endif;?>
                        </div>
                        <div class="catalog-item-name" style="text-transform: uppercase; margin-top: 10px;"><?=$product['title']?></div>
                        <div class="catalog-item-articul">Артикул: <?=$product['article']?></div>
                        <?php if(!Yii::$app->user->isGuest || Yii::$app->params['openPrice']):?>
                            <div class="catalog-item-articul">
                                Цена:
                                <?php if(!empty($product['disc'])):?>
                                    <strong class="item-price-old" style="color:#b80084"><?=$product['price']?> руб.</strong>
                                    <span class="item-price-current"><?=$product['dprice']?> руб.</span>
                                <?php else:?>
                                    <strong style="color:#b80084"><?=$product['price']?> руб.</strong>
                                <?php endif;?>
                            </div>
                        <?php endif;?>
                    </div>
                <?php endforeach;?>
            </div>
        <?php endif;?>

        <?php if(isset($banners) && count($banners)>=2):?>
            <div class="row" style="margin-right: 0;">
                <?php if($titleParam):?>
                    <h2 class="home-title"><?=$titleParam->value?></h2>
                <?php endif;?>
                <?php foreach ($banners as $k => $banner):?>
                    <?php if($k==0 || $k==1):?>
                        <?php if($banner['src_size'] == '945*288'):?>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <a href="<?=$banner['link']?>">
                                    <img src="<?=$this->params['source'].$banner['src']?>">
                                </a>
                            </div>
                        <?php else:?>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <a href="<?=$banner['link']?>">
                                    <img src="<?=$this->params['source'].$banner['src']?>">
                                </a>
                            </div>
                        <?php endif;?>
                    <?php endif;?>
                <?php endforeach;?>
            </div>
        <?php endif;?>

        <?php if(isset($sensations) && count($sensations)>0):?>
            <h2 class="home-title">Выбор покупателей</h2>
            <div class="slider wrap-slider">
                <?php foreach ($sensations as $k=> $product):?>
                    <div style="position: relative;">
                        <a href="<?=$product['url']?>" style="outline: none;">
                            <img src="<?=$product['img']?>" alt="" style="height: 320px;" style="outline: none;">
                        </a>
                        <div class="supertags-bar" style="top: 255px;">
                            <?php if(!empty($product['disc'])):?>
                                <span class="supertag red">-<?=$product['disc']?>%</span>
                            <?php endif;?>
                            <?php if($product['is_new']):?>
                            <span class="supertag">Новинка</span>
                            <?php endif;?>
                            <?php if($hit && in_array($k, $hit)):?>
                                <span class="supertag">Хит продаж</span>
                            <?php endif;?>
                            <?php if($isComing && in_array($k, $isComing)):?>
                                <span class="supertag">Скоро в продаже</span>
                            <?php endif;?>
                        </div>
                        <div class="catalog-item-name" style="text-transform: uppercase; margin-top: 10px;"><?=$product['title']?></div>
                        <div class="catalog-item-articul">Артикул: <?=$product['article']?></div>
                        <?php if(!Yii::$app->user->isGuest || Yii::$app->params['openPrice']):?>
                            <div class="catalog-item-articul">
                                Цена:
                                <?php if(!empty($product['disc'])):?>
                                    <strong class="item-price-old" style="color:#b80084"><?=$product['price']?> руб.</strong>
                                    <span class="item-price-current"><?=$product['dprice']?> руб.</span>
                                <?php else:?>
                                    <strong style="color:#b80084"><?=$product['price']?> руб.</strong>
                                <?php endif;?>
                            </div>
                        <?php endif;?>
                    </div>
                <?php endforeach;?>
            </div>
        <?php endif;?>
        <?php if(isset($banners) && count($banners)>=3):?>
            <div class="row" style="margin-right: 0; margin-bottom: 100px;">
                <?php if($titleParam2):?>
                    <h2 class="home-title"><?=$titleParam2->value?></h2>
                <?php endif;?>
                <?php foreach ($banners as $k => $banner):?>
                    <?php if($k >= 2):?>
                        <?php if($banner['src_size'] == '945*288'):?>
                            <div class="col-xs-12 col-sm-12 col-md-12" style="margin-bottom: 12px;">
                                <a href="<?=$banner['link']?>">
                                    <img src="<?=$this->params['source'].$banner['src']?>">
                                </a>
                            </div>
                        <?php else:?>
                            <div class="col-xs-6 col-sm-6 col-md-6" style="margin-bottom: 12px;">
                                <a href="<?=$banner['link']?>">
                                    <img src="<?=$this->params['source'].$banner['src']?>">
                                </a>
                            </div>
                        <?php endif;?>
                    <?php endif;?>
                <?php endforeach;?>
            </div>
        <?php endif;?>

    </div>
</div>
<?php
$script = <<<JS
$(function() {
    $('.bxslider').bxSlider({
        //mode: 'fade',
        auto: true,
        //autoControls: true,
        speed: 1000,
        pause: 7000
    });
    
    $('.slider').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 4,
        //autoplay:true,
        slidesToScroll: 1,
        prevArrow: '<button class="slick-prev" aria-label="Previous" type="button"><div class="wrap-svg-arrows"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14.12 25.42"><g id="icon-arrow-left" data-name="icon-arrow-left"><polyline class="item-icon-arrow-left" points="13.41 24.71 1.41 12.71 13.41 0.71"/></g></svg></div></button>',
        nextArrow: '<button class="slick-next" aria-label="Next" type="button"><div class="wrap-svg-arrows-1"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14.12 25.42"><g id="icon-arrow-right" data-name="icon-arrow-right"><polyline class="item-icon-arrow-right" points="0.71 0.71 12.71 12.71 0.71 24.71"/></g></svg></div></button>',
    });
    
    $('.slider2').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 4,
        //autoplay:true,
        slidesToScroll: 1,
        prevArrow: '<button class="slick-prev" aria-label="Previous" type="button"><div class="wrap-svg-arrows"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14.12 25.42"><g id="icon-arrow-left" data-name="icon-arrow-left"><polyline class="item-icon-arrow-left" points="13.41 24.71 1.41 12.71 13.41 0.71"/></g></svg></div></button>',
        nextArrow: '<button class="slick-next" aria-label="Next" type="button"><div class="wrap-svg-arrows-1"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14.12 25.42"><g id="icon-arrow-right" data-name="icon-arrow-right"><polyline class="item-icon-arrow-right" points="0.71 0.71 12.71 12.71 0.71 24.71"/></g></svg></div></button>',
    });
});
JS;
$this->registerJS($script);
?>
