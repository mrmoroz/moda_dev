<?php
use yii\helpers\Html;
$this->title = "Регистрация";
$model->region_id = 7;
$model->ignore_mail_list = 1;
?>

<div class="row">
    <div class="col-sm-3" style="text-align: right;"><h1><?=$this->title;?></h1></div>
</div>

<?php
$form = yii\bootstrap\ActiveForm::begin([
    'id' => 'registration-form',
    'action'=>'/registration',
    'layout' => 'horizontal',
]) ?>

<input type="hidden" name="_token_moda" value="<?=$model->getToken()?>">

<?= $form->field($model,'email')->textInput(['placeholder'=>'E-mail']);?>

<?= $form->field($model,'ignore_mail_list')->checkbox()?>

<?= $form->field($model,'l_name')->textInput(['placeholder'=>'Фамилия']);?>

<?= $form->field($model,'f_name')->textInput(['placeholder'=>'Имя']);?>

<?= $form->field($model,'t_name')->textInput(['placeholder'=>'Отчество']);?>

<?= $form->field($model,'phone')->textInput(['placeholder'=>'Телефон']);?>

<div class="row">
    <div class="col-sm-3"></div>
    <div class="col-sm-9">
        <ul class="notes" style="margin-bottom: 10px;">
            <li>Мобильный телефон: в формате +7 XXX XXX XX XX</li>
            <li>Городской телефон: в формате (код города) номер телефона</li>
            <li>Вы можете указать номера и мобильного, и городского телефонов</li>
        </ul>
    </div>
</div>

<?= $form->field($model,'region_id')->dropDownList($regions)?>

<?= $form->field($model,'city_name')->textInput(['placeholder'=>'Город']);?>

<?= $form->field($model,'tk')->textInput(['placeholder'=>'Транспортная компания']);?>

<?= $form->field($model,'realizations')->radioList(['1'=>'Опт','2'=>'СП', '3'=>'Розница']);?>

<?= $form->field($model,'sog')->checkbox()?>

<div style="text-align: center;">
    <?= Html::submitButton('Зарегистрироваться', ['class'=>'btn btn-default btn-lg']) ?>
</div>

<?php yii\bootstrap\ActiveForm::end(); ?>

<?php
$script = <<<JS
    $(function() {
        $('#registration-form').on('beforeSubmit', function (e) {
            alert('ok');
            return true;
        });
    });
JS;
$this->registerJS($script);
?>



