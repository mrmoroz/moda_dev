<?php
use yii\helpers\Html;
$this->title = "Вход";
?>

<div class="row">
    <div class="col-sm-3" style="text-align: right;"><h1><?=$this->title;?></h1></div>
</div>

<?php
$form = yii\bootstrap\ActiveForm::begin([
    'id' => 'login-form',
    'action'=>'/login',
    'layout' => 'horizontal',
]) ?>

<?= $form->field($model,'email')->textInput(['placeholder'=>'E-mail']);?>

<div style="text-align: center;">
    <?= Html::submitButton('Вход', ['class'=>'btn btn-default btn-lg']) ?>
</div>

<?php yii\bootstrap\ActiveForm::end(); ?>
