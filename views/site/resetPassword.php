<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->title = "Сброс пароля";
/* @var $this yii\web\View */
/* @var $model app\models\resetPasswordForm */
/* @var $form ActiveForm */
?>
<div class="site-resetPassword" style="margin: 30px 20px 0 190px;">
    <h1><?=$this->title;?></h1>
    <p>Придумайте новый пароль. Пароль должен содержать от 6 до 10 символов.</p>
    <p><strong>*</strong> - Поля, обязательные для заполнения.</p>

    <?php if(Yii::$app->session->hasFlash('warning')):?>
        <div class="alert alert-success"><?=Yii::$app->session->getFlash('warning');?></div>
    <?php endif;?>
    
    <?php $form = ActiveForm::begin(); ?>
        <div class="bg-round">
            <div class="tabl-form">
                <div class="tr">
                    <div class="th">Новый пароль: <span class="star">*</span></div>
                    <div class="td">
                        <?= $form->field($model, 'password',['template' => '{input}{error}'])->passwordInput() ?>
                    </div>
                </div>
            </div>
        </div>

    
        <div class="form-group">
            <?= Html::submitButton('Изменить', ['class' => 'add-to-cart-button']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div>
