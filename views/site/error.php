<?php
/**
 * @var \yii\web\HttpException $exception
 */
?>
<div id="content">
    <div id="content-body">
        <div class="content-columns clearfix">
            <div class="content-left-column">
                <h2 class="sub-menu-title">Помощь</h2>
                <ul class="sub-menu">
                    //sub menu
                </ul>
            </div>
            <div class="content-right-column">
                <div class="article">
                    <h1><?= $exception->statusCode ?> - <?= $exception->getMessage() ?></h1>
                    <p>
                        <b>Уважаемый посетитель сайта!</b>
                        <br>
                        <?php if ($exception->statusCode == 404): ?>
                            Запрашиваемая вами страница не существует либо произошла ошибка.<br>
                            Если вы уверены в правильности указанного адреса, то данная страница уже не существует
                            на сервере или была переименована.<br/><br/>
                        <?php endif; ?>
                        Попробуйте следующее:<br/>
                    <ul>
                        <li>Откройте <a href="/">https://beauti-full.ru</a> (главную страницу сайта) и попробуйте
                            самостоятельно
                            найти нужную вам страницу.
                        </li>
                        <li>Кликните кнопкой "Назад" ("Back") вашего браузера, чтобы вернуться к предыдущей
                            странице
                        </li>
                    </ul>
                    </p>
                </div>
            </div>
        </div>

    </div>

</div>