<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Cache-Control: post-check=0,pre-check=0", false);
header("Cache-Control: max-age=0", false);
header("Pragma: no-cache");
//vd($categories);
?>
<?php if($categories):?>
    <!DOCTYPE yml_catalog SYSTEM "shops.dtd">
    <xml version="1.0" encoding="utf-8">
        <yml_catalog date="<?= date('Y-m-d H:i') ?>">
            <shop>
                <name>Beauti-full.ru</name>
                <company>Beauti-full.ru - интернет-магазин женской одежды</company>
                <url>https://beauti-full.ru/</url>

                <currencies>
                    <currency id="RUR" rate="1"/>
                    <currency id="USD" rate="CBRF"/>
                </currencies>

                <categories>
                    <?php foreach ($categories as $cat):?>
                        <category id="<?=$cat->id ?>"><? echo strip_tags($cat->title); ?></category>
                        <?php if(isset($cat->productCategories) && !empty($cat->productCategories)):?>
                            <?php foreach ($cat->productCategories as $c):?>
                                <category id="<? echo $c->id; ?>"><? echo strip_tags($cat->title), ' ', strip_tags($c->title); ?></category>
                            <?php endforeach;?>
                        <?php endif;?>
                    <?php endforeach;?>
                </categories>

                <?php if(count($products)>0):?>
                    <offers>
                        <?php foreach ($products as $product):?>
                            <offer id="<?= $product->id ?>" available="true">
                                <url><?='https://moda-optom.ru'.$product->getUrl()?></url>
                                <price><?= (!empty($product->wholesale_discount_price) || $product->wholesale_discount_price != 0) ? $product->wholesale_discount_price : $product->wholesale_price ?></price>
                                <currencyId>RUR</currencyId>
                                <categoryId><?= $product->category_id ?></categoryId>
                                <picture><?=\app\helpers\CommonHelper::abcImg($product->getFirstImage()) ?></picture>
                                <delivery>true</delivery>
                                <name><?= $product->title ?></name>
                            </offer>
                        <?php endforeach;?>
                    </offers>
                <?php endif;?>
            </shop>
        </yml_catalog>
    </xml>
<?php endif;?>