<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\SendEmailForm */
/* @var $form ActiveForm */
$this->title = "Восстановление пароля";
?>
<div class="site-sendEmail" style="margin: 30px 20px 0 190px;">
    <h1>Восстановление пароля</h1>
    <p>Введите e-mail адрес указанный Вами при регистрации</p>
    <p><strong>*</strong> - Поля, обязательные для заполнения.</p>

    <?php if(Yii::$app->session->hasFlash('warning')):?>
        <div class="alert alert-success"><?=Yii::$app->session->getFlash('warning');?></div>
    <?php endif;?>
    <?php if(Yii::$app->session->hasFlash('error')):?>
        <div class="alert alert-danger"><?=Yii::$app->session->getFlash('error');?></div>
    <?php endif;?>


    <?php $form = ActiveForm::begin(); ?>
        <div class="bg-round">
            <div class="tabl-form">
                <div class="tr">
                    <div class="th">E-mail: <span class="star">*</span></div>
                    <div class="td">
                        <?= $form->field($model, 'email',['template' => '{input}{error}']) ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Отправить', ['class' => 'add-to-cart-button']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
