<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
$assetBundle = \app\assets\HtmlAsset::register($this);
$model->rememberMe = 1;
?>
<?php
    $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'popup-submit-form'],
        'enableAjaxValidation' => true,
    ]);
?>
<div class="modal-header">
    <a title="Закрыть" class="fancybox-close close" href="#" data-dismiss="modal" aria-label="Close"></a>
    <h1 class="modal-title">Вход</h1>
</div>
<div class="modal-body">
    <div class="icon-block">
        <a href="#"><?php echo Html::img($assetBundle->baseUrl.'/img/FB.png') ?></a>
        <a href="#"><?php echo Html::img($assetBundle->baseUrl.'/img/mail.png') ?></a>
        <a href="#"><?php echo Html::img($assetBundle->baseUrl.'/img/Odnoklasniki.png') ?></a>
        <a href="#"><?php echo Html::img($assetBundle->baseUrl.'/img/VK.png') ?></a>
    </div>
    <p style="text-align: center;">Мы не будем делать публикации от вашего имени</p>
    <?//= $form->errorSummary($model, ['header' => '']); ?>
    <div style="text-align: center; padding: 20px 0; color: #555;">
        ------------------------- или ----------------------------
    </div>

    <?= $form->field($model, 'login')->textInput(['class'=>'my-form-control','placeholder'=>'E-mail'])->label(false)?>
    <?= $form->field($model, 'password')->passwordInput(['class'=>'my-form-control','placeholder'=>'Пароль'])->label(false)?>
    <?= $form->field($model, 'rememberMe')->checkbox() ?>

    <?=Html::a('Забыли пароль?',['/site/send-email']);?>
    <div style="text-align: center; margin-top: 20px;">
        <?= \yii\bootstrap\Html::submitButton('Войти', ['class'=>'btn btn-danger btn-lg']) ?>
    </div>
</div>


<?php ActiveForm::end() ?>