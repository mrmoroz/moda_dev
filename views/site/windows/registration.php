<?php
/**
 * @var RegistrationForm $model
 */

use app\forms\RegistrationForm;
use yii\captcha\Captcha;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
$assetBundle = \app\assets\HtmlAsset::register($this);
$model->country_id = 157;
$form = ActiveForm::begin([
    'id' => 'registration-form',
    'options' => ['class' => 'popup-submit-form'],
    'fieldConfig' => [
        'template' => "{input}{error}",
        'options' => [
            'tag' => false
        ]
    ]
]) ?>
    <div class="modal-header">
        <a title="Закрыть" class="fancybox-close close" href="#" data-dismiss="modal" aria-label="Close"></a>
        <h1 class="modal-title">Регистрация</h1>
    </div>

    <div class="modal-body">
        <?php if($textBlock):?>
            <p style="color:red;"><?=$textBlock->value;?></p>
        <?php endif;?>

        <?= $form->field($model,'login')->textInput(['class'=>'my-form-control','placeholder'=>'E-mail'])->label(false)?>

        <?= $form->field($model,'password')->passwordInput(['class'=>'my-form-control','placeholder'=>'Придумайте пароль'])->label(false)?>

        <?= $form->field($model,'password_confirm')->passwordInput(['class'=>'my-form-control','placeholder'=>'Повторите пароль'])->label(false)?>

        <?= $form->field($model,'first_name')->textInput(['class'=>'my-form-control','placeholder'=>'Ваше имя'])->label(false)?>

        <div style="text-align: center;">
            <?= \yii\bootstrap\Html::submitButton('Зарегистрироваться', ['class'=>'btn btn-danger btn-lg']) ?>
        </div>

        <p>Нажимая на кнопку "Зарегистрироваться", я соглашаюсь с условиями <a href="#">Публичной оферты</a></p>
        <div class="icon-block">
            <a href="#"><?php echo Html::img($assetBundle->baseUrl.'/img/VK.png') ?></a>
            <a href="#"><?php echo Html::img($assetBundle->baseUrl.'/img/FB.png') ?></a>
            <a href="#"><?php echo Html::img($assetBundle->baseUrl.'/img/mail.png') ?></a>
            <a href="#"><?php echo Html::img($assetBundle->baseUrl.'/img/Odnoklasniki.png') ?></a>
        </div>


<?php ActiveForm::end(); ?>


