<div class="page-form-wrap">
    <form name="<?= $form->form_type ?>" data-entity-type="page" data-entity-id="<?= $entityId ?>" id="<?= $form->form_type . '_' . $position ?>">

        <?= $formFields ?>


        <?= $formButtons ?>

    </form>
</div>