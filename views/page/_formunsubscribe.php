<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div style="margin-top: 45px; margin-bottom: 45px;">

<?php $form = ActiveForm::begin(['action'=>'/unsubscribe']); ?>

    <?= $form->field($model, 'email')->textInput(['class'=>'form-control','placeholder'=>'Электронная почта'])->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton('Отписаться' , ['class' => 'btn btn-default btn-lg']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>
