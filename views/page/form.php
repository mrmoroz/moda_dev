<div class="registration-form">
    <strong>Обратная связь</strong>
    <form class="cf_form" name="<?= $form->type ?>" data-entity-type="page" data-entity-id="<?= $entityId ?>" id="<?= $form->type . '_' . $position ?>">
        <div class="clear">&nbsp;</div>
        <?= $this->render('form-fields', [
            'form' => $form
        ]) ?>
        <?= $this->render('form-buttons', [
            'form' => $form
        ]) ?>
    </form>
</div>