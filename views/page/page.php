<?php
    /** @var \app\models\Page $model_page */
?>

<div id="content">
    <div id="content-body">
        <div class="content-columns clearfix">
            <div class="content-single-column">
                <div class="article">
                    <?php use yii\helpers\Html;
                    use yii\widgets\ActiveForm;

                    if ($model_page->show_back_connect_up) {
                        $cnt = count($pageForms);
                        for ($i = 0; $i < $cnt; $i++) {
                            echo $this->render('form', [
                                'entityId' => $model_page->id,
                                'position' => 'top',
                                'form' => $pageForms[$i],
                                'formFields' => $formFields[$i],
                                'formButtons' => $formButtons[$i]
                            ]);
                        }
                    }

                    foreach ($model_page->getPageBlocks()->all() as $pageBlock): ?>
                        <h1 class="header"><?= $pageBlock->title ?></h1>
                        <main>
                            <?= $pageBlock->body ?>
                        </main>
                        <?php $gallery = $pageBlock->getGallery()->one() ?>
                        <?php if ($gallery) : ?>
                            <div class="slider-2-wrapper">
                                <div class="product-slider clearfix">
                                    <?php foreach ($gallery->photos as $photo): ?>
                                        <div class="slide">
                                            <a href="<?= $photo->image ?>" class="fancybox" rel="group">
                                                <?= \pavlinter\display\DisplayImage::widget([
                                                    'width' => $previewConfig['width'],
                                                    'height' => $previewConfig['height'],
                                                    'bgColor' => $previewConfig['color'],
                                                    'image' => $photo->image,
                                                    'category' => 'all'
                                                ]) ?>
                                            </a>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>

                    <? if ($model_page->show_back_connect_down) {
                        $cnt = count($pageForms);
                        for ($i = 0; $i < $cnt; $i++) {
                            echo $this->render('form', [
                                'entityId' => $model_page->id,
                                'position' => 'top',
                                'form' => $pageForms[$i],
                                'formFields' => $formFields[$i],
                                'formButtons' => $formButtons[$i]
                            ]);
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>