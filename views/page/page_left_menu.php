<?
use app\models\Menu;
use yii\helpers\Url;

/** @var \app\models\Page $model_page */

?>
<div class="columns-group clearfix">
    <div class="left-column">
        <div class="content">
            <?php if($leftMenu):?>
                <ul id="sub-menu">
                    <? foreach ($leftMenu as $key => $menuItem) : ?>
                        <?php if($menuItem->menuItem->entity_type!=4):?>
                            <?php
                                if (Yii::$app->request->getPathInfo() == ltrim(Url::to($menuItem->getUrl()), '/')) {
                                    $class = ' class="selected"';
                                } else {
                                    $class = '';
                                }
                            ?>
                            <li>
                                <a href="<?= $menuItem->getUrl() ?>" <?= $class ?>> <?= $menuItem->menuItem->title ?></a>
                            </li>
                            <?php if($key==0):?>
                                <li><a href="/page/brands">Все бренды</a></li>
                            <?php endif;?>
                        <?php endif;?>
                    <? endforeach; ?>
                    <li><a href="/news">Новости</a></li>
                </ul>
            <?php endif;?>
        </div>
    </div>

    <div class="right-column">
        <div class="content">
            <div class="article-content">
                <h1><?= $model_page->title ?></h1>
                <?php if (Yii::$app->session->hasFlash('success_flash')): ?>
                    <?= Yii::$app->session->getFlash('success_flash') ?>
                <?php endif; ?>
                <?php if ($model_page->show_back_connect_up) {
                    echo $this->render('form', [
                        'entityId' => $model_page->id,
                        'position' => 'top',
                        'form' => $model_page->getPluggableForm(),
                    ]);
                } ?>



                <?php foreach ($model_page->getPageBlocks()->all() as $pageBlock): ?>
                    <h2><?= $pageBlock->title ?></h2>
                    <?= $pageBlock->body ?>
                    <?php $gallery = $pageBlock->getGallery()->one() ?>
                    <?php if ($gallery) : ?>
                        <table id="Photos" width="100%">
                            <tbody>
                            <tr>
                                <?php foreach ($gallery->photos as $key => $photo): ?>
                                <td width="33%"><a class="photoblock" href="<?= $photo->image ?>" rel="gallery">
                                            <?= \pavlinter\display\DisplayImage::widget([
                                                'width' => 210,
                                                'height' => 144,
                                                'bgColor' => $previewConfig['color'],
                                                'image' => $photo->image,
                                                'category' => 'all'
                                            ]) ?>
                                        </a>
                                    </div>
                                <?php if (($key + 1) % 3 == 0): ?>
                                </tr>
                                <tr>
                                    <?php endif; ?>
                                    <?php endforeach; ?>
                            </tr>
                            </tbody>
                        </table>
                    <?php endif; ?>
                <?php endforeach; ?>


                <?php if($url=='brands'):?>
                <div class="manufact-filter">
                    <ul class="filters-char">
                        <?php foreach ($latChar as $ch):?>
                            <li class="ch-act" data-ch="<?=$ch?>"><?=$ch?></li>
                        <?php endforeach;?>
                        <li class="ch-act" data-ch="cyr">А-Я</li>
                        <li class="ch-act" data-ch="123">123</li>
                    </ul>
                </div>
                <div class="wrap-content-manufact">
                    <?php if($latGroup):?>
                        <?php foreach ($latGroup as $k=>$lat):?>
                            <div class="item-lat-group block-group-<?=$k?>">
                                <h2 class="title-group"><?=$k?></h2>
                                <?php if($lat):?>
                                    <ul class="links-group">
                                        <?php foreach ($lat as $l):?>
                                            <li><a href="/catalog/collection/<?=$l[1]?>"><?=$l[0]?></a></li>
                                        <?php endforeach;?>
                                    </ul>
                                <?php endif;?>
                            </div>
                        <?php endforeach;?>
                    <?php endif;?>
                    <?php if($numberGroup):?>
                        <?php foreach ($numberGroup as $k=>$num):?>
                            <div class="item-lat-group block-group-123">
                                <h2 class="title-group">123</h2>
                                <?php if($num):?>
                                    <ul class="links-group">
                                        <?php foreach ($num as $n):?>
                                            <li><a href="/catalog/collection/<?=$n[1]?>"><?=$n[0]?></a></li>
                                        <?php endforeach;?>
                                    </ul>
                                <?php endif;?>
                            </div>
                        <?php endforeach;?>
                    <?php endif;?>
                    <?php if($cyrilGroup):?>
                        <?php foreach ($cyrilGroup as $k=>$cyr):?>
                            <div class="item-lat-group block-group-cyr">
                                <h2 class="title-group">А-Я</h2>
                                <?php if($cyr):?>
                                    <ul class="links-group">
                                        <?php foreach ($cyr as $c):?>
                                            <li><a href="/catalog/collection/<?=$c[1]?>"><?=$c[0]?></a></li>
                                        <?php endforeach;?>
                                    </ul>
                                <?php endif;?>
                            </div>
                        <?php endforeach;?>
                    <?php endif;?>
                </div>
            <?php endif;?>


                <?php if ($model_page->show_back_connect_down) {
                    echo $this->render('form', [
                        'entityId' => $model_page->id,
                        'position' => 'bottom',
                        'form' => $model_page->getPluggableForm(),
                    ]);
                } ?>

            </div>
        </div>
        
        <div class="MessBlock">
            <?php if($messages):?>
                <? foreach ($messages as $message) : ?>
                    <div id="Quote" style="margin-bottom: 10px;">
                        <div><?=date("d.m.Y", strtotime($message->date_send))?> <?=$message->title?></div>
                        <? foreach ($message->fields as $field) : ?>
                            <? if ($field->field->published && $field->field_id!=1) : ?>
                                <div class="Date"><?= $field->value ?></div>
                            <? endif; ?>
                        <? endforeach; ?>
                    </div>
                <? endforeach ?>
            <?endif;?>
        </div>

        <?php
            if(count($pageForms)>0) {
                $cnt = count($pageForms);
                for ($i = 0; $i < $cnt; $i++) {
                    echo $this->render('formRev', [
                        'entityId' => $model_page->id,
                        'position' => 'top',
                        'form' => $pageForms[$i],
                        'formFields' => $formFields[$i],
                        'formButtons' => $formButtons[$i]
                    ]);
                }
            }
        ?>


        <?php if($url=='unsubscribe-mo'):?>
            <?php echo $this->render('_formunsubscribe',['url' => $url, 'model' => new \app\forms\Unsubsribe()]);?>
        <?php endif;?>
    </div>
</div>

<?php
$script = <<<JS
$(function() {
    $('.ch-act').click(function() {
        var id = $(this).data('ch');
        $('.ch-act').removeClass('active');
        $(this).addClass('active');
        $('.item-lat-group').hide();
        $('.block-group-'+id).show();
    });
});
JS;
$this->registerJS($script);
?>
