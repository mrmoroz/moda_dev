<?php
use yii\helpers\Url;
$this->title = 'МОДАОПТОМ | НОВОСТИ';
?>
<div class="columns-group clearfix">
    <div class="left-column">
        <div class="content">
            <?php if($this->params['main-menu']):?>
            <ul id="sub-menu">
                <? foreach ($this->params['main-menu'] as $menuItem) : ?>
                    <?php if($menuItem->menuItem->entity_type!=4):?>
                        <?
                        $url = $menuItem->getUrl();
                        if (Yii::$app->request->getPathInfo() == ltrim(Url::to($url), '/')) {
                            $class = ' class="selected"';
                        } else {
                            $class = '';
                        }
                        ?>
                        <li>
                            <a href="<?= $url ?>" <?= $class ?>> <?= $menuItem->menuItem->title ?></a>
                        </li>
                    <?php endif;?>
                <? endforeach; ?>
                <li><a href="/news">Новости</a></li>
            </ul>
            <?php endif;?>
        </div>
    </div>
    <div class="right-column">
        <div class="content">
            <?php if($news):?>
                <h1>Новости</h1>
                <?php foreach ($news as $it):?>
                    <div class="news-item">
                        <?php if(!empty($it->img)):?>
                        <div class="news-item-image">
                            <a href="/news/<?=$it->url?>"><img src="http://beautifull.loc/<?=$it->img?>" style="width: 700px;"></a>
                        </div>
                        <?php endif;?>
                        <a class="news-item-title" href="/news/<?=$it->url?>"><h2 style="margin-bottom: 0;"><?=$it->title?></h2></a>
                        <div class="news-item-date"><?=date('d.m.Y',strtotime($it->create_date))?></div>
                        <div class="details-bar">
                            <a class="details-link" href="/news/<?=$it->url?>">Подробнее</a>
                        </div>
                    </div>
                <?php endforeach;?>
            <?php else:?>
                <h1>Нет новостей</h1>
            <?php endif;?>
        </div>
    </div>
</div>
