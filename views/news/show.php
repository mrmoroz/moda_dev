<?php
use yii\helpers\Url;
$this->title = 'МОДАОПТОМ | НОВОСТИ';
?>
<div class="columns-group clearfix">
    <div class="left-column">
        <div class="content">
            <?php if($this->params['main-menu']):?>
                <ul id="sub-menu">
                    <? foreach ($this->params['main-menu'] as $menuItem) : ?>
                        <?php if($menuItem->menuItem->entity_type!=4):?>
                            <?
                            $url = $menuItem->getUrl();
                            if (Yii::$app->request->getPathInfo() == ltrim(Url::to($url), '/')) {
                                $class = ' class="selected"';
                            } else {
                                $class = '';
                            }
                            ?>
                            <li>
                                <a href="<?= $url ?>" <?= $class ?>> <?= $menuItem->menuItem->title ?></a>
                            </li>
                        <?php endif;?>
                    <? endforeach; ?>
                    <li><a href="/news">Новости</a></li>
                </ul>
            <?php endif;?>
        </div>
    </div>
    <div class="right-column">
        <div class="content">
            <?php if($news):?>
                <h1>Новости</h1>
                <div id="catalog-filter-block">
                    <div id="section-buttons">
                        <a class="back-link" onclick="window.history.back();return false;" href="#">Назад</a>
                    </div>
                </div>
                <?php if(!empty($news->img)):?>
                    <div class="news-item-image">
                        <img src="http://beautifull.loc/<?=$news->img?>" style="width: 700px;">
                    </div>
                <?php endif;?>
                <h2 style="margin-bottom: 0;"><?=$news->title?></h2>
                <div class="news-item-date"><?=date('d.m.Y',strtotime($news->create_date))?></div>
                <div id="text-block" style="margin-bottom: 30px;">
                    <div id="text-body">
                        <?=$news->body?>
                    </div>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>
