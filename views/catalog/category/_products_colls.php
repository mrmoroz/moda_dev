<?
if ($pageType == 'collection') {
    $productUrl = '/catalog/collection/'.$catalogItem->id . '/';
} elseif ($pageType == 'tag') {
    $productUrl = '/catalog/tag/'.$catalogItem->id . '/';
} else {
    if ($catalogItem->parent) {
        //$productUrl = $catalogItem->parent->url . '/' . $catalogItem->url . '/';
        $productUrl = $currentSizesUrl . '/';
    } else {
        $productUrl = $currentSizesUrl . '/';
    }
}
?>

<?php foreach ($products as $product): ?>
    <li data-prod="<?= $product->id ?>">
        <?= $this->render('_product-card', [
            'product' => $product,
            'productUrl' => $productUrl,
            'new42'=>$new42,
            'new56'=>$new56,
            'hit'=>$hit,
            'isComing'=>$isComing,
            'pageType'=>$pageType,
            'labels' => isset($labels[$product->id]) ? $labels[$product->id] : null
        ]) ?>
    </li>
<?php endforeach; ?>