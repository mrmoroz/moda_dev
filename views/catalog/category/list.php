<?php
/**
 * @var \app\models\ProductCategory $category
 * @var \yii\data\ActiveDataProvider $dataProvider
 */
/** @var $products[] Product */
/** @var $this \yii\base\View */
use app\components\pagination\CatalogLinkPager;

if(empty($this->title)) {
    $this->title = 'МОДАОПТОМ';
}
//vd($catalogItem);
?>

<div class="left-column">
    <div class="content">
        <?= $this->render('//_colls/catalogColl', array(
            'pageType' => $pageType,
            'catalogItem' => $catalogItem,
            'sizeType' => $sizeType,
            'tagsListTop' => $tagsListTop,
            'tagsListBottom' => $tagsListBottom,
            'collectionList' => $collectionList,
            'categoriesList' => $categoriesList
        )); ?>
    </div>
</div>
<div class="right-column">
    <input type="hidden" class="ajax-href" value="<?= $currentSizesUrl ?>">
    <? if (!empty($pages)) : ?>
        <?php //var_dump($pages->pagination); ?>
        <input type="hidden" class="ajax-current-page" value="<?= $pages->pagination->getPage() + 1 ?>">
        <input type="hidden" class="ajax-page" value="<?= $pages->pagination->getPage() + 2 ?>">
        <input type="hidden" class="ajax-max-page" value="<?= $pages->pagination->pageCount ?>">
    <? endif; ?>
    <input type="hidden" class="ajax-size-type" value="<?= $sizeType ?>">
    <input type="hidden" class="show-all-mode-status" value="0">

    <? /*\yii\widgets\Pjax::begin([
        'timeout' => '5000',
        'id' => 'catalog-items-container'
    ]) */?>
    <div class="article-content">
        <div class="content">
            <h1 id="items-set-header">
                <?= $titleH1 ?>
            </h1>
            <? if (!empty($catalogItem->top_text_wholesale) && $pageType == 'tag') : ?>
                <div>
                    <?= $catalogItem->top_text_wholesale ?>
                </div>
            <? endif; ?>
            <div id="catalog-filter-block">
                <div class="column1">
                    <div id="sizes-filter">
                        <? if (!empty($productSearch) && count($sizeChartSizes) > 0) : ?>
                            <?= $this->render('../filters', [
                                'productSearch' => $productSearch,
                                'sizeChartSizes' => $sizeChartSizes,
                            ]) ?>
                        <? endif; ?>
                    </div>

                </div>

                <div class="column2">
                    <div class="catalog-pager">
                        <? if (!empty($pages)) : ?>
                            <?= CatalogLinkPager::widget([
                                'pagination' => $pages->pagination,
                            ]); ?>
                        <? endif; ?>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>

        <ul id="catalog-items-list" class="catalog-items clearfix">
            <? if (!empty($products)) : ?>
                <?= $this->render('_products_colls', [
                    'products' => $products,
                    'pageType' => $pageType,
                    'catalogItem' => $catalogItem,
                    'new42'=>$new42,
                    'new56'=>$new56,
                    'hit'=>$hit,
                    'isComing'=>$isComing,
                    'currentSizesUrl' => $currentSizesUrl,
                    'labels'=>$labels
                ]) ?>
            <? else:?>
                <p>Товары не найдены</p>
            <? endif;?>
        </ul>


        <div class="content">
            <div id="catalog-filter-block2">
                <div class="catalog-pager">
                    <? if (!empty($pages)) : ?>
                        <?= CatalogLinkPager::widget([
                            'pagination' => $pages->pagination,
                        ]); ?>
                    <? endif; ?>
                </div>
            </div>
            <? if (!empty($catalogItem->bottom_text_wholesale) && $pageType == 'tag') : ?>
                <div>
                    <?= $catalogItem->bottom_text_wholesale ?>
                </div>
            <? endif; ?>
        </div>
    </div>
    <?// \yii\widgets\Pjax::end() ?>
</div>