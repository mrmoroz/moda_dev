<?php
use app\models\Product;
use app\models\UserFavourite;
use pavlinter\display\DisplayImage;
use yii\helpers\ArrayHelper;

/**
 * @var Product $product
 */
?>
<? if (!empty($_COOKIE['mp'])) : ?>
    <? if ($_COOKIE['mp'] == 2) : ?>
        <a class="product-rating"><?= (int)$product->rating ?> (<?= (int)$product->tmp_rating ?>)</a>
    <? elseif ($_COOKIE['mp'] == 4) : ?>
        <a class="product-rating"><?= (int)$product->second_rating ?> (<?= (int)$product->second_tmp_rating ?>)</a>
    <? endif; ?>
<? endif; ?>

<?php if($pageType=='search'):?>
<a class="catalog-item-link" href="<?= $product->getUrl(); ?>">
<?php else:?>
    <?php
        //vd($productUrl,false);
        //vd($product->url,false);
    ?>
<a class="catalog-item-link" href="<?= $productUrl . $product->url ?>">
<?php endif;?>
    <div class="prod-card-size-hover">
        <p>Размеры в наличии:</p>
        <?php
            $mass_sizes = [];
            $real_size = [];
            $mass_sizes = ArrayHelper::getColumn($product->getAvailableSizes(), 'name');
            if(count($mass_sizes)>0){
                foreach ($mass_sizes as $size){
                    if(!in_array($size, $real_size)){
                        $real_size[] = $size;
                    }
                }
                sort($real_size, SORT_NUMERIC);
                echo '<span>'.implode(', ', $real_size).'</span>';
            }
        ?>

    </div>

    <?php 
        /*$url = "/display-images-cache/default/220x330_static_ffffff_0/default.jpg";
        if($img = $product->getFirstImage()){
            //vd($img,false);
            //$url = "http://dev.beauti-full.ru/filemanager/get-preview-by-img-size?url=".$img."&category=all&width=220&height=330&color=ffffff";
            $url = $img;
        }*/
    ?>

    <!--img src="<?//='http://beautifull.loc'.$url?>" alt="" /-->
    <img src="<?=$product->getPrev($product->getFirstImage())?>" alt="" />

    <div class="supertags-bar">
        <?php if(!empty($product->wholesale_discount)):?>
            <span class="supertag red">-<?=$product->wholesale_discount?>%</span>
        <?php endif;?>
        <?php if(($new42 && in_array($product->id, $new42)) || ($new56 && in_array($product->id, $new56)) || $product->is_new)://if($product->isNew() || $product->isBigNew()):?>
            <span class="supertag">Новинка</span>
        <?php endif;?>
<!--        --><?php //if($hit && in_array($product->id, $hit))://if($product->isHit()):?>
<!--            <span class="supertag">Хит продаж</span>-->
<!--        --><?php //endif;?>
<!--        --><?php //if($isComing && in_array($product->id, $isComing))://if($product->isComing()):?>
<!--            <span class="supertag">Скоро в продаже</span>-->
<!--        --><?php //endif;?>
        <?php if($labels):?>
            <?php //vd($labels, false);?>
            <?php foreach ($labels as $label):?>
                <?php if(!empty($label['label_text']) && !empty($label['label_color'])):?>
                    <span class="supertag" style="background: <?=$label['label_color'].';'?>"><?=$label['label_text']?></span>
                <?php endif;?>
            <?php endforeach;?>
        <?php endif;?>
    </div>
</a>

<div style="text-transform: uppercase; height: auto;"
     class="catalog-item-name"><?= $product->title ?></div>
<div class="catalog-item-articul">Артикул:<?= $product->article ?></div>
<?php if(!Yii::$app->user->isGuest || Yii::$app->params['openPrice']):?>
<div class="catalog-item-articul">
    Цена:
    <?php if($product->wholesale_discount_price):?>
        <strong class="item-price-old" style="color:#b80084"><?=$product->wholesale_price?> руб.</strong>
        <span class="item-price-current"><?=$product->wholesale_discount_price?> руб.</span>
    <?php else:?>
        <strong style="color:#b80084"><?=$product->wholesale_price?> руб.</strong>
    <?php endif;?>
</div>
<?php endif;?>
