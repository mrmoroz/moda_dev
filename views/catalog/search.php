<?php
/**
 * @var \app\models\ProductCategory $category
 * @var \yii\data\ActiveDataProvider $dataProvider
 */
use app\components\pagination\CatalogLinkPager;
use yii\widgets\ListView;

?>
<div id="content-body">
    <div class="content-columns clearfix">
        <div class="content-left-column"></div>
        <div class="content-right-column">
            <ul id="breadcrumbs" class="clearfix">
            </ul>
            <h1>
                РЕЗУЛЬТАТЫ ПОИСКА ПО ЗАПРОСУ: <?= $searchRequest ?>
            </h1>
            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'layout' => "{pager}\n<ul class='catalog-items clearfix'>{items}</ul>\n{pager}",
                'itemView' => 'category/_product-card',
                'itemOptions' => [
                    'tag' => 'li',
                ],
                'pager' => [
                    'class' => CatalogLinkPager::className(),
                ],
                'emptyText' => "По данному запросу товаров не найдено.",
            ])
            ?>
        </div>
    </div>
</div>