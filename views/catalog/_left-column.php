<?php

use app\models\Menu;
use app\models\Tag;
use app\models\UserFavourite;
use yii\db\Expression;
use yii\helpers\Url;

$bundle = \app\assets\HtmlAsset::register($this);

$leftMenu = Menu::find()->with('menuItem')->where(['menu_id' => '3'])
    ->andWhere(['parent_id' => null])
    ->orderBy([new Expression('position IS NULL ASC, position ASC')])
    ->all();
$categories = \Yii::$app->categoryService->getCategoriesTree(true, false, 1);
?>

<h2 class="sub-menu-favourites-title">
    <a href="/<?= $this->params['category-type'] ?>/favourites">
        <img class="img-fav-small-icon" src="<?= $bundle->baseUrl ?>/img/fav_on_30_3.png">
        Избранное (<span id="fav-counter"><?= UserFavourite::getQuantityFavourites() ?></span>)
    </a>
</h2>
<?php foreach ($leftMenu as $menu) : ?>
    <? switch ($menu->menuItem->entity_type) :
        case 0 : ?>
            <h2 class="sub-menu-title" data-cls="<?= $menu->id ?>"><?= $menu->menuItem->title ?></h2>
            <?
            $childItems = Menu::find()->where(['parent_id' => $menu->id])->all();
            ?>
            <? if ($menu->menuItem->title == "Акции") {
                $style = '';
            } else {
                $style = 'display: none;';
            } ?>
            <ul class="sub-menu <?= $menu->id?>" style="<?= $style ?>">
                <?php foreach ($childItems as $child) : ?>
                    <?
                    $url = $child->getUrl();
                    $active = Yii::$app->request->getPathInfo() == ltrim(Url::to($url), '/');
                    ?>
                    <li>
                        <a href="<?= $url ?>" <?= $active ? 'class="active"' : ''?>>
                            <?= $child->menuItem->title ?>
                        </a>
                    </li>
                <? endforeach; ?>
            </ul>
            <? break; ?>
        <? case 4: ?>
            <? switch ($this->params['category-type']) :
                case "zhenskaja" : ?>
                    <h2 class="sub-menu-title" data-cls="<?= $menu->id ?>">Большие размеры</h2>
                    <ul class="sub-menu <?= $menu->id ?>"
                        <? if ($this->params['size-type'] != "bolshie-razmery") {echo 'style="display: none;"';} ?>>
                        <?php foreach (Yii::$app->categoryService->getBigWomanCategories() as $category): ?>
                            <li>
                                <?php $categoryActive = ($this->params['size-type'] == "bolshie-razmery" && Yii::$app->request->get('category')['id'] == $category->id || Yii::$app->request->get('subCategory')['id'] == $category->id); ?>
                                <a class="<?php if ($categoryActive && !Yii::$app->request->get('subCategory')): ?>active<?php endif; ?>"
                                   href="<?= $category->getBigUrl() ?>">
                                    <?= $category->title ?>
                                </a>
                                <?php if ($categoryActive && count($category->productCategories) > 0): ?>
                                    <ul>
                                        <?php foreach ($category->productCategories as $childCategory): ?>
                                            <li>
                                                <?php $childCategoryActive = ($this->params['size-type'] != "bolshie-razmery" && Yii::$app->request->get('subCategory') && Yii::$app->request->get('subCategory')->id == $childCategory->id); ?>
                                                <a class="<?php if ($childCategoryActive && !Yii::$app->request->get('subCategory')): ?>active<?php endif; ?>"
                                                   href="<?= $childCategory->getBigUrl() ?>">
                                                    <?= $childCategory->title ?>
                                                </a>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                    <h2 class="sub-menu-title" data-cls="<?= $menu->id ?>">Стандартные размеры</h2>
                    <ul class="sub-menu <?= $menu->id?>"
                        <? if ($this->params['size-type'] != "bazovie-razmery") {echo 'style="display: none;"';} ?>>
                        <?php foreach (Yii::$app->categoryService->getStandardWomanCategories() as $category): ?>
                            <li>
                                <?php $categoryActive = ($this->params['size-type'] != "bolshie-razmery" && Yii::$app->request->get('category')['id'] == $category->id || Yii::$app->request->get('subCategory')['id'] == $category->id); ?>
                                <a class="<?php if ($categoryActive && !Yii::$app->request->get('subCategory')): ?>active<?php endif; ?>"
                                   href="<?= $category->getStandardUrl() ?>">
                                    <?= $category->title ?>
                                </a>
                                <?php if ($categoryActive && count($category->productCategories) > 0): ?>
                                    <ul>
                                        <?php foreach ($category->productCategories as $childCategory): ?>
                                            <li>
                                                <?php $childCategoryActive = (Yii::$app->request->get('subCategory') && Yii::$app->request->get('subCategory')->id == $childCategory->id); ?>
                                                <a class="<?php if ($childCategoryActive): ?>active<?php endif; ?>"
                                                   href="<?= $childCategory->getStandardUrl() ?>">
                                                    <?= $childCategory->title ?>
                                                </a>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                    <h2 class="sub-menu-title" data-cls="<?= $menu->id ?>">Аксессуары</h2>
                    <ul class="sub-menu <?= $menu->id?>"
                        <? if ($this->params['size-type'] != "vse-razmery") {echo 'style="display: none;"';} ?>>
                        <?php foreach (Yii::$app->categoryService->getDimensionlessWomanCategories() as $category): ?>
                            <li>
                                <?php $categoryActive = ($this->params['size-type'] != "bolshie-razmery" && Yii::$app->request->get('category')['id'] == $category->id || Yii::$app->request->get('subCategory')['id'] == $category->id); ?>
                                <a class="<?php if ($categoryActive && !Yii::$app->request->get('subCategory')): ?>active<?php endif; ?>"
                                   href="<?= $category->getUrl() ?>">
                                    <?= $category->title ?>
                                </a>
                                <?php if ($categoryActive && count($category->productCategories) > 0): ?>
                                    <ul>
                                        <?php foreach ($category->getProductCategories(true)->all() as $childCategory): ?>
                                            <li>
                                                <?php $childCategoryActive = (Yii::$app->request->get('subCategory') && Yii::$app->request->get('subCategory')->id == $childCategory->id); ?>
                                                <a class="<?php if ($childCategoryActive): ?>active<?php endif; ?>"
                                                   href="<?= $childCategory->getUrl() ?>">
                                                    <?= $childCategory->title ?>
                                                </a>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                    <? break; ?>
                <? case "muzhskaja" : ?>
                    <h2 class="sub-menu-title" data-cls="<?= $menu->id ?>">Категории товаров</h2>
                    <ul class="sub-menu <?= $menu->id?>">
                        <?php foreach (Yii::$app->categoryService->getManCategories() as $category): ?>
                            <li>
                                <?php $categoryActive = (Yii::$app->request->get('category')['id'] == $category->id || Yii::$app->request->get('subCategory')['id'] == $category->id); ?>
                                <a class="<?php if ($categoryActive && !Yii::$app->request->get('subCategory')): ?>active<?php endif; ?>"
                                   href="<?= $category->getUrl() ?>">
                                    <?= $category->title ?>
                                </a>
                                <?php if ($categoryActive && count($category->productCategories) > 0): ?>
                                    <ul>
                                        <?php foreach ($category->productCategories as $childCategory): ?>
                                            <li>
                                                <?php $childCategoryActive = (Yii::$app->request->get('subCategory') && Yii::$app->request->get('subCategory')->id == $childCategory->id); ?>
                                                <a class="<?php if ($childCategoryActive): ?>active<?php endif; ?>"
                                                   href="<?= $childCategory->getUrl() ?>">
                                                    <?= $childCategory->title ?>
                                                </a>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                    <?break ?>
                <? case "detskaja" : ?>
                    <h2 class="sub-menu-title" data-cls="<?= $menu->id ?>">Категории товаров</h2>
                    <ul class="sub-menu <?= $menu->id?>">
                        <?php foreach (Yii::$app->categoryService->getChildrenCategories() as $category): ?>
                            <li>
                                <?php $categoryActive = (Yii::$app->request->get('category')['id'] == $category->id || Yii::$app->request->get('subCategory')['id'] == $category->id); ?>
                                <a class="<?php if ($categoryActive && !Yii::$app->request->get('subCategory')): ?>active<?php endif; ?>"
                                   href="<?= $category->getUrl() ?>">
                                    <?= $category->title ?>
                                </a>
                                <?php if ($categoryActive && count($category->productCategories) > 0): ?>
                                    <ul>
                                        <?php foreach ($category->productCategories as $childCategory): ?>
                                            <li>
                                                <?php $childCategoryActive = (Yii::$app->request->get('subCategory') && Yii::$app->request->get('subCategory')->id == $childCategory->id); ?>
                                                <a class="<?php if ($childCategoryActive): ?>active<?php endif; ?>"
                                                   href="<?= $childCategory->getUrl() ?>">
                                                    <?= $childCategory->title ?>
                                                </a>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                    <?break ?>
                <? case "dom" : ?>
                    <h2 class="sub-menu-title" data-cls="<?= $menu->id ?>">Категории товаров</h2>
                    <ul class="sub-menu <?= $menu->id?>">
                        <?php foreach (Yii::$app->categoryService->getHomeCategories() as $category): ?>
                            <li>
                                <?php $categoryActive = (Yii::$app->request->get('category')['id'] == $category->id || Yii::$app->request->get('subCategory')['id'] == $category->id); ?>
                                <a class="<?php if ($categoryActive && !Yii::$app->request->get('subCategory')): ?>active<?php endif; ?>"
                                   href="<?= $category->getUrl() ?>">
                                    <?= $category->title ?>
                                </a>
                                <?php if ($categoryActive && count($category->productCategories) > 0): ?>
                                    <ul>
                                        <?php foreach ($category->productCategories as $childCategory): ?>
                                            <li>
                                                <?php $childCategoryActive = (Yii::$app->request->get('subCategory') && Yii::$app->request->get('subCategory')->id == $childCategory->id); ?>
                                                <a class="<?php if ($childCategoryActive): ?>active<?php endif; ?>"
                                                   href="<?= $childCategory->getUrl() ?>">
                                                    <?= $childCategory->title ?>
                                                </a>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                    <?break ?>
                <? endswitch; ?>
            <? break; ?>
        <? case 5: ?>
            <ul class="sub-menu Tags">
                <?
                $redHighlight = false;
                $url = $menu->getUrl();
                $active = Yii::$app->request->getPathInfo() == ltrim(Url::to($url), '/');

                $tag = Tag::findOne($menu->menuItem->entity_id);
                if ($tag->highlight_in_red) {
                    $redHighlight = true;
                }
                unset($tag);
                ?>
                <li>
                    <a href="<?= $url; ?>" class="<?= $active ? 'active ' : ''?><?= $redHighlight ? 'red-color' : '' ?>">
                        <?= $menu->menuItem->title; ?>
                    </a>
                </li>
            </ul>
            <? break; ?>
        <? default : ?>
            <ul class="sub-menu Single">
                <?
                $url = $menu->getUrl();
                $active = Yii::$app->request->getPathInfo() == ltrim(Url::to($url), '/');
                ?>
                <li>
                    <a href="<?= $url; ?>" class="<?= $active ? 'active ' : ''?>">
                        <?= $menu->menuItem->title; ?>
                    </a>
                </li>
            </ul>
            <? break; ?>
    <? endswitch; ?>
<? endforeach; ?>