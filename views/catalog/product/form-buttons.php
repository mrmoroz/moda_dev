<? if (isset($quickAnswerForm->formSettings)) : ?>
    <? foreach ($quickAnswerForm->formSettings->buttons as $button) : ?>
        <div style="text-align:center; margin-bottom: 10px;">
            <a href="#" class="callback-button send-callback-form">
                <?= $button->name ?>
            </a>
        </div>
    <? endforeach; ?>
<? endif; ?>
