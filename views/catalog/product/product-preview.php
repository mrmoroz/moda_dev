<?php
/**
 * @var \app\models\Product $product
 */
use app\components\pagination\CatalogLinkPager;
use app\models\Color;
use app\models\ProductSearch;
use app\models\Size;
use pavlinter\display\DisplayImage;
use yii\helpers\Html;
use yii\widgets\ListView;

$bundle = \app\assets\HtmlAsset::register($this);
?>

<div id="content">
    <div id="content-body">
        <div class="content-columns clearfix">
            <div class="content-right-column">
                <div class="coda-slider-wrapper">
                    <div class="catalog-item-thumbs-column">
                        <ul class="catalog-item-thumbs">
                            <?php foreach ($product->gallery as $key => $gallery): ?>
                                <li>
                                    <a class="xtrig" href="#<?= $key + 1 ?>" rel="coda-slider-1">
                                        <?= \pavlinter\display\DisplayImage::widget([
                                            'width' => 50,
                                            'height' => 75,
                                            'image' => $gallery->path,
                                            'category' => 'all'
                                        ]) ?>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <div class="coda-slider" id="coda-slider-1">
                        <?php foreach ($product->gallery as $key => $photo): ?>
                            <div class="panel">
                                <div class="catalog-item-image-column">
                                    <div class="jzoom" title="<?= $product->article ?>" data-big-url="<?= $photo->path ?>" style="position: relative; overflow: hidden;">
                                        <?= \pavlinter\display\DisplayImage::widget([
                                            'width' => 305,
                                            'height' => 460,
                                            'image' => $photo->path,
                                            'category' => 'all'
                                        ]) ?>
                                    </div>
                                </div>
                            </div>
                        <? endforeach; ?>
                    </div>
                </div>
                <div class="catalog-item-params-column">
                    <h1>
                        <div class="catalog-item-title-big"><?= $product->title ?> <?= $product->article ?></div>
                    </h1>
                    <div class="catalog-item-price-big">
                        <?php if ($product->discount_price): ?>
                            <span class="catalog-item-old-price"><?=$product->getPrice($product->price,$this->params['kurs_many'])?> <?=$product->getSymbol($this->params['country'])?></span>
                            <?=$product->getPrice($product->discount_price, $this->params['kurs_many'])?> <?=$product->getSymbol($this->params['country'])?>
                            <strong style="text-decoration: none; color: red; font-size: 11px">
                                Экономия <?= $product->getPrice($product->price,$this->params['kurs_many']) - $product->getPrice($product->discount_price, $this->params['kurs_many']) ?> <?=$product->getSymbol($this->params['country'])?>
                            </strong>
                        <?php else: ?>
                            <?=$product->getPrice($product->price,$this->params['kurs_many'])?> <?=$product->getSymbol($this->params['country'])?>
                        <?php endif; ?>
                    </div>
                    <div class="catalog-item-description-1 clearfix">
                        <? if (!empty($product->gift)) : ?>
                            <div class="giftContainer">
                                <div>
                                    <a target="_blank" href="<?= $product->gift->getUrl() ?>" style="text-decoration:none">
                                        <span class="giftHeader"> + ПОДАРОК!</span>
                                    </a>
                                    <? if (!empty($product->gift->getFirstImage())) : ?>
                                        <a href="<?= $product->gift->getFirstImage()?>" class="fancybox">
                                            <?= \pavlinter\display\DisplayImage::widget([
                                                'width' => 85,
                                                'height' => 127,
                                                'image' => $product->gift->getFirstImage(),
                                                'category' => 'all'
                                            ]) ?>
                                        </a>
                                    <? endif; ?>
                                    <a target="_blank" href="<?= $product->gift->getUrl() ?>" style="text-decoration:none">
                                        <span class="giftName"><?= $product->gift->title ?></span>
                                    </a>
                                </div>
                            </div>
                        <? endif; ?>
                        <? if ($product->description) : ?>
                            <?= $product->description ?>
                        <? endif ?>
                        <? if ($product->additional_description_retail) : ?>
                            <?= $product->additional_description_retail ?>
                        <? endif ?>
                        <?
                        switch ($product->color_size_note) {
                            case 1:
                                echo "Оттенок цвета может незначительно отличаться от фото.";
                                break;
                            case 2:
                                echo "Оттенок цвета и расположение рисунка могут незначительно отличаться от фото.";
                                break;
                            default :
                                break;
                        }
                        ?>
                    </div>
                    <? $stocks = $product->getStockPositions(); ?>
                    <div class="catalog-item-param-block">
                        <div class="catalog-item-param-title">Цвет / артикул:</div>
                        <div class="catalog-item-color-selector">
                            <div class="catalog-item-selected-color">
                                <div class="catalog-item-articul"><?= $product->article ?></div>
                                <div class="catalog-item-color-title">
                                    <? if (!isset($stocks)) : ?>
                                        Нет доступных цветов
                                    <? else : ?>
                                        <? $checkedColor = false; ?>
                                        <? $shownColors = []; ?>
                                        <? foreach ($stocks as $stock) : ?>
                                            <? foreach ($stock as $colorId => $sizes) : ?>
                                                <? if ($colorId != 'name' && $colorId != 'id' && !in_array($colorId, $shownColors)) : ?>
                                                    <? $shownColors[] = $colorId; ?>
                                                    <input type="radio" id="color_<?= $colorId ?>"
                                                        <? if (!$checkedColor) {
                                                            echo 'checked ';
                                                            $checkedColor = true;
                                                        } ?>
                                                           name="color" value="<?= $colorId ?>">
                                                    <label for="color_<?= $colorId ?>">
                                                        <?= Color::getName($colorId) ?>
                                                    </label>
                                                    <br>
                                                <? endif; ?>
                                            <? endforeach ?>
                                        <? endforeach; ?>
                                    <? endif ?>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="catalog-item-param-block">
                        <a class="sizes-grid fancybox" href="#SizesTable" style="display:block; float:right;">Размерная
                            сетка</a>

                        <div class="catalog-item-param-title">Размеры:</div>
                        <? if (!isset($stocks)) : ?>
                            Нет доступных размеров
                        <? else : ?>
                            <ul class="catalog-item-sizes clearfix">
                                <? $shownSizes = false; ?>
                                <? foreach ($stocks as $stock) : ?>
                                    <? foreach ($stock as $colorId => $sizes) : ?>
                                        <? if ($colorId != 'name' && $colorId != 'id') : ?>
                                            <div class="color-sizes" data-sizes-color-id="<?= $colorId ?>"
                                                <? if (!$shownSizes) {
                                                    $shownSizes = true;
                                                } else { echo "style='display: none'"; } ?>>
                                                <? foreach ($sizes as $sizeId => $quantity) : ?>
                                                    <li>
                                                        <a href="#" class="prod_sizes_label" data-size="<?= $sizeId ?>">
                                                            <?= Size::getName($sizeId) ?>
                                                        </a>
                                                    </li>
                                                    <input type="checkbox" class="size_ch" name="size"
                                                           value="<?= $sizeId ?>" style="display: none"
                                                           id="ch_size_<?= $sizeId ?>">
                                                <? endforeach; ?>
                                            </div>
                                        <? endif; ?>
                                    <? endforeach ?>
                                <? endforeach; ?>
                            </ul>
                        <? endif ?>
                    </div>
                    <button class="add-to-cart-button action-add-to-cart" data-id="<?=$product->id;?>">Добавить покупку в корзину</button>
                    <div class="catalog-item-description">
                        <p>
                            <? $clothType = $product->getClothType() ?>
                            <? if (isset($clothType)) : ?>
                                Тип ткани: <?= $clothType->name ?>
                            <? endif; ?>
                            <br>
                            <? if ($product->model_growth) : ?>
                                Рост модели: <?= $product->model_growth ?>
                            <? endif; ?>
                            <br>
                            <? if ($product->model_size) : ?>
                                Размер модели: <?= $product->model_size ?>
                            <? endif; ?>
                            <br>
                            <? if ($product->composition) : ?>
                                Состав: <?= $product->composition ?>
                            <? endif; ?>
                            <br>
                            <? $manufacturer = $product->getManufacturer() ?>
                            <? if (isset($manufacturer)) : ?>
                                Производитель: <?= $manufacturer->name ?>
                            <? endif; ?>
                        </p>
                    </div>
                    <br><br>
                    <div class="icon-delivery"></div>
                    <a href="#ItemPost" class="sizes-grid fancybox" >Бесплатная доставка почтой</a>
                </div>
            </div>
            <div class="content-rightest-column">
                <div class="recommends-block2">
                    <? $recommends = $product->getRecommendProducts(); ?>
                    <? if (!empty($recommends)) : ?>
                        <div class="content">
                            <h2 class="block-title">Рекомендуем к образу</h2>
                            <div>
                                <? foreach ($recommends as $recommend) : ?>
                                    <div class="item">
                                        <a href="<?= $recommend->getUrl() ?>">
                                            <?= \pavlinter\display\DisplayImage::widget([
                                                'width' => 65,
                                                'image' => $recommend->getFirstImage(),
                                                'category' => 'all'
                                            ]) ?>
                                        </a>
                                        <div class="item-title">
                                            <?= $recommend->title ?>
                                        </div>
                                    </div>
                                <? endforeach; ?>
                            </div>
                        </div>
                    <? endif; ?>
                    <? if (!empty($buyNowProducts)) : ?>
                        <div class="content">
                            <h2 class="block-title" style="color:#cc0033;">Сейчас покупают</h2>
                            <? foreach ($buyNowProducts as $buyNowProduct) : ?>
                                <div class="item">
                                    <a href="<?= $buyNowProduct->getUrl() ?>">
                                        <?= \pavlinter\display\DisplayImage::widget([
                                            'width' => 65,
                                            'image' => $buyNowProduct->getFirstImage(),
                                            'category' => 'all'
                                        ]) ?>
                                    </a>
                                    <div class="item-title">
                                        <?= $buyNowProduct->title ?>
                                    </div>
                                </div>
                            <? endforeach; ?>
                        </div>
                    <? endif; ?>
                    <? if ($likedProducts && count($likedProducts) > 1) : ?>
                        <div class="content">
                            <h2 class="block-title" style="color:#cc0033;">Вам понравилось</h2>
                        <? foreach ($likedProducts as $likedProductId => $likedProduct) : ?>
                            <? if ($product->id != $likedProductId) : ?>
                                <div class="item">
                                    <a href="<?= $likedProduct->url ?>">
                                        <?= \pavlinter\display\DisplayImage::widget([
                                            'width' => 65,
                                            'image' => $likedProduct->firstImage,
                                            'category' => 'all'
                                        ]) ?>
                                    </a>
                                    <div class="item-title">
                                        <?= $likedProduct->title ?>
                                    </div>
                                </div>
                            <? endif; ?>
                        <? endforeach; ?>
                        </div>
                    <? endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->render('quick-answer-form', [
    'formFields' => $formFields,
    'formButtons' => $formButtons,
    'entityId' => $product->id
]) ?>


</div>
