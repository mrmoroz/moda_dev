<?php
/**
 * @var \app\models\Product $product
 */
use app\components\pagination\CatalogLinkPager;
use app\models\Color;
use app\models\Product;
use app\models\ProductSearch;
use app\models\Size;
use app\models\StockPosition;
use pavlinter\display\DisplayImage;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ListView;

$bundle = \app\assets\HtmlAsset::register($this);
$this->title = 'МОДАОПТОМ';
$manufact = $product->getManufacturer();
?>
<?php
/**
 * @var Product $product
 * @var Manufacturer $manufacturer
 */
?>

<div class="columns-group clearfix">
    <div class="left-column">
        <div class="content">
            <?= $this->render('//_colls/catalogColl', array(
                'pageType' => $pageType,
                'catalogItem' => $catalogItem,
                'sizeType' => $sizeType,
                'tagsListTop' => $tagsListTop,
                'tagsListBottom' => $tagsListBottom,
                'collectionList' => $collectionList,
                'categoriesList' => $categoriesList
            )); ?>
        </div>
    </div>
    <div class="right-column">
        <form action="/cart/additem" method="post" accept-charset="utf-8">
            <?php echo Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, \Yii::$app->getRequest()->getCsrfToken(), []);?>
            <div class="content">
                <div class="article-content">
                    <h1>
                        <?= $titleH1 ?>
                    </h1>

                    <div id="section-buttons"><a class="back-link" href="#"
                                                 onclick="window.history.back();return false;">Назад</a>
                    </div>
                    <div class="clearfix">
                        <div id="catalog-item-images">
                            <ul>
                                <? if (!empty($product->gallery)) : ?>
                                    <? foreach ($product->gallery as $photo) : ?>
                                        <li>
                                            <a class="cloud-zoom-gallery" href="<?= $product->getOrigin($photo->path) ?>" title=''
                                               rel="useZoom: 'zoom1',
                                                smallImage: '<?= $product->getPrev($photo->path) ?>',
                                                position:'catalog-item-image',
                                                zoomOffsetX:0">
                                                <img src="<?= $product->getPrev($photo->path) ?>" style="width: 50px;" alt="" />
                                            </a>
                                        </li>
                                    <? endforeach; ?>
                                <? endif; ?>
                            </ul>
                        </div>
                        <div id="catalog-item-image" style="position: relative;">
                            <a class="cloud-zoom" href="<?= $product->getOrigin($product->getFirstImage()) ?>" id='zoom1' rel="position: 'wrap', adjustX: 0, adjustY: 0, lensOpacity: 0">
                                <img src="<?= $product->getOrigin($product->getFirstImage()) ?>" style="width: 305px;" alt=""/>
                            </a>
                            <div class="label-wrap-product">
                                <?php if(!empty($product->wholesale_discount)):?>
                                    <span class="supertag red">-<?=$product->wholesale_discount?>%</span>
                                <?php endif;?>
                                <?php if($product->isNew() || $product->isBigNew()):?>
                                    <span class="supertag">Новинка</span>
                                <?php endif;?>
                                <?php if(isset($labels) && isset($labels[$product->id])):?>
                                    <?php foreach ($labels[$product->id] as $label):?>
                                        <?php if(!empty($label['label_text']) && !empty($label['label_color'])):?>
                                            <span class="supertag" style="background: <?=$label['label_color'].';'?>"><?=$label['label_text']?></span>
                                        <?php endif;?>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </div>
                            <a href="/catalog/download-zip-photo?prodId=<?=$product->id?>">Скачать одним архивом</a>
                        </div>
                        <div id="catalog-item-description">
                            <div id="item-price-block" style="margin-bottom: 20px;">
                                <h2 style="margin-bottom: 0px;"><?= $product->title ?></h2>
                            </div>
                            <?php if (!Yii::$app->user->isGuest || Yii::$app->params['openPrice']): ?>
                                <div id="item-prices-block">
                                    <?php if($product->wholesale_discount_price):?>
                                        <strong class="item-price-old" style="color:#b80084"><?=$product->wholesale_price?> руб.</strong>
                                        <span class="item-price-current"><?=$product->wholesale_discount_price?> руб.</span>
                                    <?php else:?>
                                        <strong style="color:#b80084"><?=$product->wholesale_price?> руб.</strong>
                                    <?php endif;?>
                                </div>
                            <?php endif; ?>
                            <div style="margin-bottom: 5px;">
                                <div><span style="font-weight: bold;">Артикул:</span> <?= $product->article ?></div>
                            </div>
                            <div style="margin-bottom: 5px;">
                                <span style="font-weight: bold;">Размеры:</span>
                                <?php
                                $mass_sizes = [];
                                $real_size = [];
                                $mass_sizes = ArrayHelper::getColumn($product->getAvailableSizes(), 'name');
                                //vd($mass_sizes,false);
                                if(count($mass_sizes)>0){
                                    foreach ($mass_sizes as $size){
                                        if(!in_array($size, $real_size)){
                                            $real_size[] = $size;
                                        }
                                    }
                                    sort($real_size, SORT_NUMERIC);
                                    echo '<span>'.implode(', ', $real_size).'</span>';
                                }
                                ?>
                            </div>

                            <span style="font-weight: bold;">Цвет:</span><br>
                            <?$stocks = $product->getStockPositions();?>

                            <? if (!isset($stocks)) : ?>
                                Нет доступных цветов
                            <? else : ?>
                                <? $checkedColor = false; ?>
                                <? $shownColors = []; ?>
                                <? foreach ($stocks as $stock) : ?>
                                    <? foreach ($stock as $colorId => $sizes) : ?>
                                        <? if ($colorId != 'name' && $colorId != 'id' && !in_array($colorId, $shownColors)) : ?>
                                            <? $shownColors[] = $colorId; ?>
                                            <input type="radio" name="color_id" class="colorRadio"
                                                <? if (!$checkedColor) {
                                                    echo 'checked ';
                                                    $checkedColor = true;
                                                } ?>
                                                   value="<?= $colorId ?>">
                                            <?= Color::getName($colorId) ?>
                                            <br>
                                        <? endif; ?>
                                    <? endforeach ?>
                                <? endforeach; ?>
                            <? endif ?>
                            <?php if(!Yii::$app->user->isGuest):?>
                                <? if (!isset($stocks)) : ?>
                                    Нет доступных размеров
                                <? else : ?>
                                    <div id="item-order-block">
                                        <input type="hidden" name="item_id" value="<?= $product->id ?>">
                                        <table style="width: 100%;">
                                            <tbody>
                                            <tr>
                                                <th>Размер</th>
                                                <th>Количество</th>
                                                <th></th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <?
                                                    $shownSizes = false;
                                                    $sizes_stocks = [];
                                                    ?>
                                                    <? foreach ($stocks as $stock) : ?>
                                                        <? foreach ($stock as $colorId => $sizes) : ?>
                                                            <? if ($colorId != 'name' && $colorId != 'id') : ?>
                                                                <?php
                                                                if(isset($sizes_stocks[$colorId])){
                                                                    $sizes_stocks[$colorId] += $sizes;
                                                                }else {
                                                                    $sizes_stocks[$colorId] = $sizes;
                                                                }
                                                                ?>
                                                            <? endif; ?>
                                                        <? endforeach ?>
                                                    <? endforeach; ?>
                                                    <?php if(count($sizes_stocks)>0):?>
                                                        <?php
                                                            $sizes_stocks_name_tmp = [];
                                                            foreach ($sizes_stocks as $n=>$v1){
                                                                foreach ($v1 as $n2=>$v2){
                                                                    $sizes_stocks_name_tmp[$n][$n2] = Size::getName($n2);
                                                                }
                                                            }
                                                            $sizes_stocks_name = [];
                                                            foreach ($sizes_stocks_name_tmp as $nn=>$vv){
                                                                asort($vv);
                                                                $sizes_stocks_name[$nn] = $vv;
                                                            }
                                                        ?>
                                                        <?php if(count($sizes_stocks_name)>0):?>
                                                            <?php foreach ($sizes_stocks_name as $cid=>$ssizes):?>
                                                                <?
                                                                if (!$shownSizes) {
                                                                    $shownSizes = true;
                                                                    $dis_none = "";
                                                                    $disabled = "";
                                                                } else {
                                                                    $dis_none =  "style='display: none'";
                                                                    $disabled = 'disabled="disabled"';
                                                                }
                                                                ?>
                                                                <select name="size_id" <?=$dis_none?> <?=$disabled?> title="Размер заказываемого товара"  class="colorSizeSelector"  id="sColor_<?= $cid ?>">
                                                                    <? foreach ($ssizes as $sizeId => $name) : ?>
                                                                        <option value="<?= $sizeId ?>"><?= $name ?></option>
                                                                    <? endforeach; ?>
                                                                    <option value="all">Все</option>
                                                                </select>
                                                            <?php endforeach;?>
                                                        <?php endif;?>
                                                    <?php endif;?>


                                                    <?php /*if(count($sizes_stocks)>0):?>
                                                        <?php foreach ($sizes_stocks as $cid=>$ssizes):?>
                                                            <?
                                                            if (!$shownSizes) {
                                                                $shownSizes = true;
                                                                $dis_none = "";
                                                                $disabled = "";
                                                            } else {
                                                                $dis_none =  "style='display: none'";
                                                                $disabled = 'disabled="disabled"';
                                                            }
                                                            ?>
                                                            <select name="size_id" <?=$dis_none?> <?=$disabled?> title="Размер заказываемого товара"  class="colorSizeSelector"  id="sColor_<?= $cid ?>">
                                                                <? foreach ($ssizes as $sizeId => $quantity) : ?>
                                                                    <option value="<?= $sizeId ?>"><?= Size::getName($sizeId) ?></option>
                                                                <? endforeach; ?>
                                                                <option value="all">Все</option>
                                                            </select>
                                                        <?php endforeach;?>
                                                    <?php endif;*/?>
                                                    <?php //vd($sizes_stocks,false);?>
                                                </td>
                                                <td style="text-align: center;">
                                                    <input type="text" name="items_amount" value="1" id="items-amount"
                                                           title="Количество" style="width: 30px;"></td>
                                                <td style="text-align: right;">
                                                    <input type="submit" name="submit" value="Добавить в корзину">
                                                </td>


                                            </tbody>
                                        </table>

                                    </div>
                                <? endif ?>
                            <?php else:?>
                                <div id="item-order-block">
                                    <p>Для того, чтобы сделать заказ, необходимо <a href="/login">Войти</a> или <a href="/registration">Зарегистрироваться</a></p>
                                </div>
                            <?php endif;?>

                            <?php if($manufact):?>
                                <div class="man-wrap">
                                    <a href="<?=$manufact->getUrl()?>">
                                        <img src="http://beautifull.loc<?=$manufact->image?>" style="width: 107px;" alt="">
                                    </a>
                                    <br>
                                    <a href="<?=$manufact->getUrl()?>" style="text-decoration: underline;">Вся коллекция</a>
                                </div>
                            <?php endif;?>

                            <div style="width: 305px; margin-bottom: 40px; margin-top:20px;">
                                <div id="item-description-text">
                                    <?= $product->description ?>
                                </div>
                                <div>
                                    <?
                                    switch ($product->color_size_note) {
                                        case 1:
                                            echo "Оттенок цвета может незначительно отличаться от фото.";
                                            break;
                                        case 2:
                                            echo "Оттенок цвета и расположение рисунка могут незначительно отличаться от фото.";
                                            break;
                                        default :
                                            break;
                                    }
                                    ?>
                                </div>

                                <?php if($product->opt_is_marked):?>
                                    <div style="margin: 5px 0;">
                                        <strong>Товар промаркирован</strong>
                                    </div>
                                <?php endif;?>

                                <?php
                                    $tagsIds = $product->getTagsIds();
                                    if($tagsIds && in_array('3',$tagsIds)){
                                        echo '<div style="padding: 10px 0; font-weight: bold;"> «Обратите внимание!!! Цена товара может меняться в зависимости от курса доллара. Не является публичной офертой. В случае заказа модели - отмена будет невозможна, так как данный товар закупается у поставщика под заказ за доллары»</div>';
                                    }
                                ?>

                                <? if (StockPosition::checkSizesInStock($product->id, true) && $manufact->show_to_client) : ?>
                                    <div>
                                        Ожидание отправки (рабочих дней): <?= $manufact->days_to_pdo ?>
                                    </div>
                                <? endif; ?>

                                <?php if($getLength = $product->getLen()):?>
                                    <div>
                                        Длина: <?= $getLength->value ?>.
                                    </div>
                                <?php endif;?>
                                <?php if($modelSize = $product->getPsize()):?>
                                    <div>
                                        Размер модели: <?= $modelSize->value ?>.
                                    </div>
                                <?php endif;?>
                                <?php if($modelGrow = $product->getGrowth()):?>
                                    <div>
                                        Рост модели: <?= $modelGrow->value ?>.
                                    </div>
                                <?php endif;?>
                                <?php if ($dimensions = $product->getDimensions()): ?>
                                    <div>
                                        <span style="font-weight: bold;">Габариты:</span> <?= $dimensions ?>
                                    </div>
                                <?php endif; ?>
                                <?php if ($structure = $product->getCompos()): ?>
                                    <div>
                                        <span style="font-weight: bold;">Состав:</span> <?= $structure->value?>
                                    </div>
                                <?php endif; ?>
                                <?php if ($factureType = $product->getClothType()): ?>
                                    <div>
                                        <span style="font-weight: bold;">Тип ткани:</span> <?= $factureType->name ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(document).ready(function () {
        var color_id = $('input.colorRadio:checked').val();
        $('select.colorSizeSelector').hide().attr('disabled', 'disabled');
        $('select#sColor_' + color_id).show().attr('disabled', false);
        //console.log(color_id);
        $('input.colorRadio').change(function () {
            var $elem = $(this);
            //console.log('select color: ' + $elem.val());
            $('select.colorSizeSelector').hide().attr('disabled', 'disabled');
            $('select#sColor_' + $elem.val()).show().attr('disabled', false);
        });
    });
</script>

