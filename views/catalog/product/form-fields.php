<? use yii\helpers\Html; ?>

<? if (isset($quickAnswerForm->formSettings)) : ?>
    <? foreach ($quickAnswerForm->formSettings->fields as $field) : ?>
        <?
        switch ($field->content_type) {
            case 0:
                $contentType = '';
                break;
            case 1:
                $contentType = 'callback-phone';
                break;
            case 2:
                $contentType = 'callback-email';
                break;
        }
        ?>
        <div class="tr">
            <div class="td">
                <? switch ($field->type) :
                    case '1' : ?>
                        <?= Html::textInput($field->id, null, [
                            'placeholder' => $field->name,
                            'style' => [
                                'width' => '330px'
                            ],
                            'class' => $contentType,
                            'required' => $field->required ? true : false
                        ]) ?>
                        <? break; ?>
                    <? case '2' : ?>
                        <?= Html::textarea($field->id, null, [
                            'placeholder' => $field->name,
                            'style' => [
                                'width' => '330px',
                                'height' => '80px'
                            ],
                            'required' => $field->required ? true : false
                        ]) ?>
                        <? break; ?>
                    <? case '3' : ?>
                        <?= Html::fileInput($field->id, null, [
                            'placeholder' => $field->name,
                            'required' => $field->required ? true : false]) ?>
                        <? break; ?>
                    <? case '4' : ?>
                        <?= Html::checkbox($field->id, null, [
                            'placeholder' => $field->name,
                            'required' => $field->required ? true : false]) ?>
                        <? break; ?>
                    <? case '5' : ?>
                        <div class="g-recaptcha" data-sitekey="6LcUThEUAAAAAKVSjDC0CyGfPIZbd6yHqAO4llHn"></div>
                        <? break; ?>
                    <? endswitch; ?>
            </div>
        </div>
    <? endforeach; ?>
<? endif; ?>
