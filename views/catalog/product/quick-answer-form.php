<?php
use yii\helpers\Html;
?>

<div class="PopUp" id="QuickAnswer" style="display: none">
    <h3>Спасибо за обращение!</h3>
    <form name="2" data-entity-type="product" data-entity-id="<?= $entityId ?>" id="2_top">
        <div class="bg-round" id="callback_round_prod"
             style=" background: #f5ecd9 url('catalog-item.php_files/callback.png') bottom right no-repeat;">
            <p style="width:350px; padding-left:4px;">Оставьте, пожалуйста, Ваши контактные данные. Персональный
                консультант перезвонит Вам в ближайшее время.</p>
            <div class="tabl-form">
                <?= $formFields ?>
            </div>
            <?= $formButtons ?>
        </div>
    </form>
</div>
