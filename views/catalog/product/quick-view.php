<?php
/* @var $model \app\models\Product */
use app\models\Color;
use app\models\Size;
use app\models\UserFavourite;

$favouriteProductsIds = UserFavourite::getFavouriteProducts();
?>

<div class="clearfix">
    <div class="coda-slider-wrapper">
        <div class="catalog-item-thumbs-column">
            <ul class="catalog-item-thumbs">
                <? foreach ($model->gallery as $key => $photo) : ?>
                    <li>
                        <a class="xtrig" href="#<?= $key + 1 ?>" rel="coda-slider-1">
                            <?= \pavlinter\display\DisplayImage::widget([
                                'width' => 50,
                                'height' => 75,
                                'image' => $photo->path,
                                'category' => 'all'
                            ]) ?>
                        </a>
                    </li>
                <? endforeach; ?>
            </ul>
        </div>
        <div class="coda-slider" id="coda-slider-1">
            <? foreach ($model->gallery as $key => $photo) : ?>
                <div class="panel" style="display: block;">
                    <div class="catalog-item-image-column">
                        <div class="jzoom" title="<?= $model->article ?>" data-big-url="<?= $photo->path ?>" style="position: relative; overflow: hidden;">
                            <?= \pavlinter\display\DisplayImage::widget([
                                'width' => 305,
                                'height' => 460,
                                'image' => $photo->path,
                                'category' => 'all'
                            ]) ?>
                        </div>
                    </div>
                </div>
            <? endforeach; ?>
        </div>
    </div>
    <div class="catalog-item-params-column">
        <div class="catalog-item-title-big"><?= $model->title ?></div>
        <div class="catalog-item-price-big">
            <? if ($model->discount_price) : ?>
                <span class="catalog-item-old-price">
                    <?= $model->getPrice($model->price, $this->params['kurs_many']) ?> <?=$model->getSymbol($this->params['country'])?>
                </span>
                <?= $model->getPrice($model->discount_price, $this->params['kurs_many']) ?> <?=$model->getSymbol($this->params['country'])?>
                <strong style="text-decoration: none; color: red; font-size: 14px">
                    Экономия <?= $model->getPrice($model->price - $model->discount_price, $this->params['kurs_many']) ?> <?=$model->getSymbol($this->params['country'])?>
                </strong>
            <? else : ?>
                <?= $model->getPrice($model->price, $this->params['kurs_many']) ?> <?=$model->getSymbol($this->params['country'])?>
            <? endif; ?>
        </div>
        <div class="catalog-item-description-1 clearfix">
            <br>
            <?= $model->description ?>
        </div>
        <form method="POST" id="product_form">
            <input type="hidden" name="action" value="add_to_cart">
            <input type="hidden" name="prod_id" value="<?= $model->id ?>">
            <? $stocks = $model->getStockPositions(); ?>
            <div class="catalog-item-param-block">
                <div class="catalog-item-param-title">Цвет / артикул:</div>
                <div class="catalog-item-color-selector">
                    <div class="catalog-item-selected-color">
                        <div class="catalog-item-articul"><?= $model->article ?></div>
                        <div class="catalog-item-color-title">
                            <? if (!isset($stocks)) : ?>
                                Нет доступных цветов
                            <? else : ?>
                                <? $checkedColor = false; ?>
                                <? $shownColors = []; ?>
                                <? foreach ($stocks as $stock) : ?>
                                    <? foreach ($stock as $colorId => $sizes) : ?>
                                        <? if ($colorId != 'name' && $colorId != 'id' && !in_array($colorId, $shownColors)) : ?>
                                            <? $shownColors[] = $colorId; ?>
                                            <input type="radio" id="color_<?= $colorId ?>"
                                                <? if (!$checkedColor) {
                                                    echo 'checked ';
                                                    $checkedColor = true;
                                                } ?>
                                                   name="color" value="<?= $colorId ?>">
                                            <label for="color_<?= $colorId ?>">
                                                <?= Color::getName($colorId) ?>
                                            </label>
                                            <br>
                                        <? endif; ?>
                                    <? endforeach ?>
                                <? endforeach; ?>
                            <? endif ?>
                            <br>
                        </div>
                    </div>
                </div>
            </div>

            <div class="catalog-item-param-block">
                <div class="catalog-item-param-title">Размеры:</div>
                <? if (!isset($stocks)) : ?>
                    Нет доступных размеров
                <? else : ?>
                    <ul class="catalog-item-sizes clearfix">
                        <? $shownSizes = false; ?>
                        <? foreach ($stocks as $stock) : ?>
                            <? foreach ($stock as $colorId => $sizes) : ?>
                                <? if ($colorId != 'name' && $colorId != 'id') : ?>
                                    <div class="color-sizes" data-sizes-color-id="<?= $colorId ?>"
                                        <? if (!$shownSizes) {
                                            $shownSizes = true;
                                        } else { echo "style='display: none'"; } ?>>
                                        <? foreach ($sizes as $sizeId => $quantity) : ?>
                                            <li>
                                                <a href="#" class="prod_sizes_label" data-size="<?= $sizeId ?>">
                                                    <?= Size::getName($sizeId) ?>
                                                </a>
                                            </li>
                                            <input type="checkbox" class="size_ch" name="size"
                                                   value="<?= $sizeId ?>" style="display: none"
                                                   id="ch_size_<?= $sizeId ?>">
                                        <? endforeach; ?>
                                    </div>
                                <? endif; ?>
                            <? endforeach ?>
                        <? endforeach; ?>
                    </ul>
                <? endif ?>
            </div>
            <div class="catalog-item-param-block">
                <a class="sizes-grid fancybox" href="#SizesTable" style="display:block; float:right;">Размерная сетка</a>
            </div>

            <div class="add-to-cart-amount-block catalog-item-param-block clearfix">
                <div class="catalog-item-param-title" style="float: left; line-height: 160%;">Количество:</div>
                <div class="add-to-cart-amount">
                    <a class="icon-minus" data-cnt="product" href="#">–</a>
                    <div class="cart-amount-value"><input type="text" id="product_cnt" name="product_cnt" style="width: 16px; text-align: center; border: none" value="1"></div>
                    <a class="icon-plus" data-cnt="product" href="#">+</a>
                </div>
            </div>

            <div class="add-to-cart-block">
                <b id="chooseSize" style="color:red; display: none; margin-bottom: 5px;">Нужно выбрать размер</b>
                <a href="#" id="cart_button" class="add-to-cart-button add-to-cart-from-popup action-add-to-cart" data-id="<?= $model->id ?>">Добавить покупку в корзину</a>
                <? if (in_array($model->id, $favouriteProductsIds)) : ?>
                    <a class="fav-icon-heart fav-prod-icon active" title="Удалить из избранного" data-prod="<?= $model->id ?>" style="right: 16px !important; position: relative!important;"></a>
                    <a data-url="/<?= \app\models\ProductCategory::$categoryTypeUrls[$model->category->type] ?>/favourites" class="fav-prod-link">Перейти в избранное</a>
                <? else : ?>
                    <a class="fav-icon-heart fav-prod-icon" title="Отложить в избранное" data-prod="<?= $model->id ?>" style="right: 16px !important; position: relative!important;">
                        <a data-url="/<?= \app\models\ProductCategory::$categoryTypeUrls[$model->category->type] ?>/favourites" class="fav-prod-link">Отложить в избранное</a>
                    </a>
                <? endif; ?>
                <br><br>
                <div class="icon-delivery"></div>
                <a href="#ItemPost" class="sizes-grid fancybox">Бесплатная доставка почтой</a>
            </div>

            <div class="catalog-item-share">
                <!--Поделиться: <a href="/"><img src="/public/images/catalog-item-odnoklassniki-icon.png" /></a> <a href="/"><img src="/public/images/catalog-item-vkontakte-icon.png" /></a>-->
            </div>
        </form>
        <a title="Previous" data-product-id="<?= $previousId ?>" class="fancybox-nav fancybox-prev change-quick-view" href="javascript:;"><span></span></a>
        <a title="Next" data-product-id="<?= $nextId ?>" class="fancybox-nav fancybox-next change-quick-view" href="javascript:;"><span></span></a>
    </div>
</div>
<div class="catalog-item-descriptions">
    <? if ($model->length) : ?>
        Длина: <?= $model->length ?>.<br>
    <? endif; ?>
    <? if ($model->model_size) : ?>
        Размер модели: <?= $model->model_size ?>.<br>
    <? endif; ?>
    <? if ($model->model_growth) : ?>
        Рост модели: <?= $model->model_growth ?>.<br>
    <? endif; ?>
    <? if ($model->composition) : ?>
        <b>Состав: </b> <?= $model->composition ?><br>
    <? endif; ?>
    <? if ($clothType = $model->getClothTypeName()) : ?>
        Тип ткани: <?= $clothType ?>.<br>
    <? endif; ?>
    <? if ($manufacturer = $model->getManufacturerName()) : ?>
        <b>Производитель</b> <?= $manufacturer ?>
    <? endif; ?>
    <div class="catalog-item-description-2">
    </div>
</div>