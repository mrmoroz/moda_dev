<?php
use app\models\ClothForSearch;
use app\models\Manufacturer;
use app\models\SearchColor;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

    /** @var \app\models\ProductSearch $productSearch */
?>


<a href="#" class="sizes-filter-link">Размеры</a>
<a href="#" class="sizes-filter-link" style="margin-left: 5px;text-decoration: none;"><!--<img
                                src="/public/images/dropdown_arrow.png">--></a>


<? $form = ActiveForm::begin(['id' => 'search-filter-form', 'method' => 'GET']) ?>

<div id="sizes-filter-list">
    <div class="content">
        <dl>
            <form method="post" accept-charset="utf-8">
                <?= $form->field($productSearch, 'checkedSizes')->checkboxList($sizeChartSizes, [
                    'item' => function($index, $label, $name, $checked, $value) {
                        $checkedLabel = $checked ? 'checked' : '';
                        $inputId = str_replace(['[', ']'], ['', ''], $name) . '_' . $index;

                        return "<div><input type='checkbox' name=$name value=$value id=$inputId $checkedLabel>"
                            . "<label for=$inputId style='color:#C03; margin-left: 5px;'> $label</label></div>";
                    }])->label(false); ?>

                <a class="button-link" id="size-search-submit" href="#">Искать</a>
            </form>
        </dl>
        <div class="clear"></div>
    </div>
</div>


<? ActiveForm::end() ?>