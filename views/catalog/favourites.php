<?php
/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 */
use app\components\pagination\CatalogLinkPager;
use yii\widgets\ListView;

$bundle = \app\assets\HtmlAsset::register($this);
?>
<div id="content-body">
    <div class="content-columns clearfix">
        <div class="content-left-column">
            <?= $this->render('//catalog/_left-column') ?>
        </div>
        <div class="content-right-column">
            <h1>
                Избранные вами товары
            </h1>
            <div class="infoblock">
                На этой странице отображаются товары, которые Вы откладываете для будущего просмотра и покупки.
                <br>Это Ваша личная коллекция, которая сохранится здесь даже при последующих посещениях нашего магазина.
                <br>Откладывайте товары в Избранное, нажимая на знак
                <img class="favourite-icon" src="<?= $bundle->baseUrl ?>/img//fav_on_30_3.png">.
                <br><br>
                Желаем приятного шопинга!
            </div>

            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'layout' => "{pager}\n<ul class='catalog-items clearfix'>{items}</ul>\n{pager}",
                'itemView' => 'category/_product-card',
                'itemOptions' => [
                    'tag' => 'li',
                ],
                'pager' => [
                    'class' => CatalogLinkPager::className(),
                ],
                'emptyText' => "В избранном нет товаров",
            ])
            ?>
        </div>
    </div>
</div>