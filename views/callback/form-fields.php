<? use yii\helpers\Html;

if (isset($form->formSettings->fields)) : ?>
    <? foreach ($form->formSettings->fields as $field) : ?>
        <?
            switch ($field->content_type) {
                case 0:
                    $contentType = '';
                    break;
                case 1:
                    $contentType = 'callback-phone';
                    break;
                case 2:
                    $contentType = 'callback-email';
                    break;
            }
        ?>
        <dl>
            <dt><?= $field->name ?>
                <? if ($field->required) : ?>
                    <span style="color: red;">*</span>:
                <? endif; ?>
            </dt>
            <dd>
                <? switch ($field->type) :
                    case '1' : ?>
                        <?= Html::textInput($field->id, null, [
                            'style' => [
                                'width' => '330px'
                            ],
                            'class' => $contentType,
                            'required' => $field->required ? true : false
                        ]) ?>
                        <? break; ?>
                    <? case '2' : ?>
                        <?= Html::textarea($field->id, null, [
                            'style' => [
                                'width' => '330px',
                                'height' => '80px'
                            ],
                            'required' => $field->required ? true : false
                        ]) ?>
                        <? break; ?>
                    <? case '3' : ?>
                        <?= Html::fileInput($field->id, null, [
                            'required' => $field->required ? true : false]) ?>
                        <? break; ?>
                    <? case '4' : ?>
                        <?= Html::checkbox($field->id, null, [
                            'required' => $field->required ? true : false]) ?>
                        <? break; ?>
                    <? case '5' : ?>
                        <div class="g-recaptcha" data-sitekey="6LcUThEUAAAAAKVSjDC0CyGfPIZbd6yHqAO4llHn" style="float: left;"></div>
                        <? break; ?>
                    <? endswitch; ?>
            </dd>
        </dl>
    <? endforeach; ?>
<? endif; ?>
