<? if (isset($form->formSettings->buttons)) : ?>
    <? foreach ($form->formSettings->buttons as $button) : ?>
        <dl style="clear: both;">
            <dd>
                <input type="submit" name="submit" class="send-callback-form" value="<?= $button->name ?>">
            </dd>
        </dl>
    <? endforeach; ?>
<? endif; ?>
