<?php
/** @var \app\models\Page $model_page */
?>
<div id="content">
    <div id="content-body">
        <div class="content-columns clearfix">
            <div class="content-single-column">
                <div class="article">
                    <h1></h1>
                    <!-- -->
                    <style>
                        #AddQuote { display:block; float:right;}
                        #AddQuote a { display:block; padding: 5px 15px 8px 15px; border-radius:3px; margin: 5px 0px 0px 0px; color: #ffffff; background-color:#f1008b; font-weight:bold; text-decoration:none; }
                        #AddQuote a:hover {background-color:#b6006e; box-shadow: 0px 3px 10px rgba(0,0,0, 0.2)}

                        #Quote { padding-bottom:10px; border-bottom: solid 1px #cccccc;}
                        #Quote .Name {margin: 15px 0px 5px 0px; padding: 0px; font-weight:bold; font-size:13px; text-transform:uppercase; color:#986b3b;}
                        #Quote .Date {font-size:12px; padding-bottom:5px;}
                        #Quote .Email { padding-bottom:5px;}
                        #Quote .City { padding-bottom:5px;}
                        #Quote .Text { clear:both; text-align:justify;}

                        #AddReview { display:none; padding: 10px 0px 20px 0px; border-bottom: solid 1px #542111;}

                        div.tabl-form { font-size:12px;}
                        div.tabl-form div.th { padding:5px; width:140px;}
                        div.tabl-form div.td { padding:5px;}
                        .Star { font-size:15px; font-weight:bold; color: rgb(204, 0, 51);}
                        .w-long {width: 400px;}

                    </style>
                    
                    <h1>Отзывы</h1>
                    <hr noshade="noshade" style="height:1px; width:100%;">
                    <div id="AddReview" style="display: block;">
                        <?= $this->render('form', [
                            'formFields' => $formFields,
                            'formButtons' => $formButtons,
                            'callbackPage' => $callbackPage
                        ])?>
                    </div>
                    
                    <? foreach ($messages as $message) : ?>
                        <div id="Quote">
                            <? foreach ($message->fields as $field) : ?>
                                <? if ($field->isPublished()) : ?>
                                    <div class="Date"><?= $field->value ?></div>
                                <? endif; ?>
                            <? endforeach; ?>
                        </div>
                    <? endforeach ?>
                </div>
            </div>
        </div>
    </div>
</div>