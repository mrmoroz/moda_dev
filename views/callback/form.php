<? use yii\helpers\Html;
?>
    <form name="<?= $callbackPage->type ?>" data-entity-type="callback" data-entity-id="<?= $callbackPage->type ?>"
          id="<?= $callbackPage->type . '_top'?>">
        <div class="bg-round">
            <div class="tabl-form">
                <?= $formFields ?>
            </div>
        </div>
        <div>
            <?= $formButtons ?>
        </div>
    </form>