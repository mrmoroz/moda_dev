<?php 
$this->title = 'Вы были отписаны от рассылок!'
?>
<div class="block1 hd-block">
    <h2 class="text-success">Вы были отписаны от рассылок!</h2>

    <?php if($activeForm):?>
    <div class="page-form-wrap">
        <h3>Пожалуйста, укажите причину, по которой Вы отписались от рассылки</h3><br><br>
        <div class="error-message-fld alert-danger"></div><br><br>
        <form name="<?= $form->form_type ?>" data-entity-type="page">
            <input type="hidden" name="999" value="<?=$email?>">
        <?=$formFields?>
        <?=$formButtons?>
        </form>
    </div>
    <?php endif;?>
    <br><br>

</div>