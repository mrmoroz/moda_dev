<?php
$mainMenu = $this->params['main-menu'];
?>

<? if (isset($mainMenu)) : ?>
    <div id="main-menu">
        <? foreach ($mainMenu as $item) : ?>
            <? if ($item->menuItem->entity_type == 4) {
                $url = '/catalog';
                $title = 'Каталог';
            } else {
                $url = $item->getUrl();
                $title = $item->menuItem->title;
            }?>
            <a href="<?= $url ?>"><?= $title ?></a>
        <? endforeach; ?>
    </div>
<? endif; ?>