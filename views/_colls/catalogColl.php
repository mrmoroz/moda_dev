<? if (($pageType == 'tag' || $pageType == 'collection') && (isset($catalogItem->action) && $catalogItem->action == "1")) {
    $isActionTagPage = true;
} else {
    $isActionTagPage = false;
}
//$isActionTagPage = false
?>

<div class="section-header">Каталог</div>

<div>
    <ul class="sub-menu-tabs">
        <li <?php if($pageType !== 'collection' && !$isActionTagPage):?>class="active"<?php endif;?>>
            <a href='#'' data-tab='categories'>Категории</a>
        </li>
        <li <?php if($pageType == 'collection' || ($pageType == 'collection' && $isActionTagPage)):?>class="active"<?php endif;?>>
            <a href='#' data-tab='manufact'>Производители</a>
        </li>
        <li <?php if($pageType == 'tag' && $isActionTagPage):?>class="active"<?php endif;?>>
            <a href='#' data-tab='action'>Акции</a>
        </li>
    </ul>
</div>

<ul class="sub-menu" id="sub-categories" <?php if($pageType == 'collection' || $isActionTagPage):?> style="display:none"<?php endif;?>>

    <li><a href="/novinki/zhenskaja" <?=Yii::$app->request->get('currentSizesUrl')=='/novinki/zhenskaja' ? 'class="selected"' :''?>>Новинки. Женщины</a></li>
    <li><a href="/novinki/zhenskaja-bolshie-razmery" <?=Yii::$app->request->get('currentSizesUrl')=='/novinki/zhenskaja-bolshie-razmery' ? 'class="selected"' :''?>>Новинки. Большие размеры</a></li>
    <li><a href="/novinki/muzhskaja" <?=Yii::$app->request->get('currentSizesUrl')=='/novinki/muzhskaja' ? 'class="selected"' :''?>>Новинки. Мужчины</a></li>
    <li><a href="/novinki/detskaja" <?=Yii::$app->request->get('currentSizesUrl')=='/novinki/detskaja' ? 'class="selected"' :''?>>Новинки. Дети</a></li>
    <li><a href="/novinki/dom" <?=Yii::$app->request->get('currentSizesUrl')=='/novinki/dom' ? 'class="selected"' :''?>>Новинки. Дом</a></li>

    <? if (!empty($tagsListTop)) : ?>
        <?php foreach ($tagsListTop as $topTag): ?>
            <? if (!$topTag->action || $topTag->action == "0") : ?>
                <?php
                    if(!empty($topTag->name_mo)){
                        $name = $topTag->name_mo;
                    }else{
                        $name = $topTag->name;
                    }
                ?>
                <li>
                    <a href="/catalog/tag/<?= $topTag->id ?>" class="category-name <?php if ($pageType == 'tag' && $catalogItem->id == $topTag->id): ?> selected <?php endif; ?>"><?= $name ?></a>
                </li>
            <? endif; ?>
        <?php endforeach; ?>
        <div style="width: 150px; margin-bottom: 20px; margin-top: -5px;">
            <hr>
        </div>
    <? endif; ?>

    <?php
        echo $this->render('_cache_category_list_new',[
            'pageType'=>$pageType,
            'catalogItem'=>$catalogItem,
            'sizeType'=>$sizeType
        ]);
    ?>

    <? if (!empty($tagsListBottom)) : ?>
        <li>
            <div style="border-top: 1px solid #cccccc; margin-top: 30px; padding-top: 25px; width: 70%;">
            <ul id="tags-list">
                <?php foreach ($tagsListBottom as $bottomTag): ?>
                    <?php
                    if(!empty($bottomTag->name_mo)){
                        $name_bot = $bottomTag->name_mo;
                    }else{
                        $name_bot = $bottomTag->name;
                    }
                    ?>
                    <li>
                        <a href="/catalog/tag/<?= $bottomTag->id ?>"
                           class="category-name <?php if ($pageType == 'tag' && $catalogItem->id == $bottomTag->id): ?> selected <?php endif; ?>"><?= $name_bot ?></a>
                    </li>
                <?php endforeach; ?>

            </ul>
            </div>
        </li>
    <? endif; ?>
</ul>

<ul class="sub-menu" id="sub-manufact" <?php if($pageType !== 'collection'):?> style="display:none"<?php endif;?>>
    <?php if($collectionList): ?>
        <?php
            $coll_links = [];
            foreach ($collectionList as $item){
                if(!empty($item->name_mo) || !empty($item->client_name)){
                    if (!empty($item->name_mo)) {
                        $coll_links[$item->id] =  strip_tags(trim($item->name_mo));
                    } elseif(!empty($item->client_name)) {
                        $coll_links[$item->id] = strip_tags(trim($item->client_name));
                    }
                }
            }
            asort($coll_links);
        ?>
        <?php foreach ($coll_links as $k=>$link):?>
            <?php if ($pageType == 'collection' && $catalogItem->id == $k): ?>
                <?php $sel='selected'?>
            <?php else:?>
                <?php $sel=''?>
            <?php endif; ?>
            <li>
                <a href="/catalog/collection/<?= $k ?>" class="category-name <?=$sel?>"><?=$link?></a>
            </li>
        <?php endforeach;?>
        <?/*php foreach ($collectionList as $collection): ?>
            <?php if(!empty($collection->name_mo) || !empty($collection->client_name)):?>
            <li>
                <a href="/catalog/collection/<?= $collection->id ?>"
                   class="category-name
                   <?php if ($pageType == 'collection' && $catalogItem->id == $collection->id): ?>
                    selected
                   <?php endif; ?>">
                    <?php
                    if (!empty($collection->name_mo)) {
                        echo $collection->name_mo;
                    } else {
                        echo $collection->client_name;
                    }
                    ?>
                </a>
            </li>
        <?php endif;?>
        <?php endforeach; */?>
    <?php endif;?>
</ul>

<ul class="sub-menu" id="sub-action" <?php if($pageType == 'collection' || !$isActionTagPage) : ?> style="display:none"<?php endif;?>>
    <? if (!empty($tagsListTop)) : ?>
        <?php foreach ($tagsListTop as $topTag): ?>
            <? if ($topTag->action == "1") : ?>
                <?php
                    if(!empty($topTag->name_mo)){
                        $name = $topTag->name_mo;
                    }else{
                        $name = $topTag->name;
                    }
                ?>
                <li>
                    <a href="/catalog/tag/<?= $topTag->id ?>"
                       class="category-name <?php if ($pageType == 'tag' && $catalogItem->id == $topTag->id): ?> selected <?php endif; ?>"><?= $name ?></a>
                </li>
            <? endif; ?>
        <?php endforeach; ?>
    <? endif; ?>
</ul>