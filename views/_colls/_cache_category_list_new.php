<?php
$standartCategory = Yii::$app->categoryService->getStandardWomanCategories();
//vd($standartCategory);
$bigCategory = Yii::$app->categoryService->getBigWomanCategories();
$mans = Yii::$app->categoryService->getManCategories();
$childs = Yii::$app->categoryService->getChildrenCategories();
$home = Yii::$app->categoryService->getHomeCategories();
?>

<?php if($standartCategory):?>
    <h2>Женщинам</h2>
    <ul>
        <?php foreach ($standartCategory as $category): ?>
        <?php if($category->is_active && $category->is_active_wholesale):?>
            <li>
                <a
                    href="/<?= \app\models\ProductCategory::$categoryTypeUrls[$category->type] ?>/bazovie-razmery/<?= $category->url ?>"
                    class="category-name <?php if ($pageType == 'category' && $catalogItem->id == $category->id && isset($sizeType) && $sizeType == 'bazovie-razmery'): ?> selected <?php endif; ?>"
                >
                    <?=$category->title?>
                </a>
            </li>
        <?php endif;?>
        <?php if($chCat = $category->productCategories):?>
            <?php foreach ($chCat as $childCategory): ?>
                <?php
                    $cnt = 0;
                    $key = 'categoryList'.$childCategory->id.'std';
                    $cache = Yii::$app->cache;
                    $cnt = $cache->get($key);
                    if ($cnt === false) {
                        $ids = \app\models\ProductCategory::getIdsCats($childCategory->id);
                        if ($ids) {
                            $cnt = \app\models\Product::find()->where(['IN', 'category_id', $ids])
                                ->andWhere(['is_active' => true, 'visible_s_mo' => true, 'is_wholesale_active' => true])->count();
                        }
                        $cache->set($key, $cnt, 60 * 60);
                    }
                ?>
                <? if ($childCategory->is_active && $childCategory->is_active_wholesale && $cnt > 0 ) : ?>
                    <li>
                        <a
                            href="/<?= \app\models\ProductCategory::$categoryTypeUrls[$childCategory->type] ?>/bazovie-razmery/<?= $childCategory->url ?>"
                            class="category-name <?php if ($pageType == 'category' && $catalogItem->id == $childCategory->id && isset($sizeType) && $sizeType == 'bazovie-razmery'): ?> selected <?php endif; ?>"
                        >
                            <?=$childCategory->title?>
                        </a>
                    </li>
                    <?php if($chCat3 = $childCategory->productCategories):?>
                        <?php foreach ($chCat3 as $childCategory3): ?>
                            <?php
                                $cnt = 0;
                                $key = 'categoryList'.$childCategory3->id.'std';
                                $cache = Yii::$app->cache;
                                $cnt = $cache->get($key);
                                if ($cnt === false) {
                                    $ids = \app\models\ProductCategory::getIdsCats($childCategory3->id);
                                    if ($ids) {
                                        $cnt = \app\models\Product::find()->where(['IN', 'category_id', $ids])
                                            ->andWhere(['is_active' => true, 'visible_s_mo' => true, 'is_wholesale_active' => true])->count();
                                    }
                                    $cache->set($key, $cnt, 60 * 60);
                                }
                            ?>
                            <? if ($childCategory3->is_active && $childCategory3->is_active_wholesale && $cnt > 0) : ?>
                                <li>
                                    <a
                                            href="/<?= \app\models\ProductCategory::$categoryTypeUrls[$childCategory3->type] ?>/bazovie-razmery/<?= $childCategory3->url ?>"
                                            class="category-name <?php if ($pageType == 'category' && $catalogItem->id == $childCategory3->id && isset($sizeType) && $sizeType == 'bazovie-razmery'): ?> selected <?php endif; ?>"
                                    >
                                        <?=$childCategory3->title?>
                                    </a>
                                </li>
                            <?php endif;?>
                        <?php endforeach;?>
                    <?php endif;?>
                <? endif; ?>
            <?php endforeach;?>
        <?php endif;?>
    <?php endforeach;?>
    </ul>
<?php endif;?>

<?php if($bigCategory):?>
    <h2>Большие размеры</h2>
    <ul>
        <?php foreach ($bigCategory as $category): ?>
            <?php if($category->is_active && $category->is_active_wholesale):?>
                <li>
                    <a
                        href="/<?= \app\models\ProductCategory::$categoryTypeUrls[$category->type] ?>/bolshie-razmery/<?= $category->url ?>"
                        class="category-name <?php if ($pageType == 'category' && $catalogItem->id == $category->id && isset($sizeType) && $sizeType == 'bolshie-razmery'): ?> selected <?php endif; ?>"
                    >
                        <?=$category->title?>
                    </a>
                </li>
            <?php endif;?>
            <?php if($chCat = $category->productCategories):?>
                <?php foreach ($chCat as $childCategory): ?>
                    <?php
                    $cnt = 0;
                    $key = 'categoryList'.$childCategory->id.'big';
                    $cache = Yii::$app->cache;
                    $cnt = $cache->get($key);
                    if ($cnt === false) {
                        $ids = \app\models\ProductCategory::getIdsCats($childCategory->id);
                        if ($ids) {
                            $cnt = \app\models\Product::find()->where(['IN', 'category_id', $ids])
                                ->andWhere(['is_active' => true, 'visible_b_mo' => true, 'is_wholesale_active' => true])->count();
                        }
                        $cache->set($key, $cnt, 60 * 60);
                    }
                    $isBigSizes = \app\models\ProductCategory::findBySql("SELECT size_id FROM category_sizes WHERE type ='big' AND category_id='{$childCategory->id}'")->asArray()->all();
                    //vd($isBigSizes,false);
                    $flag = true;
                    if(count($isBigSizes)==0){
                        $flag = false;
                    }
                    if(count($isBigSizes)==1 && $isBigSizes[0]['size_id']=='10000'){
                        $flag = false;
                    }
                    ?>
                    <? if ($childCategory->is_active && $childCategory->is_active_wholesale && $cnt > 0 && $flag) : ?>
                        <li>
                            <a
                                    href="/<?= \app\models\ProductCategory::$categoryTypeUrls[$childCategory->type] ?>/bolshie-razmery/<?= $childCategory->url ?>"
                                    class="category-name <?php if ($pageType == 'category' && $catalogItem->id == $childCategory->id && isset($sizeType) && $sizeType == 'bolshie-razmery'): ?> selected <?php endif; ?>"
                            >
                                <?=$childCategory->title?>
                            </a>
                        </li>
                    <? endif; ?>
                <?php endforeach;?>
            <?php endif;?>
        <?php endforeach;?>
    </ul>
<?php endif;?>

<?php if($mans):?>
    <h2>Мужчинам</h2>
    <ul>
        <?php foreach ($mans as $category): ?>
            <?php
                $cnt = 0;
                $key = 'categoryList'.$category->id;
                $cache = Yii::$app->cache;
                $cnt = $cache->get($key);
                if ($cnt === false) {
                    $ids =  \app\models\ProductCategory::getIdsCats($category->id);
                    if ($ids) {
                        $cnt = \app\models\Product::find()->where(['IN', 'category_id', $ids])
                            ->andWhere(['is_active' => true, 'is_wholesale_active' => true])->count();
                    }
                    $cache->set($key, $cnt, 60 * 60);
                }
            ?>
            <?php if($category->is_active && $category->is_active_wholesale && $cnt>0):?>
                <li>
                    <a
                        href="/<?= \app\models\ProductCategory::$categoryTypeUrls[$category->type] ?>/vse-razmery/<?= $category->url ?>"
                        class="category-name <?php if ($pageType == 'category' && $catalogItem->id == $category->id): ?> selected <?php endif; ?>"
                    >
                        <?=$category->title?>
                    </a>
                </li>
            <?php endif;?>
            <?php if($chCat = $category->productCategories):?>
                <?php foreach ($chCat as $childCategory): ?>
                    <?php
                    $cnt = 0;
                    $key = 'categoryList'.$childCategory->id;
                    $cache = Yii::$app->cache;
                    $cnt = $cache->get($key);
                    if ($cnt === false) {
                        $ids =  \app\models\ProductCategory::getIdsCats($childCategory->id);
                        if ($ids) {
                            $cnt = \app\models\Product::find()->where(['IN', 'category_id', $ids])
                                ->andWhere(['is_active' => true, 'is_wholesale_active' => true])->count();
                        }
                        $cache->set($key, $cnt, 60 * 60);
                    }
                    ?>
                    <? if ($childCategory->is_active && $childCategory->is_active_wholesale && $cnt > 0 ) : ?>
                        <li>
                            <a
                                href="/<?= \app\models\ProductCategory::$categoryTypeUrls[$childCategory->type] ?>/vse-razmery/<?= $childCategory->url ?>"
                                class="category-name <?php if ($pageType == 'category' && $catalogItem->id == $childCategory->id): ?> selected <?php endif; ?>"
                            >
                                <?=$childCategory->title?>
                            </a>
                        </li>
                    <? endif; ?>
                <?php endforeach;?>
            <?php endif;?>
        <?php endforeach;?>
    </ul>
<?php endif;?>
<?php if($childs):?>
    <h2>Детям</h2>
    <ul>
        <?php foreach ($childs as $category): ?>
            <?php
            $cnt = 0;
            $key = 'categoryList'.$category->id;
            $cache = Yii::$app->cache;
            $cnt = $cache->get($key);
            if ($cnt === false) {
                $ids =  \app\models\ProductCategory::getIdsCats($category->id);
                if ($ids) {
                    $cnt = \app\models\Product::find()->where(['IN', 'category_id', $ids])
                        ->andWhere(['is_active' => true, 'is_wholesale_active' => true])->count();
                }
                $cache->set($key, $cnt, 60 * 60);
            }
            ?>
            <?php if($category->is_active && $category->is_active_wholesale && $cnt>0):?>
                <li>
                    <a
                            href="/<?= \app\models\ProductCategory::$categoryTypeUrls[$category->type] ?>/vse-razmery/<?= $category->url ?>"
                            class="category-name <?php if ($pageType == 'category' && $catalogItem->id == $category->id): ?> selected <?php endif; ?>"
                    >
                        <?=$category->title?>
                    </a>
                </li>
            <?php endif;?>
            <?php if($chCat = $category->productCategories):?>
                <?php foreach ($chCat as $childCategory): ?>
                    <?php
                    $cnt = 0;
                    $key = 'categoryList'.$childCategory->id;
                    $cache = Yii::$app->cache;
                    $cnt = $cache->get($key);
                    if ($cnt === false) {
                        $ids =  \app\models\ProductCategory::getIdsCats($childCategory->id);
                        if ($ids) {
                            $cnt = \app\models\Product::find()->where(['IN', 'category_id', $ids])
                                ->andWhere(['is_active' => true, 'is_wholesale_active' => true])->count();
                        }
                        $cache->set($key, $cnt, 60 * 60);
                    }
                    ?>
                    <? if ($childCategory->is_active && $childCategory->is_active_wholesale && $cnt > 0 ) : ?>
                        <li>
                            <a
                                    href="/<?= \app\models\ProductCategory::$categoryTypeUrls[$childCategory->type] ?>/vse-razmery/<?= $childCategory->url ?>"
                                    class="category-name <?php if ($pageType == 'category' && $catalogItem->id == $childCategory->id): ?> selected <?php endif; ?>"
                            >
                                <?=$childCategory->title?>
                            </a>
                        </li>
                    <? endif; ?>
                <?php endforeach;?>
            <?php endif;?>
        <?php endforeach;?>
    </ul>
<?php endif;?>
<?php if($home):?>
    <h2>Дом</h2>
    <ul>
        <?php foreach ($home as $category): ?>
            <?php
            $cnt = 0;
            $key = 'categoryList'.$category->id;
            $cache = Yii::$app->cache;
            $cnt = $cache->get($key);
            if ($cnt === false) {
                $ids =  \app\models\ProductCategory::getIdsCats($category->id);
                if ($ids) {
                    $cnt = \app\models\Product::find()->where(['IN', 'category_id', $ids])
                        ->andWhere(['is_active' => true, 'is_wholesale_active' => true])->count();
                }
                $cache->set($key, $cnt, 60 * 60);
            }
            ?>
            <?php if($category->is_active && $category->is_active_wholesale && $cnt>0):?>
                <li>
                    <a
                            href="/<?= \app\models\ProductCategory::$categoryTypeUrls[$category->type] ?>/vse-razmery/<?= $category->url ?>"
                            class="category-name <?php if ($pageType == 'category' && $catalogItem->id == $category->id): ?> selected <?php endif; ?>"
                    >
                        <?=$category->title?>
                    </a>
                </li>
            <?php endif;?>
            <?php if($chCat = $category->productCategories):?>
                <?php foreach ($chCat as $childCategory): ?>
                    <?php
                    $cnt = 0;
                    $key = 'categoryList'.$childCategory->id;
                    $cache = Yii::$app->cache;
                    $cnt = $cache->get($key);
                    if ($cnt === false) {
                        $ids =  \app\models\ProductCategory::getIdsCats($childCategory->id);
                        if ($ids) {
                            $cnt = \app\models\Product::find()->where(['IN', 'category_id', $ids])
                                ->andWhere(['is_active' => true, 'is_wholesale_active' => true])->count();
                        }
                        $cache->set($key, $cnt, 60 * 60);
                    }
                    ?>
                    <? if ($childCategory->is_active && $childCategory->is_active_wholesale && $cnt > 0 ) : ?>
                        <li>
                            <a
                                    href="/<?= \app\models\ProductCategory::$categoryTypeUrls[$childCategory->type] ?>/vse-razmery/<?= $childCategory->url ?>"
                                    class="category-name <?php if ($pageType == 'category' && $catalogItem->id == $childCategory->id): ?> selected <?php endif; ?>"
                            >
                                <?=$childCategory->title?>
                            </a>
                        </li>
                    <? endif; ?>
                <?php endforeach;?>
            <?php endif;?>
        <?php endforeach;?>
    </ul>
<?php endif;?>
