<?php foreach ($categoriesList as $k=> $category): ?>
    <?php if(isset($categoriesList[$k+1]) && strcasecmp($categoriesList[$k+1]['size_id'], $category['size_id']) == 0 && $categoriesList[$k+1]['id']==$category['id']):?>
        <?php
        $cnt = 0;
        $key = 'categoryList'.$category['id'];
        $cache = Yii::$app->cache;
        $cnt = $cache->get($key);
        //vd($key,false);
        //vd($cnt);
        if ($cnt === false) {
            $ids =  \app\models\ProductCategory::getIdsCats($category['id']);
            if ($ids) {
                $cnt = \app\models\Product::find()->where(['IN', 'category_id', $ids])
                    ->andWhere(['is_active' => true, 'is_wholesale_active' => true])->count();
            }
            $cache->set($key, $cnt, 60 * 60);
        }
        ?>
        <?php if($cnt > 0):?>
            <li>
                <a href="/<?= \app\models\ProductCategory::$categoryTypeUrls[$category['type']] ?>/vse-razmery/<?= $category['url'] ?>"
                   class="category-name <?php if ($pageType == 'category' && $catalogItem->id == $category['id']): ?> selected <?php endif; ?>"><?= $category['title'] ?></a>
            </li>
        <?php endif;?>
    <?php else:?>
        <?php if(isset($categoriesList[$k-1]) && strcasecmp($categoriesList[$k-1]['size_id'], $category['size_id']) == 0 && $categoriesList[$k-1]['id']==$category['id']):?>
            <?php continue;?>
        <?php endif;?>
        <?php if($category['type_size']=='std'):?>
            <?php
            $cnt = 0;
            $key = 'categoryList'.$category['id'].'std';
            $cache = Yii::$app->cache;
            $cnt = $cache->get($key);
            if ($cnt === false) {
                $ids = \app\models\ProductCategory::getIdsCats($category['id']);
                if ($ids) {
                    $cnt = \app\models\Product::find()->where(['IN', 'category_id', $ids])
                        ->andWhere(['is_active' => true, 'visible_s_mo' => true, 'is_wholesale_active' => true])->count();
                }
                $cache->set($key, $cnt, 60 * 60);
            }
            ?>
            <?php if($cnt > 0):?>
                <li>
                    <a href="/<?= \app\models\ProductCategory::$categoryTypeUrls[$category['type']] ?>/bazovie-razmery/<?= $category['url'] ?>"
                       class="category-name <?php if ($pageType == 'category' && $catalogItem->id == $category['id'] && isset($sizeType) && $sizeType == 'bazovie-razmery'): ?> selected <?php endif; ?>"><?= $category['title'] ?></a>
                </li>
            <?php endif;?>
        <?php endif;?>
        <?php if($category['type_size']=='big'):?>
            <?php
            $cnt = 0;
            $key = 'categoryList'.$category['id'].'big';
            $cache = Yii::$app->cache;
            $cnt = $cache->get($key);
            if ($cnt === false) {
                $ids = \app\models\ProductCategory::getIdsCats($category['id']);
                if ($ids) {
                    $cnt = \app\models\Product::find()->where(['IN', 'category_id', $ids])
                        ->andWhere(['is_active' => true, 'visible_b_mo' => true, 'is_wholesale_active' => true])->count();
                }
                $cache->set($key, $cnt, 60 * 60);
            }
            ?>
            <?php if($cnt > 0):?>
                <li>
                    <a href="/<?= \app\models\ProductCategory::$categoryTypeUrls[$category['type']] ?>/bolshie-razmery/<?= $category['url'] ?>"
                       class="category-name <?php if ($pageType == 'category' && $catalogItem->id == $category['id'] && isset($sizeType) && $sizeType == 'bolshie-razmery'): ?> selected <?php endif; ?>"><?= $category['title'] ?>. Большие размеры</a>
                </li>
            <?php endif;?>
        <?php endif;?>
    <?php endif;?>

<?php endforeach; ?>