<?php
/**
 * @var \app\models\Tag $tag
 * @var \yii\data\ActiveDataProvider $dataProvider
 */
use app\components\pagination\CatalogLinkPager;
use yii\widgets\ListView;

?>
<div id="content-body">
    <div class="content-columns clearfix">
        <div class="content-left-column">
            <?= $this->render('//catalog/_left-column') ?>
        </div>
        <div class="content-right-column">
            <ul id="breadcrumbs" class="clearfix">
                // > > >a
            </ul>
            <h1>
                <?= $tag->h1_page ?>
            </h1>
            <? if ($tag->top_text) : ?>
                <div class="infoblock">
                    <?= $tag->top_text ?>
                </div>
            <? endif; ?>

            <? if (!empty($products)) : ?>
                <? \yii\widgets\Pjax::begin(['timeout' => '4000']) ?>
                <?= $this->render('../catalog/filters', [
                    'productSearch' => $productSearch,
                    'sizeChartSizes' => $tag->getSizes(),
                    'categoryTags' => $tag->getCategoryTags(),
                    'manufacturers' => $tag->getManufacturers(),
                    'textureTypes' => $tag->getTextureTypes(),
                    'searchColors' => $tag->getColorForSearch(),
                    'priceMin' => $priceMin,
                    'priceMax' => $priceMax,
                    'actualPriceMin' => $actualPriceMin,
                    'actualPriceMax' => $actualPriceMax,
                    'showManufacturerFilter' => true,
                    'showSearchColorFilter' => true,
                    'width' => '14.5%'
                ]) ?>
                <div id="top-pagination">
                    <?= CatalogLinkPager::widget([
                        'pagination' => $pages->pagination,
                    ]); ?>
                </div>
                <ul class="catalog-items clearfix">
                    <?= $this->render('/catalog/category/_products_colls', ['products' => $products]) ?>
                </ul>

                <!-- Вывод кнопок внизу -->
                <div class="next-counter-link-holder">
                    <? if ($showAjaxAddProducts) : ?>
                        <a data-href="<?= $tag->getUrl() ?>" data-page="<?= $pages->pagination->getPage() + 2 ?>"
                           class="catalog-ajax-button inline-b-links append-catalog-link" style="width: 100%">
                            Показать еще <?= $remainingProductsCount ?> товаров "<?= $tag->name ?>"
                        </a>
                    <? endif; ?>
                </div>

                <div id="bottom-pagination">
                    <?= CatalogLinkPager::widget([
                        'pagination' => $pages->pagination,
                    ]); ?>
                </div>
                <? \yii\widgets\Pjax::end(); ?>
            <? else:?>
                <p>Не найдено товаров</p>
            <? endif; ?>

            <? if ($tag->bottom_text) : ?>
                <?= $tag->bottom_text ?>
            <? endif; ?>
        </div>
    </div>
</div>