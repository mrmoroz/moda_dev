<?php
/**
 * @var \app\models\Manufacturer $manufacturer
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \app\models\Product $products[]
 */

use app\components\pagination\CatalogLinkPager;
use yii\widgets\ListView;

?>
<div id="content-body">
    <div class="content-columns clearfix">
        <div class="content-left-column">
            <?= $this->render('//catalog/_left-column') ?>
        </div>
        <div class="content-right-column">
            <ul id="breadcrumbs" class="clearfix">
                // > > >a
            </ul>
            <h1>
                <?= $manufacturer->name ?>
            </h1>
            <? if ($manufacturer->top_text) : ?>
                <div class="infoblock">
                    <?= $manufacturer->top_text ?>
                </div>
            <? endif; ?>

            <? if (!empty($products)) : ?>
                <? \yii\widgets\Pjax::begin(['timeout' => '4000']) ?>
                <?= $this->render('../catalog/filters', [
                    'productSearch' => $productSearch,
                    'sizeChartSizes' => $manufacturer->getSizes(),
                    'categoryTags' => $manufacturer->getCategoryTags(),
                    'textureTypes' => $manufacturer->getTextureTypes(),
                    'searchColors' => $manufacturer->getColorForSearch(),
                    'priceMin' => $priceMin,
                    'priceMax' => $priceMax,
                    'actualPriceMin' => $actualPriceMin,
                    'actualPriceMax' => $actualPriceMax,
                    'showManufacturerFilter' => false,
                    'showSearchColorFilter' => true,
                    'width' => '17.8%'
                ]) ?>
                <div id="top-pagination">
                    <?= CatalogLinkPager::widget([
                        'pagination' => $pages->pagination,
                    ]); ?>
                </div>
                <ul class="catalog-items clearfix">
                    <?= $this->render('/catalog/category/_products_colls', ['products' => $products]) ?>
                </ul>

                <!-- Вывод кнопок внизу -->
                <div class="next-counter-link-holder">
                    <? if ($showAjaxAddProducts) : ?>
                        <a data-href="<?= $manufacturer->getUrl() ?>" data-page="<?= $pages->pagination->getPage() + 2 ?>"
                           class="catalog-ajax-button inline-b-links append-catalog-link" style="width: 100%">
                            Показать еще <?= $remainingProductsCount ?> товаров "<?= $manufacturer->name ?>"
                        </a>
                    <? endif; ?>
                </div>

                <div id="bottom-pagination">
                    <?= CatalogLinkPager::widget([
                        'pagination' => $pages->pagination,
                    ]); ?>
                </div>
                <? \yii\widgets\Pjax::end(); ?>
            <? else:?>
                <p>У производителя <?=$manufacturer->name?> нет товаров</p>
            <? endif; ?>

            <? if ($manufacturer->bottom_text) : ?>
                <?= $manufacturer->bottom_text ?>
            <? endif; ?>
        </div>
    </div>
</div>