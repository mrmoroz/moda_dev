<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HtmlAsset extends AssetBundle
{
    public $sourcePath = '@app/assets/source/';
    public $css = [
        'css/reset.css',
        'css/inside.css',
        'css/jquery.fancybox.css',
        'css/jquery.bxslider.css',
        'css/slick-theme.css',
        'css/slick.css',
        'css/app.css'
    ];
    public $js = [
        'js/jquery.showLoading.min.js',
        'js/jquery.fancybox.js',
        'js/callback.js',
        'js/ajax.js',
        'js/spin.min.js',
        'js/scripts.js',
        'js/jquery.bxslider.min.js',
        'js/slick.js'
    ];

    public $publishOptions = [
        'forceCopy' => YII_DEBUG
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapPluginAsset'
    ];
}
