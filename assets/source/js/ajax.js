/*функция формирования запроса корзины*/
function attAjaxRequest(this_host_input,input,params,token,call_back) {
    this_host = window.location.protocol + "//" + window.location.hostname + this_host_input;
    if (input) {
        if (input.controller) {
            var output = new Object();
            output.controller = input.controller;
            if (input.data) {
                output.data = input.data;
            }
            output = JSON.stringify(output);
            $.ajax({
                url: this_host,
                type: 'POST',
                dataType: 'JSON',
                //dataType: 'html',
                data: {
                    'output': output,
                    'param': params,
                    'token': token
                },
                cache: false,
                success: function (msg) {
                    call_back(msg);
                }
            });
            /*end ajax*/
        }
    }
}/* end function from med set*/
