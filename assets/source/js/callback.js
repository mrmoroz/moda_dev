var token  =  $('meta[name=csrf-token]').attr("content");
var params = $('meta[name=csrf-param]').attr("content");

$(document).ready(function () {
    $(".send-callback-form").on('click', function (e) {
        e.preventDefault();
        var messageForm = $(this).parent().parent().parent();

        var messageFormId = messageForm.attr("id");
        var formType = messageForm.attr("name");
        var entityType = messageForm.data("entity-type");
        var entityId = messageForm.data("entity-id");

        var fieldValues = [];
        var captcha = null;
        var validationError = 0;

        $(".callback-field-label").each(function () {
            $(this).remove();
        });

        //console.log(messageFormId);
        $(messageForm).find('input, textarea').each(function() {
            if ($(this).attr('type') != 'submit') {
                //console.log($(this));
                if ($(this).prop("required") && $(this).val().length == 0) {
                    validationError = 1;
                    $(this).css({'background-color' : '#ffd1d1'});
                } else {
                    $(this).css({'background-color' : '#bfffaa'});
                }
                if ($(this).attr('type') == 'checkbox' && $(this).prop("required") && !$(this).is( ":checked" )) {
                    validationError = 1;
                }
            }
        });
        //console.log(validationError);
        if (validationError == 1) {
            return;
        }

        $(messageForm).find('input, textarea').each(function(){
            if ($(this).attr('type') != 'submit') {
                var field = new Object();
                var input = $(this); // This is the jquery object of the input, do what you will

                field.id = input.attr("name");
                if (input.attr("type") == "checkbox") {
                    field.value = $(input).is(":checked");
                } else {
                    field.value = $(input).val();
                }

                if (field.id == 'g-recaptcha-response') {
                    captcha = field;
                } else {
                    fieldValues.push(field);
                }
            }
        });
        
        attAjaxRequest('/callback/ajax/', {
            controller : "sendMessage",
            data : { formType: formType,
                     entityType: entityType,
                     entityId: entityId,
                     fields: fieldValues,
                     captcha: captcha
            }}, params, token, callback);
    });

    function callback(msg)
    {
        if (msg.init) {
            if (msg.captcha == false) {
                alert('Неверный ввод капчи!');
                return;
            }

            if(msg.gohome){
                    window.location.href="http://moda.loc";
            }else{
                location.reload();
            }

            
        }
        var textEl = '';
        if(msg.errorMessages){
            msg.errorMessages.forEach(function (el){
                textEl +='<p style="padding: 10px; margin: 0;">'+el+'</p>';
            });
            $('.error-message-fld').html(textEl);
        }
    }
});