var opts = {
    lines: 13 // The number of lines to draw
    , length: 28 // The length of each line
    , width: 14 // The line thickness
    , radius: 42 // The radius of the inner circle
    , scale: 1 // Scales overall size of the spinner
    , corners: 1 // Corner roundness (0..1)
    , color: '#000' // #rgb or #rrggbb or array of colors
    , opacity: 0.3 // Opacity of the lines
    , rotate: 0 // The rotation offset
    , direction: 1 // 1: clockwise, -1: counterclockwise
    , speed: 1 // Rounds per second
    , trail: 60 // Afterglow percentage
    , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
    , zIndex: 2e9 // The z-index (defaults to 2000000000)
    , className: 'spinner' // The CSS class to assign to the spinner
    , top: '50%' // Top position relative to parent
    , left: '50%' // Left position relative to parent
    , shadow: false // Whether to render a shadow
    , hwaccel: false // Whether to use hardware acceleration
    , position: 'absolute' // Element positioning
};

var target = document.getElementById('super-container');
var spinner = new Spinner().spin();
target.appendChild(spinner.el);
spinner.stop();

(function($){
    $.fn.serializeObject = function(){

        var self = this,
            json = {},
            push_counters = {},
            patterns = {
                "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
                "key":      /[a-zA-Z0-9_]+|(?=\[\])/g,
                "push":     /^$/,
                "fixed":    /^\d+$/,
                "named":    /^[a-zA-Z0-9_]+$/
            };


        this.build = function(base, key, value){
            base[key] = value;
            return base;
        };

        this.push_counter = function(key){
            if(push_counters[key] === undefined){
                push_counters[key] = 0;
            }
            return push_counters[key]++;
        };

        $.each($(this).serializeArray(), function(){

            // skip invalid keys
            if(!patterns.validate.test(this.name)){
                return;
            }

            var k,
                keys = this.name.match(patterns.key),
                merge = this.value,
                reverse_key = this.name;

            while((k = keys.pop()) !== undefined){

                // adjust reverse_key
                reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');

                // push
                if(k.match(patterns.push)){
                    merge = self.build([], self.push_counter(reverse_key), merge);
                }

                // fixed
                else if(k.match(patterns.fixed)){
                    merge = self.build([], k, merge);
                }

                // named
                else if(k.match(patterns.named)){
                    merge = self.build({}, k, merge);
                }
            }

            json = $.extend(true, json, merge);
        });

        return json;
    };
})(jQuery);

$(document).ready(function(){
    $('body').on('pjax:start', function() {
        $('.block').show();
        spinner.spin(target);
    }).on('pjax:end', function() {
        spinner.stop(target);
        $('.block').hide();
    });

	$('.sub-menu-tabs li a').click(function(){
		var $elem = $(this);
		$('.sub-menu').hide();
		$('#sub-'+$elem.data('tab')).show();
		$('.sub-menu-tabs li').removeClass('active')
		$elem.parent('li').addClass('active');
		return false;
	});

    $('body').on('click', '#catalog-filter-block .show-all-mode', function () {
        var showAllModeStatus = $('.show-all-mode-status').val();

        if (showAllModeStatus == 0) {
            $('.catalog-pagination').find('li').each(function () {
                if (!$(this).hasClass('show-all-mode')) {
                    $(this).remove();
                }
            });

            $('.next-counter-link-holder').remove();
            $('.show-all-mode-status').val(1);

            $('.show-all-mode').text('По страницам');
        } else {
            var url = $('.ajax-href').val(); // + '?' + $('#search-filter-form').serialize();

            window.location.href = url;
        }
    });

    $('body').on('click', '#catalog-filter-block2 .show-all-mode', function () {
        var showAllModeStatus = $('.show-all-mode-status').val();
        //console.log(showAllModeStatus);

        if (showAllModeStatus == 0) {
            $('.catalog-pagination').find('li').each(function () {
                if (!$(this).hasClass('show-all-mode')) {
                    $(this).remove();
                }
            });

            $('.next-counter-link-holder').remove();
            $('.show-all-mode-status').val(1);

            $('.show-all-mode').text('По страницам');

            var page = $('.ajax-current-page').val(),
                sizeType = $('.ajax-size-type').val(),
                url = $('.ajax-href').val();
            ajaxAppendProducts(2, sizeType, url, false);
            //console.log(page);
            //console.log(sizeType);
            //console.log(url);
        } else {
            var url = $('.ajax-href').val();// + '?' + $('#search-filter-form').serialize();

            window.location.href = url;
        }
    });
});

function ajaxAppendProducts(page, sizeType, url, isTopScroll) {


    var productSearch = $('#search-filter-form').serializeObject();

    $.ajax({
        method: 'GET',
        url: url,
        data: {
            page: page,
            sizeType: sizeType,
            ajaxAddProducts: true,
            productSearch: productSearch
        },
        success: function (data) {
            data = JSON.parse(data);
            if (data.pageCount < page + 1) {
                $('.append-catalog-link').hide();
                $('.go-to-other-sizes').css('width', '100%');
            } else {
                $('.append-catalog-link').text($('.append-catalog-link').text().replace('45', data.productCount));
            }

            if (isTopScroll) {
                $('.ajax-current-page').val(parseInt(page) - 1);

                var oldDocumentHeight = $(document).height();
                $('#catalog-items-list').prepend(data.products);
                var newDocumentHeight = $(document).height();

                $(document).scrollTop($(document).scrollTop()+(newDocumentHeight-oldDocumentHeight));
            } else {
                $('#catalog-items-list').append(data.products);
                $('.ajax-page').val(parseInt(page) + 1);
            }

            $(window).on('scroll', document, ProductsScroll);

        }
    });
}

$(window).on('scroll', document, ProductsScroll);

function ProductsScroll() {

    if ($('.catalog-items').length <= 0) {
        $(this).unbind("scroll");
        return false;
    }

    // top scroll
    var showAllModeStatus = $('.show-all-mode-status').val();
    var posTop = $(window).scrollTop() - $('.catalog-items').offset().top;


    if (posTop <= 3000 && showAllModeStatus == 1) {
        var page = $('.ajax-current-page').val(),
            sizeType = $('.ajax-size-type').val(),
            url = $('.ajax-href').val();

            //console.log(page);
            //console.log(sizeType);
            //console.log(url);

        if (parseInt(page) <= 0) {
            return false;
        }

        //ajaxAppendProducts(page, sizeType, url, true);
        //$(this).unbind("scroll");
    }

    // bottom scroll
    if ($(window).scrollTop() + $(window).height() >= $(document).height()-100) {

        var pagesCount = $('.ajax-max-page').val();

        if (showAllModeStatus == 1) {
            $(this).unbind("scroll");
            var page = $('.ajax-page').val(),
                sizeType = $('.ajax-size-type').val(),
                url = $('.ajax-href').val();

            if (parseInt(page) > parseInt(pagesCount)) {
                return false;
            }

            ajaxAppendProducts(page, sizeType, url, false);
            $(this).unbind("scroll");
        }
    }
}